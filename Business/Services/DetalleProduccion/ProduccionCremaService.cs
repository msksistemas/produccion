﻿using Data;
using System.Linq;

namespace Business.Services.DetalleProduccion
{
    public class ProduccionCremaService : BaseService<Produccion_Crema> // TODO: Inherit from BaseService<Produccion_Crema>
    {

        public Produccion_Crema Get_ByLote(string lote)
        {
            using (var db = new ProdEntities())
            {
                return db.Produccion_Crema.Where(pc => pc.Lote == lote).SingleOrDefault();
            }
        }

        public void Update_Humedad(string lote, decimal humedad)
        {
            using (var db = new ProdEntities())
            {
                Produccion_Crema detalle = Get_ByLote(lote);
                db.Produccion_Crema.Attach(detalle);
                detalle.Humedad = humedad;
                db.SaveChanges();
            }
        }
    }
}
