﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Business.Services.DetalleProduccion
{
    public class ProduccionDulceLecheService : BaseService<Produccion_Dulce_de_Leche>
    {
        public Produccion_Dulce_de_Leche Get_ByLote(string lote)
        {
            using (var db = new Data.ProdEntities())
            {
                return db.Produccion_Dulce_de_Leche.Where(pc => pc.Lote == lote).SingleOrDefault();
            }
        }

        public void Update_Humedad(string lote, decimal humedad)
        {
            using (var db = new ProdEntities())
            {
                Produccion_Dulce_de_Leche detalle = Get_ByLote(lote);
                db.Produccion_Dulce_de_Leche.Attach(detalle);
                detalle.Humedad = humedad;
                db.SaveChanges();
            }
        }
    }
}
