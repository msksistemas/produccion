﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services.DetalleProduccion
{
    public class ProduccionMuzzarellaService : BaseService<Produccion_Muzzarela>
    {
        public Produccion_Muzzarela Get_ByLote(string lote)
        {
            using (var db = new ProdEntities())
            {
                return db.Produccion_Muzzarela.Where(pc => pc.Lote == lote).SingleOrDefault();
            }
        }

        public void Update_Humedad(string lote, decimal humedad)
        {
            using (var db = new ProdEntities())
            {
                Produccion_Muzzarela detalle = Get_ByLote(lote);
                db.Produccion_Muzzarela.Attach(detalle);
                detalle.Humedad = humedad;
                db.SaveChanges();
            }
        }
    }
}
