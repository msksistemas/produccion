﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services.DetalleProduccion
{
    public class ProduccionQuesoService : BaseService<Produccion_Quesos> // TODO: Inherit from BaseService<Produccion_Quesos>
    {
        public Produccion_Quesos Get_ByLote(string lote)
        {
            using (var db = new ProdEntities())
            {
                return db.Produccion_Quesos.Where(pc => pc.Lote == lote).SingleOrDefault();
            }
        }

        public void Update_Humedad(string lote, decimal humedad)
        {
            using (var db = new ProdEntities())
            {
                Produccion_Quesos detalle = Get_ByLote(lote);
                db.Produccion_Quesos.Attach(detalle);
                detalle.Humedad = humedad;
                db.SaveChanges();
            }
        }
    }
}
