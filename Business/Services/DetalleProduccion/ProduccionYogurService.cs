﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services.DetalleProduccion
{
    public class ProduccionYogurService : BaseService<Produccion_Yogur>
    {
        public Produccion_Yogur Get_ByLote(string lote)
        {
            using (var db = new Data.ProdEntities())
            {
                return db.Produccion_Yogur.Where(pc => pc.Lote == lote).SingleOrDefault();
            }
        }

        public void Update_Humedad(string lote, decimal humedad)
        {
            using (var db = new ProdEntities())
            {
                Produccion_Yogur detalle = Get_ByLote(lote);
                db.Produccion_Yogur.Attach(detalle);
                detalle.Humedad = humedad;
                db.SaveChanges();
            }
        }
    }
}
