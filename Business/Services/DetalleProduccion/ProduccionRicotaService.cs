﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services.DetalleProduccion
{
    public class ProduccionRicotaService : BaseService<Produccion_Ricota>
    {
        public Produccion_Ricota Get_ByLote(string lote)
        {
            using (var db = new Data.ProdEntities())
            {
                return db.Produccion_Ricota.Where(pc => pc.Lote == lote).SingleOrDefault();
            }
        }

        public void Update_Humedad(string lote, decimal humedad)
        {
            using (var db = new ProdEntities())
            {
                Produccion_Ricota detalle = Get_ByLote(lote);
                db.Produccion_Ricota.Attach(detalle);
                detalle.Humedad = humedad;
                db.SaveChanges();
            }
        }
    }
}
