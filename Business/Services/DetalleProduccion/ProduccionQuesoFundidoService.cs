﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services.DetalleProduccion
{
    public class ProduccionQuesoFundidoService : BaseService<Produccion_Quesos_Fundido>
    {
        public Produccion_Quesos_Fundido Get_ByLote(string lote)
        {
            using (var db = new Data.ProdEntities())
            {
                return db.Produccion_Quesos_Fundido.Where(pc => pc.Lote == lote).SingleOrDefault();
            }
        }

        public void Update_Humedad(string lote, decimal humedad)
        {
            using (var db = new ProdEntities())
            {
                Produccion_Quesos_Fundido detalle = Get_ByLote(lote);
                db.Produccion_Quesos_Fundido.Attach(detalle);
                detalle.Humedad = humedad;
                db.SaveChanges();
            }
        }
    }
}
