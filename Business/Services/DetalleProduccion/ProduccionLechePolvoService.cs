﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Business.Services.DetalleProduccion
{
    public class ProduccionLechePolvoService : BaseService<Produccion_Leche_Polvo>
    {
        public Produccion_Leche_Polvo Get_ByLote(string lote)
        {
            using (var db = new Data.ProdEntities())
            {
                return db.Produccion_Leche_Polvo.Where(pc => pc.Lote == lote).SingleOrDefault();
            }
        }

        public void Update_Humedad(string lote, decimal humedad)
        {
            using (var db = new ProdEntities())
            {
                Produccion_Leche_Polvo detalle = Get_ByLote(lote);
                db.Produccion_Leche_Polvo.Attach(detalle);
                detalle.Humedad = humedad;
                db.SaveChanges();
            }
        }
    }
}
