﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class DetalleFormulaEnvasadoService : BaseService<Detalle_Formula_Envasado>
    {
        public void Update_Cantidad(int formula, string insumo, decimal cant)
        {
            using (var db = new ProdEntities())
            {
                Detalle_Formula_Envasado de = db.Detalle_Formula_Envasado.Find(formula, insumo);
                db.Detalle_Formula_Envasado.Attach(de);
                de.Cantidad = cant;
                db.SaveChanges();
            }
        }
    }
}
