﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Business.Services
{
    public class BaseService<TModel> : IBaseService<TModel> where TModel : class
    {
        public IEnumerable<TModel> Get(Expression<Func<TModel, bool>> filter = null)
        {
            using(var db = new ProdEntities())
            {
                return db.Set<TModel>()
                    .Where(filter)
                    .ToList();
            }
        }

        public TModel Get(int id)
        {
            using (var db = new ProdEntities())
            {
                return db.Set<TModel>()
                    .Find(id);
            }
        }

        public void Save(TModel ent)
        {
            using (var db = new ProdEntities())
            {
                db.Set<TModel>().Attach(ent);
                db.SaveChanges();
            }
        }
    }
}
