﻿using Business.Services.DetalleProduccion;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class ProduccionService : BaseService<Produccion>
    {
        public Produccion Get_ByLote(string lote)
        {
            using (var db = new ProdEntities())
            {
                return db.Produccion.Where(pc => pc.Lote == lote).SingleOrDefault();
            }
        }

        public TipoProduccion Get_TipoProduccion(string lote)
        {
            object prod = new ProduccionCremaService().Get_ByLote(lote);
            if (prod != null) return TipoProduccion.Crema;

            prod = new ProduccionQuesoService().Get_ByLote(lote);
            if (prod != null) return TipoProduccion.Queso;

            prod = new ProduccionDulceLecheService().Get_ByLote(lote);
            if (prod != null) return TipoProduccion.DulceLeche;

            prod = new ProduccionLecheService().Get_ByLote(lote);
            if (prod != null) return TipoProduccion.Leche;

            prod = new ProduccionLechePolvoService().Get_ByLote(lote);
            if (prod != null) return TipoProduccion.LechePolvo;

            prod = new ProduccionMuzzarellaService().Get_ByLote(lote);
            if (prod != null) return TipoProduccion.Muzzarella;

            prod = new ProduccionQuesoFundidoService().Get_ByLote(lote);
            if (prod != null) return TipoProduccion.QuesoFundido;

            prod = new ProduccionRicotaService().Get_ByLote(lote);
            if (prod != null) return TipoProduccion.Ricota;

            prod = new ProduccionYogurService().Get_ByLote(lote);
            if (prod != null) return TipoProduccion.Yogur;

            return TipoProduccion.None;
        }

        public object Get_Detalle(string lote)
        {
            object prod = new ProduccionCremaService().Get_ByLote(lote);
            if (prod != null) return prod;

            prod = new ProduccionQuesoService().Get_ByLote(lote);
            if (prod != null) return prod;

            prod = new ProduccionDulceLecheService().Get_ByLote(lote);
            if (prod != null) return prod;

            prod = new ProduccionLecheService().Get_ByLote(lote);
            if (prod != null) return prod;

            prod = new ProduccionLechePolvoService().Get_ByLote(lote);
            if (prod != null) return prod;

            prod = new ProduccionMuzzarellaService().Get_ByLote(lote);
            if (prod != null) return prod;

            prod = new ProduccionQuesoFundidoService().Get_ByLote(lote);
            if (prod != null) return prod;

            prod = new ProduccionRicotaService().Get_ByLote(lote);
            if (prod != null) return prod;

            prod = new ProduccionYogurService().Get_ByLote(lote);
            if (prod != null) return prod;

            return null;
        }

        public Producto Get_Producto(string lote)
        {
            using (var db = new ProdEntities())
            {
                Formula formula = db.Formulas.Find(Get_ByLote(lote).Codigo_Formula);
                return db.Productos.Where(p => p.Codigo_Producto == formula.Codigo_Producto).FirstOrDefault();
            }
        }

        public Producto Get_Producto(Produccion ent)
        {
            return ent.Formula.Producto;
        }
    }

    public enum TipoProduccion
    {
        Queso,
        Crema,
        DulceLeche,
        Leche,
        LechePolvo,
        Muzzarella,
        QuesoFundido,
        Ricota,
        Yogur,
        None,
    }

}
