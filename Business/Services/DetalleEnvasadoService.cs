﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class DetalleEnvasadoService : BaseService<Detalle_Envasado>
    {
        public Producto Get_Producto(Detalle_Envasado ent)
        {
            return ent.Formulas_Envasado.Producto;
        }

    }
}
