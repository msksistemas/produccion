﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    interface IBaseService<TModel>
    {
        IEnumerable<TModel> Get(
            // Expresion that tells what does the delegate
            Expression<Func<TModel, bool>> filter = null    
        );
        TModel Get(int id);

        void Save(TModel ent);
    }
}
