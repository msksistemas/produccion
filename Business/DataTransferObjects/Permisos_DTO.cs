﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Business.DataTransferObjects
{
    public class Permisos_DTO
    {
        int id;
        bool value;
        string name;

        public int Id { get => id; set => id = value; }
        public bool Value { get => value; set => this.value = value; }
        public string Name { get => name; set => name = value; }

        public Permisos_DTO() { }

        public void Save(int UserId) 
        {
            if (value == true) { 
                Models.SinglConn.DBPRODDCONN().Insert_Access(UserId, id);
            }
        }

    }
}