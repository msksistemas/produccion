﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.DataTransferObjects
{
    public class User_DTO
    {
        int userId;
        List<Permisos_DTO> permisos = new List<Permisos_DTO>();

        public int UserId { get => userId; set => userId = value; }
        public List<Permisos_DTO> Permisos { get => permisos; set => permisos = value; }

        public void saveAccessScheme() 
        {
            if (permisos != null && permisos.Count >= 0) 
            {
                Models.SinglConn.DBPRODDCONN().Delete_module_accessByUserId(userId);
                foreach (Permisos_DTO p in permisos) 
                {
                    p.Save(UserId);
                }
            }
        }

    }
}
