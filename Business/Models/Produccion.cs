﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using Data;
using Business.Models.DetalleProduccion;

namespace Business.Models
{
    [Obsolete("This class was deprecated due to the new implementation of EF-services.")]
    public class Produccion : baseModel
    {
        string lote;
        int? formula;
        DateTime? fecha;
        bool? historico;
        string lotePadre;
        int? fabrica;
        string planilla;
        int? quesero;
        bool? expedido;
        byte? decomisado;
        string motivoDecomiso;

        public string Lote { get => lote; set => lote = value; }
        public int? Formula { get => formula; set => formula = value; }
        public DateTime? Fecha { get => fecha; set => fecha = value; }
        public bool? Historico { get => historico; set => historico = value; }
        public string LotePadre { get => lotePadre; set => lotePadre = value; }
        public int? Fabrica { get => fabrica; set => fabrica = value; }
        public string Planilla { get => planilla; set => planilla = value; }
        public int? Quesero { get => quesero; set => quesero = value; }
        public bool? Expedido { get => expedido; set => expedido = value; }
        public byte? Decomisado { get => decomisado; set => decomisado = value; }
        public string MotivoDecomiso { get => motivoDecomiso; set => motivoDecomiso = value; }

        private Produccion(Get_Produccion_ByLote_Result p)
        {
            lote = p.Lote;
            lotePadre = p.Lote_Padre;
            formula = p.Codigo_Formula;
            fecha = p.Fecha;
            historico = p.Historico;
            fabrica = p.Codigo_Fabrica;
            planilla = p.Nro_Planilla;
            quesero = p.Codigo_Quesero;
            expedido = p.Expedida;
            decomisado = p.Decomisado;
            motivoDecomiso = p.motivo_decomiso;
        }

        public static Produccion Get_ByLote(string lote)
        {
            try
            {
                ObjectResult<Get_Produccion_ByLote_Result> r = SinglConn.DBPRODDCONN().Get_Produccion_ByLote(lote);
                return r != null
                    ? new Produccion(r.ToList()[0])
                    : null;
            } catch (Exception e)
            {
                return null;
            }
        }

        public TipoProduccion Get_TipoProduccion()
        {
            var prod = ProduccionQueso.Get_ByLote(lote);
            if (prod != null) return TipoProduccion.Queso;

            //prod = ProduccionRicota.Get_ProduccionRicota_ByLote(lote);
            //if (prod != null)
            //    return prod;
            else return TipoProduccion.None;
        }
    }
}

namespace Business.Models.DetalleProduccion
{
    public enum TipoProduccion
    {
        Queso,
        Crema,
        DulceLeche,
        Leche,
        LechePolvo,
        Muzzarella,
        QuesoFundido,
        Ricota,
        Yogur,
        None,
    }
}