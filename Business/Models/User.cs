﻿using Business.Models.Schemes;
using Data;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class User:baseModel
    {

        long idFabrica;
        string name;
        string password;
        string rol;
        bool muestraCostos;
        string idLogin;
        private List<Schemes.Access_Scheme> accessScheme;

        public long IdFabrica { get => idFabrica; set => idFabrica = value; }
        public string Name { get => name; set => name = value; }
        public string Password { get => password; set => password = value; }
        public string Rol { get => rol; set => rol = value; }
        public bool MuestraCostos { get => muestraCostos; set => muestraCostos = value; }
        public string IdLogin { get => idLogin; set => idLogin = value; }
        public List<Access_Scheme> AccessScheme { get => accessScheme; set => accessScheme = value; }

        private User(Data.Get_Usuario_ById_Result u) 
        {
            Id = u.idUsuario;
            name = u.Usuario;
            password = "password ;)";
            rol = u.Rol;
            muestraCostos = u.Muestra_Costos == null ? false : u.Muestra_Costos.Value;
            idLogin = u.Id_Login;
            AccessScheme = GetScheme();
        }

        private List<Schemes.Access_Scheme> GetScheme() 
        {
            List<Access_Scheme> ASList = new List<Access_Scheme>();
            List<Get_Access_ByUserID_Result> resultAccess =  SinglConn.DBPRODDCONN().Get_Access_ByUserID(Id).ToList();
            foreach (Module m in Module.GetAll()) 
            {
                ASList.Add(new Access_Scheme { Enabled = false, Module=m });
            }
            foreach (Access_Scheme acs in ASList) 
            {
                foreach (Get_Access_ByUserID_Result UserAccess in resultAccess) 
                {
                    if (acs.Module.Id == UserAccess.Module)
                    {
                        acs.Enabled = true;
                        break;
                    }
                }
            }
            return ASList;
        }

        public void SaveAccesScheme(List<DataTransferObjects.Permisos_DTO> DtoList) 
        {

        }

        public static User Get_UserByID(long UserID) 
        {
            try
            {
                ObjectResult<Get_Usuario_ById_Result> r = Business.Models.SinglConn.DBPRODDCONN().Get_Usuario_ById(UserID);
                return r != null ? new User(r.ToList()[0]) : null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
