﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Business.Models.DetalleProduccion
{
    public class ProduccionQueso : baseModel
    {
        int? litrosTina;
        decimal? factor;
        int? estado;
        int? silo1;
        int? silo2;
        int? quesero;
        decimal? piezasObtenidas;
        decimal? kilosTeoricos;
        decimal? kilosReales;
        int? insumo_Producto;
        decimal? grasa;
        decimal? proteina;
        decimal? grasaSuero;
        int? litrosSilo1;
        int? litrosSilo2;
        TimeSpan? horaAgregados;
        int? tiempoCoagulacion;
        int? tiempoLirado;
        int? tiempoCoccion;
        int? temperaturaCoccion;
        bool? historico;
        int? fabrica;
        int? tina;
        decimal? grasaSilo1;
        decimal? grasaSilo2;
        decimal? proteinaSilo1;
        decimal? proteinaSilo2;
        int? camara;
        bool? concentrado;
        int? acidez;
        decimal? humedad;
        string lote;
        int? tiempoPrensado;
        decimal? presionPrensado;
        decimal? temperaturaPasteurizado;
        TimeSpan? horaInicioLlenadoTina;
        TimeSpan? horaFinalLlenadoTina;
        int? motivoRetencion;
        decimal? kilosEnvasado;
        decimal? kilosAntesEnvasado;
        TimeSpan? horasAcidificacion;
        decimal? phFinal;
        decimal? acidezFinal;

        public int? LitrosTina { get => litrosTina; set => litrosTina = value; }
        public decimal? Factor { get => factor; set => factor = value; }
        public int? Estado { get => estado; set => estado = value; }
        public int? Silo1 { get => silo1; set => silo1 = value; }
        public int? Silo2 { get => silo2; set => silo2 = value; }
        public int? Quesero { get => quesero; set => quesero = value; }
        public decimal? PiezasObtenidas { get => piezasObtenidas; set => piezasObtenidas = value; }
        public decimal? KilosTeoricos { get => kilosTeoricos; set => kilosTeoricos = value; }
        public decimal? KilosReales { get => kilosReales; set => kilosReales = value; }
        public int? Insumo_Producto { get => insumo_Producto; set => insumo_Producto = value; }
        public decimal? Grasa { get => grasa; set => grasa = value; }
        public decimal? Proteina { get => proteina; set => proteina = value; }
        public decimal? GrasaSuero { get => grasaSuero; set => grasaSuero = value; }
        public int? LitrosSilo1 { get => litrosSilo1; set => litrosSilo1 = value; }
        public int? LitrosSilo2 { get => litrosSilo2; set => litrosSilo2 = value; }
        public TimeSpan? HoraAgregados { get => horaAgregados; set => horaAgregados = value; }
        public int? TiempoCoagulacion { get => tiempoCoagulacion; set => tiempoCoagulacion = value; }
        public int? TiempoLirado { get => tiempoLirado; set => tiempoLirado = value; }
        public int? TiempoCoccion { get => tiempoCoccion; set => tiempoCoccion = value; }
        public int? TemperaturaCoccion { get => temperaturaCoccion; set => temperaturaCoccion = value; }
        public bool? Historico { get => historico; set => historico = value; }
        public int? Fabrica { get => fabrica; set => fabrica = value; }
        public int? Tina { get => tina; set => tina = value; }
        public decimal? GrasaSilo1 { get => grasaSilo1; set => grasaSilo1 = value; }
        public decimal? GrasaSilo2 { get => grasaSilo2; set => grasaSilo2 = value; }
        public decimal? ProteinaSilo1 { get => proteinaSilo1; set => proteinaSilo1 = value; }
        public decimal? ProteinaSilo2 { get => proteinaSilo2; set => proteinaSilo2 = value; }
        public int? Camara { get => camara; set => camara = value; }
        public bool? Concentrado { get => concentrado; set => concentrado = value; }
        public int? Acidez { get => acidez; set => acidez = value; }
        public decimal? Humedad { get => humedad; set => humedad = value; }
        public string Lote { get => lote; set => lote = value; }
        public int? TiempoPrensado { get => tiempoPrensado; set => tiempoPrensado = value; }
        public decimal? PresionPrensado { get => presionPrensado; set => presionPrensado = value; }
        public decimal? TemperaturaPasteurizado { get => temperaturaPasteurizado; set => temperaturaPasteurizado = value; }
        public TimeSpan? HoraInicioLlenadoTina { get => horaInicioLlenadoTina; set => horaInicioLlenadoTina = value; }
        public TimeSpan? HoraFinalLlenadoTina { get => horaFinalLlenadoTina; set => horaFinalLlenadoTina = value; }
        public int? MotivoRetencion { get => motivoRetencion; set => motivoRetencion = value; }
        public decimal? KilosEnvasado { get => kilosEnvasado; set => kilosEnvasado = value; }
        public decimal? KilosAntesEnvasado { get => kilosAntesEnvasado; set => kilosAntesEnvasado = value; }
        public TimeSpan? HorasAcidificacion { get => horasAcidificacion; set => horasAcidificacion = value; }
        public decimal? PhFinal { get => phFinal; set => phFinal = value; }
        public decimal? AcidezFinal { get => acidezFinal; set => acidezFinal = value; }

        public ProduccionQueso(Data.Get_ProduccionQuesos_ByLote_Result pq)
        {
            litrosTina = pq.Litros_Tina;
            factor = pq.Factor;
            estado = pq.Codigo_Estado;
            silo1 = pq.Codigo_Silo_1;
            silo2 = pq.Codigo_Silo_2;
            quesero = pq.Codigo_Quesero;
            piezasObtenidas = pq.Piezas_Obtenidas;
            kilosTeoricos = pq.Kilos_Teoricos;
            kilosReales = pq.Kilos_Reales;
            insumo_Producto = pq.Insumo_o_Producto;
            grasa = pq.Grasa;
            proteina = pq.Proteina;
            grasaSuero = pq.Grasa_Suero;
            litrosSilo1 = pq.Litros_Silo_1;
            litrosSilo2 = pq.Litros_Silo_2;
            horaAgregados = pq.HoraAgregados;
            tiempoCoagulacion = pq.TiempoCoagulacion;
            tiempoLirado = pq.TiempoLirado;
            tiempoCoccion = pq.TiempoCoccion;
            temperaturaCoccion = pq.Temperatura_Coccion;
            historico = pq.Historico;
            fabrica = pq.Codigo_Fabrica;
            tina = pq.Codigo_Tina;
            grasaSilo1 = pq.Grasa_Silo_1;
            grasaSilo2 = pq.Grasa_Silo_2;
            proteinaSilo1 = pq.Proteina_Silo_1;
            proteinaSilo2 = pq.Proteina_Silo_2;
            camara = pq.Codigo_Camara;
            concentrado = pq.Concentrado;
            acidez = pq.Acidez;
            humedad = pq.Humedad;
            lote = pq.Lote;
            tiempoPrensado = pq.Tiempo_Prensado;
            presionPrensado = pq.Presion_Prensado;
            temperaturaPasteurizado = pq.Temperatura_Pausterizado;
            horaInicioLlenadoTina = pq.Hora_Inicio_Llenado_Tina;
            horaFinalLlenadoTina = pq.Hora_Final_Llenado_Tina;
            motivoRetencion = pq.Codigo_Motivo_Retencion;
            kilosEnvasado = pq.Kilos_Envasado;
            kilosAntesEnvasado = pq.Kilos_Antes_Envasado;
            horasAcidificacion = pq.Horas_Acificacion;
            phFinal = pq.PH_Final;
            acidezFinal = pq.Acidez_Final;
        }

        

        public static ProduccionQueso Get_ByLote(string lote)
        {
            try
            {
                ObjectResult<Get_ProduccionQuesos_ByLote_Result> r = SinglConn.DBPRODDCONN().Get_ProduccionQuesos_ByLote(lote);
                return r != null
                    ? new ProduccionQueso(r.FirstOrDefault())
                    : null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        
        public int Update_Humedad(decimal humedad)
        {
            try
            {
                return SinglConn.DBPRODDCONN().Update_ProduccionQueso_Humedad(lote, humedad);
            } catch (Exception e)
            {
                return -1;
            }
            
        }
    }
}
