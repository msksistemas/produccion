﻿namespace Business.Models
{
    public static class SinglConn
    {
        private static Data.ProdEntities DBprodConn;
        public static Data.ProdEntities DBPRODDCONN() 
        {
            if (DBprodConn != null)
            {
                return DBprodConn;
            }
            else 
            {
                return new Data.ProdEntities();
            }
        }
    }
}
