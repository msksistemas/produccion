﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models.Schemes
{
    public class Access_Scheme
    {
        private Module module=null;
        private bool enabled=false;

        public Module Module { get => module; set => module = value; }
        public bool Enabled { get => enabled; set => enabled = value; }


    }
}
