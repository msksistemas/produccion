﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class Module:baseModel
    {
        string name;
       

        public string Name { get => name; set => name = value; }

        private Module(Data.Get_Modules_Result M) 
        {
            name = M.Name;
            Id = M.Id;
        }
        private Module(Data.Get_Modules_ByID_Result M)
        {
            name = M.Name;
            Id = M.Id;
        }

        public static List<Module> GetAll() 
        {
            List<Get_Modules_Result> r = SinglConn.DBPRODDCONN().Get_Modules().ToList();

            if (r != null && r.Count>0)
            {
                List<Module> m = new List<Module>();
                foreach (Get_Modules_Result a in r)
                {
                    m.Add(new Module(a));
                }
                return m;
            }
            else 
            {
                return null;
            }
            
        }

        public static Module GetModuleByID(long id) 
        {
            Get_Modules_ByID_Result r = SinglConn.DBPRODDCONN().Get_Modules_ByID(id).ToList()[0];
            return new Module(r);
        }


    }
}
