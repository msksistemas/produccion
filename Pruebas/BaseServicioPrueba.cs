﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Servicios.Clases;
using AccesoDatos.Contexto.Produccion;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Pruebas
{
    /// <summary>
    /// Prueba el funcionamiento de la clase BaseServicio
    /// </summary>
    [TestClass]
    public class BaseServicioPrueba
    {
        public BaseServicio<Lineas> BaseServicio { get; set; }
        public BaseServicio<CompraInsumos> CompraInsumosBaseServicio { get; set; }
        public BaseServicio<Detalle_Ajuste_Insumos> DetalleAjusteInsumosServicio { get; set; }
        public BaseServicio<Ajustes_Insumos> AjusteInsumosServicio { get; set; }
        public BaseServicio<Detalle_Compra> DetalleCompraServicio { get; set; }
        public BaseServicio<Detalle_Envasado> DetalleEnvasadoServicio { get; set; }
        public BaseServicio<Insumos> InsumosServicio { get; set; }
        public BaseServicio<Lavados_Formula> LavadoFormulasServicio { get; set; }
        public BaseServicio<Productos> ProductosServicio { get; set; }



        public BaseServicioPrueba()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Obtiene o establece el contexto de las pruebas que proporciona
        ///información y funcionalidad para la serie de pruebas actual.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Atributos de prueba adicionales
        //
        // Puede usar los siguientes atributos adicionales conforme escribe las pruebas:
        //
        // Use ClassInitialize para ejecutar el código antes de ejecutar la primera prueba en la clase
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup para ejecutar el código una vez ejecutadas todas las pruebas en una clase
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        [TestInitialize()]
        public void Inicializar()
        {
            var contexto = new dbProd();
            this.BaseServicio = new BaseServicio<Lineas>();
            this.BaseServicio.SetearContexto(contexto);
            this.CompraInsumosBaseServicio = new BaseServicio<CompraInsumos>();
            this.CompraInsumosBaseServicio.SetearContexto(contexto);
            this.DetalleAjusteInsumosServicio = new BaseServicio<Detalle_Ajuste_Insumos>();
            this.DetalleAjusteInsumosServicio.SetearContexto(contexto);
            this.DetalleCompraServicio = new BaseServicio<Detalle_Compra>();
            this.DetalleCompraServicio.SetearContexto(contexto);
            this.AjusteInsumosServicio = new BaseServicio<Ajustes_Insumos>();
            this.AjusteInsumosServicio.SetearContexto(contexto);
            this.DetalleEnvasadoServicio = new BaseServicio<Detalle_Envasado>();
            this.DetalleEnvasadoServicio.SetearContexto(contexto);
            this.InsumosServicio = new BaseServicio<Insumos>();
            this.InsumosServicio.SetearContexto(contexto);
            this.LavadoFormulasServicio = new BaseServicio<Lavados_Formula>();
            this.LavadoFormulasServicio.SetearContexto(contexto);
            this.ProductosServicio = new BaseServicio<Productos>();
            this.ProductosServicio.SetearContexto(contexto);
        }

        // Use TestCleanup para ejecutar el código una vez ejecutadas todas las pruebas
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Agregar()
        {
            this.BaseServicio.Agregar(new Lineas() { Codigo_Linea = 3, Nombre = "linea Test" });
            var nuevaLinea = this.BaseServicio.Obtener((l) => l.Codigo_Linea == 3).SingleOrDefault();
            var insumo = new Insumos { Codigo_Insumo = "987", Descripcion = "Agregando test" };
            this.InsumosServicio.Agregar(insumo);
            Assert.IsNotNull(nuevaLinea);
        }
        [TestMethod]
        public void AgregarComplejo()
        {
            var compraInsumos = new CompraInsumos()
            {
                Numero_Comprobante = 9999,
                Prefijo = 1,
                Codigo_Fabrica = 1,
                Codigo_Proveedor = 565,
                Suma_Stock = false,
            };
            compraInsumos.Detalle_Compra.Add(new Detalle_Compra
            {
                Cantidad = 555,
                Kilos_Litros = 555,
                Codigo_Insumo = "0020",
                Importe = Convert.ToDecimal(137.1 * 555),
            });

            this.CompraInsumosBaseServicio.Agregar(compraInsumos);
            var compraInsumosNueva = this.CompraInsumosBaseServicio.Obtener((compra) => compra.Numero_Comprobante == 9999 
                                                                                   && compra.Prefijo == 1 
                                                                                   && compra.Codigo_Fabrica == 1 
                                                                                   && compra.Codigo_Proveedor == 565).SingleOrDefault();
            Assert.IsNotNull(compraInsumosNueva);
        }
        [TestMethod]
        public void Editar()
        {
            var linea = this.BaseServicio.Obtener((l) => l.Codigo_Linea == 3).SingleOrDefault();
            linea.Nombre = "nombre editado 2";
            this.BaseServicio.Editar(linea);
            var lineaEditada = this.BaseServicio.Obtener((l) => l.Codigo_Linea == 3).SingleOrDefault();

            
            Assert.AreEqual(lineaEditada.Nombre, linea.Nombre);
        }

        [TestMethod]
        public void EditarComplejo()
        {
            var compraInsumo = this.CompraInsumosBaseServicio.Obtener((compra) => compra.Numero_Comprobante == 9999
                                                                                   && compra.Prefijo == 1
                                                                                   && compra.Codigo_Fabrica == 1
                                                                                   && compra.Codigo_Proveedor == 565).SingleOrDefault();
            foreach (var item in compraInsumo.Detalle_Compra)
            {
                item.Cantidad = 666;
            }
            this.CompraInsumosBaseServicio.Editar(compraInsumo);
            var compraInsumoEditada = this.CompraInsumosBaseServicio.Obtener((compra) => compra.Numero_Comprobante == 9999
                                                                                   && compra.Prefijo == 1
                                                                                   && compra.Codigo_Fabrica == 1
                                                                                   && compra.Codigo_Proveedor == 565).SingleOrDefault();
            bool seEdito = false;
            foreach (var item in compraInsumoEditada.Detalle_Compra)
            {
                if (item.Cantidad == 666)
                    seEdito = true;
            }
            Assert.IsTrue(seEdito);
        }
        [TestMethod]
        public void Borrar()
        {
            var linea = this.BaseServicio.Obtener((l) => l.Codigo_Linea == 3).SingleOrDefault();
            this.BaseServicio.Borrar(linea);
            var borrada = this.BaseServicio.Obtener((l) => l.Codigo_Linea == 3).Count == 0 ? true : false;

            Assert.IsTrue(borrada);
        }
        [TestMethod]
        public void BorrarComplejo()
        {
            var compraInsumos = this.CompraInsumosBaseServicio.Obtener((compra) => compra.Numero_Comprobante == 9999
                                                                                   && compra.Prefijo == 1
                                                                                   && compra.Codigo_Fabrica == 1
                                                                                   && compra.Codigo_Proveedor == 565).SingleOrDefault();
            this.CompraInsumosBaseServicio.Borrar(compraInsumos);
            var borrada = this.CompraInsumosBaseServicio.Obtener((compra) => compra.Numero_Comprobante == 9999
                                                                                    && compra.Prefijo == 1
                                                                                    && compra.Codigo_Fabrica == 1
                                                                                    && compra.Codigo_Proveedor == 565).Count == 0 ? true : false;

            Assert.IsTrue(borrada);
        }

        [TestMethod]
        public void Obtener()
        {
            //var linea = this.BaseServicio.ObtenerConRawSql("SELECT * FROM dbo.Lineas ORDERBY [Codigo_Interno_Linea]");
          var linea = this.BaseServicio.ObtenerConRawSql("SELECT TOP(1) [Codigo_Interno_Linea], [Codigo_Linea],[Nombre] " +
                                                                        "FROM dbo.Lineas " +
                                                                        "WHERE [Codigo_Interno_Linea] = @Codigo AND @P = 1 "+
                                                                        "ORDER BY [Codigo_Interno_Linea] DESC", new [] { new SqlParameter("Codigo",1), new SqlParameter("P", 1) });

            var compraInsumos = this.CompraInsumosBaseServicio.Obtener((compra) => compra.Codigo_Fabrica == 1, null).FirstOrDefault();
            // var detalleCompras = this.DetalleCompraServicio.Obtener((det) => det.Codigo_Insumo == "0020",null,"Insumos").FirstOrDefault();
            // var compraInsumos_2 = this.CompraInsumosBaseServicio.Obtener((c)=> c.Codigo_Interno_Compra == 1045).SingleOrDefault();
            // var detalles = this.DetalleAjusteInsumosServicio.Obtener((dai) => dai.Codigo_Interno_Ajuste == 7,null,"Insumos");
            // var listado = this.AjusteInsumosServicio.Obtener(null,null);
            var prod = this.ProductosServicio.Obtener();
            var listaAjuste = this.AjusteInsumosServicio.Obtener((ai) => ai.Codigo_Interno_Ajuste == 7).FirstOrDefault();
            var lavado = this.LavadoFormulasServicio.Obtener().FirstOrDefault();
            var detalleEnvasado1 = this.DetalleEnvasadoServicio.Obtener();

            var detalleEnvasado2 = this.DetalleEnvasadoServicio.Obtener(null,null,"Envasado");
            Assert.IsNotNull(compraInsumos);
            Assert.IsNotNull(compraInsumos.Detalle_Compra);
            Assert.IsNotNull(linea);
            Assert.IsNotNull(this.BaseServicio.Obtener());
            Assert.IsNotNull(this.BaseServicio.Obtener((l) => l.Codigo_Linea == 1));

        }
    }
}
