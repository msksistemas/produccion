﻿namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Produccion_Quesos_Fundido
    {
        [Key]
        public int Codigo_Interno_Produccion { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        public int? Kilos_Reproceso { get; set; }

        public int? Codigo_Estado { get; set; }

        public int? Codigo_Quesero { get; set; }

        public decimal Piezas_Obtenidas { get; set; }

        public decimal? Kilos_Teoricos { get; set; }

        public decimal? Kilos_Reales { get; set; }

        public decimal? Kilos_Envasado { get; set; }
        
        public bool? Historico { get; set; }

        public int? Codigo_Fabrica { get; set; }

        public int? Codigo_Camara { get; set; }

        public TimeSpan? Tiempo_Calentamiento { get; set; }
        
        public TimeSpan? Tiempo_Fundido { get; set; }
        
        public TimeSpan? Tiempo_Envasado { get; set; }

        public TimeSpan? Tiempo_Enfriamiento { get; set; }

        public decimal? Ph { get; set; }

        public int? Codigo_Motivo_Retencion { get; set; }
        
        public virtual Motivos_Retencion Motivos_Retencion { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
