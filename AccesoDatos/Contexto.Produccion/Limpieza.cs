namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Limpieza")]
    public partial class Limpieza
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Limpieza()
        {
            Detalle_Limpieza = new HashSet<Detalle_Limpieza>();
        }

        [Key]
        public int Codigo { get; set; }

        public int Codigo_Formula_Lavado { get; set; }

        public string Comentario { get; set; }

        public int Codigo_Fabrica { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? fecha { get; set; }

        public bool historica { get; set; }

        public int? Codigo_Quesero { get; set; }

        public decimal? Concentracion1 { get; set; }

        public TimeSpan? Hora1 { get; set; }

        public decimal? Concentracion2 { get; set; }

        public TimeSpan? Hora2 { get; set; }

        public decimal? Concentracion3 { get; set; }

        public TimeSpan? Hora3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Limpieza> Detalle_Limpieza { get; set; }

        public virtual Lavados_Formula Lavados_Formula { get; set; }
    }
}
