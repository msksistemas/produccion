namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class Lavados_Formula
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Lavados_Formula()
        {
            Costos_Indirectos_Formula_Lavados = new HashSet<Costos_Indirectos_Formula_Lavados>();
            Detalle_Formula_Lavados = new HashSet<Detalle_Formula_Lavados>();
            Limpieza = new HashSet<Limpieza>();
        }

        [Key]
        [Display(Name = "N�mero F�rmula")]
        public int Codigo { get; set; }

        public int Codigo_Lavado { get; set; }

        public string Comentario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Formula_Lavados> Costos_Indirectos_Formula_Lavados { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Formula_Lavados> Detalle_Formula_Lavados { get; set; }

        public virtual Lavados Lavados { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Limpieza> Limpieza { get; set; }
    }
}
