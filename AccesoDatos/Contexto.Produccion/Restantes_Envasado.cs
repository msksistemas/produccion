﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contexto.Produccion
{
    public partial class Restantes_Envasado
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Restantes_Envasado { get; set; }

        public string Motivo { get; set; }

        [Required]
        public int Codigo_Interno_Envasado { get; set; }

        [DefaultValue(0)]
        public decimal Hormas { get; set; }

        [DefaultValue(0)]
        public decimal Kilos { get; set; }

        [Required]
        public string Lote { get; set; }

        public string Codigo_Insumo { get; set; }

        public virtual Envasado Envasado { get; set; }

        public virtual Insumos Insumos { get; set; }
    }
}
