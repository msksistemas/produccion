namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Formulas_Envasado
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Formulas_Envasado()
        {
            Costos_Indirectos_Formula_Envasado = new HashSet<Costos_Indirectos_Formula_Envasado>();
            Detalle_Envasado = new HashSet<Detalle_Envasado>();
            Detalle_Formula_Envasado = new HashSet<Detalle_Formula_Envasado>();
        }

        [Key]
        public int Codigo_Interno_Formula { get; set; }

        [Required]
        [StringLength(13)]
        public string Codigo_Producto { get; set; }

        public int Cantidad_Envasada { get; set; }

        [StringLength(20)]
        public string Numero_Producto_Ventas { get; set; }

        [StringLength(1)]
        public string Unidad_Medida { get; set; }

        public int? Obtenidas_Por_Envasada { get; set; }

        public bool? Es_Insumo { get; set; }

        public bool? Es_Insumo_Prod_Ventas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Formula_Envasado> Costos_Indirectos_Formula_Envasado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Envasado> Detalle_Envasado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Formula_Envasado> Detalle_Formula_Envasado { get; set; }

        public virtual Productos Productos { get; set; }
    }
}
