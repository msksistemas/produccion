﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccesoDatos.Contexto.Produccion
{
    public partial class Detalle_Resultado_Analisis
    {
        [Key]        
        public int Codigo_Interno_Detalle { get; set; }

        public decimal Resultado_Analisis { get; set; }

        public int? Codigo_Interno_Analisis { get; set; }

        public int? Codigo_Interno_Resultado { get; set; }

        public virtual Analisis_Microbiologico Analisis_Microbiologico { get; set; }

        public virtual Resultado_Analisis_Microbiologico Resultado_Analisis_Microbiologico { get; set; }


    }
}
