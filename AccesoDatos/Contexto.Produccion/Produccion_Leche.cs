namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Produccion_Leche
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Interno_Produccion { get; set; }

        public decimal? Litros_Teoricos { get; set; }

        public decimal? Litros_Reales { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha_Vencimiento { get; set; }

        [Required]
        [StringLength(13)]
        public string Lote { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
