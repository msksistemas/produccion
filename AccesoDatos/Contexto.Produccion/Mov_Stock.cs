namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Mov_Stock
    {
        public int id { get; set; }

        [Required]
        [StringLength(13)]
        public string Codigo_Prod { get; set; }

        [Required]
        [StringLength(20)]
        public string Tipo_Mov { get; set; }

        public DateTime Fecha { get; set; }

        [Required]
        [StringLength(30)]
        public string Estado { get; set; }

        public decimal? Cantidad { get; set; }

        public decimal? Kilos { get; set; }

        [StringLength(20)]
        public string Nro_Comprobante { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        public int? Fabrica { get; set; }

        public bool? EsAjuste { get; set; }

        public int? Codigo_Interno_Produccion { get; set; }

        public int? Codigo_Interno_Detalle_Envasado { get; set; }

        public int? Codigo_Interno_Restante_Envasado { get; set; }
    }
}
