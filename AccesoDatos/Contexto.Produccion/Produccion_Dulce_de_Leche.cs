namespace AccesoDatos.Contexto.Produccion
{
    using System.ComponentModel.DataAnnotations;

    public partial class Produccion_Dulce_de_Leche
    {
        [Key]
        public int Codigo_Interno_Produccion { get; set; }

        public int? Silo { get; set; }

        public decimal? Kilos_Teoricos { get; set; }

        public decimal? Kilos_Reales { get; set; }

        [Required]
        [StringLength(13)]
        public string Lote { get; set; }

        public decimal? Acidez { get; set; }

        public decimal? Ph { get; set; }

        public decimal? Temperatura { get; set; }

        public decimal? Densidad { get; set; }

        public decimal? Litros { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
