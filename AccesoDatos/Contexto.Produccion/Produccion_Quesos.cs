namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Produccion_Quesos
    {
        [Key]
        public int Codigo_Interno_Produccion { get; set; }

        public int? Litros_Tina { get; set; }

        public decimal? Factor { get; set; }

        public int? Codigo_Estado { get; set; }

        public int? Codigo_Silo_1 { get; set; }

        public int? Codigo_Silo_2 { get; set; }

        public int? Codigo_Quesero { get; set; }

        public decimal? Piezas_Obtenidas { get; set; }

        [DisplayFormat(DataFormatString = "{0:F3}", ApplyFormatInEditMode = true)]
        public decimal? Kilos_Teoricos { get; set; }

        [DisplayFormat(DataFormatString = "{0:F3}", ApplyFormatInEditMode = true)]
        public decimal? Kilos_Reales { get; set; }

        public int? Insumo_o_Producto { get; set; }

        public decimal? Grasa { get; set; }

        public decimal? Proteina { get; set; }

        public decimal? Grasa_Suero { get; set; }

        public int? Litros_Silo_1 { get; set; }

        public int? Litros_Silo_2 { get; set; }

        public TimeSpan? HoraAgregados { get; set; }

        public int? TiempoCoagulacion { get; set; }
        public decimal? Temperatura_Coagulacion { get; set; }

        public int? TiempoLirado { get; set; }

        public int? TiempoCoccion { get; set; }

        public int? Temperatura_Coccion { get; set; }

        public bool? Historico { get; set; }

        public int? Codigo_Fabrica { get; set; }

        public int? Codigo_Tina { get; set; }

        public decimal? Grasa_Silo_1 { get; set; }

        public decimal? Grasa_Silo_2 { get; set; }

        public decimal? Proteina_Silo_1 { get; set; }

        public decimal? Proteina_Silo_2 { get; set; }

        public int? Codigo_Camara { get; set; }

        public bool? Concentrado { get; set; }

        [DisplayFormat(DataFormatString = "{0:F3}", ApplyFormatInEditMode = true)]
        public decimal? Factor_Grasa_Proteina { get; set; }

        public bool Test_Fosfatasa_Negativa { get; set; }

        public int? Acidez { get; set; }

        public decimal? Humedad { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        public int? Tiempo_Prensado { get; set; }

        public decimal? Presion_Prensado { get; set; }

        public decimal? Temperatura_Pausterizado { get; set; }

        public TimeSpan? Hora_Inicio_Llenado_Tina { get; set; }

        public TimeSpan? Hora_Final_Llenado_Tina { get; set; }

        public int? Codigo_Motivo_Retencion { get; set; }

        public decimal? Kilos_Envasado { get; set; }

        public decimal? Kilos_Antes_Envasado { get; set; }

        public TimeSpan? Horas_Acificacion { get; set; }

        public decimal? PH_Final { get; set; }
        
        public decimal? Acidez_Final { get; set; }

        public DateTime? Fecha_Pesada { get; set; }

        public decimal? Rendimiento { get; set; }

        public virtual Motivos_Retencion Motivos_Retencion { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
