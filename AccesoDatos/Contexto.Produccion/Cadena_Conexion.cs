namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cadena_Conexion
    {
        public int ID { get; set; }

        public string Servidor { get; set; }

        public string Usuario { get; set; }

        public string Contraseña { get; set; }

        public string Tabla { get; set; }
    }
}
