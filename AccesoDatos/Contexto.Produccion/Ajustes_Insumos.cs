namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Ajustes_Insumos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Ajustes_Insumos()
        {
            Detalle_Ajuste_Insumos = new HashSet<Detalle_Ajuste_Insumos>();
        }

        [Key]
        public int Codigo_Interno_Ajuste { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime? Fecha { get; set; }

        [StringLength(100)]
        public string Comentario { get; set; }

        [StringLength(50)]
        public string Tipo_Movimiento { get; set; }

        public int? idUsuario { get; set; }

        [StringLength(8)]
        [Required]
        public string Numero_Comprobante { get; set; }

        public int Prefijo { get; set; }

        public decimal? Descuento { get; set; }

        public bool? Historico { get; set; }
        public int? Fabrica { get; set; }
        public int? Fabrica_Origen { get; set; }

        public bool? Es_Traspaso { get; set; }

        [StringLength(50)]
        public string Estado { get; set; }

        public string Motivo_Rechazo { get; set; }

        public int? Codigo_Proveedor { get; set; }

        public virtual Usuarios Usuarios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Ajuste_Insumos> Detalle_Ajuste_Insumos { get; set; }
    }
}
