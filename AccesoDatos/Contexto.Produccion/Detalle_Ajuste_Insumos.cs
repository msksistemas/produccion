namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Ajuste_Insumos
    {
        [Key]
        public int Codigo_Interno_Detalle { get; set; }

        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        [StringLength(13)]
        public string Numero_Producto { get; set; }

        public decimal? Cantidad { get; set; }

        public decimal? Kilos_Litros { get; set; }

        public int Codigo_Interno_Ajuste { get; set; }

        [StringLength(15)]
        public string Lote { get; set; }

        public double Precio_Compra { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha_Vencimiento { get; set; }

        public decimal? Ph { get; set; }
        public decimal? Humedad { get; set; }

        public int? Codigo_Proveedor { get; set; }

        public virtual Ajustes_Insumos Ajustes_Insumos { get; set; }

        [ForeignKey("Codigo_Insumo")]
        public virtual Insumos Insumos { get; set; }
    }
}
