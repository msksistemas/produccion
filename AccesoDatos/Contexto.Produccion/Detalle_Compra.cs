namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Compra
    {
        [Key]
        public int Codigo_Detalle_Interno { get; set; }

        [Required]
        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public decimal? Cantidad { get; set; }

        public decimal? Kilos_Litros { get; set; }

        public decimal? Sin_Cargo_Kilos_Litros { get; set; }

        public decimal? Precio_Costo { get; set; }

        public decimal? Precio_Lista { get; set; }

        [StringLength(8)]
        public string Codigo_Imputacion { get; set; }

        public decimal? Importe { get; set; }

        public int? Codigo_Interno_Compra { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }

        public decimal? Descuento { get; set; }

        public decimal? Sin_Cargo_Cantidad { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha_Vencimiento { get; set; }

        public decimal? Ph { get; set; }

        public decimal? Humedad { get; set; }

        public int? codigo_detalle_OC { get; set; }

        public int? Codigo_Fabrica { get; set; }

        public virtual CompraInsumos CompraInsumos { get; set; }

        public virtual Insumos Insumos { get; set; }
    }
}
