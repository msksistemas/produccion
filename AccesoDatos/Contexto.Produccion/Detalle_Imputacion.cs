namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Imputacion
    {
        [Key]
        public int Codigo_Detalle_Interno { get; set; }

        [Required]
        [StringLength(8)]
        public string Codigo_Imputacion { get; set; }

        public decimal Importe { get; set; }

        public int? Codigo_Interno_Compra { get; set; }

        public int Amortizacion { get; set; }

        public virtual CompraInsumos CompraInsumos { get; set; }
    }
}
