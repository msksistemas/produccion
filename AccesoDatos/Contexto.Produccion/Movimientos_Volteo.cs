namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Movimientos_Volteo
    {
        [Key]
        public int Codigo_Volteo { get; set; }

        public int Responsable_Volteo { get; set; }

        public int Usuario { get; set; }

        public DateTime Fecha_Volteo { get; set; }

        public DateTime Fecha_Carga { get; set; }

        [Required]
        [StringLength(13)]
        public string Lote { get; set; }

        public string Comentario { get; set; }
    }
}
