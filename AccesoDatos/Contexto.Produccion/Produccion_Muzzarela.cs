namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Produccion_Muzzarela
    {
        [Key]
        public int Codigo_Interno_Produccion { get; set; }
        public decimal? Kilos_Teoricos { get; set; }
        public decimal? Kilos_Reales { get; set; }
        public decimal? Ph_Final { get; set; }
        public decimal? Ph_Masa { get; set; }
        public decimal? Rendimiento_Masa { get; set; }
        public decimal? Crema_Obtenida_Masa { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Elaboracion { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Vencimiento { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        public DateTime? Fecha_Elaboracion_Masa { get; set; }
        public int? Piezas_Obtenidas { get; set; }
        public decimal? Kilos_Masa { get; set; }
        public TimeSpan? Hora_Inicio { get; set; }
        public TimeSpan? Hora_Fin { get; set; }
        public TimeSpan? Hora_Inicio_Descarga { get; set; }
        public TimeSpan? Hora_Fin_Descarga { get; set; }
        public decimal? Unidades_Masa { get; set; }
        public decimal? Ph_Mezcla { get; set; }
        public decimal? Temp_Hilado { get; set; }
        public decimal? Perdidas { get; set; }
        public decimal? Humedad { get; set; }
        public virtual Produccion Produccion { get; set; }
    }
}
