﻿namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class dbProd : DbContext
    {
        public dbProd()
            : base("name=dbProd")
        {
            Database.CommandTimeout = 60;
        }

        public virtual DbSet<Acidificacion> Acidificacion { get; set; }
        public virtual DbSet<Ajustes_Insumos> Ajustes_Insumos { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Cadena_Conexion> Cadena_Conexion { get; set; }
        public virtual DbSet<Camaras> Camaras { get; set; }
        public virtual DbSet<Cambio_Estado> Cambio_Estado { get; set; }
        public virtual DbSet<CompraInsumos> CompraInsumos { get; set; }
        public virtual DbSet<Costos_Indirectos> Costos_Indirectos { get; set; }
        public virtual DbSet<Costos_Indirectos_Formula> Costos_Indirectos_Formula { get; set; }
        public virtual DbSet<Costos_Indirectos_Formula_Envasado> Costos_Indirectos_Formula_Envasado { get; set; }
        public virtual DbSet<Costos_Indirectos_Formula_Lavados> Costos_Indirectos_Formula_Lavados { get; set; }
        public virtual DbSet<Costos_Indirectos_Produccion> Costos_Indirectos_Produccion { get; set; }
        public virtual DbSet<Datos_Empresa> Datos_Empresa { get; set; }
        public virtual DbSet<Detalle_Ajuste_Insumos> Detalle_Ajuste_Insumos { get; set; }
        public virtual DbSet<Detalle_Compra> Detalle_Compra { get; set; }
        public virtual DbSet<Detalle_Envasado> Detalle_Envasado { get; set; }
        public virtual DbSet<Detalle_Expedicion> Detalle_Expedicion { get; set; }
        public virtual DbSet<Detalle_Formula> Detalle_Formula { get; set; }
        public virtual DbSet<Detalle_Formula_Envasado> Detalle_Formula_Envasado { get; set; }
        public virtual DbSet<Detalle_Formula_Lavados> Detalle_Formula_Lavados { get; set; }
        public virtual DbSet<Detalle_Imputacion> Detalle_Imputacion { get; set; }
        public virtual DbSet<Detalle_Ingreso> Detalle_Ingreso { get; set; }
        public virtual DbSet<Detalle_Insumo_Envasado> Detalle_Insumo_Envasado { get; set; }
        public virtual DbSet<Detalle_Limpieza> Detalle_Limpieza { get; set; }
        public virtual DbSet<Detalle_Orden_Compra> Detalle_Orden_Compra { get; set; }
        public virtual DbSet<Detalle_Produccion> Detalle_Produccion { get; set; }
        public virtual DbSet<Detalle_Salida_Productos> Detalle_Salida_Productos { get; set; }
        public virtual DbSet<Detalle_Traspaso_Fabricas> Detalle_Traspaso_Fabricas { get; set; }
        public virtual DbSet<Envasado> Envasado { get; set; }
        public virtual DbSet<Estados> Estados { get; set; }
        public virtual DbSet<Expedicion> Expedicion { get; set; }
        public virtual DbSet<Fabricas> Fabricas { get; set; }
        public virtual DbSet<Familias> Familias { get; set; }
        public virtual DbSet<Formulas> Formulas { get; set; }
        public virtual DbSet<Formulas_Envasado> Formulas_Envasado { get; set; }
        public virtual DbSet<Ingreso_Leche> Ingreso_Leche { get; set; }
        public virtual DbSet<Insumos> Insumos { get; set; }
        public virtual DbSet<Insumos_Formulas_Recuperado> Insumos_Formulas_Recuperado { get; set; }
        public virtual DbSet<Lavados> Lavados { get; set; }
        public virtual DbSet<Lavados_Formula> Lavados_Formula { get; set; }
        public virtual DbSet<Limpieza> Limpieza { get; set; }
        public virtual DbSet<Lineas> Lineas { get; set; }
        public virtual DbSet<Monedas> Monedas { get; set; }
        public virtual DbSet<Motivos_Retencion> Motivos_Retencion { get; set; }
        public virtual DbSet<Mov_Ins_Asoc> Mov_Ins_Asoc { get; set; }
        public virtual DbSet<Mov_Stock> Mov_Stock { get; set; }
        public virtual DbSet<Mov_Stock_Prod_Ventas> Mov_Stock_Prod_Ventas { get; set; }
        public virtual DbSet<Movimientos_Volteo> Movimientos_Volteo { get; set; }
        public virtual DbSet<OpcionesTrabajo> OpcionesTrabajo { get; set; }
        public virtual DbSet<Orden_Compra> Orden_Compra { get; set; }
        public virtual DbSet<Permisos> Permisos { get; set; }
        public virtual DbSet<Precios_Historicos> Precios_Historicos { get; set; }
        public virtual DbSet<Produccion> Produccion { get; set; }
        public virtual DbSet<Produccion_Crema> Produccion_Crema { get; set; }
        public virtual DbSet<Produccion_Dulce_de_Leche> Produccion_Dulce_de_Leche { get; set; }
        public virtual DbSet<Produccion_Leche> Produccion_Leche { get; set; }
        public virtual DbSet<Produccion_Muzzarela> Produccion_Muzzarela { get; set; }
        public virtual DbSet<Produccion_Quesos> Produccion_Quesos { get; set; }
        public virtual DbSet<Produccion_Ricota> Produccion_Ricota { get; set; }
        public virtual DbSet<Produccion_Yogur> Produccion_Yogur { get; set; }
        public virtual DbSet<Productos> Productos { get; set; }
        public virtual DbSet<Productos_LaHuerta> Productos_LaHuerta { get; set; }
        public virtual DbSet<Queseros> Queseros { get; set; }
        public virtual DbSet<Rubros> Rubros { get; set; }
        public virtual DbSet<Salida_Productos> Salida_Productos { get; set; }
        public virtual DbSet<Sesiones> Sesiones { get; set; }
        public virtual DbSet<Silos> Silos { get; set; }
        public virtual DbSet<Tinas> Tinas { get; set; }
        public virtual DbSet<Traspaso_Fabricas> Traspaso_Fabricas { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<Detalle_Resultado_Analisis> Detalle_Resultado_Analisis { get; set; }
        public virtual DbSet<Resultado_Analisis_Microbiologico> Resultado_Analisis_Microbiologico { get; set; }
        public virtual DbSet<Analisis_Microbiologico> Analisis_Microbiologico { get; set; }
        public virtual DbSet<Rango_Analisis_Microbiologico> Rango_Analisis_Microbiologico { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Acidificacion>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Acidificacion>()
                .Property(e => e.Valor_Ph)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Acidificacion>()
                .Property(e => e.Valor_Acidez)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Ajustes_Insumos>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Ajustes_Insumos>()
                .Property(e => e.Tipo_Movimiento)
                .IsUnicode(false);

            modelBuilder.Entity<Ajustes_Insumos>()
                .Property(e => e.Numero_Comprobante)
                .IsUnicode(false);

            modelBuilder.Entity<Ajustes_Insumos>()
                .Property(e => e.Descuento)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Ajustes_Insumos>()
                .Property(e => e.Estado)
                .IsUnicode(false);

            modelBuilder.Entity<Ajustes_Insumos>()
                .Property(e => e.Motivo_Rechazo)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Cadena_Conexion>()
                .Property(e => e.Servidor)
                .IsUnicode(false);

            modelBuilder.Entity<Cadena_Conexion>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Cadena_Conexion>()
                .Property(e => e.Contraseña)
                .IsUnicode(false);

            modelBuilder.Entity<Cadena_Conexion>()
                .Property(e => e.Tabla)
                .IsUnicode(false);

            modelBuilder.Entity<Camaras>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Cambio_Estado>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Tipo_Movimiento)
                .IsUnicode(false);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Tipo_Factura)
                .IsUnicode(false);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Codigo_Comprobante)
                .IsUnicode(false);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Total)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Imp_Varios)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Excento)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Percepcion_IB)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Percepcion_IVA)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Total_Descuento)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Neto_Gravado)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.IVA_21)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.IVA_10)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Descuento)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Precio_Dolar)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Precio_Euros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<CompraInsumos>()
                .Property(e => e.Nro_Remito)
                .IsUnicode(false);

            modelBuilder.Entity<CompraInsumos>()
                .HasMany(e => e.Detalle_Compra)
                .WithOptional(e => e.CompraInsumos)
                .WillCascadeOnDelete();

            modelBuilder.Entity<CompraInsumos>()
                .HasMany(e => e.Detalle_Imputacion)
                .WithOptional(e => e.CompraInsumos)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Costos_Indirectos>()
                .Property(e => e.Codigo_Costo_Indirecto)
                .IsUnicode(false);

            modelBuilder.Entity<Costos_Indirectos>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Costos_Indirectos>()
                .Property(e => e.Porcentaje)
                .HasPrecision(8, 2);

            modelBuilder.Entity<Costos_Indirectos>()
                .Property(e => e.Precio)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Costos_Indirectos>()
                .Property(e => e.Precio_Fijo)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Costos_Indirectos>()
                .Property(e => e.Porcentaje_Aplica)
                .IsUnicode(false);

            modelBuilder.Entity<Costos_Indirectos>()
                .HasMany(e => e.Costos_Indirectos_Formula)
                .WithRequired(e => e.Costos_Indirectos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Costos_Indirectos_Formula>()
                .Property(e => e.Codigo_Costo_Indirecto)
                .IsUnicode(false);

            modelBuilder.Entity<Costos_Indirectos_Formula>()
                .Property(e => e.Precio)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Costos_Indirectos_Formula>()
                .Property(e => e.Porcentaje)
                .HasPrecision(8, 2);

            modelBuilder.Entity<Costos_Indirectos_Formula_Envasado>()
                .Property(e => e.Codigo_Costo_Indirecto)
                .IsUnicode(false);

            modelBuilder.Entity<Costos_Indirectos_Formula_Envasado>()
                .Property(e => e.Precio)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Costos_Indirectos_Formula_Envasado>()
                .Property(e => e.Porcentaje)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Costos_Indirectos_Formula_Lavados>()
                .Property(e => e.Codigo_Costo_Indirecto)
                .IsUnicode(false);

            modelBuilder.Entity<Costos_Indirectos_Formula_Lavados>()
                .Property(e => e.Precio)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Costos_Indirectos_Formula_Lavados>()
                .Property(e => e.Porcentaje)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Costos_Indirectos_Produccion>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Costos_Indirectos_Produccion>()
                .Property(e => e.Codigo_Costo_Indirecto)
                .IsUnicode(false);

            modelBuilder.Entity<Costos_Indirectos_Produccion>()
                .Property(e => e.Precio)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Costos_Indirectos_Produccion>()
                .Property(e => e.Porcentaje)
                .HasPrecision(8, 2);

            modelBuilder.Entity<Datos_Empresa>()
                .Property(e => e.CUIT)
                .IsUnicode(false);

            modelBuilder.Entity<Datos_Empresa>()
                .Property(e => e.Razon_Social)
                .IsUnicode(false);

            modelBuilder.Entity<Datos_Empresa>()
                .Property(e => e.Imagen)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Ajuste_Insumos>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Ajuste_Insumos>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Ajuste_Insumos>()
                .Property(e => e.Kilos_Litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Ajuste_Insumos>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Kilos_Litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Sin_Cargo_Kilos_Litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Precio_Costo)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Precio_Lista)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Codigo_Imputacion)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Importe)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Descuento)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Sin_Cargo_Cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Compra>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Envasado>()
                .Property(e => e.Kilos)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Detalle_Envasado>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Envasado>()
                .Property(e => e.Descarte)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Detalle_Envasado>()
                .Property(e => e.Hormas)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Detalle_Envasado>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Detalle_Envasado>()
                .Property(e => e.Codigo_Insumo_Reproceso)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Envasado>()
                .Property(e => e.Comentario_Retencion)
                .IsUnicode(false);
            modelBuilder.Entity<Detalle_Envasado>()
               .Property(e => e.Comentario_Decomiso)
               .IsUnicode(false);

            modelBuilder.Entity<Detalle_Envasado>()
                .Property(e => e.Kilos_Desnudo)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Detalle_Expedicion>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Expedicion>()
                .Property(e => e.Numero_Producto_Ventas)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Expedicion>()
                .Property(e => e.Kilos)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Detalle_Traspaso_Fabricas>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Traspaso_Fabricas>()
                .Property(e => e.Numero_Producto_Ventas)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Traspaso_Fabricas>()
                .Property(e => e.Kilos)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Detalle_Formula>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Formula>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Detalle_Formula>()
                .Property(e => e.Costo)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Detalle_Formula>()
                .Property(e => e.Precio_Unitario)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Detalle_Formula_Envasado>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Formula_Envasado>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Formula_Envasado>()
                .Property(e => e.Ajuste)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Formula_Lavados>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Formula_Lavados>()
                .Property(e => e.cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Formula_Lavados>()
                .Property(e => e.kilos_litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Imputacion>()
                .Property(e => e.Codigo_Imputacion)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Imputacion>()
                .Property(e => e.Importe)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Ingreso>()
                .Property(e => e.Litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Ingreso>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Ingreso>()
                .Property(e => e.Acidez)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Detalle_Ingreso>()
                .Property(e => e.Ph)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Ingreso>()
                .Property(e => e.Densidad)
                .HasPrecision(10, 4);

            modelBuilder.Entity<Detalle_Ingreso>()
                .Property(e => e.Grasa)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Detalle_Ingreso>()
                .Property(e => e.Proteina)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Detalle_Ingreso>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Insumo_Envasado>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Insumo_Envasado>()
                .Property(e => e.Kilos)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Insumo_Envasado>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Limpieza>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Limpieza>()
                .Property(e => e.cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Limpieza>()
                .Property(e => e.kilos_litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Limpieza>()
                .Property(e => e.lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Limpieza>()
                .Property(e => e.ajuste)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Kilos_Litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Sin_Cargo_Kilos_Litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Precio_Costo)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Precio_Lista)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Descuento)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Importe)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Recibidos)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Faltantes)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Orden_Compra>()
                .Property(e => e.Sin_Cargo_Cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Produccion>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Produccion>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Produccion>()
                .Property(e => e.Precio_Unitario)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Detalle_Produccion>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Detalle_Produccion>()
                .Property(e => e.Movimiento)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Produccion>()
                .Property(e => e.Ajuste)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Detalle_Produccion>()
                .Property(e => e.Lote_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Salida_Productos>()
                .Property(e => e.Codigo_Producto)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Salida_Productos>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Salida_Productos>()
                .Property(e => e.Kilos_Litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Detalle_Salida_Productos>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Salida_Productos>()
                .Property(e => e.tipo)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle_Salida_Productos>()
                .Property(e => e.Codigo_Insumo_Reproceso)
                .IsUnicode(false);

            modelBuilder.Entity<Envasado>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Envasado>()
                .Property(e => e.Humedad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Envasado>()
                .Property(e => e.Kg_Descarte)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Envasado>()
                .Property(e => e.Hormas_Descarte)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Envasado>()
                .Property(e => e.Destino_Descarte)
                .IsUnicode(false);

            modelBuilder.Entity<Envasado>()
                .Property(e => e.Codigo_Insumo_Reproceso)
                .IsUnicode(false);

            modelBuilder.Entity<Envasado>()
                .HasMany(e => e.Detalle_Insumo_Envasado)
                .WithOptional(e => e.Envasado)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Envasado>()
                .HasMany(e => e.Produccion)
                .WithMany(e => e.Envasado)
                .Map(m => m.ToTable("EnvasadoCompuesto").MapLeftKey("Codigo_Interno_Envasado").MapRightKey("Lote"));

            modelBuilder.Entity<Restantes_Envasado>()
                .Property(e => e.Hormas)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Restantes_Envasado>()
                .Property(e => e.Kilos)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Estados>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Estados>()
                .HasMany(e => e.Cambio_Estado)
                .WithRequired(e => e.Estados)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Expedicion>()
                .Property(e => e.Egreso_Ingreso)
                .IsUnicode(false);

            modelBuilder.Entity<Expedicion>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Expedicion>()
                .Property(e => e.Numero_Comprobante)
                .IsUnicode(false);

            modelBuilder.Entity<Expedicion>()
                .Property(e => e.Tipo_Comprobante)
                .IsUnicode(false);

            modelBuilder.Entity<Expedicion>()
                .Property(e => e.Prefijo)
                .IsUnicode(false);

            modelBuilder.Entity<Expedicion>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Expedicion>()
                .Property(e => e.Motivo_Rechazo)
                .IsUnicode(false);

            modelBuilder.Entity<Expedicion>()
                .HasMany(e => e.Detalle_Expedicion)
                .WithOptional(e => e.Expedicion)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Traspaso_Fabricas>()
                .Property(e => e.Egreso_Ingreso)
                .IsUnicode(false);

            modelBuilder.Entity<Traspaso_Fabricas>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Traspaso_Fabricas>()
                .Property(e => e.Numero_Comprobante)
                .IsUnicode(false);

            modelBuilder.Entity<Traspaso_Fabricas>()
                .Property(e => e.Prefijo)
                .IsUnicode(false);

            modelBuilder.Entity<Traspaso_Fabricas>()
                .Property(e => e.Motivo_Rechazo)
                .IsUnicode(false);

            modelBuilder.Entity<Traspaso_Fabricas>()
                .HasMany(e => e.Detalle_Traspaso)
                .WithOptional(e => e.Traspaso_Fabricas)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Fabricas>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Fabricas>()
                .Property(e => e.Nro_Planilla)
                .IsUnicode(false);

            modelBuilder.Entity<Familias>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Familias>()
                .HasMany(e => e.Productos)
                .WithOptional(e => e.Familias)
                .HasForeignKey(e => e.Codigo_Familia);

            modelBuilder.Entity<Familias>()
                .HasMany(e => e.Productos1)
                .WithOptional(e => e.Familias1)
                .HasForeignKey(e => e.Codigo_Familia);

            modelBuilder.Entity<Formulas>()
                .Property(e => e.Codigo_Producto)
                .IsUnicode(false);

            modelBuilder.Entity<Formulas>()
                .Property(e => e.Factor)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Formulas>()
                .Property(e => e.Kilos_Teoricos)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Formulas>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Formulas>()
                .Property(e => e.Kilos_Masa)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Formulas>()
                .Property(e => e.Litros_Suero)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Formulas>()
                .Property(e => e.Litros_Crema)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Formulas>()
                .HasMany(e => e.Insumos_Formulas_Recuperado)
                .WithRequired(e => e.Formulas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Formulas>()
                .HasMany(e => e.Produccion)
                .WithOptional(e => e.Formulas)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Formulas>()
                .HasMany(e => e.Insumos)
                .WithMany(e => e.Formulas)
                .Map(m => m.ToTable("Insumos_Formula").MapLeftKey("Codigo_Formula").MapRightKey("Codigo_Insumo"));

            modelBuilder.Entity<Formulas_Envasado>()
                .Property(e => e.Codigo_Producto)
                .IsUnicode(false);

            modelBuilder.Entity<Formulas_Envasado>()
                .Property(e => e.Numero_Producto_Ventas)
                .IsUnicode(false);

            modelBuilder.Entity<Formulas_Envasado>()
                .Property(e => e.Unidad_Medida)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Ingreso_Leche>()
                .Property(e => e.Codigo_Comprobante)
                .IsUnicode(false);

            modelBuilder.Entity<Ingreso_Leche>()
                .Property(e => e.Estado)
                .IsUnicode(false);

            modelBuilder.Entity<Ingreso_Leche>()
                .Property(e => e.Tipo_Factura)
                .IsUnicode(false);

            modelBuilder.Entity<Ingreso_Leche>()
                .HasMany(e => e.Detalle_Ingreso)
                .WithOptional(e => e.Ingreso_Leche)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Codigo_Insumo_Proveedor)
                .IsUnicode(false);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Descripcion_Larga)
                .IsUnicode(false);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Codigo_Imputacion)
                .IsUnicode(false);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Unidad_Medida)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Precio_Ultima_Compra)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Precio_Lista)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Punto_Pedido)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Precio_Por)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Insumos>()
                .Property(e => e.Moneda)
                .IsUnicode(false);

            modelBuilder.Entity<Insumos>()
                .HasMany(e => e.Detalle_Envasado)
                .WithOptional(e => e.Insumos)
                .HasForeignKey(e => e.Codigo_Insumo_Reproceso);

            modelBuilder.Entity<Insumos>()
                .HasMany(e => e.Detalle_Formula_Envasado)
                .WithRequired(e => e.Insumos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Insumos>()
                .HasMany(e => e.Envasado)
                .WithOptional(e => e.Insumos)
                .HasForeignKey(e => e.Codigo_Insumo_Reproceso);

            modelBuilder.Entity<Insumos>()
                .HasMany(e => e.Insumos_Formulas_Recuperado)
                .WithRequired(e => e.Insumos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Insumos>()
                .HasMany(e => e.Mov_Ins_Asoc)
                .WithRequired(e => e.Insumos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Insumos>()
                .HasMany(e => e.Productos)
                .WithOptional(e => e.Insumos)
                .HasForeignKey(e => e.Codigo_Insumo);

            modelBuilder.Entity<Insumos>()
                .HasMany(e => e.Productos1)
                .WithOptional(e => e.Insumos1)
                .HasForeignKey(e => e.Codigo_Insumo);

            modelBuilder.Entity<Insumos>()
                .HasMany(e => e.Detalle_Ajuste_Insumos)
                .WithOptional(e => e.Insumos)
                .HasForeignKey(e => e.Codigo_Insumo);

            modelBuilder.Entity<Insumos>()
                .HasMany(e => e.Productos2)
                .WithMany(e => e.Insumos2)
                .Map(m => m.ToTable("Producto_Insumos").MapLeftKey("Codigo_Insumo").MapRightKey("Codigo_Producto"));

            modelBuilder.Entity<Insumos_Formulas_Recuperado>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Insumos_Formulas_Recuperado>()
                .Property(e => e.CantidadRecuperada)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Lavados>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Lavados>()
                .HasMany(e => e.Lavados_Formula)
                .WithRequired(e => e.Lavados)
                .HasForeignKey(e => e.Codigo_Lavado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lavados_Formula>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Lavados_Formula>()
                .HasMany(e => e.Costos_Indirectos_Formula_Lavados)
                .WithRequired(e => e.Lavados_Formula)
                .HasForeignKey(e => e.Codigo_Formula_lavado);

            modelBuilder.Entity<Lavados_Formula>()
                .HasMany(e => e.Detalle_Formula_Lavados)
                .WithRequired(e => e.Lavados_Formula)
                .HasForeignKey(e => e.Codigo_Formula_Lavado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lavados_Formula>()
                .HasMany(e => e.Limpieza)
                .WithRequired(e => e.Lavados_Formula)
                .HasForeignKey(e => e.Codigo_Formula_Lavado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Limpieza>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Limpieza>()
                .HasMany(e => e.Detalle_Limpieza)
                .WithRequired(e => e.Limpieza)
                .HasForeignKey(e => e.Codigo_Limpieza)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lineas>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Monedas>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Monedas>()
                .HasMany(e => e.Insumos)
                .WithOptional(e => e.Monedas)
                .HasForeignKey(e => e.Moneda);

            modelBuilder.Entity<Motivos_Retencion>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Motivos_Retencion>()
                .Property(e => e.Tipo)
                .IsUnicode(false);

            modelBuilder.Entity<Mov_Ins_Asoc>()
                .Property(e => e.Litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Mov_Ins_Asoc>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Mov_Ins_Asoc>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Mov_Stock>()
                .Property(e => e.Codigo_Prod)
                .IsUnicode(false);

            modelBuilder.Entity<Mov_Stock>()
                .Property(e => e.Tipo_Mov)
                .IsUnicode(false);

            modelBuilder.Entity<Mov_Stock>()
                .Property(e => e.Estado)
                .IsUnicode(false);

            modelBuilder.Entity<Mov_Stock>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Mov_Stock>()
                .Property(e => e.Kilos)
                .HasPrecision(14, 3);

            modelBuilder.Entity<Mov_Stock>()
                .Property(e => e.Nro_Comprobante)
                .IsUnicode(false);

            modelBuilder.Entity<Mov_Stock>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Movimientos_Volteo>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Movimientos_Volteo>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<OpcionesTrabajo>()
                .Property(e => e.Fabrica_Deposito)
                .IsUnicode(false);

            modelBuilder.Entity<OpcionesTrabajo>()
                .Property(e => e.Ruta_EXCELS)
                .IsUnicode(false);

            modelBuilder.Entity<Orden_Compra>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Orden_Compra>()
                .Property(e => e.Lugar_Entrega)
                .IsUnicode(false);

            modelBuilder.Entity<Orden_Compra>()
                .Property(e => e.Plazo_Entrega)
                .IsUnicode(false);

            modelBuilder.Entity<Orden_Compra>()
                .Property(e => e.Descripcion_Condicion_Pago)
                .IsUnicode(false);

            modelBuilder.Entity<Orden_Compra>()
                .Property(e => e.Total)
                .HasPrecision(12, 4);

            modelBuilder.Entity<Orden_Compra>()
                .Property(e => e.Precio_Dolar)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Orden_Compra>()
                .Property(e => e.Descuento)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Orden_Compra>()
                .Property(e => e.IVA)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Orden_Compra>()
                .Property(e => e.Estado)
                .IsUnicode(false);

            modelBuilder.Entity<Precios_Historicos>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Precios_Historicos>()
                .Property(e => e.Precio)
                .HasPrecision(12, 2);


            modelBuilder.Entity<Produccion_Leche_Polvo>()
                .Property(e => e.Piezas_Obtenidas)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Leche_Polvo>()
                .Property(e => e.Kilos_Teoricos)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Leche_Polvo>()
                .Property(e => e.Kilos_Reales)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Dens_Leche_Liquida)
               .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Proteinas_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Graso_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Materia_Grasa_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Cenizas_Totales_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Humedad_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Acidez_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Acidez_1_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Ph_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Indice_Insolubilidad_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Particulas_Quemadas_Leche_Polvo)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Bacterias_Aerobias_Mesofilas_Totales)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Coliformes_Totales)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Coliformes_Fecales)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Escherichia_Coli)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.salmonella)
               .HasPrecision(12, 3);
            modelBuilder.Entity<Produccion_Leche_Polvo>()
               .Property(e => e.Staphylococcus_aureus)
               .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion>()
                .Property(e => e.Nro_Planilla)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion>()
                .Property(e => e.motivo_decomiso)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion>()
                .Property(e => e.Lote_Padre)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Detalle_Produccion)
                .WithOptional(e => e.Produccion)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Produccion_Crema)
                .WithRequired(e => e.Produccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Produccion_Quesos_Fundido)
                .WithRequired(e => e.Produccion)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Produccion_Leche_Polvo)
                .WithRequired(e => e.Produccion)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Produccion_Dulce_de_Leche)
                .WithRequired(e => e.Produccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Produccion_Leche)
                .WithRequired(e => e.Produccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Produccion_Muzzarela)
                .WithOptional(e => e.Produccion)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Produccion_Quesos)
                .WithOptional(e => e.Produccion)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Produccion_Ricota)
                .WithRequired(e => e.Produccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produccion>()
                .HasMany(e => e.Produccion_Yogur)
                .WithRequired(e => e.Produccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produccion_Crema>()
                .Property(e => e.Materia_Grasa)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Crema>()
                .Property(e => e.Acidez)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Produccion_Crema>()
                .Property(e => e.Ph)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Produccion_Crema>()
                .Property(e => e.Litros_Teoricos)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Crema>()
                .Property(e => e.Litros_Reales)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Crema>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion_Crema>()
                .Property(e => e.Litros_Crema)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Dulce_de_Leche>()
                .Property(e => e.Kilos_Teoricos)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Dulce_de_Leche>()
                .Property(e => e.Kilos_Reales)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Dulce_de_Leche>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion_Dulce_de_Leche>()
                .Property(e => e.Acidez)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Produccion_Dulce_de_Leche>()
                .Property(e => e.Ph)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Dulce_de_Leche>()
                .Property(e => e.Temperatura)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Dulce_de_Leche>()
                .Property(e => e.Densidad)
                .HasPrecision(10, 4);

            modelBuilder.Entity<Produccion_Dulce_de_Leche>()
                .Property(e => e.Litros)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Leche>()
                .Property(e => e.Litros_Teoricos)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Leche>()
                .Property(e => e.Litros_Reales)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Leche>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion_Muzzarela>()
                .Property(e => e.Kilos_Teoricos)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Muzzarela>()
                .Property(e => e.Kilos_Reales)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Muzzarela>()
                .Property(e => e.Ph_Final)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Produccion_Muzzarela>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion_Muzzarela>()
                .Property(e => e.Kilos_Masa)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Quesos_Fundido>()
                .Property(e => e.Piezas_Obtenidas)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Quesos_Fundido>()
                .Property(e => e.Kilos_Teoricos)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Quesos_Fundido>()
                .Property(e => e.Kilos_Reales)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Factor)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Piezas_Obtenidas)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Kilos_Teoricos)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Kilos_Reales)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Grasa)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Proteina)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Grasa_Suero)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Grasa_Silo_1)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Grasa_Silo_2)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Proteina_Silo_1)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Proteina_Silo_2)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Humedad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Presion_Prensado)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Temperatura_Pausterizado)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Kilos_Envasado)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Kilos_Antes_Envasado)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Quesos>()
                .Property(e => e.Factor_Grasa_Proteina)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Produccion_Ricota>()
                .Property(e => e.Acidez_Inicial)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Produccion_Ricota>()
                .Property(e => e.Acidez_Final)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Produccion_Ricota>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Produccion_Ricota>()
                .Property(e => e.Litros_Suero)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Ricota>()
                .Property(e => e.Porcentaje_Solidos)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Ricota>()
                .Property(e => e.Factor_Concentracion)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Ricota>()
                .Property(e => e.Temperatura_Corte)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Ricota>()
                .Property(e => e.Kilos_Teoricos)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Ricota>()
                .Property(e => e.Kilos_Reales)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Produccion_Yogur>()
                .Property(e => e.Ph)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Produccion_Yogur>()
                .Property(e => e.Lote)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Codigo_Producto)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Relacion_Teorica)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Que_Usa)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Tolerancia)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Productos>()
                .HasMany(e => e.Formulas)
                .WithRequired(e => e.Productos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Productos>()
                .HasMany(e => e.Formulas_Envasado)
                .WithRequired(e => e.Productos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Productos_LaHuerta>()
                .Property(e => e.Codigo_Producto)
                .IsUnicode(false);

            modelBuilder.Entity<Productos_LaHuerta>()
                .Property(e => e.Codigo_Insumo)
                .IsUnicode(false);

            modelBuilder.Entity<Productos_LaHuerta>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Productos_LaHuerta>()
                .Property(e => e.Relacion_Teorica)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Productos_LaHuerta>()
                .Property(e => e.Que_Usa)
                .IsUnicode(false);

            modelBuilder.Entity<Productos_LaHuerta>()
                .Property(e => e.Tolerancia)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Queseros>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Rubros>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Salida_Productos>()
                .HasMany(e => e.Detalle_Salida_Productos)
                .WithOptional(e => e.Salida_Productos)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Silos>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Contraseña)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Rol)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Ajustes_Insumos)
                .WithOptional(e => e.Usuarios)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Cambio_Estado)
                .WithOptional(e => e.Usuarios)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Resultado_Analisis_Microbiologico>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<Resultado_Analisis_Microbiologico>()
               .Property(e => e.Lote)
               .IsUnicode(false);

            modelBuilder.Entity<Detalle_Resultado_Analisis>()
            .Property(e => e.Resultado_Analisis)
            .HasPrecision(7, 3);

            modelBuilder.Entity<Analisis_Microbiologico>()
            .Property(e => e.Descripcion)
            .IsUnicode(false);

            modelBuilder.Entity<Rango_Analisis_Microbiologico>()
           .Property(e => e.Desde)
           .HasPrecision(10, 3);
            
            modelBuilder.Entity<Rango_Analisis_Microbiologico>()
           .Property(e => e.Hasta)
           .HasPrecision(10, 3);

        }
    }
}
