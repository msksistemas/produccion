namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Formula
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Formula { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Formula { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public decimal Cantidad { get; set; }

        public decimal Costo { get; set; }

        public decimal Precio_Unitario { get; set; }

        public bool Marca { get; set; }

        public virtual Formulas Formulas { get; set; }

        public virtual Insumos Insumos { get; set; }
    }
}
