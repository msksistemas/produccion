namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Expedicion
    {
        [Key]
        public int Codigo_Interno_Detalle { get; set; }

        public int? Codigo_Interno_Expedicion { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        [StringLength(13)]
        public string Numero_Producto_Ventas { get; set; }

        public decimal? Kilos { get; set; }

        public int? Cantidad { get; set; }

        public int? Tipo { get; set; }

        public virtual Expedicion Expedicion { get; set; }
    }
}
