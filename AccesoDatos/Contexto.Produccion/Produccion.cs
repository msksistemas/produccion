namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Produccion")]
    public partial class Produccion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Produccion()
        {
            Acidificacion = new HashSet<Acidificacion>();
            Cambio_Estado = new HashSet<Cambio_Estado>();
            Costos_Indirectos_Produccion = new HashSet<Costos_Indirectos_Produccion>();
            Detalle_Produccion = new HashSet<Detalle_Produccion>();
            Mov_Ins_Asoc = new HashSet<Mov_Ins_Asoc>();
            Produccion_Crema = new HashSet<Produccion_Crema>();
            Produccion_Dulce_de_Leche = new HashSet<Produccion_Dulce_de_Leche>();
            Produccion_Leche = new HashSet<Produccion_Leche>();
            Produccion_Muzzarela = new HashSet<Produccion_Muzzarela>();
            Produccion_Quesos = new HashSet<Produccion_Quesos>();
            Produccion_Quesos_Fundido = new HashSet<Produccion_Quesos_Fundido>();
            Produccion_Leche_Polvo = new HashSet<Produccion_Leche_Polvo>();
            Produccion_Ricota = new HashSet<Produccion_Ricota>();
            Produccion_Yogur = new HashSet<Produccion_Yogur>();
            Envasado = new HashSet<Envasado>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Produccion { get; set; }

        [Key]
        [StringLength(13)]
        public string Lote { get; set; }

        public int? Codigo_Formula { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        public bool? Historico { get; set; }

        public int? Codigo_Fabrica { get; set; }

        [StringLength(12)]
        public string Nro_Planilla { get; set; }

        public int? Codigo_Quesero { get; set; }

        public bool? Expedida { get; set; }

        public byte? Decomisado { get; set; }

        [StringLength(250)]
        public string motivo_decomiso { get; set; }

        [StringLength(13)]
        public string Lote_Padre { get; set; }
        
        public string Observacion { get; set; }

        public int? idUsuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Acidificacion> Acidificacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cambio_Estado> Cambio_Estado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Produccion> Costos_Indirectos_Produccion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Produccion> Detalle_Produccion { get; set; }

        public virtual Fabricas Fabricas { get; set; }

        public virtual Formulas Formulas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Mov_Ins_Asoc> Mov_Ins_Asoc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Crema> Produccion_Crema { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Dulce_de_Leche> Produccion_Dulce_de_Leche { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Leche> Produccion_Leche { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Muzzarela> Produccion_Muzzarela { get; set; }

        public virtual Queseros Queseros { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Quesos> Produccion_Quesos { get; set; }

        public virtual ICollection<Produccion_Quesos_Fundido> Produccion_Quesos_Fundido { get; set; }

        public virtual ICollection<Produccion_Leche_Polvo> Produccion_Leche_Polvo { get; set; }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Ricota> Produccion_Ricota { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Yogur> Produccion_Yogur { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Envasado> Envasado { get; set; }
    }
}
