namespace AccesoDatos.Contexto.Produccion
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Costos_Indirectos_Formula_Lavados
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Costo_Indirecto_Interno { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Formula_lavado { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(13)]
        public string Codigo_Costo_Indirecto { get; set; }

        public decimal? Precio { get; set; }

        public int? Cantidad { get; set; }

        public bool? Precio_Fijo { get; set; }

        public decimal? Porcentaje { get; set; }

        public bool? Marca { get; set; }

        public virtual Lavados_Formula Lavados_Formula { get; set; }
        public virtual Costos_Indirectos Costos_Indirectos { get; set; }
    }
}
