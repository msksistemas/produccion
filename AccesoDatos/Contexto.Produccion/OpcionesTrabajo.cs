namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OpcionesTrabajo")]
    public partial class OpcionesTrabajo
    {
        [Key]
        public int Codigo_Interno_Opcion { get; set; }

        public bool ImputarCompras { get; set; }

        public bool ActualizarCostoInsumos { get; set; }

        public bool NroPlanillaDigitable { get; set; }

        public int NumeroDigitos { get; set; }

        public bool FabricaPredefinida { get; set; }

        public bool Usa_Stock_Camara { get; set; }

        [Required]
        [StringLength(50)]
        public string Fabrica_Deposito { get; set; }

        public bool Utiliza_Ventas { get; set; }

        public bool Utiliza_Login { get; set; }

        [Required]
        [StringLength(100)]
        public string Ruta_EXCELS { get; set; }

        public bool Envasa_Sin_Kilos { get; set; }

        
        public bool Imputar_Total_O_Neto { get; set; }

        public bool MostrarIVAFormulas { get; set; }

        public int Cantidad_Usuarios { get; set; }

        public int Cantidad_Maxima_Usuarios { get; set; }

        public bool Kg_Desnudo { get; set; }

        public bool Requiere_Rol_Administrador { get; set; }

        public bool UtilizaBandejas { get; set; }

        public bool Stock_Venta_Blanco { get; set; }
    }
}
