namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CompraInsumos
    {
        public CompraInsumos()
        {
            Detalle_Compra = new HashSet<Detalle_Compra>();
            Detalle_Imputacion = new HashSet<Detalle_Imputacion>();
        }

        [Key]
        public int Codigo_Interno_Compra { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        [StringLength(30)]
        public string Tipo_Movimiento { get; set; }

        [StringLength(10)]
        public string Tipo_Factura { get; set; }

        public int? Prefijo { get; set; }

        public int? Numero_Comprobante { get; set; }

        public int? Codigo_Proveedor { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Periodo_Contable { get; set; }

        [StringLength(3)]
        public string Codigo_Comprobante { get; set; }

        public int? Codigo_Interno_Orden { get; set; }

        public decimal? Total { get; set; }

        public decimal? Imp_Varios { get; set; }

        public decimal? Excento { get; set; }

        public decimal? Percepcion_IB { get; set; }

        public decimal? Percepcion_IVA { get; set; }

        public decimal? Total_Descuento { get; set; }

        public decimal? Neto_Gravado { get; set; }

        public decimal? IVA_21 { get; set; }

        [StringLength(100)]
        public string Comentario { get; set; }

        public int? Condicion_Pago { get; set; }

        public bool? Historico { get; set; }

        public int? Codigo_Fabrica { get; set; }

        public decimal? IVA_10 { get; set; }
        public decimal? Neto_IVA_10 { get; set; }
        public decimal? Neto_IVA_21 { get; set; }


        public decimal? Descuento { get; set; }

        public decimal? Precio_Dolar { get; set; }

        public decimal? Precio_Euros { get; set; }

        [StringLength(30)]
        public string Nro_Remito { get; set; }

        public bool Suma_Stock { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Vencimiento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Compra> Detalle_Compra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Imputacion> Detalle_Imputacion { get; set; }
    }
}
