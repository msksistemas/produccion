namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Precios_Historicos
    {
        [Key]
        public int Codigo_Interno_Precio { get; set; }

        [Required]
        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        [Column(TypeName = "date")]
        public DateTime Fecha { get; set; }

        public decimal Precio { get; set; }

        public int? Codigo_Interno_Compra { get; set; }
    }
}
