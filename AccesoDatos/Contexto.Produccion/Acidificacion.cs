namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Acidificacion")]
    public partial class Acidificacion
    {
        [Key]
        public int Codigo_Interno_Acidificacion { get; set; }

        [Required]
        [StringLength(13)]
        public string Lote { get; set; }

        public decimal? Valor_Ph { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }

        public decimal? Valor_Acidez { get; set; }

        public TimeSpan? Hora { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
