namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Formula_Envasado
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Detalle { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Interno_Formula { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public decimal Cantidad { get; set; }

        public bool Marca { get; set; }

        public decimal? Ajuste { get; set; }

        public virtual Formulas_Envasado Formulas_Envasado { get; set; }

        public virtual Insumos Insumos { get; set; }
    }
}
