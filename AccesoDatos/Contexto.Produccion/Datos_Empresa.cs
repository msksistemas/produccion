namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Datos_Empresa
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string CUIT { get; set; }

        [StringLength(50)]
        public string Razon_Social { get; set; }

        public string Imagen { get; set; }
    }
}
