namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Produccion
    {
        [Key]
        public int Codigo_Interno_Detalle { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        [Required]
        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public decimal? Precio_Unitario { get; set; }

        public decimal? Cantidad { get; set; }

        public decimal? Cantidad_Unidades { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }

        [StringLength(100)]
        public string Movimiento { get; set; }

        public decimal? Ajuste { get; set; }

        [StringLength(50)]
        public string Lote_Insumo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha_Vencimiento { get; set; }
        public decimal? Ph { get; set; }
        public decimal? Humedad { get; set; }

        public decimal? Rendimiento { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
