namespace AccesoDatos.Contexto.Produccion
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    public partial class Monedas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Monedas()
        {
            Insumos = new HashSet<Insumos>();
        }

        [Key]
        [StringLength(100)]
        public string nombre { get; set; }

        public double intercambio { get; set; }

        public bool nacional { get; set; }

        public virtual ICollection<Insumos> Insumos { get; set; }
    }
}
