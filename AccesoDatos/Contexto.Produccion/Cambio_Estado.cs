namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Cambio_Estado
    {
        [Key]
        public int Codigo_Cambio_Interno { get; set; }

        [Required(ErrorMessage = "Campo requerido.")]
        public int Codigo_Estado { get; set; }

        [Required]
        [StringLength(13)]
        public string Lote { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        public int? idUsuario { get; set; }

        public int? Codigo_Camara { get; set; }

        public virtual Camaras Camaras { get; set; }

        public virtual Estados Estados { get; set; }

        public virtual Produccion Produccion { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
