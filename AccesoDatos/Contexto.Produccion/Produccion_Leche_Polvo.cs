﻿
namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Produccion_Leche_Polvo
    {
        [Key]
        public int Codigo_Interno_Produccion { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        public int? Litros { get; set; }

        public int? Codigo_Estado { get; set; }

        public int? Codigo_Quesero { get; set; }

        public decimal Piezas_Obtenidas { get; set; }

        public decimal? Kilos_Teoricos { get; set; }

        public decimal? Kilos_Reales { get; set; }

        public decimal? Kilos_Envasado { get; set; }

        public bool? Historico { get; set; }

        public int? Codigo_Fabrica { get; set; }

        public int? Codigo_Camara { get; set; }

        public int? Codigo_Motivo_Retencion { get; set; }
        //leche liquida
        public decimal Dens_Leche_Liquida { get; set; }
        public decimal Ph_Leche_Liquida { get; set; }
        public decimal Temp_Leche_Liquida { get; set; }
        public string Prueba_Alcohol { get; set; }
        public string Prueba_Antibioticos { get; set; }
        public decimal G { get; set; }
        public decimal SNG { get; set; }
        public decimal Proteinas { get; set; }
        public decimal Acidez { get; set; }
        //fin

        //leche polvo
        //caracteristicas fisico-quimicas
        public decimal Proteinas_Leche_Polvo { get; set; }
        public decimal Graso_Leche_Polvo { get; set; }
        public decimal Materia_Grasa_Leche_Polvo { get; set; }
        public decimal Cenizas_Totales_Leche_Polvo { get; set; }
        public decimal Humedad_Leche_Polvo { get; set; }
        public decimal Acidez_Leche_Polvo { get; set; }
        public decimal Acidez_1_Leche_Polvo { get; set; }
        public decimal Ph_Leche_Polvo { get; set; }
        public decimal Indice_Insolubilidad_Leche_Polvo { get; set; }
        public decimal Particulas_Quemadas_Leche_Polvo { get; set; }
        //fin
        //caracteristicas microbiologicas
        public decimal Bacterias_Aerobias_Mesofilas_Totales { get; set; }
        public decimal Coliformes_Totales { get; set; }
        public decimal Coliformes_Fecales { get; set; }
        public decimal Escherichia_Coli { get; set; }
        public decimal salmonella { get; set; }
        public decimal Staphylococcus_aureus { get; set; }
        //fin
        //fin





        public virtual Motivos_Retencion Motivos_Retencion { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
