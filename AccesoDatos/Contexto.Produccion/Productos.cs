namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Productos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Productos()
        {
            Formulas = new HashSet<Formulas>();
            Formulas_Envasado = new HashSet<Formulas_Envasado>();
            Insumos2 = new HashSet<Insumos>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Producto { get; set; }

        [Key]
        [StringLength(13)]
        public string Codigo_Producto { get; set; }

        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        public bool? Salida_Directa { get; set; }

        public decimal? Relacion_Teorica { get; set; }

        [StringLength(50)]
        public string Que_Usa { get; set; }

        public int? Dias_Frescura { get; set; }

        public int? Dias_Volteo { get; set; }

        public decimal? Tolerancia { get; set; }

        public int? Codigo_Familia { get; set; }

        public bool? Vencimiento_Envasado { get; set; }

        public bool? Activo { get; set; }

        public virtual Familias Familias { get; set; }

        public virtual Familias Familias1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Formulas> Formulas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Formulas_Envasado> Formulas_Envasado { get; set; }

        public virtual Insumos Insumos { get; set; }

        public virtual Insumos Insumos1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Insumos> Insumos2 { get; set; }
    }
}
