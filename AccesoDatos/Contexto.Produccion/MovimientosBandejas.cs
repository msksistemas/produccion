﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contexto.Produccion
{
    public partial class MovimientosBandejas
    {
        [Key]
        public int CodigoMovimiento { get; set; }

        public int Origen { get; set; }
        public int? Destino { get; set; }
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }
        public int? Codigo_Interno_Expedicion { get; set; }
        public bool Historico { get; set; }
        public string Prefijo { get; set; }
        public string NroComprobante { get; set; }

        public ICollection<DetallesMovimientosBandejas> Detalles { get; set; }
    }
}
