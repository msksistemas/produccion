namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Orden_Compra
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Orden_Compra()
        {
            Detalle_Orden_Compra = new HashSet<Detalle_Orden_Compra>();
        }

        [Key]
        public int Codigo_Interno_Orden { get; set; }

        public int? Numero_Comprobante { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }


        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Entrega { get; set; }

        [StringLength(100)]
        public string Comentario { get; set; }

        public int? Codigo_Proveedor { get; set; }

        public int? Codigo_Transporte { get; set; }

        [StringLength(50)]
        public string Lugar_Entrega { get; set; }

        [StringLength(50)]
        public string Plazo_Entrega { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Condicion_Pago { get; set; }

        [StringLength(50)]
        public string Descripcion_Condicion_Pago { get; set; }

        public decimal? Total { get; set; }

        public decimal? Precio_Dolar { get; set; }

        public decimal? Descuento { get; set; }

        public decimal? IVA { get; set; }

        public int? Codigo_Fabrica { get; set; }

        [StringLength(30)]
        public string Estado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Orden_Compra> Detalle_Orden_Compra { get; set; }
    }
}
