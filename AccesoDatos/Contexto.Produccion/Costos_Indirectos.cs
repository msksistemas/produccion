namespace AccesoDatos.Contexto.Produccion
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Costos_Indirectos
    {
        public Costos_Indirectos()
        {
            Costos_Indirectos_Formula = new HashSet<Costos_Indirectos_Formula>();
            Costos_Indirectos_Produccion = new HashSet<Costos_Indirectos_Produccion>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Costo_Indirecto { get; set; }

        [Key]
        [StringLength(13)]
        public string Codigo_Costo_Indirecto { get; set; }

        [Required]
        [StringLength(100)]
        public string Descripcion { get; set; }

        public decimal? Porcentaje { get; set; }

        public decimal? Precio { get; set; }

        public decimal? Precio_Fijo { get; set; }

        [StringLength(10)]
        public string Porcentaje_Aplica { get; set; }

        [StringLength(100)]
        public string Moneda { get; set; }

        [ForeignKey("Moneda")]
        public virtual Monedas Monedas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Formula> Costos_Indirectos_Formula { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Produccion> Costos_Indirectos_Produccion { get; set; }
    }
}
