namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Ingreso
    {
        [Key]
        public int Codigo_Interno_Detalle { get; set; }

        public int? Codigo_Interno_Ingreso { get; set; }

        public int? Codigo_Silo { get; set; }

        public decimal? Litros { get; set; }

        public int? Codigo_Fabrica { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        public decimal? Acidez { get; set; }

        public decimal? Ph { get; set; }

        public decimal? Densidad { get; set; }

        public decimal? Grasa { get; set; }

        public decimal? Proteina { get; set; }

        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public virtual Ingreso_Leche Ingreso_Leche { get; set; }

        public virtual Silos Silos { get; set; }
    }
}
