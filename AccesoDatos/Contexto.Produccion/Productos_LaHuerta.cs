namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Productos_LaHuerta
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Producto { get; set; }

        [Key]
        [StringLength(13)]
        public string Codigo_Producto { get; set; }

        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        public bool? Salida_Directa { get; set; }

        public decimal? Relacion_Teorica { get; set; }

        [StringLength(10)]
        public string Que_Usa { get; set; }

        public int? Dias_Frescura { get; set; }

        public int? Dias_Volteo { get; set; }

        public decimal? Tolerancia { get; set; }

        public int? Codigo_Familia { get; set; }

        public bool? Vencimiento_Envasado { get; set; }

        public bool? Activo { get; set; }
    }
}
