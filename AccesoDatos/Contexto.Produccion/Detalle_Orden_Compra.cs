namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Orden_Compra
    {
        [Key]
        public int Codigo_Interno_Detalle { get; set; }

        [Required]
        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public decimal? Cantidad { get; set; }

        public decimal? Kilos_Litros { get; set; }

        public decimal? Sin_Cargo_Kilos_Litros { get; set; }

        public decimal? Precio_Costo { get; set; }

        public decimal? Precio_Lista { get; set; }

        public decimal? Descuento { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha_Entrega { get; set; }

        [Required]
        public int Codigo_Interno_Orden { get; set; }

        public decimal? Importe { get; set; }

        public decimal? Recibidos { get; set; }

        public decimal? Faltantes { get; set; }

        public decimal? Sin_Cargo_Cantidad { get; set; }

        public virtual Insumos Insumos { get; set; }

        public virtual Orden_Compra Orden_Compra { get; set; }
    }
}
