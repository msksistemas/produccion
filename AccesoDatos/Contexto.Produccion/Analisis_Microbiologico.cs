﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace AccesoDatos.Contexto.Produccion
{

    public class Analisis_Microbiologico
    {
        [Key]
        public int Codigo_Interno_Analisis { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        public bool Visible_Listado { get; set; }

        public bool Activo { get; set; }
    }
}
