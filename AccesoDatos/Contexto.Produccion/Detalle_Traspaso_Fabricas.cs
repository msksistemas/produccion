namespace AccesoDatos.Contexto.Produccion
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Detalle_Traspaso_Fabricas
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Detalle { get; set; }
        public int? Codigo_Interno_Traspaso { get; set; }
        public string Lote { get; set; }
        public string Numero_Producto_Ventas { get; set; }
        public int? Cantidad { get; set; }
        public decimal? Kilos { get; set; }
        public int? Tipo { get; set; }

        public virtual Traspaso_Fabricas Traspaso_Fabricas { get; set; }
    }
}
