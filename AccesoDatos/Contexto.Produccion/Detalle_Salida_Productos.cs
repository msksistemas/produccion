namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Salida_Productos
    {
        [Key]
        public int Codigo_Interno_Detalle { get; set; }

        [StringLength(13)]
        public string Codigo_Producto { get; set; }

        public decimal? Cantidad { get; set; }

        public decimal? Kilos_Litros { get; set; }

        public int? Codigo_Interno_Salida { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        [StringLength(50)]
        public string tipo { get; set; }

        [StringLength(13)]
        public string Codigo_Insumo_Reproceso { get; set; }

        public virtual Salida_Productos Salida_Productos { get; set; }
    }
}
