namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuarios
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuarios()
        {
            Ajustes_Insumos = new HashSet<Ajustes_Insumos>();
            Cambio_Estado = new HashSet<Cambio_Estado>();
        }

        [Key]
        public int idUsuario { get; set; }

        [Required]
        [StringLength(50)]
        public string Usuario { get; set; }

        [StringLength(50)]
        public string Contraseña { get; set; }

        [StringLength(50)]
        public string Rol { get; set; }

        public int? Fabrica { get; set; }

        public bool? Muestra_Costos { get; set; }

        [StringLength(128)]
        public string Id_Login { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ajustes_Insumos> Ajustes_Insumos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cambio_Estado> Cambio_Estado { get; set; }
    }
}
