namespace AccesoDatos.Contexto.Produccion
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Costos_Indirectos_Formula
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Costo_Indirecto_Interno { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(13)]
        public string Codigo_Costo_Indirecto { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Formula { get; set; }

        public decimal? Precio { get; set; }

        public int? Cantidad { get; set; }

        public bool? Precio_Fijo { get; set; }

        public decimal? Porcentaje { get; set; }

        public bool? Marca { get; set; }

        public virtual Costos_Indirectos Costos_Indirectos { get; set; }

        public virtual Formulas Formulas { get; set; }
    }
}
