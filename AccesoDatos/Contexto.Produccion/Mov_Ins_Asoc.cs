namespace AccesoDatos.Contexto.Produccion
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Mov_Ins_Asoc
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Movimiento { get; set; }

        public decimal? Litros { get; set; }

        [Column(Order = 0)]
        [StringLength(13)]
        [Key]
        public string Lote { get; set; }

        [Column(Order = 1)]
        [StringLength(13)]
        [Key]
        public string Codigo_Insumo { get; set; }

        public virtual Insumos Insumos { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
