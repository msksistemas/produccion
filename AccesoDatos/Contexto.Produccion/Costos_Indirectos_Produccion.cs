namespace AccesoDatos.Contexto.Produccion
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Costos_Indirectos_Produccion
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Detalle { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(13)]
        public string Lote { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(13)]
        public string Codigo_Costo_Indirecto { get; set; }

        public decimal? Precio { get; set; }

        public int? Cantidad { get; set; }

        public bool? Precio_Fijo { get; set; }

        public decimal? Porcentaje { get; set; }

        public virtual Costos_Indirectos Costos_Indirectos { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
