namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Formula_Lavados
    {
        [Key]
        public int Codigo { get; set; }

        public int Codigo_Formula_Lavado { get; set; }

        [Required]
        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public decimal? cantidad { get; set; }

        public decimal? kilos_litros { get; set; }

        public virtual Lavados_Formula Lavados_Formula { get; set; }

        public virtual Insumos Insumos { get; set; }
    }
}
