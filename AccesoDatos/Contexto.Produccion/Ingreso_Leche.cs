namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Ingreso_Leche
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Ingreso_Leche()
        {
            Detalle_Ingreso = new HashSet<Detalle_Ingreso>();
        }

        [Key]
        public int Codigo_Interno_Ingreso { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        public int? Prefijo { get; set; }

        public int? Numero_Comprobante { get; set; }

        [StringLength(3)]
        public string Codigo_Comprobante { get; set; }

        public int? Codigo_Proveedor { get; set; }

        [StringLength(50)]
        public string Estado { get; set; }

        [StringLength(10)]
        public string Tipo_Factura { get; set; }

        public string Tipo_Movimiento { get; set; }

        public string Observacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Ingreso> Detalle_Ingreso { get; set; }
    }
}
