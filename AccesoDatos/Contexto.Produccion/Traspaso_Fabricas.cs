namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Traspaso_Fabricas")]
    public partial class Traspaso_Fabricas
    {
        public Traspaso_Fabricas()
        {
            Detalle_Traspaso = new HashSet<Detalle_Traspaso_Fabricas>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Traspaso { get; set; }
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }
        public int? Fabrica_Destino { get; set; }
        public int? Fabrica_Origen { get; set; }
        public string Egreso_Ingreso { get; set; }
        public string Comentario { get; set; }
        public string Prefijo { get; set; }
        public string Numero_Comprobante { get; set; }
        public bool? Recepcion_Confirmada { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Recepcion { get; set; }
        public string Motivo_Rechazo { get; set; }
        public int? idUsuario { get; set; }
        public bool? Historico { get; set; }

        public virtual ICollection<Detalle_Traspaso_Fabricas> Detalle_Traspaso { get; set; }
    }
}
