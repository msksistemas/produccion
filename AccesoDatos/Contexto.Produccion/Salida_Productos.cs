namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Salida_Productos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Salida_Productos()
        {
            Detalle_Salida_Productos = new HashSet<Detalle_Salida_Productos>();
        }

        [Key]
        public int Codigo_Interno_Salida { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        public bool? Historico { get; set; }

        public int? Sucursal_Origen { get; set; }

        public int? Sucursal_Destino { get; set; }

        public int? Prefijo { get; set; }
        public int? NroComprobante { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Salida_Productos> Detalle_Salida_Productos { get; set; }
    }
}
