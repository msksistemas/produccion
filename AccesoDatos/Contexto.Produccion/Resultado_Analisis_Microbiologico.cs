﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccesoDatos.Contexto.Produccion
{
    public class Resultado_Analisis_Microbiologico
    {
        public Resultado_Analisis_Microbiologico(){
            detalleResultado = new HashSet<Detalle_Resultado_Analisis>();
        }
        [Key]
        public int Codigo_Interno_Resultado { get; set; }

        [Required]
        [StringLength(13)]
        public string Lote { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        public int Codigo_Fabrica { get; set; }

        public string Comentario { get; set; }

        public virtual ICollection<Detalle_Resultado_Analisis> detalleResultado { get; set; }


    }
}
