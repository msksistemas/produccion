namespace AccesoDatos.Contexto.Produccion
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Costos_Indirectos_Formula_Envasado
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Costo_Indirecto_Interno { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Interno_Formula { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(13)]
        public string Codigo_Costo_Indirecto { get; set; }

        public decimal? Precio { get; set; }

        public int? Cantidad { get; set; }

        public bool? Precio_Fijo { get; set; }

        public decimal? Porcentaje { get; set; }

        public bool? Marca { get; set; }

        public virtual Formulas_Envasado Formulas_Envasado { get; set; }
        public virtual Costos_Indirectos Costos_Indirectos { get; set; }
    }
}
