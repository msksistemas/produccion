namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Fabricas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Fabricas()
        {
            Produccion = new HashSet<Produccion>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Fabrica { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Fabrica { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        [StringLength(12)]
        public string Nro_Planilla { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion> Produccion { get; set; }
    }
}
