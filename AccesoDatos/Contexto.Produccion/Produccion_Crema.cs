namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Produccion_Crema
    {
        [Key]
        public int Codigo_Interno_Produccion { get; set; }

        public decimal? Materia_Grasa { get; set; }

        public decimal? Acidez { get; set; }

        public decimal? Ph { get; set; }

        public decimal? Litros_Teoricos { get; set; }

        public decimal? Litros_Reales { get; set; }

        [Required]
        [StringLength(13)]
        public string Lote { get; set; }

        public decimal? Piezas_Obtenidas { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Vencimiento { get; set; }

        public decimal? Litros_Crema { get; set; }

        public decimal? Materia_Grasa_Final { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
