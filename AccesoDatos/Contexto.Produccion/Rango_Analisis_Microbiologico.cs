﻿using System.ComponentModel.DataAnnotations;

namespace AccesoDatos.Contexto.Produccion
{
    public class Rango_Analisis_Microbiologico
    {
        [Key]
        public int Codigo_Interno_Rango { get; set; }

        public int Codigo_Interno_Analisis { get; set; }
        
        public decimal? Desde { get; set; }
        
        public decimal? Hasta { get; set; }
    }
}
