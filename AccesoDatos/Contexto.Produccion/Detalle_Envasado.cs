namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Detalle_Envasado
    {
        [Key]
        public int Codigo_Interno_Detalle { get; set; }

        public int Codigo_Interno_Envasado { get; set; }

        public int Codigo_Interno_Formula { get; set; }

        public decimal? Kilos { get; set; }

        public decimal? Cantidad { get; set; }

        [DefaultValue(false)]
        public bool Historico { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        public DateTime? Fecha_Liberado { get; set; }

        public int? Codigo_Motivo_Retencion { get; set; }

        public int? Codigo_Motivo_Reproceso { get; set; }

        public int? Codigo_Motivo_Decomiso { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Vencimiento { get; set; }

        public decimal? Descarte { get; set; }

        public decimal? Hormas { get; set; }

        public bool? Decomisado { get; set; }

        [StringLength(13)]
        public string Codigo_Insumo_Reproceso { get; set; }

        public int? Id_Usuario_Libera { get; set; }

        public int? Id_Usuario_Decomisa { get; set; }

        public int? Id_Usuario_Reprocesa { get; set; }

        public int? Id_Usuario_Retiene { get; set; }

        public DateTime? Fecha_Retenido { get; set; }

        public DateTime? Fecha_Decomiso { get; set; }
        public DateTime? Fecha_Reproceso { get; set; }

        public bool? Liberado_Condicional { get; set; }

        public int? Id_Usuario_Libera_Condicional { get; set; }

        [StringLength(600)]
        public string Comentario_Retencion { get; set; }

        [StringLength(600)]
        public string Comentario_Decomiso { get; set; }

        [StringLength(600)]
        public string Comentario_Reproceso { get; set; }

        public decimal? Kilos_Desnudo { get; set; }

        public virtual Envasado Envasado { get; set; }

        public virtual Formulas_Envasado Formulas_Envasado { get; set; }

        public virtual Insumos Insumos { get; set; }

        public virtual Motivos_Retencion Motivos_Retencion { get; set; }

        public virtual Motivos_Retencion Motivos_Decomiso { get; set; }

    }
}
