﻿namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Mov_Stock_Prod_Ventas
    {
        public int Id { get; set; }

        [Required]
        [StringLength(13)]
        public string Numero_Producto { get; set; }

        [Required]
        [StringLength(20)]
        public string Tipo_Mov { get; set; }

        public DateTime Fecha { get; set; }

        [Required]
        [StringLength(30)]
        public string Estado { get; set; }

        public decimal? Cantidad { get; set; }

        public decimal? Kilos { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }
        public int? Fabrica { get; set; }

        [StringLength(20)]
        public string Nro_Comprobante { get; set; }

        public int? Id_Expedicion { get; set; }

        public int? Id_Salida { get; set; }

        public bool? EsAjuste { get; set; }

        public int? Codigo_Interno_Detalle_Envasado { get; set; }

        public int? Codigo_Interno_Detalle_Expedicion { get; set; }

        public int? Codigo_Interno_Detalle_Traspaso { get; set; }
    }
}
