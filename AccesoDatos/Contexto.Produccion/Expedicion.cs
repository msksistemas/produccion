namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Expedicion")]
    public partial class Expedicion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Expedicion()
        {
            Detalle_Expedicion = new HashSet<Detalle_Expedicion>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Interno_Expedicion { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        [StringLength(15)]
        public string Egreso_Ingreso { get; set; }

        [StringLength(100)]
        public string Comentario { get; set; }

        [StringLength(8)]
        public string Numero_Comprobante { get; set; }

        [StringLength(1)]
        public string Tipo_Comprobante { get; set; }

        [StringLength(15)]
        public string Prefijo { get; set; }

        public int? idUsuario { get; set; }

        public int? Sucursal { get; set; }

        public bool? Historico { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        public bool? Recepcion_Confirmada { get; set; }

        [StringLength(1000)]
        public string Motivo_Rechazo { get; set; }

        public int? Origen { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FechaRecepcion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Expedicion> Detalle_Expedicion { get; set; }
    }
}
