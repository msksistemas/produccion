namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Produccion_Ricota
    {
        [Key]
        public int Codigo_Interno_Produccion { get; set; }

        public decimal? Acidez_Inicial { get; set; }

        public decimal? Acidez_Final { get; set; }

        [Required]
        [StringLength(13)]
        public string Lote { get; set; }

        public int? Tina { get; set; }

        public int? Codigo_Silo { get; set; }

        public decimal? Litros_Suero { get; set; }

        public decimal? Porcentaje_Solidos { get; set; }

        public decimal? Factor_Concentracion { get; set; }

        public TimeSpan? Hora_Inicio { get; set; }

        public TimeSpan? Hora_Final { get; set; }

        public decimal? Temperatura_Corte { get; set; }

        public decimal? Kilos_Teoricos { get; set; }

        public int? Piezas_Obtenidas { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Vencimiento { get; set; }

        public decimal? Kilos_Reales { get; set; }

        public decimal? Litros_Tina { get; set; }

        public virtual Produccion Produccion { get; set; }
    }
}
