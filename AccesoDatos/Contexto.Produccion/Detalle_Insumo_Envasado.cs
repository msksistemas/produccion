namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Insumo_Envasado
    {
        [Key]
        public int Codigo_Interno_Detalle { get; set; }

        public int? Codigo_Interno_Envasado { get; set; }

        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public int? Cantidad { get; set; }

        public decimal? Kilos { get; set; }

        [StringLength(50)]
        public string Lote { get; set; }

        public virtual Envasado Envasado { get; set; }
    }
}
