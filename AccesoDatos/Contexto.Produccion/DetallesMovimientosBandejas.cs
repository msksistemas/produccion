﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Contexto.Produccion
{
    public partial class DetallesMovimientosBandejas
    {
        [Key]
        public int CodigoDetalle { get; set; }

        public string Codigo_Insumo { get; set; }

        public int Unidades { get; set; }

        public int CodigoMovimiento { get; set; }

        public virtual MovimientosBandejas Movimiento { get; set; }

        public virtual Insumos Insumo { get; set; }

    }
}
