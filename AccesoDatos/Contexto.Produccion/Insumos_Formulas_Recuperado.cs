namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Insumos_Formulas_Recuperado
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Formula { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public decimal CantidadRecuperada { get; set; }

        public virtual Formulas Formulas { get; set; }

        public virtual Insumos Insumos { get; set; }
    }
}
