namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Detalle_Limpieza
    {
        [Key]
        public int Codigo { get; set; }

        public int Codigo_Limpieza { get; set; }

        [Required]
        [StringLength(13)]
        public string Codigo_Insumo { get; set; }

        public decimal? cantidad { get; set; }

        public decimal? kilos_litros { get; set; }

        [StringLength(13)]
        public string lote { get; set; }

        public decimal? ajuste { get; set; }

        public virtual Limpieza Limpieza { get; set; }

        public virtual Insumos Insumos { get; set; }
    }
}
