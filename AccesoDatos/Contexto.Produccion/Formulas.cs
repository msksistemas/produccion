namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Formulas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Formulas()
        {
            Costos_Indirectos_Formula = new HashSet<Costos_Indirectos_Formula>();
            Detalle_Formula = new HashSet<Detalle_Formula>();
            Insumos_Formulas_Recuperado = new HashSet<Insumos_Formulas_Recuperado>();
            Produccion = new HashSet<Produccion>();
            Insumos = new HashSet<Insumos>();
        }

        [Key]
        public int Codigo_Formula { get; set; }

        [Required]
        [StringLength(13)]
        public string Codigo_Producto { get; set; }

        public decimal? Factor { get; set; }

        public decimal? Kilos_Teoricos { get; set; }

        [StringLength(200)]
        public string Comentario { get; set; }

        public int Litros_Tina { get; set; }

        public decimal? Kilos_Masa { get; set; }
        public decimal? Kilos_Reproceso { get; set; }

        public decimal? Litros_Suero { get; set; }

        public decimal? Litros_Crema { get; set; }

        public decimal? Unidades_Esperadas { get; set; }

        public bool? Historico { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha_Alta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Formula> Costos_Indirectos_Formula { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Formula> Detalle_Formula { get; set; }

        public virtual Productos Productos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Insumos_Formulas_Recuperado> Insumos_Formulas_Recuperado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion> Produccion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Insumos> Insumos { get; set; }
    }
}
