namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Insumos
    {
        public Insumos()
        {
            DetallesMovimientosBandejas = new HashSet<DetallesMovimientosBandejas>();
            Detalle_Ajuste_Insumos = new HashSet<Detalle_Ajuste_Insumos>();
            Detalle_Compra = new HashSet<Detalle_Compra>();
            Detalle_Envasado = new HashSet<Detalle_Envasado>();
            Detalle_Formula = new HashSet<Detalle_Formula>();
            Detalle_Formula_Envasado = new HashSet<Detalle_Formula_Envasado>();
            Detalle_Orden_Compra = new HashSet<Detalle_Orden_Compra>();
            Envasado = new HashSet<Envasado>();
            Insumos_Formulas_Recuperado = new HashSet<Insumos_Formulas_Recuperado>();
            Mov_Ins_Asoc = new HashSet<Mov_Ins_Asoc>();
            Productos = new HashSet<Productos>();
            Productos1 = new HashSet<Productos>();
            Formulas = new HashSet<Formulas>();
            Productos2 = new HashSet<Productos>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Insumos { get; set; }

        [Key]
        [StringLength(13)]
        [Required(ErrorMessage = "Campo requerido")]
        public string Codigo_Insumo { get; set; }

        [StringLength(8)]
        public string Codigo_Insumo_Proveedor { get; set; }

        [StringLength(8)]
        public string Codigo_Insumo_Proveedor_Alt { get; set; }

        [StringLength(1000)]
        [Required(ErrorMessage = "Campo requerido")]
        public string Descripcion { get; set; }

        [StringLength(1000)]
        public string Descripcion_Larga { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public int Codigo_Linea { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public int Codigo_Rubro { get; set; }

        [StringLength(500)]
        public string Comentario { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public int Codigo_Proveedor { get; set; }

        public int? Codigo_Proveedor_Alternativo { get; set; }

        public int? Codigo_Ultimo_Proveedor { get; set; }

        public int? Codigo_Iva { get; set; }

        public int? Codigo_Impuesto_Interno { get; set; }

        [StringLength(8)]
        public string Codigo_Imputacion { get; set; }

        [StringLength(1)]
        public string Unidad_Medida { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Ultima_Compra { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.####}")]
        public decimal? Precio_Ultima_Compra { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.####}")]
        public decimal? Precio_Lista { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.####}")]
        public decimal? Punto_Pedido { get; set; }

        public bool? Activo { get; set; }

        [StringLength(1)]
        public string Precio_Por { get; set; }

        [StringLength(100)]
        public string Moneda { get; set; }

        public bool Materia_Prima { get; set; }

        public bool Leche { get; set; }

        public bool Masa { get; set; }

        public bool Reproceso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Ajuste_Insumos> Detalle_Ajuste_Insumos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Compra> Detalle_Compra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Envasado> Detalle_Envasado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Formula> Detalle_Formula { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Formula_Envasado> Detalle_Formula_Envasado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Orden_Compra> Detalle_Orden_Compra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Envasado> Envasado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Insumos_Formulas_Recuperado> Insumos_Formulas_Recuperado { get; set; }

        public virtual Monedas Monedas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Mov_Ins_Asoc> Mov_Ins_Asoc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Productos> Productos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Productos> Productos1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Formulas> Formulas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Productos> Productos2 { get; set; }

        public virtual ICollection<DetallesMovimientosBandejas> DetallesMovimientosBandejas { get; set; }

    }

}
