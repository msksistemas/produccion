namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Lineas
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Codigo_Interno_Linea { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Codigo_Linea { get; set; }

        [StringLength(100)]
        public string Nombre { get; set; }
    }
}
