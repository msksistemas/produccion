namespace AccesoDatos.Contexto.Produccion
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Envasado")]
    public partial class Envasado
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Envasado()
        {
            Detalle_Envasado = new HashSet<Detalle_Envasado>();
            Detalle_Insumo_Envasado = new HashSet<Detalle_Insumo_Envasado>();
            Produccion = new HashSet<Produccion>();
            Restantes_Envasados = new HashSet<Restantes_Envasado>();
        }

        [Key]
        public int Codigo_Interno_Envasado { get; set; }

        [StringLength(13)]
        public string Lote { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha { get; set; }

        public decimal? Humedad { get; set; }
        [Display(Name = "Sucursal destino")]
        public int? Sucursal { get; set; }

        public bool? Historico { get; set; }

        public decimal? Kg_Descarte { get; set; }

        public decimal? Hormas_Descarte { get; set; }

        [StringLength(50)]
        public string Destino_Descarte { get; set; }

        [StringLength(13)]
        public string Codigo_Insumo_Reproceso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Envasado> Detalle_Envasado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Insumo_Envasado> Detalle_Insumo_Envasado { get; set; }

        public virtual Insumos Insumos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion> Produccion { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Restantes_Envasado> Restantes_Envasados { get; set; }
    }
}
