//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Ventas
{
    using System;
    using System.Collections.Generic;
    
    public partial class Carga_Traspaso
    {
        public int Comprobante { get; set; }
        public string NroComprobante_Asociado { get; set; }
        public Nullable<System.DateTime> strFecha { get; set; }
        public Nullable<short> SUC_ORIGEN { get; set; }
        public Nullable<short> SUC_DESTINO { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<byte> Estado { get; set; }
        public Nullable<short> NroPc { get; set; }
        public string Comentario { get; set; }
    }
}
