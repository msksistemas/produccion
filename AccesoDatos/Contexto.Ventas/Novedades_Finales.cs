//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Ventas
{
    using System;
    using System.Collections.Generic;
    
    public partial class Novedades_Finales
    {
        public short Prefijo_Comprob { get; set; }
        public int Num_Real { get; set; }
        public string Tipo_Documento { get; set; }
        public string Codigo_Movimiento { get; set; }
        public int CodIntNovFinales { get; set; }
        public Nullable<int> Nro_Cliente { get; set; }
        public Nullable<int> Nro_Vendedor { get; set; }
        public Nullable<int> Cond_Venta { get; set; }
        public Nullable<int> Concepto { get; set; }
        public Nullable<int> Nro_Factura { get; set; }
        public Nullable<int> Nro_Remito { get; set; }
        public Nullable<int> Nro_Venta_Tel { get; set; }
        public string Nro_Documento { get; set; }
        public Nullable<System.DateTime> strFecha_Comprobante { get; set; }
        public Nullable<System.DateTime> strFecha_Venc { get; set; }
        public Nullable<decimal> Bonificacion { get; set; }
        public Nullable<int> Transporte { get; set; }
        public string Orden_de_Compra { get; set; }
        public string Pedido { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> IMP_EXENTO { get; set; }
        public Nullable<decimal> IMP_GRAVADO { get; set; }
        public Nullable<decimal> IMPORTE_IVA { get; set; }
        public Nullable<decimal> Imp_Iva_No_Insc { get; set; }
        public Nullable<decimal> Importe_Interno { get; set; }
        public Nullable<decimal> IMP_TOTAL { get; set; }
        public Nullable<short> ALICUOTA_DE_IVA { get; set; }
        public string Tipo_Comprob { get; set; }
        public Nullable<int> NRO_ZONA { get; set; }
        public Nullable<decimal> Kilos_Totales { get; set; }
        public Nullable<int> C_FACT { get; set; }
        public string Usuario { get; set; }
        public Nullable<short> Prefijo_Doc_Relacionado { get; set; }
        public string Tipo_Doc_Relacionado { get; set; }
        public Nullable<int> Nro_Suc { get; set; }
        public Nullable<System.DateTime> strFecha_Pase { get; set; }
        public string Nro_Presupuesto { get; set; }
        public string Leyenda { get; set; }
        public string RemAgrupados { get; set; }
        public string Nro_CAE { get; set; }
        public string Codigo_Barra { get; set; }
        public string Cod_Barra_Convertido { get; set; }
        public Nullable<byte> Cod_ProductoServicio { get; set; }
        public Nullable<System.DateTime> strFecha_ServicioD { get; set; }
        public Nullable<System.DateTime> strFecha_ServicioH { get; set; }
        public Nullable<System.DateTime> strFecha_VtoFactura { get; set; }
        public string CAEA_Orden { get; set; }
        public string CAEA_Periodo { get; set; }
        public Nullable<byte> Factura_Informada { get; set; }
        public Nullable<byte> Marca_Cot { get; set; }
        public Nullable<decimal> Imp_Ing_Brutos { get; set; }
        public Nullable<decimal> Porcentaje_IB { get; set; }
        public Nullable<decimal> Imp_Gravado_21 { get; set; }
        public Nullable<decimal> Imp_Iva_21 { get; set; }
        public Nullable<decimal> Imp_Gravado_10 { get; set; }
        public Nullable<decimal> Imp_Iva_10 { get; set; }
        public Nullable<int> Nro_Terminal { get; set; }
        public Nullable<int> Nro_ScrapDecomiso { get; set; }
        public Nullable<short> Prefijo_Real { get; set; }
        public Nullable<int> NroAutorizacion { get; set; }
        public Nullable<short> CodTarjeta { get; set; }
        public string Titular { get; set; }
        public Nullable<int> NroTarjeta { get; set; }
        public Nullable<short> NroCaja { get; set; }
        public Nullable<decimal> ImporteTarjeta { get; set; }
        public Nullable<decimal> ImporteCaja { get; set; }
        public Nullable<byte> Factura_Especial { get; set; }
        public Nullable<int> Destinatario { get; set; }
        public Nullable<decimal> Importe_Bonificacion { get; set; }
        public Nullable<decimal> Saldo_Anterior { get; set; }
        public Nullable<decimal> Valor_Dolar { get; set; }
        public Nullable<decimal> Otros_Conceptos { get; set; }
        public Nullable<decimal> Importe_Arancel { get; set; }
        public string Leyenda_IB { get; set; }
        public Nullable<int> Cod_Seguridad { get; set; }
        public Nullable<short> CantCuotas { get; set; }
        public string DNI { get; set; }
        public Nullable<bool> Cuenta_Orden { get; set; }
        public string Codigo_Comprobante { get; set; }
        public Nullable<decimal> Imp_No_Gravado { get; set; }
        public Nullable<bool> Asoc_FCE { get; set; }
    
        public virtual Clientes Clientes { get; set; }
        public virtual Conceptos Conceptos { get; set; }
        public virtual Cond_Venta Cond_Venta1 { get; set; }
        public virtual Sucursales_Propias Sucursales_Propias { get; set; }
        public virtual Transportes Transportes { get; set; }
        public virtual Zonas Zonas { get; set; }
    }
}
