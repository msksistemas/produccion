//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Ventas
{
    using System;
    using System.Collections.Generic;
    
    public partial class Control_Retenciones
    {
        public int Cod_Interno { get; set; }
        public Nullable<int> Nro_Retencion { get; set; }
        public short Tipo_Retencion { get; set; }
        public System.DateTime strFecha_Retencion { get; set; }
        public Nullable<decimal> Importe { get; set; }
        public Nullable<short> Cod_Cliente { get; set; }
        public Nullable<int> Operacion_Afectada { get; set; }
        public string Observaciones { get; set; }
        public Nullable<System.DateTime> strFecha_Aplicacion { get; set; }
        public short Cod_Provincia { get; set; }
        public Nullable<byte> Marca { get; set; }
    }
}
