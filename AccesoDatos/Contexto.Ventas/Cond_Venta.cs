//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Ventas
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cond_Venta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cond_Venta()
        {
            this.Novedades_Finales = new HashSet<Novedades_Finales>();
        }
    
        public int CODIGO { get; set; }
        public string DESCRIPCION { get; set; }
        public Nullable<int> CANT_CUOTAS { get; set; }
        public Nullable<int> DIAS1 { get; set; }
        public Nullable<decimal> PORCENTAJE1 { get; set; }
        public Nullable<int> DIAS2 { get; set; }
        public Nullable<decimal> PORCENTAJE2 { get; set; }
        public Nullable<int> DIAS3 { get; set; }
        public Nullable<decimal> PORCENTAJE3 { get; set; }
        public Nullable<int> DIAS4 { get; set; }
        public Nullable<decimal> PORCENTAJE4 { get; set; }
        public Nullable<int> DIAS5 { get; set; }
        public Nullable<decimal> PORCENTAJE5 { get; set; }
        public Nullable<int> DIAS6 { get; set; }
        public Nullable<decimal> PORCENTAJE6 { get; set; }
        public Nullable<decimal> Carga_Financiera { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Novedades_Finales> Novedades_Finales { get; set; }
    }
}
