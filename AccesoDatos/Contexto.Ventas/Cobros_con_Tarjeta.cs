//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Ventas
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cobros_con_Tarjeta
    {
        public int NroAutorizacion { get; set; }
        public short CodTarjeta { get; set; }
        public string Titular { get; set; }
        public Nullable<int> NroTarjeta { get; set; }
        public Nullable<short> PrefijoComprobante { get; set; }
        public Nullable<int> NroComprobante { get; set; }
        public string TipoComprobante { get; set; }
        public string CodigoMovimiento { get; set; }
        public string TipoDocumento { get; set; }
        public Nullable<System.DateTime> strFecha { get; set; }
        public Nullable<decimal> ImporteTarjeta { get; set; }
        public Nullable<decimal> ImportePagado { get; set; }
    }
}
