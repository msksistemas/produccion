//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Ventas
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cant_CabexCliente
    {
        public int NroCliente { get; set; }
        public string Producto { get; set; }
        public Nullable<int> Cant_Cabezas { get; set; }
        public Nullable<decimal> Consumo { get; set; }
    
        public virtual Clientes Clientes { get; set; }
        public virtual Productos Productos { get; set; }
    }
}
