//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Ventas
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cobros_con_Tarjeta_Aux
    {
        public Nullable<byte> MARCA { get; set; }
        public int NROAUTORIZACION { get; set; }
        public short CODTARJETA { get; set; }
        public string TITULAR { get; set; }
        public Nullable<int> NROTARJETA { get; set; }
        public Nullable<short> PREFIJOCOMPROBANTE { get; set; }
        public Nullable<int> NROCOMPROBANTE { get; set; }
        public string TIPOCOMPROBANTE { get; set; }
        public string CODIGOMOVIMIENTO { get; set; }
        public string TIPODOCUMENTO { get; set; }
        public Nullable<System.DateTime> STRFECHA { get; set; }
        public Nullable<decimal> IMPORTETARJETA { get; set; }
        public Nullable<decimal> IMPORTEPAGADO { get; set; }
    }
}
