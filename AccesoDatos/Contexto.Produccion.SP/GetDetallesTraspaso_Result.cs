//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Produccion.SP
{
    using System;
    
    public partial class GetDetallesTraspaso_Result
    {
        public int Codigo_Interno_Detalle { get; set; }
        public int Codigo_Interno_Traspaso { get; set; }
        public string Numero_Producto_Ventas { get; set; }
        public string Lote { get; set; }
        public string Descripcion_Producto { get; set; }
        public decimal Kilos { get; set; }
        public int Cantidad { get; set; }
        public Nullable<int> Tipo { get; set; }
    }
}
