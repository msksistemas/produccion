//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Produccion.SP
{
    using System;
    
    public partial class ObtenerTiposProveedores_Result
    {
        public int CodigoProveedor { get; set; }
        public string DescripcionTipoProveedor { get; set; }
    }
}
