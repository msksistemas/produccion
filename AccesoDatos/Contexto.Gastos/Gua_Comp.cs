//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Gastos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Gua_Comp
    {
        public int NRO_COMP { get; set; }
        public Nullable<System.DateTime> strFecHA_COMP { get; set; }
        public Nullable<int> NRO_PROVEDOR { get; set; }
        public string RAZON_SOCIAL { get; set; }
        public Nullable<decimal> IMPORTE { get; set; }
        public Nullable<decimal> PAGO_EFECTIVO { get; set; }
    
        public virtual Provee_Cta_Cte Provee_Cta_Cte { get; set; }
    }
}
