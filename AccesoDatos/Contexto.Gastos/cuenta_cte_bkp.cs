//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccesoDatos.Contexto.Gastos
{
    using System;
    using System.Collections.Generic;
    
    public partial class cuenta_cte_bkp
    {
        public int Prefijo { get; set; }
        public int Nro_COMPROB { get; set; }
        public byte TIPO_MOVIM { get; set; }
        public string TIPO_COMPROB { get; set; }
        public int COD_PROVEEDOR { get; set; }
        public int NroIntCtaCte { get; set; }
        public string Comentario { get; set; }
        public Nullable<System.DateTime> strFecHA_COMPROB { get; set; }
        public Nullable<decimal> IMPORTE { get; set; }
        public Nullable<System.DateTime> strPERIODO_CONTABLE { get; set; }
        public Nullable<System.DateTime> strFecha_Pase { get; set; }
        public string NroPresupuesto { get; set; }
        public string NroOT { get; set; }
        public Nullable<short> Prefijo_Suc { get; set; }
    }
}
