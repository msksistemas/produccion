﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;
using Servicios.Interfaces;

namespace Servicios.Clases
{
    /// <summary>
    /// Servicio encargado del CRUD de una entidad especificada.
    /// </summary>
    /// <typeparam name="TEntity">Entidad que se quiere hacer el CRUD.</typeparam>
    public class BaseServicio<TEntity> : IBaseServicio<TEntity> where TEntity : class
    {
        internal DbContext contexto;
        internal DbSet<TEntity> dbSet;

        public BaseServicio()
        {
        }
        /// <summary>
        /// Inicializa el contexto de BD para poder trabajar.
        /// </summary>
        /// <param name="db">Contexto de BD en donde se encuentra la entidad.</param>
        public void SetearContexto(DbContext db)
        {
            this.contexto = db;
            this.dbSet = contexto.Set<TEntity>();
        }

        /// <summary>
        /// Obtener registro ejecuntando una Query SQL.
        /// </summary>
        /// <param name="query"> Query SQL, si tiene parametros colocar @param1,@paramN</param>
        /// <param name="parametros">new[] { new SqlParameter("param1",valor) ,new SqlParameter("paramN",valor)  }</param>
        /// <returns>Retorna un Ilist<Entidad> con los reguistros que cumplan la condicion</returns>
        public virtual IList<TEntity> ObtenerConRawSql(string query,
            params object[] parametros)
        {
            return dbSet.SqlQuery(query, parametros).ToList();
        }
        /// <summary>
        /// Permite ejecutar SQL sobre la bd.
        /// </summary>
        /// <param name="query">Consulta SQL</param>
        /// <param name="parametros">Parametros de la consulta.</param>
        public virtual void EjecutarSQL(string query,
            params object[] parametros)
        {
            contexto.Database.ExecuteSqlCommand(query,parametros);
        }
        /// <summary>
        /// Obtener registros con Linq
        /// </summary>
        /// <param name="filter">(entidad)=> condicion </param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties">Colocar nombre de las propiedades de navegacion que se quieren obtener en conjunto</param>
        /// <returns>Retorna un Ilist<Entidad> con los reguistros que cumplan la condicion</returns>
        public virtual IList<TEntity> Obtener(
            Expression<Func<TEntity, bool>> filtro = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            if (filtro != null)
                query = query.Where(filtro);

            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }


            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual void Agregar(TEntity entidad)
        {
            dbSet.Add(entidad);
            contexto.SaveChanges();
        }

        public virtual void Borrar(TEntity entidad)
        {
            if (contexto.Entry(entidad).State == EntityState.Detached)
            {
                dbSet.Attach(entidad);
            }
            dbSet.Remove(entidad);
            contexto.SaveChanges();
        }

        public virtual void Editar(TEntity entidad)
        {
            dbSet.Attach(entidad);
            contexto.Entry(entidad).State = EntityState.Modified;
            contexto.SaveChanges();
        }
    }
}
