﻿using AccesoDatos.Contexto.Produccion;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Servicios.Clases
{
    public class BandejaServicio : IBandejaServicio
    {
        private DbSet<MovimientosBandejas> dbSetMovimiento;
        private DbSet<DetallesMovimientosBandejas> dbSetDetalles;
        private IBaseServicio<MovimientosBandejas> ServiciosMovimientos;
        private IBaseServicio<DetallesMovimientosBandejas> ServiciosDetalles;
        private DbContext Contexto;

        public BandejaServicio(IBaseServicio<MovimientosBandejas> _movimientosBandeja,IBaseServicio<DetallesMovimientosBandejas> _detalles)
        {
            this.ServiciosDetalles = _detalles;
            this.ServiciosMovimientos = _movimientosBandeja;
            this.Contexto = new dbProd();
            this.dbSetMovimiento = Contexto.Set<MovimientosBandejas>();
            this.dbSetDetalles = Contexto.Set<DetallesMovimientosBandejas>();
        }
        public void Agregar(MovimientosBandejas movimientosBandejas)
        {
            dbSetMovimiento.Add(movimientosBandejas);
            Contexto.SaveChanges();
        }

        public void BorrarPorId(int idMovimiento)
        {
            var movimiento = ObtenerPorId(idMovimiento);
            if (movimiento == null)
                throw new Exception("No se encontro el movimiento de bandejas");
            BorrarMovimiento(movimiento);
        }

        public void BorrarPorIdExpedicion(int idExpedicion)
        {
            var movimiento = dbSetMovimiento.SingleOrDefault(m => m.Codigo_Interno_Expedicion == idExpedicion);
            if (movimiento != null)
                BorrarMovimiento(movimiento);
        }
        private void BorrarMovimiento(MovimientosBandejas movimiento)
        {
            dbSetMovimiento.Remove(movimiento);
            Contexto.SaveChanges();
        }


        public void Editar(int CodigoMovimiento, DateTime fecha, int origen, int destino,string prefijo, string nroComprobante, List<DetallesMovimientosBandejas> detalles)
        {
            var movimientos = dbSetMovimiento.SingleOrDefault(m => m.CodigoMovimiento == CodigoMovimiento);
            movimientos.Fecha = fecha;
            movimientos.Origen = origen;
            movimientos.Destino = destino;
            movimientos.Prefijo = prefijo;
            movimientos.NroComprobante = nroComprobante;
            Contexto.SaveChanges();
            this._BorrarDetallesMovimiento(movimientos.CodigoMovimiento);
            foreach (var item in detalles)
            {
                this.ServiciosDetalles.Agregar(item);
            }
        }

        public void EditarMovimientoExpedicion(int codigoInternoExpedicion, int nuevoDestino, List<DetallesMovimientosBandejas> detalles)
        {
            var movimiento = dbSetMovimiento.SingleOrDefault(m => m.Codigo_Interno_Expedicion == codigoInternoExpedicion);
            if (movimiento != null)
            {
                movimiento.Destino= nuevoDestino;
                Contexto.SaveChanges();
                foreach (var item in dbSetDetalles.Where(d => d.CodigoMovimiento == movimiento.CodigoMovimiento).ToList())
                {
                    dbSetDetalles.Remove(item);
                }
                Contexto.SaveChanges();

            }
            if (detalles != null)
            {
                foreach (var det in detalles)
                {
                    det.CodigoMovimiento = movimiento.CodigoMovimiento;
                    dbSetDetalles.Add(det);
                }
                Contexto.SaveChanges();
            }
        }

        public List<DetallesMovimientosBandejas> ObtenerDetallesPorIdExpedicion(int idExpedicion)
        {
             var detalles = dbSetDetalles.Where(d => d.Movimiento.Codigo_Interno_Expedicion == idExpedicion);
            if (detalles == null)
                return new List<DetallesMovimientosBandejas>();
            return detalles.ToList();
        }

        public MovimientosBandejas ObtenerPorId(int id)
        {
            var movimiento = dbSetMovimiento.SingleOrDefault(m => m.CodigoMovimiento == id);
            if (movimiento == null)
                throw new Exception("No se encontro el movimiento de bandejas");

            return movimiento;
        }
        public void PasarHistorico(int CodigoMovimiento = 0, int CodigoInternoExpedicion = 0)
        {
            var movimiento = dbSetMovimiento.SingleOrDefault(m => m.CodigoMovimiento == CodigoMovimiento || m.Codigo_Interno_Expedicion == CodigoInternoExpedicion);
            if (movimiento != null)
            {
                movimiento.Historico = true;

                dbSetMovimiento.Attach(movimiento);
                Contexto.Entry(movimiento).State = EntityState.Modified;
                Contexto.SaveChanges();
            }
        }

        public List<MovimientosBandejas> Obtener(bool Estado)
        {
            var lista = this.dbSetMovimiento.Where(m => m.Historico == Estado).ToList();

            return lista;
        }

        public List<DetallesMovimientosBandejas> ObtenerDetallesPorId(int idMovimiento)
        {
            var detalles = dbSetDetalles.Where(d => d.Movimiento.CodigoMovimiento == idMovimiento);
            if (detalles == null)
                return new List<DetallesMovimientosBandejas>();
            return detalles.ToList();
        }

        private void _BorrarDetallesMovimiento(int id)
        {
            var borrar = dbSetDetalles.Where(d => d.CodigoMovimiento == id).ToList();
            if (borrar.Count > 0)
            {
                dbSetDetalles.RemoveRange(borrar);
                Contexto.SaveChanges();
            }
        }

        public bool ExisteMovimiento(int codigoMovimiento, string prefijo, string nroComprobante)
        {
            if (codigoMovimiento == 0)
               return dbSetMovimiento.SingleOrDefault(m => m.Prefijo == prefijo && m.NroComprobante == nroComprobante) != null;
            else
                return dbSetMovimiento.SingleOrDefault(m => m.Prefijo == prefijo && m.NroComprobante == nroComprobante && m.CodigoMovimiento != codigoMovimiento) != null;

        }
    }
}
