﻿using AccesoDatos.Contexto.Produccion;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Clases
{
    public class ProduccionServicio : IProduccionServicio
    {
        private DbContext _Contexto;
        private DbSet<Produccion> _Prod;
        private DbSet<Produccion_Quesos> _ProdQuesos;
        private DbSet<Produccion_Quesos_Fundido> _ProdQuesosFundido;
        private DbSet<Produccion_Leche_Polvo> _ProdLechePolvo;
        private DbSet<Produccion_Ricota> _ProdRicota;
        private DbSet<Produccion_Crema> _ProdCrema;
        private DbSet<Produccion_Muzzarela> _ProdMuzza;
        private DbSet<Produccion_Dulce_de_Leche> _ProdDulceLeche;
        private DbSet<Envasado> _Envasado;

        public ProduccionServicio()
        {
            this._Contexto = new dbProd();
            this._Prod = this._Contexto.Set<Produccion>();
            this._ProdQuesos = this._Contexto.Set<Produccion_Quesos>();
            this._ProdQuesosFundido = this._Contexto.Set<Produccion_Quesos_Fundido>();
            this._ProdLechePolvo = this._Contexto.Set<Produccion_Leche_Polvo>();
            this._ProdRicota = this._Contexto.Set<Produccion_Ricota>();
            this._ProdCrema = this._Contexto.Set<Produccion_Crema>();
            this._ProdMuzza = this._Contexto.Set<Produccion_Muzzarela>();
            this._ProdDulceLeche = this._Contexto.Set<Produccion_Dulce_de_Leche>();
            this._Envasado = this._Contexto.Set<Envasado>();

        }

        public Dictionary<string, decimal> ObtenerCantidadesReales(string lote)
        {
            Dictionary<string, decimal> cantidades = new Dictionary<string, decimal>();
            var prodQueso = _ProdQuesos.SingleOrDefault((p) => p.Lote == lote);
            if (prodQueso != null)
            {
                cantidades.Add("kilos", prodQueso.Kilos_Reales.Value != 0 ? prodQueso.Kilos_Reales.Value : 0);
                cantidades.Add("piezas", prodQueso.Piezas_Obtenidas != null ? prodQueso.Piezas_Obtenidas.Value : 0);

                return cantidades;
            }
            var prodLechePolvo = _ProdLechePolvo.SingleOrDefault((p) => p.Lote == lote);
            if (prodLechePolvo != null)
            {
                cantidades.Add("kilos",  prodLechePolvo.Kilos_Reales.Value != 0 ? prodLechePolvo.Kilos_Reales.Value : 0);
                cantidades.Add("piezas", prodLechePolvo.Piezas_Obtenidas);

                return cantidades;
            }
            var prodQuesoFundido = _ProdQuesosFundido.SingleOrDefault(p => p.Lote == lote);
            if (prodQuesoFundido != null)
            {
                var kilos = prodQuesoFundido.Kilos_Reales.Value != 0 ? prodQuesoFundido.Kilos_Reales.Value : 0;
                cantidades.Add("kilos", kilos);
                cantidades.Add("piezas", prodQuesoFundido.Piezas_Obtenidas);

                return cantidades;
            }
            var prodRicota = _ProdRicota.SingleOrDefault((p) => p.Lote == lote);
            if (prodRicota != null)
            {
                cantidades.Add("kilos", prodRicota.Kilos_Reales.HasValue ? prodRicota.Kilos_Reales.Value : 0);
                cantidades.Add("piezas", prodRicota.Piezas_Obtenidas.HasValue ? prodRicota.Piezas_Obtenidas.Value : 0);

                return cantidades;
            }

            var prodCrema = _ProdCrema.SingleOrDefault((p) => p.Lote == lote);
            if (prodCrema != null)
            {
                cantidades.Add("kilos", prodCrema.Litros_Reales.HasValue ? prodCrema.Litros_Reales.Value : 0);
                cantidades.Add("piezas", prodCrema.Piezas_Obtenidas.HasValue ? prodCrema.Piezas_Obtenidas.Value : 0);

                return cantidades;
            }
            
            var prodMuzza = _ProdMuzza.SingleOrDefault((p) => p.Lote == lote);
            if (prodMuzza != null)
            {
                cantidades.Add("kilos", prodMuzza.Kilos_Reales.HasValue ? prodMuzza.Kilos_Reales.Value : 0);
                cantidades.Add("piezas", prodMuzza.Piezas_Obtenidas.HasValue ? prodMuzza.Piezas_Obtenidas.Value : 0);

                return cantidades;
            }

            var prodDulceLeche = _ProdDulceLeche.SingleOrDefault((p) => p.Lote == lote);
            if (prodDulceLeche != null)
            {
                cantidades.Add("kilos", prodDulceLeche.Kilos_Reales.HasValue ? prodDulceLeche.Kilos_Reales.Value : 0);
                cantidades.Add("piezas", prodDulceLeche.Litros.HasValue ? prodDulceLeche.Litros.Value : 0);

                return cantidades;
            }

            throw new NotSupportedException("No se encontro produccion");
        }
        [Obsolete("Replaced with Database first approach services.")]
        public string ObtenerTipoProduccion(string lote)
        {
            var prodQueso = _ProdQuesos.SingleOrDefault((p) => p.Lote == lote);
            var prodQuesoFundido = _ProdQuesosFundido.SingleOrDefault((p) => p.Lote == lote);
            var prodLechePolvo = _ProdLechePolvo.SingleOrDefault((p) => p.Lote == lote);
            if (prodQueso != null || prodQuesoFundido != null || prodLechePolvo != null)
                return "queso";

            var prodRicota = _ProdRicota.SingleOrDefault((p) => p.Lote == lote);
            if (prodRicota != null)
                return "ricota";

            var prodCrema = _ProdCrema.SingleOrDefault((p) => p.Lote == lote);
            if (prodCrema != null)
                return "crema";

            var prodMuzza = _ProdMuzza.SingleOrDefault((p) => p.Lote == lote);
            if (prodMuzza != null)
                return "muzzarella";

            var prodDulceLeche = _ProdDulceLeche.SingleOrDefault((p) => p.Lote == lote);
            if (prodDulceLeche != null)
                return "dulceleche";

            throw new NotSupportedException("No se encontro produccion");
        }
        public void PasarProduccionHistorica(string lote)
        {
            var prodQueso = _ProdQuesos.SingleOrDefault((pq) => pq.Lote == lote);
            var prodQuesoFundido = _ProdQuesosFundido.SingleOrDefault((pqf) => pqf.Lote == lote);
            var prodRicota = _ProdRicota.SingleOrDefault((pr) => pr.Lote == lote);
            var prodLechePolvo = _ProdLechePolvo.SingleOrDefault((plp) => plp.Lote == lote);
            var prodDDL = _ProdDulceLeche.SingleOrDefault((pdl) => pdl.Lote == lote);
            if (prodQueso != null)
                prodQueso.Historico = true;
            else if (prodQuesoFundido != null)
                prodQuesoFundido.Historico = true;
            else if (prodRicota != null)
                prodRicota.Produccion.Historico = true;
            else if (prodDDL != null)
                prodDDL.Produccion.Historico = true;
            else if (prodLechePolvo != null)
                prodLechePolvo.Historico = true;

            var prod = _Prod.SingleOrDefault((p) => p.Lote == lote);
            if (prod != null)
                prod.Historico = true;

            _Contexto.SaveChanges();
        }

        public int ObtenerFabrica(string lote)
        {
            int? codigo = null;
            var prod = _Prod.SingleOrDefault((p) => p.Lote == lote);
            if (prod != null)
                codigo = prod.Codigo_Fabrica;

            return codigo.HasValue ? codigo.Value : -1;
        }


        /// <summary>
        /// Suma los Restanes_Envasados y los Detalle_Envasado de un lote.
        /// </summary>
        /// <param name="Lote">Lote del que se quiere saber las Cantidades</param>
        /// <returns>Diccionario con las claves: "kilos" y "piezas"</returns>
        public  Dictionary<string, decimal> ObtenerCantidadesEnvasadas(string Lote = "")
        {
            Dictionary<string, decimal> cantidades = new Dictionary<string, decimal>();
            decimal kilosEnvasados = 0;
            decimal piezasEnvasadas = 0;
            foreach (var env in this._Envasado.Where((e) => e.Lote == Lote))
            {
                foreach (var restante in env.Restantes_Envasados)
                {
                    kilosEnvasados += restante.Kilos;
                    piezasEnvasadas += restante.Hormas;
                }
                foreach (var detalle in env.Detalle_Envasado.ToList())
                {
                    
                    kilosEnvasados += detalle.Kilos.HasValue ? detalle.Kilos.Value : 0;
                    var hormas_detalle = detalle.Hormas.HasValue ? detalle.Hormas.Value : 0;
                    if (hormas_detalle != 0)
                        piezasEnvasadas += hormas_detalle;
                    else
                    {
                        var obtenidasPorEnvasada = detalle.Formulas_Envasado.Obtenidas_Por_Envasada.HasValue ? detalle.Formulas_Envasado.Obtenidas_Por_Envasada.Value : 1;
                        piezasEnvasadas += (detalle.Cantidad.HasValue ? detalle.Cantidad.Value : 0) / obtenidasPorEnvasada;
                    }
                }

            }
            cantidades.Add("kilos", kilosEnvasados);
            cantidades.Add("piezas", piezasEnvasadas);

            return cantidades;
        }

        public bool ExisteProduccion(string lote)
        {
            var prodQueso = _ProdQuesos.SingleOrDefault((p) => p.Lote == lote);
            if (prodQueso != null)
                return true;

            var prodRicota = _ProdRicota.SingleOrDefault((p) => p.Lote == lote);
            if (prodRicota != null)
                return true;

            var prodCrema = _ProdCrema.SingleOrDefault((p) => p.Lote == lote);
            if (prodCrema != null)
                return true;

            var prodMuzza = _ProdMuzza.SingleOrDefault((p) => p.Lote == lote);
            if (prodMuzza != null)
                return true;

            var prodDulceLeche = _ProdDulceLeche.SingleOrDefault((p) => p.Lote == lote);
            if (prodDulceLeche != null)
                return true;

            return false;
        }
    }
}
