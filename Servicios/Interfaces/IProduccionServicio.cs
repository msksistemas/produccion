﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface IProduccionServicio
    {
        void PasarProduccionHistorica(string lote);
        Dictionary<string, decimal> ObtenerCantidadesReales(string lote);
        int ObtenerFabrica(string lote);
        Dictionary<string, decimal> ObtenerCantidadesEnvasadas(string Lote = "");
        string ObtenerTipoProduccion(string lote);
        bool ExisteProduccion(string lote);
    }
}
