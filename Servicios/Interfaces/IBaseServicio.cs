﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Servicios.Interfaces
{
    public interface IBaseServicio<TEntity> where TEntity : class
    {
        void Borrar(TEntity entidad);
        IList<TEntity> Obtener(
            Expression<Func<TEntity, bool>> filtro = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");
        IList<TEntity> ObtenerConRawSql(string query,
            params object[] parameters);
        void Agregar(TEntity entidad);
        void Editar(TEntity entidad);
        void EjecutarSQL(string query, params object[] parametros);
    }
}
