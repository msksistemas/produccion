﻿using AccesoDatos.Contexto.Produccion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface IBandejaServicio
    {
        void Agregar(MovimientosBandejas movimientosBandejas);
        void Editar(int CodigoMovimiento, DateTime fecha, int origen, int destino,string prefijo,string nroComprobante, List<DetallesMovimientosBandejas> detalles);
        void BorrarPorId(int idMovimiento);
        void BorrarPorIdExpedicion(int idExpedicion);
        List<MovimientosBandejas> Obtener(bool Estado);
        MovimientosBandejas ObtenerPorId(int id);
        List<DetallesMovimientosBandejas> ObtenerDetallesPorIdExpedicion(int idExpedicion);
        List<DetallesMovimientosBandejas> ObtenerDetallesPorId(int idMovimiento);
        void EditarMovimientoExpedicion(int codigoInternoExpedicion, int nuevoDestino, List<DetallesMovimientosBandejas> detalles);
        void PasarHistorico(int CodigoMovimiento = 0, int CodigoInternoExpedicion = 0);
        bool ExisteMovimiento(int codigoMovimiento, string prefijo, string nroComprobante);
    }
}
