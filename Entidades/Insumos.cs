//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_Produccion.ContextoProduccion
{
    using System;
    using System.Collections.Generic;
    
    public partial class Insumos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Insumos()
        {
            this.Ajustes_Insumos = new HashSet<Ajustes_Insumos>();
            this.Detalle_Compra = new HashSet<Detalle_Compra>();
            this.Detalle_Formula = new HashSet<Detalle_Formula>();
            this.Detalle_Formula_Envasado = new HashSet<Detalle_Formula_Envasado>();
            this.Detalle_Orden_Compra = new HashSet<Detalle_Orden_Compra>();
            this.Productos = new HashSet<Productos>();
            this.Productos1 = new HashSet<Productos>();
        }
    
        public int Codigo_Interno_Insumos { get; set; }
        public string Codigo_Insumo { get; set; }
        public string Codigo_Insumo_Proveedor { get; set; }
        public string Descripcion { get; set; }
        public string Descripcion_Larga { get; set; }
        public Nullable<int> Codigo_Linea { get; set; }
        public Nullable<int> Codigo_Rubro { get; set; }
        public string Comentario { get; set; }
        public Nullable<int> Codigo_Proveedor { get; set; }
        public Nullable<int> Codigo_Proveedor_Alternativo { get; set; }
        public Nullable<int> Codigo_Ultimo_Proveedor { get; set; }
        public Nullable<int> Codigo_Iva { get; set; }
        public Nullable<int> Codigo_Impuesto_Interno { get; set; }
        public string Codigo_Imputacion { get; set; }
        public string Unidad_Medida { get; set; }
        public Nullable<System.DateTime> Fecha_Ultima_Compra { get; set; }
        public Nullable<decimal> Precio_Ultima_Compra { get; set; }
        public Nullable<decimal> Precio_Lista { get; set; }
        public Nullable<decimal> Punto_Pedido { get; set; }
        public Nullable<bool> Activo { get; set; }
        public string Precio_Por { get; set; }
        public string Moneda { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ajustes_Insumos> Ajustes_Insumos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Compra> Detalle_Compra { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Formula> Detalle_Formula { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Formula_Envasado> Detalle_Formula_Envasado { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Orden_Compra> Detalle_Orden_Compra { get; set; }
        public virtual Insumos Insumos1 { get; set; }
        public virtual Insumos Insumos2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Productos> Productos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Productos> Productos1 { get; set; }
    }
}
