﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using AccesoDatos.Contexto.Produccion;
using Servicios.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;

namespace MVC_Produccion.Models
{
    public class HttpService
    {
        public enum ProcesoProduccion
        {
            Creacion,
            Edicion,
            Eliminacion
        }

        //Conexión con app de Zulu para ajustes de stock en San Satur
        private readonly HttpClient _httpClient;
        private static string errorMessage = "No se pudo realizar el ajuste de stock en Zulu, intente nuevamente.";
        private static readonly string baseUrl = "https://rdweb.zulu.com.ar";

        private IBaseServicio<Insumos> InsumosServicio;
        private IBaseServicio<Formulas_Envasado> FormulasEnvasadoServicio;
        ContextoProduccion.Entities entities = new ContextoProduccion.Entities();

        public HttpService(IBaseServicio<Insumos> insumosServicio,
                            IBaseServicio<Formulas_Envasado> formulasEnvasadoServicio,
                            HttpClient httpClient)
        {
            this.InsumosServicio = insumosServicio;
            this.FormulasEnvasadoServicio = formulasEnvasadoServicio;
            _httpClient = httpClient;
        }

        public bool EsSanSatur()
        {
            ContextoProduccion.Cadena_Conexion cadena = entities.Cadena_Conexion.FirstOrDefault();
            if (cadena.Servidor == "SSSRV04\\MSK") //Comprueba si la empresa actual es San Satur
                return true;
            else
                return false;
        }

        public async Task<HttpResponseMessage> PostData(List<MovimientoStockZulu> data, string endpoint = "/sansaturAPI/Stock")
        {
            if(data.Count == 0)
                return new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "No se pudo realizar el ajuste de stock en Zulu ya que el detalle es inválido."
                };
            StringContent body = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _httpClient.PostAsync(baseUrl + endpoint, body);

            return new HttpResponseMessage(response.StatusCode)
            {
                ReasonPhrase = errorMessage + JsonConvert.SerializeObject(data)
            };
        }

        public string FechaComprobante(DateTime fechaComprob)
        {
            DateTime fechaActual = DateTime.Now;
            return new DateTime(fechaComprob.Year, fechaComprob.Month, fechaComprob.Day, fechaActual.Hour, fechaActual.Minute, fechaActual.Second, 
                DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
        }

        public async Task<HttpResponseMessage> AjusteStockProduccion(List<Detalle_Produccion> detalleProduccion, ProcesoProduccion proceso, 
            DateTime fechaComprobante, string numeroProducto = "", List<Detalle_Produccion> detalleAnterior = null)
        {
            try {
                HttpResponseMessage response = new HttpResponseMessage();
                MovimientoStockZulu movimiento = new MovimientoStockZulu(FechaComprobante(fechaComprobante));

                switch (proceso)
                {
                    case ProcesoProduccion.Creacion:
                        movimiento.detalle = CrearCuerpoDetalleProduccion(detalleProduccion);
                        movimiento.codigoItemHeadID = numeroProducto.Trim();
                        movimiento.observaciones = $"Producción de producto {numeroProducto.Trim()}";
                        return await PostData(new List<MovimientoStockZulu> { movimiento });

                    //case ProcesoProduccion.Edicion:
                    //    if(detalleAnterior == null)
                    //        return new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                    //        {
                    //            ReasonPhrase = "No se pudo realizar el ajuste de stock en Zulu ya que el detalle es inválido."
                    //        };
                    //    response = await AjusteStockInsumos(
                    //            detalleMovimiento: CrearCuerpoDetalleProduccion(detalleAnterior),
                    //            fechaComprobante: DateTime.Now,
                    //            tipoAjuste: 1);
                    //    if(!response.IsSuccessStatusCode)
                    //        return new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                    //        {
                    //            ReasonPhrase = errorMessage
                    //        };

                    //    return await AjusteStockProduccion(
                    //            detalleProduccion: detalleProduccion,
                    //            proceso: ProcesoProduccion.Creacion,
                    //            fechaComprobante: fechaComprobante,
                    //            numeroProducto: numeroProducto);
                    //    break;

                    //case ProcesoProduccion.Eliminacion:
                    //    response = await AjusteStockInsumos(
                    //            detalleMovimiento: CrearCuerpoDetalleProduccion(detalleProduccion),
                    //            fechaComprobante: DateTime.Now,
                    //            tipoAjuste: 1);
                    //    break;
                }
                return response;
            } catch (Exception e)
            {
                return new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = errorMessage
                };
            }
        }

        public List<DetalleMovimientoStock> CrearCuerpoDetalleProduccion(List<Detalle_Produccion> detalleProd)
        {
            List<DetalleMovimientoStock> detallesInsumos = new List<DetalleMovimientoStock>();

            foreach (Detalle_Produccion detalle in detalleProd)
            {
                if((detalle.Cantidad != null && detalle.Cantidad != 0) || (detalle.Ajuste != null && detalle.Ajuste != 0))
                    detallesInsumos.Add(new DetalleMovimientoStock(
                        detalle.Codigo_Insumo.Trim(),
                        (float)((detalle.Ajuste != null && detalle.Ajuste != 0) ? detalle.Ajuste : detalle.Cantidad),
                        0,
                        InsumosServicio.Obtener((i) => i.Codigo_Insumo == detalle.Codigo_Insumo).FirstOrDefault().Reproceso ? 1 : 0
                    ));
            }

            return detallesInsumos;
        }

        /// <summary>Crea el cuerpo de la request que se enviará a la API de Zulu.</summary>
        /// <param name="fechaComprobante">Fecha del movimiento, se utilizará siempre la que carga el usuario y no la actual.</param>
        /// <param name="observaciones">Observaciones del movimiento, explica que movimiento se está realizando.</param>
        /// <param name="detalle">Detalle del movimiento, en caso de ser una liberación llevará los datos del producto de ventas, 
        /// en caso de un ajuste de insumos llevará los datos de los insumos.</param>
        /// <param name="codigoItemHeadID">Código del producto que se produce, si el movimiento no es una producción debe ir vacío.</param>
        /// <param name="tipoajuste">Determina si el movimiento es de ajuste positivo (1) o negativo (2).</param>
        /// <param name="tipoDescarte">Determina que tipo de descarte se realiza, si es normal (0), si es reproceso (1) o si es scrap/desecho (2).</param>
        /// <returns>Un objeto con el formato que necesita la request para funcionar correctamente.</returns>
        public object CreateRequestBody(DateTime fechaComprobante, string observaciones, List<object> detalle, 
            string codigoItemHeadID = "", int tipoajuste = 1, int tipoDescarte = 0)
        {
            return new {
                tipoajuste,
                codigoItemHeadID,
                fechaComprobante = FechaComprobante(fechaComprobante),
                idUsuario = 130,
                fN_pfac_id = 7,
                tipoDescarte,
                observaciones,
                key = "5a2b2e4c-e683-482f-9abf-e0de126ebc79",
                detalle
                //detalle = new[]
                //{
                //    new
                //    {
                //        codigoItemDetail = "154",
                //        reproceso = 0,
                //        observaciones = "Ajuste por interfaz de MSK",
                //        cantidad = 1.0,
                //        bultos = 1
                //    }
                //}
            };
            //return new[]
            //{
            //    new {
            //        tipoajuste,
            //        codigoItemHeadID,
            //        fechaComprobante = FechaComprobante(fechaComprobante),
            //        idUsuario = 130,
            //        fN_pfac_id = 7,
            //        tipoDescarte,
            //        observaciones,
            //        key = "5a2b2e4c-e683-482f-9abf-e0de126ebc79",
            //        detalle
            //        //detalle = new[]
            //        //{
            //        //    new
            //        //    {
            //        //        codigoItemDetail = "154",
            //        //        reproceso = 0,
            //        //        observaciones = "Ajuste por interfaz de MSK",
            //        //        cantidad = 1.0,
            //        //        bultos = 1
            //        //    }
            //        //}
            //    }
            //};
        }
    }
}