﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Produccion.Models.Utilidades
{
    public class Desglose_Produccion
    {
        public string Numero_Producto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Kilos { get; set; }
        public string Lote { get; set; }
    }
}