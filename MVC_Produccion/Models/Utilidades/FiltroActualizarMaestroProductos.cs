﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Produccion.Models.Utilidades
{
    public class FiltroActualizarMaestroProductos
    {
        public DateTime FechaDesde1 { get; set; }
        public DateTime FechaHasta1 { get; set; }
        public DateTime FechaDesde2 { get; set; }
        public DateTime FechaHasta2 { get; set; }
    }
}