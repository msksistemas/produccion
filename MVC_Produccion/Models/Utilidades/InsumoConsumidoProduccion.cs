﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_Produccion.Models.Utilidades
{
    public class InsumoConsumidoProduccion
    {
        public string CodigoInsumo { get; set; }
        public string DescripcionInsumo { get; set; }
        public string PrecioPor { get; set; }
        public decimal Cantidad { get; set; }
        public decimal? Cantidad_Unidades { get; set; }
        public decimal PrecioUnitario { get; set; }

        public decimal Ajuste { get; set; }

        public string Lote { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }
        [Column(TypeName = "date")]
        public DateTime? Fecha_Vencimiento { get; set; }

        public decimal? Ph { get; set; }
        public decimal? Humedad { get; set; }
        public decimal? Rendimiento { get; set; }

        public bool EsMasa { get; set; }
        public bool EsLeche { get; set; }
    }
}