﻿using System.Collections.Generic;

namespace MVC_Produccion.Models
{
    public class MovimientoStockZulu
    {
        public int tipoajuste { get; set; }
        public string codigoItemHeadID { get; set; }
        public string fechaComprobante { get; set; }
        public int idUsuario { get; } = 130;
        public int fN_pfac_id { get; } = 7;
        public int tipoDescarte { get; set; }
        public string observaciones { get; set; }
        public string key { get; } = "5a2b2e4c-e683-482f-9abf-e0de126ebc79";
        public List<DetalleMovimientoStock> detalle { get; set; }

        public MovimientoStockZulu(
            string fechaComprobante, 
            string codigoItemHeadID = "", 
            string observaciones = "",
            int tipoajuste = 1
            )
        {
            this.tipoajuste = tipoajuste;
            this.codigoItemHeadID = codigoItemHeadID;
            this.fechaComprobante = fechaComprobante;
            this.observaciones = observaciones;
        }

        public MovimientoStockZulu(
            string fechaComprobante,
            List<DetalleMovimientoStock> detalle,
            string codigoItemHeadID = "",
            string observaciones = "",
            int tipoajuste = 1,
            int tipoDescarte = 0
            )
        {
            this.tipoajuste = tipoajuste;
            this.tipoDescarte = tipoDescarte;
            this.codigoItemHeadID = codigoItemHeadID;
            this.fechaComprobante = fechaComprobante;
            this.observaciones = observaciones;
            this.detalle = detalle;
        }
    }

    public class DetalleMovimientoStock
    {
        public string codigoItemDetail { get; set; }
        public int reproceso { get; set; }
        public string observaciones { get; set; }
        public float cantidad { get; set; }
        public int bultos { get; set; }

        public DetalleMovimientoStock(
            string codigoItemDetail, 
            float cantidad, 
            int bultos,
            int reproceso = 0, 
            string observaciones = "Ajuste por interfaz de MSK")
        {
            this.codigoItemDetail = codigoItemDetail;
            this.reproceso = reproceso;
            this.observaciones = observaciones;
            this.cantidad = cantidad;
            this.bultos = bultos;
        }
    }
}
