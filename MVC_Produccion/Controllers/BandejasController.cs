﻿using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace MVC_Produccion.Controllers
{
    public class BandejasController : Controller
    {
        private IBandejaServicio BandejaServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoPermisos;

        private OpcionesTrabajo Configuraciones;

        public BandejasController(IBandejaServicio _bandejaServicio, IBaseServicio<OpcionesTrabajo> _opcionesTrabajo)
        {
            this.OpcionesTrabajoPermisos = _opcionesTrabajo;
            this.BandejaServicio = _bandejaServicio;
            this.Configuraciones = this.OpcionesTrabajoPermisos.Obtener().FirstOrDefault();

        }
        // GET: Bandejas
        public ActionResult Index()
        {
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="No Historica", Value="false"},
                                new SelectListItem() {Text="Historica", Value="true"}
                            };

            ViewBag.ListadoEstados = ListadoEstados;
            return View();
        }
        public ActionResult ObtenerInformacion(bool Historicas = false)
        {
            var lista = this.BandejaServicio.Obtener(Historicas);
            return Json(new { data = lista }, JsonRequestBehavior.AllowGet);
        }
       
        // GET: Bandejas/Create
        public ActionResult AddOrEdit(int idMovimiento)
        {
            MovimientosBandejas mov = new MovimientosBandejas() {Fecha = DateTime.Today };
            try
            {
                if (idMovimiento != 0)
                    mov = this.BandejaServicio.ObtenerPorId(idMovimiento);
            }
            catch (Exception)
            {

            }
            return View(mov);
        }
        [HttpPost]
        public ActionResult AddOrEdit(MovimientosBandejas movimiento)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) 
            {             
                try
                {
                    if (this.BandejaServicio.ExisteMovimiento(movimiento.CodigoMovimiento,movimiento.Prefijo,movimiento.NroComprobante))
                        return Json(new { success = false, mensaje ="Combinacion de Prefijo y Comprobante ya cargada."}, JsonRequestBehavior.AllowGet);

                    var mensaje = "Se agrego correctamente";
                    if (movimiento.Detalles == null)
                    {
                        mensaje = "Por favor, agregue un Insumo";
                        return Json(new { success = false, mensaje }, JsonRequestBehavior.AllowGet);
                    }
                    if (movimiento.CodigoMovimiento == 0)
                        this.BandejaServicio.Agregar(movimiento);
                    else
                    {
                        foreach (var item in movimiento.Detalles)
                        {
                            item.CodigoMovimiento = movimiento.CodigoMovimiento;
                        }
                        this.BandejaServicio.Editar(movimiento.CodigoMovimiento,movimiento.Fecha,movimiento.Origen,movimiento.Destino.Value,movimiento.Prefijo, movimiento.NroComprobante, movimiento.Detalles.ToList());
                        mensaje = "Se edito Correctamente";

                    }
                    scope.Complete();
                    return Json(new { success= true,mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = ex.Message}, JsonRequestBehavior.AllowGet);
                }
            }
        } 

    
        public ActionResult Delete(int id)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    // TODO: Add delete logic here
                    this.BandejaServicio.BorrarPorId(id);
                    scope.Complete();
                    return Json(new { success = true}, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public ActionResult Actualizar(List<int> codigos)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (var c in codigos)
                    {
                        this.BandejaServicio.PasarHistorico(c);
                    }
                    scope.Complete();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public ActionResult TablaBandejas(string parametroPost, int codigoExpedicion = 0, int codigoMovimiento = 0,bool soloLectura = false)
        {
            ViewBag.soloLectura = soloLectura;
            List<DetallesMovimientosBandejas> lista;
            if (codigoExpedicion != 0)
                lista = this.BandejaServicio.ObtenerDetallesPorIdExpedicion(codigoExpedicion);
            else
                lista = this.BandejaServicio.ObtenerDetallesPorId(codigoMovimiento);

            ViewBag.parametroPost = parametroPost;

            return PartialView(lista);
        }
    }
}
