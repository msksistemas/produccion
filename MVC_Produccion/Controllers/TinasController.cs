﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class TinasController : Controller
    {
        private IBaseServicio<Tinas> TinasServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<Fabricas> FabricasServicio;

        private OpcionesTrabajo Configuraciones;

        public TinasController( IBaseServicio<Tinas> tinasServicio,
                                IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                IBaseServicio<Permisos> permisosServicios,
                                IBaseServicio<Fabricas> fabricasServicios)
        {
            this.TinasServicio = tinasServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.PermisosServicio = permisosServicios;
            this.FabricasServicio = fabricasServicios;

            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            var listado = this.TinasServicio.Obtener().Select(t => new
            {
                Codigo_Fabrica = t.Codigo_Fabrica,
                CodigoTina = t.CodigoTina,
                LitrosTina = t.LitrosTina,
            });

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0, int fabrica = 0)
        {
            if (id == 0 || fabrica == 0)
                return View(new Tinas());
            else
                return View(this.TinasServicio.Obtener((x) => x.CodigoTina == id && x.Codigo_Fabrica == fabrica).FirstOrDefault<Tinas>());
        }

        [HttpPost]
        public ActionResult AddOrEdit(int Codigo_Fabrica, int CodigoTina, int LitrosTina)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var existe = this.TinasServicio.Obtener((e) => e.CodigoTina == CodigoTina && e.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault();
                    string mensaje = "Registro guardado satisfactoriamente.";
                    if (existe == null)
                    {
                        existe = new Tinas();
                        existe.Codigo_Fabrica = Codigo_Fabrica;
                        existe.CodigoTina = CodigoTina;
                        existe.LitrosTina = LitrosTina;
                        if (this.FabricasServicio.Obtener((f)=> f.Codigo_Fabrica == Codigo_Fabrica).SingleOrDefault() == null)
                            return Json(new { success = false, message = "Porfavor, verifique que exista la fabrica especificada." }, JsonRequestBehavior.AllowGet);

                        this.TinasServicio.Agregar(existe);
                    }
                    else
                    {
                        existe.LitrosTina = LitrosTina;
                        mensaje = "Registro modificado satisfactoriamente.";
                        this.TinasServicio.Editar(existe);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpGet]
        public JsonResult Detalle(int CodigoTina, int Codigo_Fabrica)
        {
            var tina = TinasServicio.Obtener((t) => t.CodigoTina == CodigoTina && t.Codigo_Fabrica == Codigo_Fabrica).SingleOrDefault();

            if (tina != null)
                return Json(new { data = new { tina.Codigo_Fabrica, tina.CodigoTina, tina.LitrosTina }, success = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = 1;
                var tinas = this.TinasServicio.Obtener();
                if (tinas.Count != 0)
                    ultimoRegistro = tinas.Max(t => t.CodigoTina);

                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int CodigoTina = 0, int Codigo_Fabrica = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Tinas est = this.TinasServicio.Obtener((x) => x.CodigoTina == CodigoTina && x.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault<Tinas>();
                    this.TinasServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }

            }
        }
    }
}
