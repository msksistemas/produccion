﻿using AccesoDatos.Contexto.Ventas;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace MVC_Produccion.Controllers
{
    public class LocalidadesController : Controller
    {
        private IBaseServicio<Localidades> LocalidadesServicio;
        private IBaseServicio<Provincias> ProvinciasServicio;

        public LocalidadesController(IBaseServicio<Localidades> localidadesServicio,
                                    IBaseServicio<Provincias> provinciasServicio)
        {
            this.ProvinciasServicio = provinciasServicio;
            this.LocalidadesServicio = localidadesServicio;
        }

        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ObtenerProvincias()
        {
            var listado = this.ProvinciasServicio.Obtener().Select(p => new {
                Nro_Provincia =  p.Nro_Provincia,
                Descripcion =  p.Descripcion
            });

            return Json(new
            {
               listado =  listado
            }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Obtener()
        {
            var listado = this.LocalidadesServicio.Obtener().Select(l => new {
                l.CODIGO,
                l.DESCRIPCION,
                l.PREFIJO,
                l.PROVINCIA
            });

            return Json(new
            {
                success = true,
                mensaje = "",
                listado = listado
            }, JsonRequestBehavior.AllowGet);
        }

        // GET: Localidades/Create
        public ActionResult addOrEdit(string codigo = "")
        {
            bool agregar = false;
            var localidad = this.LocalidadesServicio.Obtener((l) => l.CODIGO == codigo).SingleOrDefault();
            if (localidad == null)
            {
                localidad = new Localidades() {CODIGO = (this.LocalidadesServicio.Obtener().Count + 1).ToString() };
                agregar = true;
            }
            ViewBag.agregar = agregar;

            return View(localidad);
        }

        // POST: Localidades/Create
        [HttpPost]
        public ActionResult addOrEdit(Localidades model,bool agregar)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    if (model.PROVINCIA != null && model.PROVINCIA != "")
                        model.PROVINCIA = model.PROVINCIA.Trim();
                    if (agregar)
                        this.LocalidadesServicio.Agregar(model);
                    else
                        this.LocalidadesServicio.Editar(model);
                    scope.Complete();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.agregar = agregar;
                    ModelState.AddModelError("", ex.Message);

                    return View(model);
                }
            }
        }

        // POST: Localidades/Delete/5
        [HttpPost]
        public ActionResult Delete(string codigo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var elminar = this.LocalidadesServicio.Obtener((l) => l.CODIGO == codigo).SingleOrDefault();
                    if (elminar != null)
                    {
                        this.LocalidadesServicio.Borrar(elminar);
                        scope.Complete();
                        return Json(new
                        {
                            success = true,
                            message = "Se elimino Correctamente",
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new
                        {
                            success = false,
                            message = "No se encontro Localidad",
                        }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        success = false,
                        message = ex.Message,
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
