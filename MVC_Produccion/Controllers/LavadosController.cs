﻿using MVC_Produccion.Sesiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class LavadosController : Controller
    {
        private IBaseServicio<Lavados> LavadosServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private OpcionesTrabajo Configuraciones;

        public LavadosController(IBaseServicio<Lavados> lavadosServicio,
                                  IBaseServicio<Permisos> permisosServicio,
                                  IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio)
        {
            this.LavadosServicio = lavadosServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;

            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Activo", Value="true"},
                new SelectListItem() {Text="Inactivo", Value="false"}
            };
            ViewBag.ListadoEstados = ListadoEstados;
            return View();
        }

        public ActionResult GetOne(int Codigo)
        {
            var productoNuevo = this.LavadosServicio.Obtener(x => x.Codigo == Codigo).ToList();
            if (productoNuevo.Count == 0)
                return Json(new { message = "Error, no existe un registro con el Código ingresado.", success = false }, JsonRequestBehavior.AllowGet);

            var name = productoNuevo.FirstOrDefault().Descripcion;
            return Json(new { data = name, success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetData(int codigo = 0, bool estado = true, bool sin_formula = false)
        {
            if (sin_formula)
            {
                var lavados_sin_formulas = this.LavadosServicio.Obtener((p) => p.Lavados_Formula.Count == 0 && p.Activo == estado).Select(l => new
                {
                    l.Activo,
                    l.Codigo,
                    l.Descripcion
                }).ToList();

                return Json(new { data = lavados_sin_formulas, success = true }, JsonRequestBehavior.AllowGet);

            }

            var lista = this.LavadosServicio.Obtener((p) => (p.Codigo == codigo || codigo == 0) && p.Activo == estado).Select(l => new
            {
                l.Activo,
                l.Codigo,
                l.Descripcion
            }).ToList();

            return Json(new { data = lista, success = true }, JsonRequestBehavior.AllowGet);
        }


        // GET: Lavados/Create
        public ActionResult AddOrEdit(int codigo = 0)
        {
            var editar = this.LavadosServicio.Obtener((l) => l.Codigo == codigo).SingleOrDefault();
            bool agregar = false;
            if (editar == null)
            {
                var maximo = 1;
                if (this.LavadosServicio.Obtener().Count != 0)
                    maximo = this.LavadosServicio.Obtener().Max(l => l.Codigo) + 1;
                editar = new Lavados() { Codigo = maximo };
                agregar = true;
            }
            ViewBag.agregar = agregar;
            return View(editar);
        }

        // POST: Lavados/Create
        [HttpPost]
        public ActionResult AddOrEdit(Lavados lavado,bool agregar)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    lavado.Activo = true;
                    var mensaje = "Se Guardo Correctamente.";

                    if (agregar)
                        this.LavadosServicio.Agregar(lavado);
                    else
                    { 
                       mensaje = "Se Edito Correctamente."; 
                       this.LavadosServicio.Editar(lavado);
                        return Json(new { message = mensaje, success = true }, JsonRequestBehavior.AllowGet);

                    }
                    scope.Complete();
                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);

                    return View(lavado);
                }

            }
        }



        // POST: Lavados/Delete/5
        [HttpPost]
        public ActionResult Delete(int codigo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var eliminar =this.LavadosServicio.Obtener((l) => l.Codigo == codigo).SingleOrDefault();
                    if (eliminar.Lavados_Formula.Count > 0)
                        return Json(new { success = false, message = "No se puede eliminar un lavado que tenga Formula" }, JsonRequestBehavior.AllowGet);

                    this.LavadosServicio.Borrar(eliminar);
                    scope.Complete();
                    return Json(new { success = true, message = "eliminado correctamente" }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        public ActionResult CambiarEstado(int codigo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var lavado = this.LavadosServicio.Obtener((l) => l.Codigo == codigo).SingleOrDefault();
                lavado.Activo = !lavado.Activo;

                this.LavadosServicio.Editar(lavado);

                scope.Complete();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
