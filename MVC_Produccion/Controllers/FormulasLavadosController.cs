﻿using MVC_Produccion.Sesiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion.SP;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class FormulasLavadosController : Controller
    {
        private IBaseServicio<Detalle_Formula_Lavados> DetalleFormulaLavadoServicio;
        private IBaseServicio<Costos_Indirectos_Formula_Lavados> CostosIndirectosLavadoServicio;
        private IBaseServicio<Lavados_Formula> LavadosFormulaServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Monedas> MonedasServicio;
        private OpcionesTrabajo Configuraciones;

        public FormulasLavadosController(IBaseServicio<Detalle_Formula_Lavados> detalleFormulaLavadoServicio,
                                        IBaseServicio<Costos_Indirectos_Formula_Lavados> costosIndirectosLavadosServicio,
                                        IBaseServicio<Lavados_Formula> lavadosFormulaServicio,
                                        IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                        IBaseServicio<Permisos> permisosServicio,
                                        IBaseServicio<Monedas> monedasServicio)
        {
            this.DetalleFormulaLavadoServicio = detalleFormulaLavadoServicio;
            this.CostosIndirectosLavadoServicio = costosIndirectosLavadosServicio;
            this.LavadosFormulaServicio = lavadosFormulaServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.PermisosServicio = permisosServicio;
            this.MonedasServicio = monedasServicio;

            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }
        private dbProdSP db = new dbProdSP();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData(int codigo = 0)
        {
            if (codigo == 0)
            {
                var lista = this.LavadosFormulaServicio.Obtener((lf) => lf.Lavados.Activo).Select(lf => new
                {
                    lf.Codigo,
                    lf.Comentario,
                    Codigo_Lavado = lf.Lavados.Codigo,
                    Descripcion_Lavado = lf.Lavados.Descripcion
                }).ToList();

                return Json(new { data = lista, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var formula = this.LavadosFormulaServicio.Obtener((f) => f.Codigo == codigo).SingleOrDefault();
                if (formula == null)
                {
                    return Json(new { success = false, message = "Formula no Existe" }, JsonRequestBehavior.AllowGet);

                }
                var desc_formula = formula.Lavados.Descripcion;
                var lista = formula.Detalle_Formula_Lavados.Select(dfl => new
                {
                    dfl.Codigo,
                    dfl.Codigo_Insumo,
                    dfl.kilos_litros,
                    dfl.cantidad,
                    Descripcion_Insumo = dfl.Insumos.Descripcion,
                    Precio_Por = dfl.Insumos.Precio_Por
                }).ToList();

                return Json(new { data = lista, success = true, DescFormula = desc_formula }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetCostosIndirectos(int Codigo_Lavado = 0)
        {
            if (Codigo_Lavado == 0)
            {
                List<Detalle_Formula_Lavados> list = new List<Detalle_Formula_Lavados>();
                return Json(new { data = list, success = true }, JsonRequestBehavior.AllowGet);
            }
            var listado = this.CostosIndirectosLavadoServicio
                              .Obtener((cife) => cife.Codigo_Formula_lavado == Codigo_Lavado)
                              .Select(cife => new
                              {
                                  cife.Codigo_Costo_Indirecto_Interno,
                                  cife.Codigo_Formula_lavado,
                                  cife.Codigo_Costo_Indirecto,
                                  cife.Costos_Indirectos.Descripcion,
                                  Precio = cife.Costos_Indirectos.Precio * (decimal)(cife.Costos_Indirectos.Monedas != null ? cife.Costos_Indirectos.Monedas.intercambio : 1),
                                  cife.Cantidad,
                                  cife.Costos_Indirectos.Precio_Fijo,
                                  cife.Costos_Indirectos.Porcentaje,
                                  cife.Marca,
                                  Total = cife.Cantidad * cife.Costos_Indirectos.Precio * (decimal)(cife.Costos_Indirectos.Monedas != null ? cife.Costos_Indirectos.Monedas.intercambio : 1)
                              });
            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }
        // GET: Lavados/Create
        public ActionResult AddOrEdit(int codigo = 0)
        {
            var editar = this.LavadosFormulaServicio.Obtener((l) => l.Codigo == codigo).SingleOrDefault();
            bool agregar = false;
            if (editar != null)
            {
                ViewBag.lavado_desc = editar.Lavados.Descripcion;
                ViewBag.detalleInsumos = editar.Detalle_Formula_Lavados.Select(dfl => {
                    JOIN_insumo_proveedores_Result insumo = db.JOIN_insumo_proveedores(dfl.Codigo_Insumo, true).FirstOrDefault();
                    return new
                    {
                        dfl.Codigo,
                        dfl.Codigo_Insumo,
                        dfl.cantidad,
                        dfl.kilos_litros,
                        Descripcion_Insumo = insumo.Descripcion,
                        precio_unitario = insumo.Precio_Unitario,
                        total = (decimal)insumo.Precio_Unitario
                                * (dfl.cantidad == null ? 1 : dfl.cantidad)
                                * (dfl.kilos_litros == null ? 1 : dfl.kilos_litros)
                    };
                });

                ViewBag.CostosIndirectos = editar.Costos_Indirectos_Formula_Lavados.Select(ci => new
                {
                    ci.Cantidad,
                    ci.Codigo_Costo_Indirecto,
                    ci.Marca,
                    ci.Porcentaje,
                    Precio = ci.Costos_Indirectos.Precio * (decimal)(ci.Costos_Indirectos.Monedas != null ? ci.Costos_Indirectos.Monedas.intercambio : 1),
                    ci.Precio_Fijo,
                    ci.Costos_Indirectos.Descripcion,
                    ci.Costos_Indirectos.Porcentaje_Aplica,
                    Total = ci.Cantidad * ci.Costos_Indirectos.Precio * (decimal)(ci.Costos_Indirectos.Monedas != null ? ci.Costos_Indirectos.Monedas.intercambio : 1)
                });

            }
            else
            {
                editar = new Lavados_Formula();
                if (this.LavadosFormulaServicio.Obtener().Count() > 0)
                    editar.Codigo = this.LavadosFormulaServicio.Obtener().Max(lf => lf.Codigo) + 1;
                else
                    editar.Codigo = 1;
                ViewBag.lavado_desc = "";
                agregar = true;
            }
            ViewBag.agregar = agregar;
            return View(editar);
        }
        // POST: Lavados/Create
        [HttpPost]
        public ActionResult AddOrEdit(Lavados_Formula lavado, bool agregar)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    if (agregar)
                        this.LavadosFormulaServicio.Agregar(lavado);
                    else
                    {
                        foreach (var item in this.DetalleFormulaLavadoServicio.Obtener((dfl) => dfl.Codigo_Formula_Lavado == lavado.Codigo))
                        {
                            this.DetalleFormulaLavadoServicio.Borrar(item);
                        }
                        foreach (var item in lavado.Detalle_Formula_Lavados)
                        {
                            item.Codigo_Formula_Lavado = lavado.Codigo;
                            this.DetalleFormulaLavadoServicio.Agregar(item);
                        }

                        foreach (var item in this.CostosIndirectosLavadoServicio.Obtener((ci) => ci.Codigo_Formula_lavado == lavado.Codigo))
                        {
                            this.CostosIndirectosLavadoServicio.Borrar(item);
                        }

                        foreach (var item in lavado.Costos_Indirectos_Formula_Lavados)
                        {
                            item.Codigo_Formula_lavado = lavado.Codigo;
                            this.CostosIndirectosLavadoServicio.Agregar(item);
                        }
                        this.LavadosFormulaServicio.Editar(lavado);
                    }
                    scope.Complete();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);

                    return View(lavado);
                }
            }
        }

        // POST: Lavados/Delete/5
        [HttpPost]
        public ActionResult Delete(int codigo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Lavados_Formula eliminar = this.LavadosFormulaServicio.Obtener((l) => l.Codigo == codigo).SingleOrDefault();

                    if (eliminar != null)
                        this.LavadosFormulaServicio.Borrar(eliminar);
                    scope.Complete();
                    return Json(new { success = true, message = "Eliminado correctamente" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}

