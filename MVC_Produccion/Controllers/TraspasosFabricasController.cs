﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using MVC_Produccion.Sesiones;
using MVC_Produccion.Utilidades;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class TraspasosFabricasController : Controller
    {
        private IBaseServicio<Traspaso_Fabricas> TraspasoServicio;
        private IBaseServicio<Permisos> PermisoServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Detalle_Traspaso_Fabricas> DetalleTraspasoServicio;
        private IBaseServicio<Formulas_Envasado> FormulasEnvasadoServicio;
        private IBaseServicio<Fabricas> FabricasServicio;
        private IBandejaServicio BandejaServicio;

        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();

        public TraspasosFabricasController(IBaseServicio<Traspaso_Fabricas> traspasoServicio,
                                      IBaseServicio<Permisos> permisoServicio,
                                      IBaseServicio<Fabricas> fabricasServicios,
                                      IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                      IBaseServicio<Detalle_Traspaso_Fabricas> detalleTraspasoServicio,
                                      IBaseServicio<Formulas_Envasado> formulasEnvasadoServicio,
                                      IBandejaServicio bandejaServicio)
        {
            BandejaServicio = bandejaServicio;
            TraspasoServicio = traspasoServicio;
            FormulasEnvasadoServicio = formulasEnvasadoServicio;
            DetalleTraspasoServicio = detalleTraspasoServicio;
            PermisoServicio = permisoServicio;
            OpcionesTrabajoServicio = opcionesTrabajoServicio;
            FabricasServicio = fabricasServicios;
            Configuraciones = OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Traspasos_Pendientes()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Confirmar_Traspaso_Pendiente(int Codigo_Interno_Traspaso)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Traspaso_Fabricas traspaso = TraspasoServicio.Obtener((t) => t.Codigo_Interno_Traspaso == Codigo_Interno_Traspaso).FirstOrDefault();
                    traspaso.Fecha_Recepcion = DateTime.Now;
                    traspaso.Recepcion_Confirmada = true;
                    TraspasoServicio.Editar(traspaso);
                    db.Movimiento_Stock_Venta_Traspaso(Codigo_Interno_Traspaso, "Confirmación");
                    scope.Complete();
                    return Json(new { message = "Traspaso confirmado correctamente.", success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { message = e.ToString(), success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Rechazar_Traspaso_Pendiente(int Codigo_Interno_Traspaso, string Motivo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Traspaso_Fabricas traspaso = TraspasoServicio.Obtener((e) => e.Codigo_Interno_Traspaso == Codigo_Interno_Traspaso).SingleOrDefault();
                    traspaso.Motivo_Rechazo = Motivo;
                    traspaso.Historico = false;
                    traspaso.Recepcion_Confirmada = false;
                    traspaso.Egreso_Ingreso = "Ingreso";
                    TraspasoServicio.Editar(traspaso);
                    db.Movimiento_Stock_Venta_Traspaso(traspaso.Codigo_Interno_Traspaso, "Rechazo");
                    scope.Complete();
                    return Json(new { message = "Traspaso rechazado correctamente.", success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { message = e.ToString(), success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult GetData(int Codigo_Interno_Traspaso = 0)
        {
            int Codigo_Fabrica = -1;
            if (Configuraciones.Utiliza_Login)
                Codigo_Fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : -1;

            var listado = db.GetTraspasos(Codigo_Interno_Traspaso, Codigo_Fabrica);
            List<GetDetallesTraspaso_Result> detalles = new List<GetDetallesTraspaso_Result>();
            if (Codigo_Interno_Traspaso != -1)
                detalles = db.GetDetallesTraspaso(Codigo_Interno_Traspaso).ToList();
            return Json(new { data = listado, detalles = detalles, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTraspasosPendientes()
        {
            int fabrica = 0;
            if (Configuraciones.Utiliza_Login)
                fabrica = Sesion.UsuarioActual.Fabrica.HasValue ? Sesion.UsuarioActual.Fabrica.Value : 0;

            var listado = TraspasoServicio.Obtener((e) => e.Historico == true && e.Recepcion_Confirmada == false && e.Fabrica_Destino == (fabrica == -1 ? e.Fabrica_Destino : fabrica))
                                                   .Select(e => new
                                                   {
                                                       e.Codigo_Interno_Traspaso,
                                                       e.Prefijo,
                                                       e.Numero_Comprobante,
                                                       e.Fecha,
                                                       e.Fecha_Recepcion,
                                                       e.Egreso_Ingreso

                                                   });


            return Json(new { data = listado.ToList(), success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetTraspasosPendientes(string id, string FechaRecepcion)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    CampoEditable<DateTime> campo = new CampoEditable<DateTime>(id, FechaRecepcion);
                    var tras = TraspasoServicio.Obtener((e) => e.Codigo_Interno_Traspaso == campo.id).FirstOrDefault();
                    if (tras != null)
                    {
                        tras.Fecha_Recepcion = campo.valor;
                        TraspasoServicio.Editar(tras);
                        scope.Complete();
                        return Json(new { message = "UPDATED" });
                    }
                    else
                    {
                        return Json(new { message = "FAIL" });
                    }
                }
                catch 
                {
                    return Json(new { message = "FAIL" });
                }
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int Codigo_Interno_Traspaso = 0)
        {
            int origen = 0;
            if (Configuraciones.Utiliza_Login)
                origen = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;

            List<SelectListItem> TipoTraspaso = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Egreso", Value="Egreso"},
                new SelectListItem() {Text="Ingreso", Value="Ingreso"}
            };
            ViewBag.TipoTraspaso = TipoTraspaso;

            Traspaso_Fabricas traspaso = new Traspaso_Fabricas();
            if (Codigo_Interno_Traspaso == 0)
            {
                traspaso.Fecha = DateTime.Now;
                if (origen != 0)
                    traspaso.Fabrica_Origen = origen;
            }
            else
                traspaso = TraspasoServicio.Obtener((x) => x.Codigo_Interno_Traspaso == Codigo_Interno_Traspaso).FirstOrDefault();
            if (traspaso.Fabrica_Origen != null && traspaso.Fabrica_Origen != 0)
            {
                Fabricas sp = FabricasServicio.Obtener((s) => s.Codigo_Fabrica == traspaso.Fabrica_Origen).SingleOrDefault();
                ViewBag.Fabrica_Origen = sp.Descripcion;
            }
            if (traspaso.Fabrica_Destino != null && traspaso.Fabrica_Destino != 0)
            {
                Fabricas sp = FabricasServicio.Obtener((s) => s.Codigo_Fabrica == traspaso.Fabrica_Destino).SingleOrDefault();
                ViewBag.Fabrica_Destino  = sp.Descripcion;
            }
            return View(traspaso);
        }

        [HttpGet]
        public ActionResult VerDetalle(int Codigo_Interno_Traspaso = 0)
        {
            List<SelectListItem> TipoTraspaso = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Egreso", Value="Egreso"},
                new SelectListItem() {Text="Ingreso", Value="Ingreso"}
            };

            ViewBag.TipoTraspaso = TipoTraspaso;
            ViewBag.Fabrica = "";

            Traspaso_Fabricas traspaso = new Traspaso_Fabricas();

            if (Codigo_Interno_Traspaso == 0)
            {
                traspaso.Fecha = DateTime.Now;
            }
            else
            {
                traspaso = TraspasoServicio.Obtener((x) => x.Codigo_Interno_Traspaso == Codigo_Interno_Traspaso).FirstOrDefault();
                Fabricas sp = FabricasServicio.Obtener((s) => s.Codigo_Fabrica == traspaso.Fabrica_Origen).FirstOrDefault();
                ViewBag.Fabrica_Origen = sp != null ? sp.Descripcion : "Desconocida";
                sp = FabricasServicio.Obtener((s) => s.Codigo_Fabrica == traspaso.Fabrica_Destino).FirstOrDefault();
                ViewBag.Fabrica_Destino = sp != null ? sp.Descripcion : "Desconocida";
            }

            return View(traspaso);
        }

        [HttpPost]
        public ActionResult AddOrEdit(Traspaso_Fabricas traspaso)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                List<ObtenerLotesLiberados_Result> lotesLiberados = db.ObtenerLotesLiberados(null, traspaso.Fabrica_Origen).ToList();

                if (traspaso.Fecha == null)
                    traspaso.Fecha = DateTime.Now;
                try
                {
                    if (traspaso.Codigo_Interno_Traspaso == 0)
                    {
                        Traspaso_Fabricas traspaso_existente = TraspasoServicio.Obtener((x) => x.Prefijo == traspaso.Prefijo && x.Numero_Comprobante == traspaso.Numero_Comprobante).FirstOrDefault();
                        if (traspaso_existente != null)
                            return Json(new { success = false, message = "Ya existe otro traspaso con el mismo prefijo y número de comprobante" }, JsonRequestBehavior.AllowGet);

                        List<Detalle_Traspaso_Fabricas> detalleActual = this.DetalleTraspasoServicio.Obtener((de) => de.Codigo_Interno_Traspaso == traspaso.Codigo_Interno_Traspaso).ToList();
                        foreach (var item in traspaso.Detalle_Traspaso)
                        {
                            var stockLote = lotesLiberados.FirstOrDefault(s => s.Numero_Producto_Ventas == item.Numero_Producto_Ventas &&
                                                                                s.Lote == item.Lote);
                            Detalle_Traspaso_Fabricas detalle = detalleActual.Where((de) => de.Lote == item.Lote).FirstOrDefault();
                            decimal cantidadDisponible = (stockLote == null ? 0 : stockLote.Cantidad) + (detalle == null ? 0 : detalle.Cantidad) ?? 0;
                            decimal kilosDisponibles = (stockLote == null ? 0 : stockLote.Kilos) + (detalle == null ? 0 : detalle.Kilos) ?? 0;

                            if (cantidadDisponible < item.Cantidad || kilosDisponibles < item.Kilos)
                            {
                                var mensaje = $"Se quiere traspasar de más en el lote: {item.Lote}";
                                mensaje += $"<br>Cantidad Disponible: {cantidadDisponible}";
                                mensaje += $"<br>Kilos Disponible: {kilosDisponibles}";
                                return Json(new { success = false, message = mensaje }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        traspaso.idUsuario = Sesion.UsuarioActual.idUsuario;
                        traspaso.Egreso_Ingreso = "Egreso";
                        traspaso.Historico = false;
                        traspaso.Recepcion_Confirmada = false;
                        TraspasoServicio.Agregar(traspaso);
                        scope.Complete();
                        return Json(new { success = true, alert = false, message = "Registro guardado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        IEnumerable<Detalle_Traspaso_Fabricas> detalleAnterior = DetalleTraspasoServicio.Obtener((de) => de.Codigo_Interno_Traspaso == traspaso.Codigo_Interno_Traspaso);
                        foreach (var item in traspaso.Detalle_Traspaso)
                        {
                            var stockLote = lotesLiberados.FirstOrDefault(s => s.Numero_Producto_Ventas == item.Numero_Producto_Ventas &&
                                                                                s.Lote == item.Lote);
                            var detalle = detalleAnterior.FirstOrDefault(det => det.Numero_Producto_Ventas == item.Numero_Producto_Ventas &&
                                                                                det.Lote == item.Lote);
                            if (stockLote == null && detalle == null)
                            {
                                return Json(new { success = false, message = $"LOTE {item.Lote}: Sin stock" }, JsonRequestBehavior.AllowGet);
                            }

                            decimal? cantidadDisponible = (stockLote != null ? stockLote.Cantidad : 0) + (detalle != null ? detalle.Cantidad : 0);
                            decimal? kilosDisponibles = (stockLote != null ? stockLote.Kilos : 0) + (detalle != null ? detalle.Kilos : 0);

                            if (cantidadDisponible < item.Cantidad || kilosDisponibles < item.Kilos)
                            {
                                string mensaje = $"Se quiere traspasar de mas en el lote: {item.Lote} ";
                                mensaje += $"\n Cantidad Disponible: {cantidadDisponible}";
                                mensaje += $"\n Kilos Disponible: {kilosDisponibles}";
                                return Json(new { success = false, message = mensaje }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        traspaso.idUsuario = Sesion.UsuarioActual.idUsuario;

                        foreach (var detalle in detalleAnterior)
                        {
                            DetalleTraspasoServicio.Borrar(detalle);
                        }

                        foreach (var detalle in traspaso.Detalle_Traspaso.ToList())
                        {
                            if (detalle != null)
                            {
                                detalle.Codigo_Interno_Traspaso = traspaso.Codigo_Interno_Traspaso;
                                DetalleTraspasoServicio.Agregar(detalle);
                            }
                        }

                        TraspasoServicio.Editar(traspaso);
                        scope.Complete();
                        return Json(new { success = true, alert = false, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, alert = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Interno_Traspaso = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Traspaso_Fabricas est = TraspasoServicio.Obtener((x) => x.Codigo_Interno_Traspaso == Codigo_Interno_Traspaso).FirstOrDefault();
                    TraspasoServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Pasar_A_Historico(int Codigo_Interno_Traspaso = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Traspaso_Fabricas traspaso = TraspasoServicio.Obtener((x) => x.Codigo_Interno_Traspaso == Codigo_Interno_Traspaso).FirstOrDefault();
                    traspaso.Historico = true;
                    traspaso.Egreso_Ingreso = "Ingreso";
                    traspaso.Motivo_Rechazo = null;
                    TraspasoServicio.Editar(traspaso);
                    db.Movimiento_Stock_Venta_Traspaso(Codigo_Interno_Traspaso, "Actualización");
                    scope.Complete();
                    return Json(new { success = true, message = "Registro actualizado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
