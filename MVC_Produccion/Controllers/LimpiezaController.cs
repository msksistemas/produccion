﻿using MVC_Produccion.Sesiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class LimpiezaController : Controller
    {
        private IBaseServicio<Fabricas> FabricaServicio;
        private IBaseServicio<Limpieza> LimpiezaServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Detalle_Limpieza> DetalleLimpieza;
        private IBaseServicio<Queseros> QueserosServicio;

        private OpcionesTrabajo Configuraciones;

        public LimpiezaController(IBaseServicio<Limpieza> limpiezaServicio,
                                    IBaseServicio<Fabricas> _fabricasServicio,
                                    IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                    IBaseServicio<Permisos> permisosServicio,
                                    IBaseServicio<Detalle_Limpieza> detalleLimpieza,
                                    IBaseServicio<Queseros> queserosServicio)
        {
            this.FabricaServicio = _fabricasServicio;
            this.LimpiezaServicio = limpiezaServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.DetalleLimpieza = detalleLimpieza;
            this.PermisosServicio = permisosServicio;
            this.QueserosServicio = queserosServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
            {
                new SelectListItem() {Text="No Historicos", Value="false"},
                new SelectListItem() {Text="Historicos", Value="true"},
                new SelectListItem() {Text="Todos", Value="todos"}

            };
            ViewBag.configuraciones = this.Configuraciones;
            ViewBag.ListadoEstados = ListadoEstados;
            return View();
        }

        public ActionResult GetData(string estado = "false")
        {
            var lista = this.LimpiezaServicio.Obtener((l) => estado == "todos" ? true : l.historica.ToString().ToLower() == estado).Select(l => new
            {
                l.Codigo,
                l.Codigo_Formula_Lavado,
                Descripcion_Lavado = l.Lavados_Formula.Lavados.Descripcion,
                l.Comentario,
                l.fecha,
                l.historica
            }).ToList();

            return Json(new { data = lista, success = true }, JsonRequestBehavior.AllowGet);
        }

        // GET: Lavados/Create
        public ActionResult AddOrEdit(int codigo = 0)
        {
            var editar = this.LimpiezaServicio.Obtener((l) => l.Codigo == codigo).SingleOrDefault();
            bool agregar = false;
            if (editar != null)
            {
                ViewBag.detalleLimpieza = editar.Detalle_Limpieza.Select(dfl => new
                {
                    dfl.cantidad,
                    dfl.Codigo,
                    dfl.Codigo_Insumo,
                    dfl.kilos_litros,
                    Descripcion_Insumo = dfl.Insumos.Descripcion,
                    Precio_Por = dfl.Insumos.Precio_Por,
                    dfl.lote,
                    dfl.ajuste
                }).ToList();
                ViewBag.DescFormula = editar.Lavados_Formula.Lavados.Descripcion;
            }
            else
            {
                editar = new Limpieza();
                if (this.LimpiezaServicio.Obtener().Count() > 0)
                    editar.Codigo = this.LimpiezaServicio.Obtener().Max(lf => lf.Codigo) + 1;
                else
                    editar.Codigo = 1;
                agregar = true;
                //editar.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value;
            }
            ViewBag.agregar = agregar;
            ViewBag.queseros = this.QueserosServicio.Obtener((q) => q.Activo == true).Select(q => new { q.Nombre, q.Codigo_Quesero });
            return View(editar);
        }

        // POST: Lavados/Create
        [HttpPost]
        public ActionResult AddOrEdit(Limpieza limpieza, bool agregar)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    if (agregar)
                    {
                        limpieza.historica = false;
                        this.LimpiezaServicio.Agregar(limpieza);
                    }
                    else
                    {
                        foreach (var item in this.DetalleLimpieza.Obtener((dl) => dl.Codigo_Limpieza == limpieza.Codigo))
                        {
                            this.DetalleLimpieza.Borrar(item);
                        }
                        foreach (var item in limpieza.Detalle_Limpieza)
                        {
                            this.DetalleLimpieza.Agregar(item);
                        }
                        this.LimpiezaServicio.Editar(limpieza);
                    }
                    scope.Complete();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(limpieza);
                }

            }
        }

        // POST: Lavados/Delete/5
        [HttpPost]
        public ActionResult Delete(int codigo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (var item in this.DetalleLimpieza.Obtener((dl) => dl.Codigo_Limpieza == codigo))
                    {
                        this.DetalleLimpieza.Borrar(item);
                    }
                    var eliminar = this.LimpiezaServicio.Obtener((l) => l.Codigo == codigo).SingleOrDefault();
                    this.LimpiezaServicio.Borrar(eliminar);
                    scope.Complete();
                    return Json(new { success = true, message = "eliminado correctamente" }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Pasar_Historica(List<int> codigos)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (var item in codigos)
                    {
                        var limpieza = this.LimpiezaServicio.Obtener((l) => l.Codigo == item).SingleOrDefault();
                        limpieza.historica = true;
                        this.LimpiezaServicio.Editar(limpieza);
                    }
                    scope.Complete();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = "Ocurrio un problema", error = ex }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
