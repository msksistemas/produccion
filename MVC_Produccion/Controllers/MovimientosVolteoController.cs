﻿using MVC_Produccion.Sesiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class MovimientosVolteoController : Controller
    {
        private IBaseServicio<Movimientos_Volteo> MovimientosVolteoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();

        public MovimientosVolteoController(IBaseServicio<Movimientos_Volteo> movimientosVolteoServicio,
                                           IBaseServicio<Permisos> permisosServicio,
                                           IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio)
        {
            this.MovimientosVolteoServicio = movimientosVolteoServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }
        // GET: MovimientosVolteo
        public ActionResult Index()
        {            
            int fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : -1;
            ViewBag.fabrica = fabrica;
            return View();
        }

        public ActionResult Obtener_Volteos(int fabrica = -1)
        {
            var volteos = db.Obtener_Lotes_Volteo(fabrica).ToList();
            var resultado = volteos.Select(v => new {v.Lote, v.Dias_Proximo_Volteo});
            return Json(new { volteos }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Voltear(List<string> lotes,int responsable,string comentario, DateTime fecha)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (var lote in lotes)
                    {
                        Movimientos_Volteo movimiento = new Movimientos_Volteo();
                        movimiento.Lote = lote;
                        movimiento.Fecha_Volteo = fecha;
                        movimiento.Fecha_Carga = DateTime.Now;
                        movimiento.Comentario = comentario;
                        movimiento.Usuario = Sesion.UsuarioActual.idUsuario;
                        this.MovimientosVolteoServicio.Agregar(movimiento);
                    }
                    scope.Complete();
                    return Json(new { ok = true, mensaje = "Se volteo Correctamente" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { ok = false, mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }

            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
