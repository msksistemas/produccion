﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using Servicios.Interfaces;
namespace MVC_Produccion.Controllers
{
    public class AnalisisMicrobiologicosController : Controller
    {
        private IBaseServicio<Analisis_Microbiologico> AnalisisMicrobiologicoServicio;
        private IBaseServicio<Detalle_Resultado_Analisis> DetalleResultadoAnalisisServicio;
        private IBaseServicio<Resultado_Analisis_Microbiologico> ResultadoAnalisisMicrobiologicoServicio;
        private IBaseServicio<Rango_Analisis_Microbiologico> RangoAnalisisMicrobiologicoServicio;

        private dbProd db = new dbProd();
        private dbProdSP dbSP = new dbProdSP();
        dbProdSP dbAccesoDatos = new dbProdSP();


        public AnalisisMicrobiologicosController(IBaseServicio<Analisis_Microbiologico> analisisMicrobiologicoServicio,
                                        IBaseServicio<Detalle_Resultado_Analisis> detalleResultadoAnalisisServicio,
                                        IBaseServicio<Resultado_Analisis_Microbiologico> resultadoAnalisisMicrobiologicoServicio,
                                        IBaseServicio<Rango_Analisis_Microbiologico> rangoAnalisisMicrobiologicoServicio)
        {
            this.AnalisisMicrobiologicoServicio = analisisMicrobiologicoServicio;
            this.DetalleResultadoAnalisisServicio = detalleResultadoAnalisisServicio;
            this.ResultadoAnalisisMicrobiologicoServicio = resultadoAnalisisMicrobiologicoServicio;
            this.RangoAnalisisMicrobiologicoServicio = rangoAnalisisMicrobiologicoServicio;

        }

        [HttpGet]
        public ActionResult Index()
        {
            List<SelectListItem> ListadoResultados = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Lotes Sin Analizar", Value="true"},
                new SelectListItem() {Text="Lotes Analizados", Value="false"}
            };
            ViewBag.ListadoResultados = ListadoResultados;

            return View();
        }
        public ActionResult AddOrEdit_Rangos()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Analisis(string mensaje = "", bool resultado = true)
        {
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Activo", Value="true"},
                new SelectListItem() {Text="Inactivo", Value="false"}
            };
            ViewBag.ListadoEstados = ListadoEstados;

            if (resultado)
                ViewBag.Resultado = mensaje;
            else
                ViewBag.error = mensaje;

            return View();
        }

        [HttpPost]
        public ActionResult AddOrEdit_Listado(string Descripcion, int Codigo_Interno_Analisis = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {

                Analisis_Microbiologico analisis = this.AnalisisMicrobiologicoServicio.Obtener((a) => a.Codigo_Interno_Analisis == Codigo_Interno_Analisis).FirstOrDefault();
                try
                {
                    string mensaje = "Registro modificado satisfactoriamente.";

                    if (analisis == null)
                    {
                        analisis = new Analisis_Microbiologico
                        {
                            Descripcion = Descripcion,
                            Activo = true
                        };
                        this.AnalisisMicrobiologicoServicio.Agregar(analisis);
                        mensaje = "Registro guardado satisfactoriamente.";
                    }
                    else
                    {
                        analisis.Descripcion = Descripcion;
                        this.AnalisisMicrobiologicoServicio.Editar(analisis);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult GetDataRangos()
        {
            try
            {
                var rangos = dbSP.Obtener_Info_Rangos().ToList();
                return Json(new { data = rangos, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetData(int Codigo_Interno_Resultado)
        {
            IList listado;

            var analisisActivos = this.AnalisisMicrobiologicoServicio.Obtener((am) => am.Activo == true).ToList();

            if (Codigo_Interno_Resultado != 0)
            {
                var detallesResultados = this.DetalleResultadoAnalisisServicio.Obtener((dr) => dr.Codigo_Interno_Resultado == Codigo_Interno_Resultado).ToList();

                listado = analisisActivos.Select(a => new
                {
                    a.Codigo_Interno_Analisis,
                    a.Descripcion,
                    Resultado_Analisis = detallesResultados.FirstOrDefault(dr => dr.Codigo_Interno_Analisis == a.Codigo_Interno_Analisis)?.Resultado_Analisis ?? 0
                }).ToList();
            }
            else
            {
                listado = analisisActivos.Select(a => new
                {
                    a.Codigo_Interno_Analisis,
                    a.Descripcion,
                    Resultado_Analisis = 0
                }).ToList();
            }

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetRangos(int Codigo_Interno_Analisis)
        {
            var rangos = this.RangoAnalisisMicrobiologicoServicio.Obtener((ra) => ra.Codigo_Interno_Analisis == Codigo_Interno_Analisis).Select(ra => new
            {
                ra.Codigo_Interno_Rango,
                ra.Codigo_Interno_Analisis,
                ra.Desde,
                ra.Hasta,
            });
            return Json(new { data = rangos, success = true}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetLotesSinAnalizar()
        {
            var lotesSinAnalizar = dbSP.ObtenerLotesSinAnalizar().ToList();
            return Json(new { data = lotesSinAnalizar }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetUltimoRango()
        {
            try
            {
                var ultimoRegistro = 1;
                var rango = this.RangoAnalisisMicrobiologicoServicio.Obtener();
                if (rango.Count != 0)
                    ultimoRegistro = rango.Max(a => a.Codigo_Interno_Rango);

                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit_Rango(int Codigo_Interno_Rango, int Codigo_Interno_Analisis, decimal? Desde, decimal? Hasta)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {

                var existe = this.RangoAnalisisMicrobiologicoServicio.Obtener((r) => r.Codigo_Interno_Rango == Codigo_Interno_Rango).SingleOrDefault();
                try
                {
                    string mensaje = "Registro modificado satisfactoriamente.";

                    if (existe == null)
                    {
                        existe = new Rango_Analisis_Microbiologico
                        {
                            Codigo_Interno_Analisis = Codigo_Interno_Analisis,
                            Desde = Desde,
                            Hasta = Hasta,
                        };
                        this.RangoAnalisisMicrobiologicoServicio.Agregar(existe);
                        mensaje = "Registro guardado satisfactoriamente.";
                    }
                    else
                    {
                        existe.Codigo_Interno_Analisis = Codigo_Interno_Analisis;
                        existe.Desde = Desde;
                        existe.Hasta = Hasta;
                        this.RangoAnalisisMicrobiologicoServicio.Editar(existe);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete_Rango(int Codigo_Interno_Rango)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) { 
                try
                {
                    var r = this.RangoAnalisisMicrobiologicoServicio.Obtener((x) => x.Codigo_Interno_Rango == Codigo_Interno_Rango).SingleOrDefault();
                    this.RangoAnalisisMicrobiologicoServicio.Borrar(r);
                    scope.Complete();
                    return Json(new { success = true, message = "Rango eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult GetResultados()
        {
            var resultados = this.ResultadoAnalisisMicrobiologicoServicio.Obtener().Select(a => new
            {
                a.Codigo_Interno_Resultado,
                a.Lote,
                a.Fecha,
                a.Comentario,
            });

            return Json(new { data = resultados, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0, string lote = "")
        {
            if (id == 0)
            {
                Resultado_Analisis_Microbiologico resultAnalisis = new Resultado_Analisis_Microbiologico();
                resultAnalisis.Fecha = DateTime.Now;
                if (lote != "")
                    resultAnalisis.Lote = lote;
                return View(resultAnalisis);
            }
            else
            {
                var RA = this.ResultadoAnalisisMicrobiologicoServicio.Obtener((x) => x.Codigo_Interno_Resultado == id).FirstOrDefault();
                return View(RA);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(Resultado_Analisis_Microbiologico resultadoAnalisis)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {

                var detalleResultado = resultadoAnalisis.detalleResultado;
                if (resultadoAnalisis.Codigo_Interno_Resultado < 0)
                    return Json(new { success = false, message = "Error, por favor ingrese un Código mayor a 0." }, JsonRequestBehavior.AllowGet);
                // NUEVO REGISTRO
                if (resultadoAnalisis.Codigo_Interno_Resultado == 0)
                {
                    this.ResultadoAnalisisMicrobiologicoServicio.Agregar(resultadoAnalisis);
                    scope.Complete();
                    return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
                }

                // BORRA TODOS LOS DETALLES ASOCIADOS
                foreach (var detalle in this.DetalleResultadoAnalisisServicio.Obtener((dra) => dra.Codigo_Interno_Resultado == resultadoAnalisis.Codigo_Interno_Resultado))
                {
                    this.DetalleResultadoAnalisisServicio.Borrar(detalle);
                }

                // GUARDA TODOS LOS DETALLES ASOCIADOS
                foreach (var detalle in detalleResultado)
                {
                    detalle.Codigo_Interno_Resultado = resultadoAnalisis.Codigo_Interno_Resultado;
                    this.DetalleResultadoAnalisisServicio.Agregar(detalle);
                }

                this.ResultadoAnalisisMicrobiologicoServicio.Editar(resultadoAnalisis);
                scope.Complete();
                return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete_Resultado(int Codigo_Interno_Resultado)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) { 
                try
                {
                    Resultado_Analisis_Microbiologico res = this.ResultadoAnalisisMicrobiologicoServicio.Obtener((ra) => ra.Codigo_Interno_Resultado == Codigo_Interno_Resultado).SingleOrDefault();

                    this.ResultadoAnalisisMicrobiologicoServicio.Borrar(res);
                    scope.Complete();
                    return Json(new { success = true, message = "Resultado eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, message = "Error al eliminar." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult GetListado(bool estado = true)
        {
            var listado = this.AnalisisMicrobiologicoServicio.Obtener((am) => am.Activo == estado).Select(a => new
            {
                a.Codigo_Interno_Analisis,
                a.Descripcion,
            });

            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }       

        [HttpGet]
        public ActionResult GetOne(int Codigo_Interno_Analisis)
        {
            if (Codigo_Interno_Analisis > 0)
            {
                try
                {                    
                    var analisis = this.AnalisisMicrobiologicoServicio.Obtener((a) => a.Codigo_Interno_Analisis == Codigo_Interno_Analisis).SingleOrDefault();
                    if (analisis != null)
                    {
                    return Json(new { data = new { analisis.Codigo_Interno_Analisis, analisis.Descripcion }, success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false, mensaje = "Por favor, ingrese un Código." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CambiarEstado(int Codigo_Interno_Analisis)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var analisis = this.AnalisisMicrobiologicoServicio.Obtener((p) => p.Codigo_Interno_Analisis == Codigo_Interno_Analisis).SingleOrDefault();
                if (analisis.Activo == true)
                    analisis.Activo = false;
                else
                    analisis.Activo = true;

                this.AnalisisMicrobiologicoServicio.Editar(analisis);
                scope.Complete();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Interno_Analisis)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var am = this.AnalisisMicrobiologicoServicio.Obtener((x) => x.Codigo_Interno_Analisis == Codigo_Interno_Analisis).SingleOrDefault();
                    this.AnalisisMicrobiologicoServicio.Borrar(am);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
