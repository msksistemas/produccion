﻿using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Produccion.Controllers
{
    public class InfoSimulador
    {
        public bool Marca { get; set; }
        public string Codigo_Insumo { get; set; }
        public string Descripcion { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal Total { get; set; }
    }

    public class InfoSimuladorCostos
    {
        public bool Marca { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public decimal Porcentaje { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal PrecioFijo { get; set; }
        public decimal Total { get; set; }
    }

    public class SimuladorDeCostosController : Controller
    {
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<Formulas_Envasado> FormulasEnvasadoServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> ProductosVentasServicio;
        private OpcionesTrabajo Configuraciones;

        public SimuladorDeCostosController(IBaseServicio<Formulas> _formulasServicio,
                                           IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> _productosVentaServicio,
                                           IBaseServicio<Formulas_Envasado> _formulasEnvasado,
                                           IBaseServicio<OpcionesTrabajo> _OpcionesTrabajo,
                                           IBaseServicio<Permisos> _permisosServicio)
        {
            this.FormulasServicio = _formulasServicio;
            this.ProductosVentasServicio = _productosVentaServicio;
            this.FormulasEnvasadoServicio = _formulasEnvasado;
            this.OpcionesTrabajoServicio = _OpcionesTrabajo;
            this.PermisosServicio = _permisosServicio;
            this.Configuraciones = OpcionesTrabajoServicio.Obtener().First();
        }
        // GET: SimuladorDeCostos
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProductosVentasRelacionados(string codigo_producto)
        {
            var codigos = this.FormulasEnvasadoServicio.Obtener((f) => f.Codigo_Producto == codigo_producto).Select(f => f.Numero_Producto_Ventas).ToList();
            var resultado = this.ProductosVentasServicio.Obtener((pv) => codigos.Contains(pv.Numero_de_Producto)).Select(pv => new
            {
                Numero_de_Producto = pv.Numero_de_Producto.Trim(),
                pv.Descripcion
            }).ToList();

            return Json(new { data = resultado }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ObtenerInsumos(string codigo_producto, string numero_de_producto, int cantidadAEnvasar)
        {
            List<InfoSimulador> resultado = new List<InfoSimulador>();
            var formulaEnvasado = this.FormulasEnvasadoServicio.Obtener((f) => f.Codigo_Producto.Trim() == codigo_producto && f.Numero_Producto_Ventas.Trim() == numero_de_producto).FirstOrDefault();
            decimal obtenidasPorEnvasado = (formulaEnvasado.Obtenidas_Por_Envasada.HasValue ? formulaEnvasado.Obtenidas_Por_Envasada.Value : 1);
            var formula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == codigo_producto).FirstOrDefault();

            foreach (var detalle in formulaEnvasado.Detalle_Formula_Envasado)
            {
                var precio = detalle.Insumos.Precio_Ultima_Compra.HasValue ? detalle.Insumos.Precio_Ultima_Compra.Value :
                                    detalle.Insumos.Precio_Lista.HasValue ? detalle.Insumos.Precio_Lista.Value : 0;
                var cambio = detalle.Insumos.Monedas != null ? detalle.Insumos.Monedas.intercambio : 1;
                precio = precio * Convert.ToDecimal(cambio);
                resultado.Add(new InfoSimulador()
                {
                    Marca = detalle.Marca,
                    Codigo_Insumo = detalle.Codigo_Insumo,
                    Descripcion = detalle.Insumos.Descripcion,
                    Cantidad = detalle.Cantidad * cantidadAEnvasar / obtenidasPorEnvasado,
                    Precio = precio,
                    Total = precio * (detalle.Cantidad * cantidadAEnvasar / obtenidasPorEnvasado)
                });
            }

            foreach (var detalle in formula.Detalle_Formula)
            {
                var precio = detalle.Insumos.Precio_Ultima_Compra.HasValue ? detalle.Insumos.Precio_Ultima_Compra.Value :
                                detalle.Insumos.Precio_Lista.HasValue ? detalle.Insumos.Precio_Lista.Value : 0;
                precio = precio * Convert.ToDecimal(detalle.Insumos.Monedas.intercambio);
                resultado.Add(new InfoSimulador()
                {
                    Marca = detalle.Marca,
                    Codigo_Insumo = detalle.Codigo_Insumo,
                    Descripcion = detalle.Insumos.Descripcion,
                    Cantidad = detalle.Cantidad,
                    Precio = precio,
                    Total = precio * detalle.Cantidad
                });
            }
            foreach (var detalle in formula.Insumos_Formulas_Recuperado)
            {
                var precio = detalle.Insumos.Precio_Ultima_Compra.HasValue ? detalle.Insumos.Precio_Ultima_Compra.Value :
                                detalle.Insumos.Precio_Lista.HasValue ? detalle.Insumos.Precio_Lista.Value : 0;
                precio = precio * Convert.ToDecimal(detalle.Insumos.Monedas.intercambio);
                var add = new InfoSimulador()
                {
                    Marca = true,
                    Codigo_Insumo = detalle.Codigo_Insumo,
                    Descripcion = detalle.Insumos.Descripcion,
                    Cantidad = detalle.CantidadRecuperada * -1,
                    Precio = precio,
                    Total = precio * detalle.CantidadRecuperada * -1
                };
                resultado.Add(add);
            }

            return Json(new { data = resultado }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ObtenerCostosIndirectos(string codigo_producto, string numero_de_producto)
        {
            List<InfoSimuladorCostos> resultado = new List<InfoSimuladorCostos>();
            var formulaEnvasado = this.FormulasEnvasadoServicio.Obtener((f) => f.Codigo_Producto.Trim() == codigo_producto && f.Numero_Producto_Ventas.Trim() == numero_de_producto).FirstOrDefault();
            var formula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == codigo_producto).FirstOrDefault();

            foreach (var detalle in formulaEnvasado.Costos_Indirectos_Formula_Envasado)
            {
                decimal total = 0;
                if (detalle.Costos_Indirectos.Precio.HasValue && detalle.Costos_Indirectos.Precio.Value > 0)
                    total = Convert.ToDecimal(detalle.Cantidad.Value) * detalle.Costos_Indirectos.Precio.Value;
                if (detalle.Costos_Indirectos.Precio_Fijo.HasValue && detalle.Costos_Indirectos.Precio_Fijo.Value > 0)
                    total = detalle.Costos_Indirectos.Precio_Fijo.Value;

                resultado.Add(new InfoSimuladorCostos
                {
                    Marca = detalle.Marca.Value,
                    Codigo = detalle.Codigo_Costo_Indirecto,
                    Descripcion = detalle.Costos_Indirectos.Descripcion,
                    PrecioFijo = detalle.Costos_Indirectos.Precio_Fijo.HasValue ? detalle.Costos_Indirectos.Precio_Fijo.Value : 0,
                    PrecioUnitario = detalle.Precio.HasValue ? detalle.Precio.Value : 0,
                    Cantidad = detalle.Cantidad.HasValue ? detalle.Cantidad.Value : 0,
                    Porcentaje = detalle.Costos_Indirectos.Porcentaje.HasValue ? detalle.Costos_Indirectos.Porcentaje.Value : 0,
                    Total = total
                });
            }

            foreach (var detalle in formula.Costos_Indirectos_Formula)
            {
                decimal total = 0;
                if (detalle.Costos_Indirectos.Precio.HasValue && detalle.Costos_Indirectos.Precio.Value > 0)
                    total = Convert.ToDecimal(detalle.Cantidad.Value) * detalle.Costos_Indirectos.Precio.Value;
                if (detalle.Costos_Indirectos.Precio_Fijo.HasValue && detalle.Costos_Indirectos.Precio_Fijo.Value > 0)
                    total = detalle.Costos_Indirectos.Precio_Fijo.Value;
                resultado.Add(new InfoSimuladorCostos
                {
                    Marca = detalle.Marca.Value,
                    Codigo = detalle.Codigo_Costo_Indirecto,
                    Descripcion = detalle.Costos_Indirectos.Descripcion,
                    PrecioFijo = detalle.Costos_Indirectos.Precio_Fijo.HasValue ? detalle.Costos_Indirectos.Precio_Fijo.Value : 0,
                    PrecioUnitario = detalle.Costos_Indirectos.Precio.HasValue ? detalle.Costos_Indirectos.Precio.Value : 0,
                    Cantidad = detalle.Cantidad.HasValue ? detalle.Cantidad.Value : 0,
                    Porcentaje = detalle.Costos_Indirectos.Porcentaje.HasValue ? detalle.Costos_Indirectos.Porcentaje.Value : 0,
                    Total = total
                });
            }

            return Json(new { data = resultado }, JsonRequestBehavior.AllowGet);
        }

    }
}