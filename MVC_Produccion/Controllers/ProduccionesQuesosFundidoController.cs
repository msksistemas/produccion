﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using MVC_Produccion.Sesiones;
using MVC_Produccion.Models.Utilidades;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion.SP;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Models;
using System.Net.Http;
using System.Threading.Tasks;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class ProduccionesQuesosFundidoController : Controller
    {
        private IProduccionServicio ProduccionServicioExtra;
        private IBaseServicio<Queseros> QueserosServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Envasado> EnvasadoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Estados> EstadosServicio;
        private IBaseServicio<Camaras> CamarasServicio;
        private IBaseServicio<Familias> FamiliasServicio;
        private IBaseServicio<Motivos_Retencion> MotivosRetencionServicio;
        private IBaseServicio<Restantes_Envasado> RestanteEnvasadoServicio;
        private IBaseServicio<Detalle_Produccion> DetalleProduccionServicio;
        private IBaseServicio<Produccion> ProduccionServicio;
        private IBaseServicio<Produccion_Quesos_Fundido> ProduccionQuesosFundidoServicio;
        private IBaseServicio<Mov_Ins_Asoc> MovInsAsocServicio;
        private IBaseServicio<Costos_Indirectos_Produccion> CostosIndirectosProduccion;
        private IBaseServicio<Acidificacion> AcidificacionServicio;
        private dbProdSP db = new dbProdSP();
        private OpcionesTrabajo Configuraciones;
        private readonly HttpService httpClient;

        public ProduccionesQuesosFundidoController(IProduccionServicio _ProduccionServicioExtra,
                                                   IBaseServicio<Queseros> _queserosServicio,
                                                   IBaseServicio<Costos_Indirectos_Produccion> _costosIndirectosProduccion,
                                                   IBaseServicio<Acidificacion> _asidificacionServicio,
                                                   IBaseServicio<Formulas> _formulasServicio,
                                                   IBaseServicio<Mov_Ins_Asoc> _MovInsAsocServicio,
                                                   IBaseServicio<Envasado> _EnvasadoServicio,
                                                   IBaseServicio<Permisos> _permisoServicio,
                                                   IBaseServicio<OpcionesTrabajo> _opcionesTrabajoServicio,
                                                   IBaseServicio<Estados> _estadosServicio,
                                                   IBaseServicio<Camaras> _CamarasServicio,
                                                   IBaseServicio<Familias> _familiasServicio,
                                                   IBaseServicio<Motivos_Retencion> _MotivosRetecionServicio,
                                                   IBaseServicio<Restantes_Envasado> _RestanteEnvasadoServicio,
                                                   IBaseServicio<Detalle_Produccion> _DetalleProduccionServicio,
                                                   IBaseServicio<Produccion> _produccionServicio,
                                                   IBaseServicio<Produccion_Quesos_Fundido> _produccionQuesoFundido,
                                                   HttpService _httpClient)
        {
            this.QueserosServicio = _queserosServicio;
            this.AcidificacionServicio = _asidificacionServicio;
            this.MovInsAsocServicio = _MovInsAsocServicio;
            this.CostosIndirectosProduccion = _costosIndirectosProduccion;
            this.FormulasServicio = _formulasServicio;
            this.ProduccionServicioExtra = _ProduccionServicioExtra;
            this.EnvasadoServicio = _EnvasadoServicio;
            this.PermisosServicio = _permisoServicio;
            this.OpcionesTrabajoServicio = _opcionesTrabajoServicio;
            this.EstadosServicio = _estadosServicio;
            this.CamarasServicio = _CamarasServicio;
            this.FamiliasServicio = _familiasServicio;
            this.MotivosRetencionServicio = _MotivosRetecionServicio;
            this.RestanteEnvasadoServicio = _RestanteEnvasadoServicio;
            this.DetalleProduccionServicio = _DetalleProduccionServicio;
            this.ProduccionQuesosFundidoServicio = _produccionQuesoFundido;
            this.ProduccionServicio = _produccionServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().First();
            this.httpClient = _httpClient;
        }

        // GET: ProduccionesQuesosFundido
        public ActionResult Index(int? resultado)
        {
            if (Sesion.UsuarioActual.Fabrica == 0 || Sesion.UsuarioActual.Fabrica == null && Sesion.UsuarioActual.Rol.ToLower() != "administrador")
                ViewBag.Error = "No tiene Fábrica asignada, por favor, solicite Permisos al Administrador.";

            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";

            else if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";
            ViewBag.configuraciones = this.Configuraciones;

            List<SelectListItem> ListadoHistoricos = new List<SelectListItem>()
                        {
                            new SelectListItem() {Text="No Históricos", Value="0"},
                            new SelectListItem() {Text="Históricos", Value="1"},
                            new SelectListItem() {Text="Todos", Value="-1"}
                        };

            ViewBag.ListadoHistoricos = ListadoHistoricos;
            ViewBag.estados = this.EstadosServicio.Obtener().Select(e => new { e.Codigo_Estado, e.Descripcion });

            return View();
        }

        public ActionResult GetHistoricos(string Lote = "", int Historico = -1, int Codigo = 0)
        {
            //TODO: CAMBIAR POR SP ObtenerProduccionesQuesos

            var listado = this.ProduccionQuesosFundidoServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)
                                                                        && (Historico == -1 || p.Produccion.Historico == (Historico == 1 ? true : false))).Select(p => new
                                                                        {
                                                                            p.Lote,
                                                                            p.Produccion.Nro_Planilla,
                                                                            p.Produccion.Fecha,
                                                                            p.Piezas_Obtenidas,
                                                                            p.Kilos_Teoricos,
                                                                            p.Kilos_Reales,
                                                                            p.Kilos_Envasado,
                                                                            p.Codigo_Estado,
                                                                            Estado = this.EstadosServicio.Obtener((e) => e.Codigo_Estado == p.Codigo_Estado).SingleOrDefault() != null ? this.EstadosServicio.Obtener((e) => e.Codigo_Estado == p.Codigo_Estado).SingleOrDefault().Descripcion : "",
                                                                            Se_Puede_Envasar = this.EstadosServicio.Obtener((e) => e.Codigo_Estado == p.Codigo_Estado).SingleOrDefault() != null ? this.EstadosServicio.Obtener((e) => e.Codigo_Estado == p.Codigo_Estado).SingleOrDefault().Se_Puede_Envasar : false,
                                                                            Codigo_Producto = p.Produccion.Formulas != null ? p.Produccion.Formulas.Codigo_Producto : "",
                                                                            Producto = p.Produccion.Formulas != null ? p.Produccion.Formulas.Productos != null ? p.Produccion.Formulas.Productos.Descripcion : "" : "",
                                                                            Quesero = p.Produccion.Queseros != null ? p.Produccion.Queseros.Nombre : "",
                                                                            Resta_Envasar = p.Piezas_Obtenidas - PiezasEnvasadas(p.Lote)
                                                                        }).ToList();

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetData(string Lote = "")
        {
            //TODO: CAMBIAR POR SP ObtenerProduccionesQuesos

            if ((Sesion.UsuarioActual.Fabrica != 0 && Sesion.UsuarioActual.Fabrica != null) || Sesion.UsuarioActual.Rol.ToLower() == "administrador")
            {
                var listado = this.ProduccionQuesosFundidoServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)
                                                                           && p.Historico == false
                                                                           && p.Produccion.Codigo_Fabrica == (Sesion.UsuarioActual.Fabrica == -1 ? p.Produccion.Codigo_Fabrica : Sesion.UsuarioActual.Fabrica))
                                                            .Select(p => new
                                                            {
                                                                p.Lote,
                                                                p.Produccion.Nro_Planilla,
                                                                p.Produccion.Fecha,
                                                                p.Piezas_Obtenidas,
                                                                p.Kilos_Teoricos,
                                                                p.Kilos_Reales,
                                                                p.Kilos_Envasado,
                                                                p.Codigo_Estado,
                                                                Estado = this.EstadosServicio.Obtener((e) => e.Codigo_Estado == p.Codigo_Estado).SingleOrDefault() != null ? this.EstadosServicio.Obtener((e) => e.Codigo_Estado == p.Codigo_Estado).SingleOrDefault().Descripcion : "",
                                                                Se_Puede_Envasar = this.EstadosServicio.Obtener((e) => e.Codigo_Estado == p.Codigo_Estado).SingleOrDefault() != null ? this.EstadosServicio.Obtener((e) => e.Codigo_Estado == p.Codigo_Estado).SingleOrDefault().Se_Puede_Envasar : false,
                                                                Codigo_Producto = p.Produccion.Formulas != null ? p.Produccion.Formulas.Codigo_Producto : "",
                                                                Producto = p.Produccion.Formulas != null ? p.Produccion.Formulas.Productos != null ? p.Produccion.Formulas.Productos.Descripcion : "" : "",
                                                                Quesero = p.Produccion.Queseros != null ? p.Produccion.Queseros.Nombre : "",
                                                                Vencimiento_Envasado = p.Produccion.Formulas != null ? p.Produccion.Formulas.Productos != null ? p.Produccion.Formulas.Productos.Vencimiento_Envasado : false : false,
                                                                Resta_Envasar = p.Piezas_Obtenidas - PiezasEnvasadas(p.Lote)
                                                            }).ToList();

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { error = 1, Sesion.UsuarioActual.Usuario, success = false }, JsonRequestBehavior.AllowGet);

        }

        private decimal PiezasEnvasadas(string lote)
        {
            decimal piezasEnvasadas = 0;
            decimal hormas_detalle;
            foreach (var env in this.EnvasadoServicio.Obtener((e) => e.Lote == lote).ToList())
            {
                piezasEnvasadas += env.Hormas_Descarte == null ? 0 : env.Hormas_Descarte.Value;
                if (env != null && env.Detalle_Envasado.Count() > 0)
                {
                    foreach (var detalle in env.Detalle_Envasado.ToList())
                    {
                        if (detalle != null)
                        {
                            hormas_detalle = detalle.Hormas == null ? 0 : detalle.Hormas.Value;
                            if (hormas_detalle != 0)
                                piezasEnvasadas += hormas_detalle;
                            else
                                piezasEnvasadas += (detalle.Cantidad == null ? 0 : detalle.Cantidad.Value) / (detalle.Formulas_Envasado.Obtenidas_Por_Envasada == null ? 0 : detalle.Formulas_Envasado.Obtenidas_Por_Envasada.Value);
                        }

                    }
                }
            }
            return piezasEnvasadas;
        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote = "")
        {
            ViewBag.queseros = this.QueserosServicio.Obtener((q) => q.Activo == true).Select(q => new { q.Nombre, q.Codigo_Quesero });
            ViewBag.estados = this.EstadosServicio.Obtener().Select(e => new { e.Codigo_Estado, e.Descripcion });
            ViewBag.motivosRetencion = this.MotivosRetencionServicio.Obtener((m) => m.Tipo == "Produccion" && m.Activo).Select(m => new { m.Codigo_Motivo_Retencion, m.Descripcion });
            ViewBag.camaras = this.CamarasServicio.Obtener().Select(c => new { c.Codigo_Camara, c.Nombre });
            ViewBag.Codigo_Familia = this.FamiliasServicio.Obtener((f) => f.Nombre.ToLower() == "queso fundido").FirstOrDefault().Codigo_Familia;
            ViewBag.Envasa_Sin_Kilos = this.Configuraciones.Envasa_Sin_Kilos;

            if (Lote == "")
            {
                Produccion_Quesos_Fundido prod = new Produccion_Quesos_Fundido();
                Produccion produccion = new Produccion();
                prod.Codigo_Estado = 2;
                produccion.Fecha = DateTime.Now;
                produccion.Historico = false;
                prod.Historico = false;
                prod.Produccion = produccion;
                if (this.Configuraciones.Utiliza_Login == true)
                    prod.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica;
                if (!this.Configuraciones.NroPlanillaDigitable)
                {
                    produccion.Nro_Planilla = (this.ProduccionQuesosFundidoServicio.Obtener().Count() + 1).ToString();
                    ViewBag.NroAuto = true;
                }
                else
                    ViewBag.NroAuto = false;

                ViewBag.esDesglose = false;
                return View(prod);
            }
            else
            {
                var lote = this.ProduccionQuesosFundidoServicio.Obtener((x) => x.Lote == Lote).FirstOrDefault();
                if (lote.Produccion.Lote_Padre != null && lote.Produccion.Lote_Padre != "undefined")
                {
                    ViewBag.esDesglose = true;
                }
                else
                {
                    ViewBag.esDesglose = false;
                }
                return View(lote);
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddOrEdit(Produccion_Quesos_Fundido prod, List<Desglose_Produccion> desglose, string Codigo_Producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //if (httpClient.EsSanSatur())
                    //{
                    //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                    //        prod.Produccion.Detalle_Produccion.ToList(),
                    //        HttpService.ProcesoProduccion.Creacion,
                    //        prod.Produccion.Fecha ?? DateTime.Now,
                    //        httpClient.ObtenerPrimerNumProdVentas(Codigo_Producto)
                    //    );
                    //    if (!response.IsSuccessStatusCode)
                    //    {
                    //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                    //    }
                    //}

                    prod.Lote = prod.Produccion.Lote;
                    prod.Produccion.idUsuario = Sesion.UsuarioActual.idUsuario;
                    if (prod.Codigo_Interno_Produccion == 0)
                    {
                        if ((prod.Kilos_Teoricos.HasValue ? prod.Kilos_Teoricos.Value : 0) == 0)
                            return Json(new { success = false, message = "Los kilos no pueden ser 0." }, JsonRequestBehavior.AllowGet);

                        if (this.ProduccionServicioExtra.ExisteProduccion(prod.Produccion.Lote))
                            return Json(new { success = false, message = "Lote " + prod.Produccion.Lote + " ya registrado" }, JsonRequestBehavior.AllowGet);
                        if (desglose != null)
                            foreach (var d in desglose)
                            {
                                if (this.ProduccionServicioExtra.ExisteProduccion(d.Lote))
                                    return Json(new { success = false, message = "Lote " + d.Lote + " ya registrado" }, JsonRequestBehavior.AllowGet);

                            }
                        prod.Kilos_Reales = prod.Kilos_Reales == null ? 0 : prod.Kilos_Reales;
                        prod.Kilos_Envasado = this.Configuraciones.Envasa_Sin_Kilos ? 0 : prod.Kilos_Reales;
                        prod.Produccion.Historico = false;
                        prod.Historico = false;

                        if (desglose != null)
                        {
                            var cantidadDesglosada = desglose.Sum(d => d.Cantidad);

                            if (cantidadDesglosada != prod.Piezas_Obtenidas)
                            {
                                return Json(new { success = false, message = "La cantidad desglosada no es la misma que las piezas obtenidas." }, JsonRequestBehavior.AllowGet);
                            }

                            prod.Produccion.Historico = true;
                            prod.Historico = true;
                            Produccion_Quesos_Fundido pqDesglose;
                            Produccion prodDesglose;
                            Formulas formulaDesglose;
                            foreach (Desglose_Produccion d in desglose)
                            {
                                pqDesglose = new Produccion_Quesos_Fundido();
                                pqDesglose.Codigo_Estado = prod.Codigo_Estado;
                                pqDesglose.Codigo_Quesero = prod.Codigo_Quesero;
                                pqDesglose.Codigo_Camara = prod.Codigo_Camara;
                                pqDesglose.Piezas_Obtenidas = d.Cantidad;
                                pqDesglose.Kilos_Reales = d.Kilos;
                                pqDesglose.Lote = d.Lote;
                                pqDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                pqDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                pqDesglose.Codigo_Fabrica = prod.Codigo_Fabrica;
                                pqDesglose.Tiempo_Calentamiento = prod.Tiempo_Calentamiento;
                                pqDesglose.Tiempo_Fundido = prod.Tiempo_Fundido;
                                pqDesglose.Tiempo_Envasado = prod.Tiempo_Envasado;
                                pqDesglose.Tiempo_Enfriamiento = prod.Tiempo_Enfriamiento;
                                pqDesglose.Ph = prod.Ph;
                                pqDesglose.Historico = false;
                                var kilos_padre = prod.Kilos_Reales.Value != 0 ? prod.Kilos_Reales.Value : prod.Kilos_Teoricos.Value;
                                var coeficienteDeRepresentacion = d.Kilos / kilos_padre;
                                pqDesglose.Kilos_Reproceso = Convert.ToInt32(prod.Kilos_Reproceso * coeficienteDeRepresentacion);
                                pqDesglose.Kilos_Teoricos = Convert.ToInt32(prod.Kilos_Teoricos * coeficienteDeRepresentacion);

                                formulaDesglose = this.FormulasServicio.Obtener((x) => x.Codigo_Producto == d.Numero_Producto).FirstOrDefault();

                                prodDesglose = new Produccion();
                                prodDesglose.Lote = d.Lote;
                                prodDesglose.Lote_Padre = prod.Produccion.Lote;
                                prodDesglose.Historico = false;
                                prodDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                prodDesglose.Fecha = prod.Produccion.Fecha;
                                prodDesglose.Nro_Planilla = prod.Produccion.Nro_Planilla;
                                prodDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                prodDesglose.Codigo_Formula = formulaDesglose.Codigo_Formula;
                                prodDesglose.idUsuario = Sesion.UsuarioActual.idUsuario;
                                pqDesglose.Produccion = prodDesglose;

                                this.ProduccionQuesosFundidoServicio.Agregar(pqDesglose);
                            }
                        }

                        var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                        prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;
                        //GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS
                        foreach (var detalle in prod.Produccion.Detalle_Produccion.ToList())
                        {
                            detalle.Movimiento = "Producción";
                            detalle.Fecha = prod.Produccion.Fecha == null ? DateTime.Now : prod.Produccion.Fecha;
                            detalle.Lote = prod.Produccion.Lote;
                        }
                        foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion.ToList())
                        {
                            detalle.Lote = prod.Produccion.Lote;
                        }
                        foreach (var acidificacion in prod.Produccion.Acidificacion.ToList())
                        {
                            acidificacion.Lote = prod.Lote;
                        }
                        //GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS
                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                        {
                            movimiento.Lote = prod.Produccion.Lote;
                        }

                        this.ProduccionQuesosFundidoServicio.Agregar(prod);
                        db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);
                        if (desglose != null)
                        {
                            foreach (Desglose_Produccion d in desglose)
                            {
                                int Codigo_Interno_Desglose = this.ProduccionServicio.Obtener((p) => p.Lote == d.Lote).SingleOrDefault().Codigo_Interno_Produccion;
                                db.Movimiento_Stock_Produccion(Codigo_Interno_Desglose, false);
                            }
                        }
                        scope.Complete();
                        return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        try
                        {
                            prod.Lote = prod.Produccion.Lote;

                            if (desglose != null)
                            {
                                var lote = this.ProduccionServicio.Obtener((x) => x.Lote_Padre == prod.Lote).FirstOrDefault();
                                if (lote != null)
                                {
                                    return Json(new { success = false, message = "Ya existen desgloses de esta producción." }, JsonRequestBehavior.AllowGet);
                                }
                                else if (prod.Historico == true)
                                {
                                    return Json(new { success = false, message = "No se puede desglosar una producción histórica." }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    prod.Produccion.Historico = true;
                                    prod.Historico = true;
                                    Produccion_Quesos_Fundido pqDesglose;
                                    Produccion prodDesglose;
                                    Formulas formulaDesglose;
                                    foreach (Desglose_Produccion d in desglose)
                                    {
                                        pqDesglose = new Produccion_Quesos_Fundido();
                                        pqDesglose.Codigo_Estado = prod.Codigo_Estado;
                                        pqDesglose.Codigo_Quesero = prod.Codigo_Quesero;
                                        pqDesglose.Piezas_Obtenidas = d.Cantidad;
                                        pqDesglose.Kilos_Reales = d.Kilos;
                                        pqDesglose.Lote = d.Lote;
                                        pqDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                        pqDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                        pqDesglose.Codigo_Fabrica = prod.Codigo_Fabrica;
                                        pqDesglose.Tiempo_Calentamiento = prod.Tiempo_Calentamiento;
                                        pqDesglose.Tiempo_Fundido = prod.Tiempo_Fundido;
                                        pqDesglose.Tiempo_Envasado = prod.Tiempo_Envasado;
                                        pqDesglose.Tiempo_Enfriamiento = prod.Tiempo_Enfriamiento;
                                        pqDesglose.Ph = prod.Ph;
                                        pqDesglose.Historico = false;
                                        var kilos_padre = prod.Kilos_Reales.Value != 0 ? prod.Kilos_Reales.Value : prod.Kilos_Teoricos.Value;
                                        var coeficienteDeRepresentacion = d.Kilos / kilos_padre;
                                        pqDesglose.Kilos_Reproceso = Convert.ToInt32(prod.Kilos_Reproceso * coeficienteDeRepresentacion);
                                        pqDesglose.Kilos_Teoricos = Convert.ToInt32(prod.Kilos_Teoricos * coeficienteDeRepresentacion);

                                        formulaDesglose = this.FormulasServicio.Obtener((x) => x.Codigo_Producto == d.Numero_Producto).FirstOrDefault();

                                        prodDesglose = new Produccion();
                                        prodDesglose.Lote = d.Lote;
                                        prodDesglose.Lote_Padre = prod.Produccion.Lote;
                                        prodDesglose.Historico = false;
                                        prodDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                        prodDesglose.Fecha = prod.Produccion.Fecha;
                                        prodDesglose.Nro_Planilla = prod.Produccion.Nro_Planilla;
                                        prodDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                        prodDesglose.Codigo_Formula = formulaDesglose.Codigo_Formula;
                                        prodDesglose.idUsuario = Sesion.UsuarioActual.idUsuario;
                                        pqDesglose.Produccion = prodDesglose;

                                        this.ProduccionQuesosFundidoServicio.Agregar(pqDesglose);
                                    }
                                }
                            }

                            //TRAE EL PRODUCTO ASOCIADO A LA FÒRMULA PARA SABER SI ES UN INSUMO, PROD TERMINADO O AMBOS
                            var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                            prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;

                            List<Detalle_Produccion> detalleAnterior = this.DetalleProduccionServicio.Obtener((dp) => dp.Lote == prod.Lote).ToList();

                            //if (httpClient.EsSanSatur())
                            //{
                            //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                            //        prod.Produccion.Detalle_Produccion.ToList(),
                            //        HttpService.ProcesoProduccion.Edicion,
                            //        Codigo_Producto,
                            //        detalleAnterior);
                            //    if (!response.IsSuccessStatusCode)
                            //    {
                            //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                            //    }
                            //}

                            //BORRA LOS DETALLES DE LA PRODUCCIÓN A EDITAR
                            foreach (var detalle in detalleAnterior)
                            {
                                this.DetalleProduccionServicio.Borrar(detalle);
                            }

                            //GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS
                            foreach (var detalle in prod.Produccion.Detalle_Produccion.ToList())
                            {
                                detalle.Movimiento = "Producción";
                                detalle.Fecha = prod.Produccion.Fecha == null ? DateTime.Now : prod.Produccion.Fecha;
                                detalle.Lote = prod.Lote;
                                this.DetalleProduccionServicio.Agregar(detalle);
                            }

                            foreach (var detalle in this.CostosIndirectosProduccion.Obtener((ci) => ci.Lote == prod.Lote))
                            {
                                this.CostosIndirectosProduccion.Borrar(detalle);
                            }
                            foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion.ToList())
                            {
                                detalle.Lote = prod.Produccion.Lote;
                                this.CostosIndirectosProduccion.Agregar(detalle);
                            }
                            //BORRA TODOS LOS DETALLES DE PRODUCTOS ASOCIADOS
                            foreach (var movimiento in this.MovInsAsocServicio.Obtener((mia) => mia.Lote == prod.Lote))
                            {
                                this.MovInsAsocServicio.Borrar(movimiento);
                            }

                            //GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS
                            foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                            {
                                movimiento.Lote = prod.Lote;
                                this.MovInsAsocServicio.Agregar(movimiento);
                            }

                            foreach (var acidificacion in this.AcidificacionServicio.Obtener((a) => a.Lote == prod.Lote))
                            {
                                this.AcidificacionServicio.Borrar(acidificacion);
                            }
                            foreach (var acidificacion in prod.Produccion.Acidificacion.ToList())
                            {
                                acidificacion.Lote = prod.Lote;
                                this.AcidificacionServicio.Agregar(acidificacion);
                            }
                            this.ProduccionQuesosFundidoServicio.Editar(prod);
                            this.ProduccionServicio.Editar(prod.Produccion);
                            db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);
                            if (desglose != null)
                            {
                                foreach (Desglose_Produccion d in desglose)
                                {
                                    int Codigo_Interno_Desglose = this.ProduccionServicio.Obtener((p) => p.Lote == d.Lote).SingleOrDefault().Codigo_Interno_Produccion;
                                    db.Movimiento_Stock_Produccion(Codigo_Interno_Desglose, false);
                                }
                            }
                            scope.Complete();
                            return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return Json(new { success = false, message = e.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}