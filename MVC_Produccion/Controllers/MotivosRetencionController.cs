﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class MotivosRetencionController : Controller
    {
        private IBaseServicio<Motivos_Retencion> MotivosRetencionServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private OpcionesTrabajo Configuraciones;

        public MotivosRetencionController(IBaseServicio<Motivos_Retencion> motivosRetencionServicio,
                                           IBaseServicio<Permisos> permisosServicio,
                                           IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio)
        {
            this.MotivosRetencionServicio = motivosRetencionServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            var listado = this.MotivosRetencionServicio.Obtener((m) => m.Activo).Select(t => new
            {
                t.Codigo_Interno_Motivo,
                t.Codigo_Motivo_Retencion,
                t.Descripcion,
                t.Tipo
            });

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult AddOrEdit(int id = 0, int fabrica = 0)
        {
            if (id == 0 || fabrica == 0)
                return View(new Motivos_Retencion());
            else
                return View(this.MotivosRetencionServicio.Obtener((x) => x.Codigo_Motivo_Retencion == id).SingleOrDefault());
        }

        [HttpPost]
        public ActionResult AddOrEdit(int Codigo_Motivo_Retencion, string Descripcion, string Tipo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var mensaje = "Registro modificado satisfactoriamente.";
                try
                {
                    var motivo = this.MotivosRetencionServicio.Obtener((m) => m.Codigo_Motivo_Retencion == Codigo_Motivo_Retencion).SingleOrDefault();
                    if (motivo == null)
                    {
                        motivo = new Motivos_Retencion
                        {
                            Codigo_Motivo_Retencion = Codigo_Motivo_Retencion,
                            Tipo = Tipo,
                            Descripcion = Descripcion,
                            Activo = true
                        };
                        this.MotivosRetencionServicio.Agregar(motivo);
                        mensaje = "Registro Guardado Satifactoriamente.";
                    }
                    else
                    {
                        motivo.Descripcion = Descripcion;
                        motivo.Tipo = Tipo;
                        motivo.Activo = true;
                        this.MotivosRetencionServicio.Editar(motivo);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
                scope.Complete();
                return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public JsonResult Detalle(int CodigoMotivo, int Codigo_Fabrica)
        {
            var motivo = this.MotivosRetencionServicio.Obtener((t) => t.Codigo_Motivo_Retencion == CodigoMotivo).SingleOrDefault();

            if (motivo != null)
                return Json(new { data = new { Codigo_Motivo_Retencion = motivo.Codigo_Motivo_Retencion, Descripcion = motivo.Descripcion }, success = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimo = 1;
                if (this.MotivosRetencionServicio.Obtener().Count != 0)
                    ultimo = this.MotivosRetencionServicio.Obtener().Max(p => p.Codigo_Motivo_Retencion);

                return Json(new { data = ultimo, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int CodigoMotivo = 0, int Codigo_Fabrica = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Motivos_Retencion est = this.MotivosRetencionServicio.Obtener((x) => x.Codigo_Motivo_Retencion == CodigoMotivo).SingleOrDefault();
                    est.Activo = false;
                    this.MotivosRetencionServicio.Editar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
