﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Produccion.Sesiones;
using MVC_Produccion.ContextoProduccion;

namespace MVC_Produccion.Backend.Security
{
    public class SecurityActions
    {
        private Entities db;

        public bool ValidarLogin()
        {
            OpcionesTrabajo configuraciones = new OpcionesTrabajo();
            configuraciones = db.OpcionesTrabajo.FirstOrDefault();

            bool logueado = true;

            if (configuraciones.Utiliza_Login)
            {
                logueado = Sesion.UsuarioActual != null;

                Permisos permisosUsuario = new Permisos();

                permisosUsuario = db.Permisos.Where(p => p.idUsuario == Sesion.UsuarioActual.idUsuario).FirstOrDefault();

                logueado = permisosUsuario.Opciones_Trabajo;
            }

            return logueado;
        }

        public SecurityActions(Entities dbConnection)
        {
            this.db = dbConnection;
        }
    }
}