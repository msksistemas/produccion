﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC_Produccion.ContextoProduccion;

namespace MVC_Produccion.Backend.Formulas
{
    public class UtilidadesFormulas
    {

        private static UtilidadesFormulas instance;

        private UtilidadesFormulas() { }

        public static UtilidadesFormulas Instance
        {
            get
            {

                if (instance == null)
                    instance = new UtilidadesFormulas();

                return instance;

            }
        }

        public ContextoProduccion.Formulas CopiarFormula(ContextoProduccion.Formulas formula, Entities db)
        {

            int maxID = db.Formulas.Max(f => f.Codigo_Formula);

            var formulaNueva = new ContextoProduccion.Formulas();

            formulaNueva.Historico = false;

            formulaNueva.Codigo_Formula = maxID + 1;
            formulaNueva.Fecha_Alta = DateTime.Now;
            formulaNueva.Codigo_Producto = formula.Codigo_Producto;
            formulaNueva.Comentario = "[COPIA DE FÓRMULA " + formula.Codigo_Formula.ToString() + "]  " + formula.Comentario;
            formulaNueva.Factor = formula.Factor;
            formulaNueva.Kilos_Teoricos = formula.Kilos_Teoricos;
            formulaNueva.Litros_Tina = formula.Litros_Tina;
            formulaNueva.Kilos_Masa = formula.Kilos_Masa;
            formulaNueva.Litros_Suero = formula.Litros_Suero;
            formulaNueva.Litros_Crema = formula.Litros_Crema;

            /*GUARDA TODOS LOS DETALLES ASOCIADOS*/
            foreach (var detalle in formula.Detalle_Formula.ToList())
            {
                if (detalle != null)
                {
                    var nuevoDetalle = new Detalle_Formula();

                    nuevoDetalle.Cantidad = detalle.Cantidad;
                    nuevoDetalle.Codigo_Formula = formulaNueva.Codigo_Formula;
                    nuevoDetalle.Codigo_Insumo = detalle.Codigo_Insumo;
                    nuevoDetalle.Codigo_Interno_Formula = formulaNueva.Codigo_Formula;
                    nuevoDetalle.Costo = detalle.Costo;
                    nuevoDetalle.Marca = detalle.Marca;
                    nuevoDetalle.Precio_Unitario = detalle.Precio_Unitario;
                    nuevoDetalle.Insumos = detalle.Insumos;

                    formulaNueva.Detalle_Formula.Add(nuevoDetalle);
                }
            }

            /*GUARDA TODOS LOS COSTOS INDIRECTOS ASOCIADOS EN COSTOS_INDIRECTOS_FORMULA*/
            foreach (var costo in formula.Costos_Indirectos_Formula.ToList())
            {
                if (costo != null)
                {

                    var nuevoCosto = new Costos_Indirectos_Formula();

                    nuevoCosto.Cantidad = costo.Cantidad;
                    nuevoCosto.Codigo_Formula = formulaNueva.Codigo_Formula;
                    nuevoCosto.Costos_Indirectos = costo.Costos_Indirectos;
                    nuevoCosto.Marca = costo.Marca;
                    nuevoCosto.Porcentaje = costo.Porcentaje;
                    nuevoCosto.Precio = costo.Precio;
                    nuevoCosto.Precio_Fijo = costo.Precio_Fijo;

                    formulaNueva.Costos_Indirectos_Formula.Add(nuevoCosto);

                }

            }

            foreach (var insumo in formula.Insumos.ToList())
            {
                if (insumo != null)
                {
                    formulaNueva.Insumos.Add(insumo);

                    var recuperado = formula.Insumos_Formulas_Recuperado.Where(i => i.Codigo_Insumo == insumo.Codigo_Insumo).Single();

                    formulaNueva.Insumos_Formulas_Recuperado.Add(recuperado);
                }
            }

            formula.Historico = true;

            return formulaNueva;

        }

    }
}