﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVC_Produccion.ContextoProduccion;

namespace MVC_Produccion.Backend.Envasados
{
    /**
     *  <summary>Representa un envasado de algún producto</summary> 
     */
    public class Envasado
    {
        private bool creado; // indica si ya agregue este envasado al contexto

        private Entities db;

        private ContextoProduccion.Envasado envasado = new ContextoProduccion.Envasado();

        /**
         *  <param name="lote">Lote asociado al envasado</param>
         *  <param name="fecha">Fecha de envasado</param>
         *  <param name="humedad">Humedad del envasado</param>
         *  <param name="sucursal">Sucursal de emision de envasado</param>
         *  <param name="db">Contexto de base de datos a usar</param>
         */
        public Envasado(string lote, DateTime? fecha, decimal? humedad, int? sucursal, Entities db)
        {
            this.db = db;

            this.envasado.Historico = false;
            this.envasado.Humedad = humedad;
            this.envasado.Lote = lote;
            this.envasado.Fecha = fecha;
            this.envasado.Sucursal = sucursal;

            this.creado = false;
        }
        //

        /**
         *  <summary>Crea un nuevo detalle en el envasado</summary> 
         *  <param name="producto">Número de producto de ventas</param>
         *  <param name="kilos">Kilos envasados</param>
         *  <param name="cantidad">unidades envasadas</param>
         */
        public Envasado CrearDetalle(string producto, decimal? kilos, int? cantidad)
        {
            Detalle_Envasado nuevoDetalle = new Detalle_Envasado();

            Formulas_Envasado formulaEnvasadoRetornado = db
                        .Formulas_Envasado
                            .FirstOrDefault(formula => formula.Numero_Producto_Ventas == producto);

            nuevoDetalle.Formulas_Envasado = formulaEnvasadoRetornado;
            nuevoDetalle.Codigo_Interno_Formula = formulaEnvasadoRetornado.Codigo_Interno_Formula;
            nuevoDetalle.Cantidad = cantidad;
            nuevoDetalle.Kilos = kilos;
            nuevoDetalle.Historico = false;

            this.envasado.Detalle_Envasado.Add(nuevoDetalle);

            return this;
        }
        //

        /**
         *  <summary>
         *      Crea el envasado asociandolo al contexto (no guarda) debe llamarse luego a 
         *      SaveChanges() del db context asociado
         *  </summary> 
         */
        public Envasado Crear()
        {
            // asocio este envasado al contexto
            // si no fue asociado antes
            if (!this.creado)
            {
                this.creado = false;
                this.db.Envasado.Add(this.envasado);
            }

            return this;
        }
        //
    }
    //
}