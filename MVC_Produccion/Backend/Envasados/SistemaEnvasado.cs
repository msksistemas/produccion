﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Produccion.ContextoProduccion;

namespace MVC_Produccion.Backend.Envasados
{
    /**
     *  <summary>Utilidades para generación de envasados</summary> 
     */
    public class SistemaEnvasado
    {
        private Entities db = null;

        public SistemaEnvasado(Entities db = null)
        {
            this.db = db;
            if (this.db == null) this.db = new Entities();
        }

        /// <summary>
        ///     Retiene un detalle de envasado segun la informacion proporcionada.
        ///     Solo pasar los datos del detalle de envasado y la descripcion de la retencion como parametro
        ///     en un objeto InfoRetencionEnvasado. El envasado sera retenido con dicha información.
        /// </summary>
        /// <param name="info">Información proporcionada para retener el envasado</param>
        public void RetenerDetalleEnvasado(InfoRetencionEnvasado info)
        {
            // obtengo el detale de envasado a retener por codigo
            var detalle = db.Detalle_Envasado.Where(d => d.Codigo_Interno_Detalle == info.CodigoDetalle).Single();
            detalle.Historico = false;
            // verifico si existe algun motivo ya creado igual
            Motivos_Retencion motivo = db.Motivos_Retencion.FirstOrDefault(motivor => motivor.Descripcion == info.MotivoRetencion);
            
            // si no existe motivo con la descripcion brindada
            // creo uno para asignar al detalle retenido
            if(motivo == null)
            {
                // compruebo si ya existe alguno
                int motivoid = db.Motivos_Retencion.Max(motivor => motivor.Codigo_Motivo_Retencion) + 1;

                motivo = new Motivos_Retencion() {
                    Codigo_Motivo_Retencion = motivoid,
                    Descripcion = info.MotivoRetencion,
                    Tipo = "Calidad"
                };
            }

            if (detalle.Kilos == info.Kilos && detalle.Cantidad == info.Cantidad)
            {
                detalle.Motivos_Retencion = motivo;
                detalle.Fecha_Retenido = info.Fecha;
                detalle.Id_Usuario_Retiene = info.IdUsuario;
                detalle.Comentario_Retencion = info.ComentarioRetencion;
            }
            else
            {
                detalle.Cantidad -= info.Cantidad;
                detalle.Kilos -= info.Kilos;

                Detalle_Envasado nuevo_detalle = new Detalle_Envasado();
                nuevo_detalle.Historico = false;
                nuevo_detalle.Cantidad = info.Cantidad;
                nuevo_detalle.Kilos = info.Kilos;
                nuevo_detalle.Codigo_Interno_Formula = detalle.Codigo_Interno_Formula;
                nuevo_detalle.Motivos_Retencion = motivo;
                nuevo_detalle.Lote = detalle.Lote;
                nuevo_detalle.Codigo_Interno_Envasado = detalle.Codigo_Interno_Envasado;
                nuevo_detalle.Vencimiento = detalle.Vencimiento;
                nuevo_detalle.Fecha_Retenido = info.Fecha;
                nuevo_detalle.Id_Usuario_Retiene = info.IdUsuario;
                nuevo_detalle.Comentario_Retencion = info.ComentarioRetencion;
                db.Detalle_Envasado.Add(nuevo_detalle);
            }
            db.SaveChanges();
        }
    }
    //

    public class InfoRetencionEnvasado
    {
        public int CodigoDetalle { get; set; }
        public string MotivoRetencion { get; set; }
        public decimal Kilos { get; set; }
        public int Cantidad { get; set; }
        public int IdUsuario { get; set; }
        public DateTime Fecha { get; set; }
        public string ComentarioRetencion { get; set; }
    }

}