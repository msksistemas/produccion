﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Produccion.Utilidades
{
    public class CampoEditable<T>
    {
        public int id { get; private set; }
        public T valor { get; private set; }

        public CampoEditable(string id, string campo, IFormatProvider format = null)
        {
            IFormatProvider provider = format;

            if (provider == null)
                provider = CultureInfo.CurrentCulture;

            int ids = int.Parse(id);
            
            string campoLimpio = campo.Replace('\n', char.MinValue);
            campoLimpio = campoLimpio.Replace('\r', char.MinValue);

            this.valor = (T) Convert.ChangeType(campoLimpio, typeof(T), provider);
            this.id = ids;
        }
    }
}