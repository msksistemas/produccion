//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_Produccion.ContextoGastos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Entregas
    {
        public int Nro_Entrega { get; set; }
        public string Tipo_Entrega { get; set; }
        public int TAMBO { get; set; }
        public Nullable<int> DESCARGA { get; set; }
        public string TURNO { get; set; }
        public Nullable<int> LITROS { get; set; }
        public Nullable<int> Litros_x_Remito { get; set; }
        public Nullable<int> Litros_SRL { get; set; }
        public Nullable<int> Nro_Constancia { get; set; }
        public Nullable<int> Nro_Remito { get; set; }
        public Nullable<decimal> Tenor_Graso { get; set; }
        public Nullable<decimal> PROTEINAS { get; set; }
        public Nullable<decimal> DENSIDAD { get; set; }
        public Nullable<decimal> TEMPERATURA { get; set; }
        public string AGUADO { get; set; }
        public Nullable<decimal> REDUCTASA { get; set; }
        public string INHIBIDORES { get; set; }
        public System.DateTime STRFECHA { get; set; }
        public Nullable<decimal> GRASA { get; set; }
        public Nullable<byte> APTA { get; set; }
        public string Tipo_No_Apta { get; set; }
        public Nullable<byte> ANTIBIOTICOS { get; set; }
        public Nullable<byte> ACIDEZ { get; set; }
        public string BORRAR { get; set; }
        public short DESTINO { get; set; }
        public string COMENTARIO { get; set; }
        public Nullable<byte> Nro_Silo { get; set; }
        public Nullable<decimal> Porc_Penalizado { get; set; }
        public Nullable<int> Litros_Sin_Penalizar { get; set; }
        public Nullable<int> LitrosRemito_Sin_Penalizar { get; set; }
        public string Nro_Muestra { get; set; }
        public string Patente_Camion { get; set; }
        public Nullable<byte> HORA { get; set; }
        public Nullable<byte> MINUTOS { get; set; }
        public Nullable<byte> Control_Oficina { get; set; }
        public Nullable<byte> Ya_Liquidado { get; set; }
        public Nullable<decimal> Porc_Aguado { get; set; }
        public Nullable<decimal> Porc_Inhibidores { get; set; }
        public Nullable<byte> HISTORICO { get; set; }
        public string NroRemito_Camion { get; set; }
        public Nullable<System.DateTime> StrFecha_Entrega { get; set; }
        public Nullable<int> COD_PLANTA { get; set; }
        public Nullable<int> CODIGO_PATRON_ANALISIS { get; set; }
    }
}
