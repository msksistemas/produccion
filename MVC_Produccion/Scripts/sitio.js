﻿function setCursorToEnd(ele) {
    var range = document.createRange();
    var sel = window.getSelection();
    range.setStart(ele, 1);
    range.collapse(true);
    sel.removeAllRanges();
    sel.addRange(range);
}

function getPrevSib(n) {
    var x = n.previousSibling;
    while (x && x.nodeType != 1) {
        x = x.previousSibling;
    }
    return x;
}

// Control editable para tabla
class ControlEditable {
    constructor(selector, action="", fieldName="") {
        // cargo el editor
        this.action = action;
        this.fieldName = fieldName;
        this.editing = -1;

        this.editcontent = "";

        this.sending = false;
        this.typing = false;

        // editor init
        this.loadEditor(selector);
    }

    loadEditor(selector = null) {
        // busco los elementos para llevar el seguimiento
        // sí proporciono nuevo solector creo nuevos elementos
        if (selector != null) {
            this.elms = $(selector);
            this.editing = -1;
            this.obj_selector = selector;
        }

        else {
            this.elms = $(this.obj_selector);
            this.editable(this.elms[this.editing]);
        }

        // preparo los editables
        for (let i = 0; i < this.elms.length; i++) {
            // instancia del elemento jquery
            let control = $(this.elms[i]);

            // marco como editable a los elementos para usarlos
            if (!control.hasClass("editable"))
                control.addClass("editable");

            var _this = this;

            // registro el evento para agarrar el input del usuario
            control.click((e) => {
                _this.elm_onEditarContenido(i);
            });

            control.on("input", function (e) {
                e.preventDefault();
                _this.elm_onInput(e.target.textContent);
            });

            control.keydown(function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    _this.stopEdit();
                }
            });
        }
    }

    editable(element) {
        if (element) {
            let elm = $(element);
            elm.attr("contenteditable", true);
            elm.attr("data-toggle", "tooltip");
            elm.attr("data-placement", "bottom");
            elm.attr("title", "Haga click para editar y presiona tecla <ENTER> para guardar.");
            elm.tooltip();

            if (!elm.hasClass("editing"))
                elm.addClass("editing");

            if (!this.typing)
                this.editcontent = elm.text();

            elm.text(this.editcontent);

            setCursorToEnd(element);
        }
    }

    stopEdit() {
        var control = null;

        try {
            control = $(this.elms[this.editing]);
        } catch (err) {}

        if (this.fieldName && this.action && !this.sending) {
            var _this = this;
            this.sending = true;

            let data = new FormData();
            data.append(_this.fieldName, this.editcontent);

            let field = $(this.elms[this.editing]);

            let rowID = null;

            try {
                rowID = parseInt(field.parent().children('td').first().text());
            }
            catch (err) {
                console.log("[CAMPO EDITABLE] ERROR AL ENVIAR CAMPO NO SE ENCONTRO COLUMNA DE ID: ", err);
            }

            console.log("[CAMPO EDITABLE] ENVIANDO CAMPO ID:", rowID);

            if (rowID != null && rowID != undefined) {
                data.append("id", rowID);

                if (field.hasClass("editing"))
                    field.removeClass("editing");

                $.ajax({
                    url: _this.action,
                    method: "POST",
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function () {
                        console.log("[CAMPO EDITABLE] CONTROL:", control);

                        if (control) control.blur();

                        _this.editcontent = "";
                        _this.editing = -1;
                        _this.sending = false;
                        _this.typing = false;
                    },
                    error: function () {
                        if (control) control.blur();

                        _this.editcontent = "";
                        _this.editing = -1;
                        _this.sending = false;
                        _this.typing = false;
                    }
                });
            }
            else {
                if (control) control.blur();

                this.editcontent = "";
                this.editing = -1;
                this.sending = false;
                this.typing = false;
            }
        }
        else {
            if (this.fieldName == "" || this.action == "") {
                if (control) control.blur();

                let faltante = this.fieldName == "" ? 'fieldName' : "action";
                console.log("Error al editar campo: no se proporciono " + faltante + " en el constructor.");
            }

            this.editcontent = "";
            this.editing = -1;
        }
    }

    elm_onInput(input) {
        let element = null;
        this.typing = true;

        if (this.editing > 0)
            element = this.elms[this.editing];

        let data = input.replace(/(\r\n|\n|\r)/gm, "");
        data = data.replace(/[a-z]/gi, '');
        data = data.replace(/ +/g, '');

        this.editcontent = data;

        if (element != null)
            $(element).text(this.editcontent);

        if (this.editcontent != "") {
            try {
                setCursorToEnd(element);
            }
            catch (err) {}
        }
    }

    elm_onEditarContenido(index) {
        if (this.editing != index && index >= 0) {
            this.editing = index;

            let element = this.elms[this.editing];

            if (!$(element).hasClass("editable"))
                $(element).addClass("editable");

            this.editcontent = $(element).text();
            this.editable(element);
        }
    }
}
