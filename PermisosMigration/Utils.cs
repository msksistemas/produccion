﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace PermisosMigration
{
    class Utils
    {
        /// <summary>
        /// Los nombres de modulo en el nuevo esquema son diferentes a los viejos, se utiliza esta funcion para normalizarlos
        /// </summary>
        /// <param name="name">Nombre del modulo</param>
        /// <returns></returns>
        public static string NormalizeModuleName(string name)
        {
            switch (name)
            {
                case "Liberar_Por_Calidad":
                    return "Liberar_Calidad";
                case "Menu_Insumos":
                    return "Menu_Articulos";
                case "Ajustes_Insumos":
                    return "Ajuste_Articulos";
                case "Ingreso_Leche":
                    return "Movimientos_Leche";
                case "Detallado_Formula":
                    return "Listado_Detallado_Formula";
                case "Detallado_Formula_Envasado":
                    return "Listado_Detallado_Formula_Envasado";
                case "Detallado_Produccion":
                    return "Listado_Producciones_Detalladas";
                case "Stock_Insumos":
                    return "Listado_Stock_Articulos";
                case "Resumen_Produccion":
                    return "Listado_Resumen_Produccion";
                case "Expedicion":
                    return "Expediciones";
                case "Compras_Proveedor":
                    return "Listado_Compras_Proveedor";
                case "Listados_Insumos":
                    return "Listado_Articulos";
                case "Stock_Leche":
                    return "Movimientos_Leche";
                case "Costos_Productos":
                    return "Listado_Costos_Producto";
                case "Seguimiento_Lote":
                    return "Listado_Seguimiento_Lote";
                case "Variacio_Precios":
                    return "Listado_Variacion_Precios";
                case "Listado_Merma":
                    return "Listado_Restante_Produccion";
                case "Liberacion_Condicional":
                    return "Liberar_Calidad";
                case "Listado_Expedicion":
                    return "Listado_Expedido";
                case "SimuladorCostos":
                    return "Simulador_Costos";
                default:
                    return name;
            }
        }

        /// <summary>
        /// Obtiene una lista de nombres de modulos del esquema previo
        /// </summary>
        /// <returns></returns>
        public static List<string> GetOldModuleNames()
        {
            ProdEntities prod = new ProdEntities();
            var results = prod.Database.SqlQuery<string>("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_catalog = 'Prod' AND table_name = 'Permisos' ");

            // Obtenemos la lista de modulos ignorando id de la fila e id de usuario
            return results.Skip(2).ToList();
        }

        /// <summary>
        /// Obtiene true o false dependiendo si el usuario tiene accesso al modulo
        /// </summary>
        /// <param name="name">Nombre del modulo</param>
        /// <param name="userId">Id de Usuario</param>
        /// <returns></returns>
        public static bool GetAccessFromModule(string name, int userId)
        {
            ProdEntities prod = new ProdEntities();
            var raw = prod.Database.SqlQuery<bool>($"SELECT {name} FROM Permisos where idUsuario = {userId}");

            return raw.ToList()[0];
        }

        /// <summary>
        /// Obtiene el id de un modulo en el nuevo esquema
        /// </summary>
        /// <param name="name">Nombre del modulo (sin normalizar)</param>
        /// <returns></returns>
        public static int GetIdNewModule(string name)
        {
            ProdEntities prod = new ProdEntities();
            var results = prod.Database.SqlQuery<Int64>($"SELECT Id FROM Modules WHERE Name = '{NormalizeModuleName(name)}'");

            List<Int64> ids = results.ToList();

            return ids.Count > 0 ? (int)ids[0] : -1;
        }

        /// <summary>
        /// Funcion helper para escribir en pantalla los modulos que estan siendo actualizados
        /// </summary>
        /// <param name="id">Id del modulo</param>
        /// <param name="name">Nombre del modulo</param>
        public static void ConsoleWriteModule(int id, string name)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write($"{id}");
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write($" - {name} / {Utils.NormalizeModuleName(name)}");
        }

        /// <summary>
        /// Inserta un registro en la tabla Modules_Access para darle acceso a un usuario en el nuevo esquema
        /// </summary>
        /// <param name="moduleId">Id del modulo</param>
        /// <param name="userId">Id de usuario</param>
        /// <returns></returns>
        public static bool InsertNewAccess(int moduleId, int userId)
        {
            bool inserted = false;

            // Significa que el modulo existe en el nuevo esquema
            if (moduleId != -1)
            {
                ProdEntities prod = new ProdEntities();

                var access = prod.Database.SqlQuery<Int64>($"SELECT UserId FROM Modules_Access WHERE UserID = {userId} AND Module = {moduleId}");

                // No hay registros ingresados sobre dicho usuario y modulo aun
                if (access.ToList().Count == 0)
                {
                    int results = prod.Database.ExecuteSqlCommand($"INSERT INTO Modules_Access VALUES ({moduleId}, {userId})");
                    if (results > 0) inserted = true;
                }
            }

            return inserted;
        }
    }
}
