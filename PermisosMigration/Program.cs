﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace PermisosMigration
{
    class Program
    {
        static int Main(string[] args)
        {
            
            if (args.Length == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nDebe pasarse el id de usuario.");
                Console.ForegroundColor = ConsoleColor.White;
                return 0;
            }

            int userId = int.Parse(args[0]);
            Console.WriteLine($"\nObteniendo modulos del usuario {userId}");

            int i = 1;
            List<string> modules = Utils.GetOldModuleNames();
            foreach (string name in modules)
            {
                bool access = Utils.GetAccessFromModule(name, userId);
                if (access)
                {
                    // Obtiene el id del modulo en el nuevo esquema de permisos
                    int id = Utils.GetIdNewModule(name);

                    if (Utils.InsertNewAccess(id, userId))
                    {
                        Console.Write($"\n{i++} \t");
                        Utils.ConsoleWriteModule(id, name);
                    }
                }
            }

            if (i == 1)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n\nNingún registro fue agregado.");
                Console.ForegroundColor = ConsoleColor.White;
            }


            // Espera a cerrar
            Console.WriteLine("\n\nPresiona cualquier tecla para terminar...");
            Console.ReadKey();
            return 0;
           
        }
    }
}