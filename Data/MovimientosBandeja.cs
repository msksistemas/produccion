//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class MovimientosBandeja
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MovimientosBandeja()
        {
            this.DetallesMovimientosBandejas = new HashSet<DetallesMovimientosBandeja>();
        }
    
        public int CodigoMovimiento { get; set; }
        public int Origen { get; set; }
        public Nullable<int> Destino { get; set; }
        public System.DateTime Fecha { get; set; }
        public Nullable<int> Codigo_Interno_Expedicion { get; set; }
        public bool Historico { get; set; }
        public string Prefijo { get; set; }
        public string NroComprobante { get; set; }
        public Nullable<int> idUsuario { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesMovimientosBandeja> DetallesMovimientosBandejas { get; set; }
    }
}
