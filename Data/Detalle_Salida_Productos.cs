//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Detalle_Salida_Productos
    {
        public int Codigo_Interno_Detalle { get; set; }
        public string Codigo_Producto { get; set; }
        public Nullable<decimal> Cantidad { get; set; }
        public Nullable<decimal> Kilos_Litros { get; set; }
        public Nullable<int> Codigo_Interno_Salida { get; set; }
        public string Lote { get; set; }
        public string tipo { get; set; }
        public string Codigo_Insumo_Reproceso { get; set; }
    
        public virtual Salida_Productos Salida_Productos { get; set; }
    }
}
