//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Detalle_Produccion1
    {
        public int Codigo_Interno_Detalle { get; set; }
        public string Codigo_Insumo { get; set; }
        public Nullable<decimal> Precio_Unitario { get; set; }
        public Nullable<decimal> Cantidad { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Movimiento { get; set; }
        public Nullable<decimal> Ajuste { get; set; }
        public string Lote_Insumo { get; set; }
    
        public virtual Produccion1 Produccion { get; set; }
    }
}
