//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Usuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuario()
        {
            this.Ajustes_Insumos = new HashSet<Ajustes_Insumos>();
            this.Cambio_Estado = new HashSet<Cambio_Estado>();
        }
    
        public int idUsuario { get; set; }
        public string Usuario1 { get; set; }
        public string Contraseña { get; set; }
        public string Rol { get; set; }
        public Nullable<int> Fabrica { get; set; }
        public Nullable<bool> Muestra_Costos { get; set; }
        public string Id_Login { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ajustes_Insumos> Ajustes_Insumos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cambio_Estado> Cambio_Estado { get; set; }
    }
}
