//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Produccion_Leche_Polvo1
    {
        public int Codigo_Interno_Produccion { get; set; }
        public Nullable<int> Litros { get; set; }
        public Nullable<int> Codigo_Estado { get; set; }
        public Nullable<int> Codigo_Quesero { get; set; }
        public decimal Piezas_Obtenidas { get; set; }
        public Nullable<decimal> Kilos_Teoricos { get; set; }
        public Nullable<decimal> Kilos_Reales { get; set; }
        public Nullable<decimal> Kilos_Envasado { get; set; }
        public Nullable<bool> Historico { get; set; }
        public Nullable<int> Codigo_Fabrica { get; set; }
        public Nullable<int> Codigo_Camara { get; set; }
        public decimal Dens_Leche_Liquida { get; set; }
        public decimal Ph_Leche_Liquida { get; set; }
        public decimal Temp_Leche_Liquida { get; set; }
        public string Prueba_Alcohol { get; set; }
        public string Prueba_Antibioticos { get; set; }
        public decimal Proteinas_Leche_Polvo { get; set; }
        public decimal Graso_Leche_Polvo { get; set; }
        public decimal Materia_Grasa_Leche_Polvo { get; set; }
        public decimal Cenizas_Totales_Leche_Polvo { get; set; }
        public decimal Humedad_Leche_Polvo { get; set; }
        public decimal Acidez_Leche_Polvo { get; set; }
        public decimal Acidez_1_Leche_Polvo { get; set; }
        public decimal Ph_Leche_Polvo { get; set; }
        public decimal Indice_Insolubilidad_Leche_Polvo { get; set; }
        public decimal Particulas_Quemadas_Leche_Polvo { get; set; }
        public decimal Bacterias_Aerobias_Mesofilas_Totales { get; set; }
        public decimal Coliformes_Totales { get; set; }
        public decimal Coliformes_Fecales { get; set; }
        public decimal Escherichia_Coli { get; set; }
        public decimal salmonella { get; set; }
        public decimal Staphylococcus_aureus { get; set; }
        public decimal G { get; set; }
        public decimal SNG { get; set; }
        public decimal Proteinas { get; set; }
        public decimal Acidez { get; set; }
        public Nullable<decimal> Humedad { get; set; }
    
        public virtual Motivos_Retencion1 Motivos_Retencion { get; set; }
        public virtual Produccion1 Produccion { get; set; }
    }
}
