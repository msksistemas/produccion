//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Costos_Indirectos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Costos_Indirectos()
        {
            this.Costos_Indirectos_Formula = new HashSet<Costos_Indirectos_Formula>();
            this.Costos_Indirectos_Produccion = new HashSet<Costos_Indirectos_Produccion>();
            this.Costos_Indirectos_Formula_Envasado = new HashSet<Costos_Indirectos_Formula_Envasado>();
            this.Costos_Indirectos_Formula_Lavados = new HashSet<Costos_Indirectos_Formula_Lavados>();
        }
    
        public int Codigo_Interno_Costo_Indirecto { get; set; }
        public string Codigo_Costo_Indirecto { get; set; }
        public string Descripcion { get; set; }
        public Nullable<decimal> Porcentaje { get; set; }
        public Nullable<decimal> Precio { get; set; }
        public Nullable<decimal> Precio_Fijo { get; set; }
        public string Porcentaje_Aplica { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Formula> Costos_Indirectos_Formula { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Produccion> Costos_Indirectos_Produccion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Formula_Envasado> Costos_Indirectos_Formula_Envasado { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Formula_Lavados> Costos_Indirectos_Formula_Lavados { get; set; }
        public virtual Moneda Monedas { get; set; }
    }
}
