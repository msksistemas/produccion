//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Detalle_Formula
    {
        public int Codigo_Interno_Formula { get; set; }
        public int Codigo_Formula { get; set; }
        public string Codigo_Insumo { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Costo { get; set; }
        public decimal Precio_Unitario { get; set; }
        public bool Marca { get; set; }
    
        public virtual Insumo Insumo { get; set; }
        public virtual Formula Formula { get; set; }
    }
}
