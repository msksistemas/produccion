//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Mov_Stock
    {
        public int id { get; set; }
        public string Codigo_Prod { get; set; }
        public string Tipo_Mov { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Estado { get; set; }
        public Nullable<decimal> Cantidad { get; set; }
        public Nullable<decimal> Kilos { get; set; }
        public string Nro_Comprobante { get; set; }
        public string Lote { get; set; }
        public Nullable<int> Fabrica { get; set; }
        public Nullable<bool> EsAjuste { get; set; }
    
        public virtual Detalle_Envasado Detalle_Envasado { get; set; }
        public virtual Produccion Produccion { get; set; }
    }
}
