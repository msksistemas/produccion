//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ajustes_Insumos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Ajustes_Insumos()
        {
            this.Detalle_Ajuste_Insumos = new HashSet<Detalle_Ajuste_Insumos>();
        }
    
        public int Codigo_Interno_Ajuste { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Comentario { get; set; }
        public string Tipo_Movimiento { get; set; }
        public Nullable<int> idUsuario { get; set; }
        public string Numero_Comprobante { get; set; }
        public Nullable<decimal> Descuento { get; set; }
        public Nullable<bool> Historico { get; set; }
        public Nullable<int> Fabrica { get; set; }
        public Nullable<int> Fabrica_Origen { get; set; }
        public Nullable<bool> Es_Traspaso { get; set; }
        public string Estado { get; set; }
        public string Motivo_Rechazo { get; set; }
        public int Prefijo { get; set; }
        public Nullable<int> Codigo_Proveedor { get; set; }
    
        public virtual Usuario Usuario { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Ajuste_Insumos> Detalle_Ajuste_Insumos { get; set; }
    }
}
