//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetallesMovimientosBandeja
    {
        public int CodigoDetalle { get; set; }
        public string Codigo_Insumo { get; set; }
        public int Unidades { get; set; }
        public int CodigoMovimiento { get; set; }
    
        public virtual Insumo Insumo { get; set; }
        public virtual MovimientosBandeja MovimientosBandeja { get; set; }
    }
}
