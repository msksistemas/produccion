//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    
    public partial class GetDetallesExpedicion_Result
    {
        public int Codigo_Interno_Detalle { get; set; }
        public Nullable<int> Codigo_Interno_Expedicion { get; set; }
        public string Numero_Producto_Ventas { get; set; }
        public string Lote { get; set; }
        public string Descripcion_Producto { get; set; }
        public Nullable<decimal> Kilos { get; set; }
        public Nullable<int> Cantidad { get; set; }
        public string Codigo_Producto_Produccion { get; set; }
        public string Descripcion_Producto_Produccion { get; set; }
    }
}
