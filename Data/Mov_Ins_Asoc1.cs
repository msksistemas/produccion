//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Mov_Ins_Asoc1
    {
        public int Codigo_Interno_Movimiento { get; set; }
        public Nullable<decimal> Litros { get; set; }
        public string Lote { get; set; }
        public string Codigo_Insumo { get; set; }
    
        public virtual Insumos Insumos { get; set; }
        public virtual Produccion1 Produccion { get; set; }
    }
}
