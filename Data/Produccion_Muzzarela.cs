//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Produccion_Muzzarela
    {
        public int Codigo_Interno_Produccion { get; set; }
        public Nullable<decimal> Kilos_Teoricos { get; set; }
        public Nullable<decimal> Kilos_Reales { get; set; }
        public Nullable<decimal> Ph_Final { get; set; }
        public string Lote { get; set; }
        public Nullable<System.DateTime> Fecha_Vencimiento { get; set; }
        public Nullable<int> Piezas_Obtenidas { get; set; }
        public Nullable<decimal> Kilos_Masa { get; set; }
        public Nullable<decimal> Ph_Masa { get; set; }
        public Nullable<decimal> Rendimiento_Masa { get; set; }
        public Nullable<decimal> Crema_Obtenida_Masa { get; set; }
        public Nullable<System.DateTime> Fecha_Elaboracion_Masa { get; set; }
        public Nullable<System.TimeSpan> Hora_Inicio { get; set; }
        public Nullable<decimal> Ph_Mezcla { get; set; }
        public Nullable<decimal> Temp_Hilado { get; set; }
        public Nullable<decimal> Perdidas { get; set; }
        public Nullable<decimal> Humedad { get; set; }
        public Nullable<System.DateTime> Fecha_Elaboracion { get; set; }
        public Nullable<System.TimeSpan> Hora_Fin { get; set; }
        public Nullable<System.TimeSpan> Hora_Inicio_Descarga { get; set; }
        public Nullable<System.TimeSpan> Hora_Fin_Descarga { get; set; }
        public Nullable<decimal> Unidades_Masa { get; set; }
    
        public virtual Produccion Produccion { get; set; }
    }
}
