﻿using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Gastos;
using AccesoDatos.Contexto.Ventas;
using Servicios.Interfaces;
using Servicios.Clases;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SimpleInjector.Integration.Web.Mvc;
using System.Reflection;
using SimpleInjector.Advanced;
using MVC_Produccion.Models;
using System.Net.Http;

namespace MVC_Produccion
{
    public class UltimoParametroConstructorBehavior : IConstructorResolutionBehavior
    {
        public ConstructorInfo GetConstructor(Type implementationType) => (

            from ctor in implementationType.GetConstructors()

            orderby ctor.GetParameters().Length ascending

            select ctor)

            .First();
    }
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = new Container();
            container.Options.ConstructorResolutionBehavior = new UltimoParametroConstructorBehavior();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            RegistrarServicios(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        protected void Session_End(Object sender, EventArgs e)
        {
            //var user = HttpContext.Current.User;
            //if (user != null)
            //{
            //    var account = new AccountController();
            //    string id_user = user.Identity.GetUserId();
            //    if (!string.IsNullOrEmpty(id_user))
            //        account.LogOff();
            //}
        }
        /// <summary>
        /// Este metodo registra todos los servicios en el container que se pasa por parametro.
        /// Agregar aqui aquellos servicios que quiera utilizar.
        /// </summary>
        /// <param name="container"></param>
        private void RegistrarServicios(Container container)
        {
            // Registro de los servicios.
            //Se encuentran acomodados alfabeticamente segun la entidad.

            container.Register<IProduccionServicio, ProduccionServicio>(Lifestyle.Scoped);
            container.Register<IBandejaServicio, BandejaServicio>(Lifestyle.Scoped);

            container.Register<IBaseServicio<Acidificacion>, BaseServicio<Acidificacion>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Acidificacion>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<AspNetUsers>, BaseServicio<AspNetUsers>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<AspNetUsers>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Ajustes_Insumos>, BaseServicio<Ajustes_Insumos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Ajustes_Insumos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<MovimientosBandejas>, BaseServicio<MovimientosBandejas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<MovimientosBandejas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<DetallesMovimientosBandejas>, BaseServicio<DetallesMovimientosBandejas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<DetallesMovimientosBandejas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Camaras>, BaseServicio<Camaras>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Camaras>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Cambio_Estado>, BaseServicio<Cambio_Estado>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Cambio_Estado>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<CompraInsumos>, BaseServicio<CompraInsumos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<CompraInsumos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Cond_Pago>, BaseServicio<Cond_Pago>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Cond_Pago>>(instancia => { instancia.SetearContexto(new dbGastos()); });

            container.Register<IBaseServicio<Costos_Indirectos>, BaseServicio<Costos_Indirectos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Costos_Indirectos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Costos_Indirectos_Formula>, BaseServicio<Costos_Indirectos_Formula>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Costos_Indirectos_Formula>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Costos_Indirectos_Formula_Envasado>, BaseServicio<Costos_Indirectos_Formula_Envasado>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Costos_Indirectos_Formula_Envasado>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Costos_Indirectos_Formula_Lavados>, BaseServicio<Costos_Indirectos_Formula_Lavados>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Costos_Indirectos_Formula_Lavados>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Costos_Indirectos_Produccion>, BaseServicio<Costos_Indirectos_Produccion>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Costos_Indirectos_Produccion>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Ajuste_Insumos>, BaseServicio<Detalle_Ajuste_Insumos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Ajuste_Insumos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Produccion>, BaseServicio<Detalle_Produccion>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Produccion>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Datos_Empresa>, BaseServicio<Datos_Empresa>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Datos_Empresa>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Compra>, BaseServicio<Detalle_Compra>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Compra>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Insumo_Envasado>, BaseServicio<Detalle_Insumo_Envasado>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Insumo_Envasado>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Envasado>, BaseServicio<Detalle_Envasado>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Envasado>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Expedicion>, BaseServicio<Detalle_Expedicion>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Expedicion>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Ingreso>, BaseServicio<Detalle_Ingreso>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Ingreso>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Imputacion>, BaseServicio<Detalle_Imputacion>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Imputacion>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Formula>, BaseServicio<Detalle_Formula>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Formula>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Formula_Lavados>, BaseServicio<Detalle_Formula_Lavados>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Formula_Lavados>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Formula_Envasado>, BaseServicio<Detalle_Formula_Envasado>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Formula_Envasado>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Limpieza>, BaseServicio<Detalle_Limpieza>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Limpieza>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Salida_Productos>, BaseServicio<Detalle_Salida_Productos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Salida_Productos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Orden_Compra>, BaseServicio<Detalle_Orden_Compra>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Orden_Compra>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Traspaso_Fabricas>, BaseServicio<Detalle_Traspaso_Fabricas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Traspaso_Fabricas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Envasado>, BaseServicio<Envasado>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Envasado>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<AccesoDatos.Contexto.Produccion.Estados>, BaseServicio<AccesoDatos.Contexto.Produccion.Estados>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<AccesoDatos.Contexto.Produccion.Estados>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Expedicion>, BaseServicio<Expedicion>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Expedicion>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Familias>, BaseServicio<Familias>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Familias>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Fabricas>, BaseServicio<Fabricas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Fabricas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Formulas>, BaseServicio<Formulas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Formulas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Formulas_Envasado>, BaseServicio<Formulas_Envasado>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Formulas_Envasado>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<HttpService>(Lifestyle.Scoped);
            container.Register<HttpClient>(Lifestyle.Scoped);

            container.Register<IBaseServicio<Ingreso_Leche>, BaseServicio<Ingreso_Leche>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Ingreso_Leche>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Imputaciones>, BaseServicio<Imputaciones>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Imputaciones>>(instancia => { instancia.SetearContexto(new dbGastos()); });

            container.Register<IBaseServicio<Impuestos>, BaseServicio<Impuestos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Impuestos>>(instancia => { instancia.SetearContexto(new dbVentas()); });

            container.Register<IBaseServicio<Insumos>, BaseServicio<Insumos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Insumos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Insumos_Formulas_Recuperado>, BaseServicio<Insumos_Formulas_Recuperado>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Insumos_Formulas_Recuperado>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<AccesoDatos.Contexto.Produccion.Lineas>, BaseServicio<AccesoDatos.Contexto.Produccion.Lineas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<AccesoDatos.Contexto.Produccion.Lineas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Lavados>, BaseServicio<Lavados>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Lavados>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Lavados_Formula>, BaseServicio<Lavados_Formula>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Lavados_Formula>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Limpieza>, BaseServicio<Limpieza>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Limpieza>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Localidades>, BaseServicio<Localidades>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Localidades>>(instancia => { instancia.SetearContexto(new dbVentas()); });

            container.Register<IBaseServicio<Motivos_Retencion>, BaseServicio<Motivos_Retencion>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Motivos_Retencion>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Monedas>, BaseServicio<Monedas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Monedas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Movimientos_Volteo>, BaseServicio<Movimientos_Volteo>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Movimientos_Volteo>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Mov_Ins_Asoc>, BaseServicio<Mov_Ins_Asoc>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Mov_Ins_Asoc>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Mov_Stock>, BaseServicio<Mov_Stock>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Mov_Stock>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Mov_Stock_Prod_Ventas>, BaseServicio<Mov_Stock_Prod_Ventas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Mov_Stock_Prod_Ventas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Rubros>, BaseServicio<Rubros>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Rubros>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Restantes_Envasado>, BaseServicio<Restantes_Envasado>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Restantes_Envasado>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Salida_Productos>, BaseServicio<Salida_Productos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Salida_Productos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Silos>, BaseServicio<Silos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Silos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Sucursales_Propias>, BaseServicio<Sucursales_Propias>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Sucursales_Propias>>(instancia => { instancia.SetearContexto(new dbVentas()); });

            container.Register<IBaseServicio<OpcionesTrabajo>, BaseServicio<OpcionesTrabajo>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<OpcionesTrabajo>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Orden_Compra>, BaseServicio<Orden_Compra>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Orden_Compra>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Tinas>, BaseServicio<Tinas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Tinas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Transportes>, BaseServicio<Transportes>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Transportes>>(instancia => { instancia.SetearContexto(new dbVentas()); });

            container.Register<IBaseServicio<Permisos>, BaseServicio<Permisos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Permisos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Provincias>, BaseServicio<Provincias>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Provincias>>(instancia => { instancia.SetearContexto(new dbVentas()); });

            container.Register<IBaseServicio<Produccion>, BaseServicio<Produccion>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Produccion>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Produccion_Quesos>, BaseServicio<Produccion_Quesos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Produccion_Quesos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Produccion_Quesos_Fundido>, BaseServicio<Produccion_Quesos_Fundido>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Produccion_Quesos_Fundido>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Produccion_Leche_Polvo>, BaseServicio<Produccion_Leche_Polvo>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Produccion_Leche_Polvo>>(instancia => { instancia.SetearContexto(new dbProd()); });


            container.Register<IBaseServicio<Produccion_Ricota>, BaseServicio<Produccion_Ricota>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Produccion_Ricota>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Produccion_Muzzarela>, BaseServicio<Produccion_Muzzarela>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Produccion_Muzzarela>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Produccion_Dulce_de_Leche>, BaseServicio<Produccion_Dulce_de_Leche>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Produccion_Dulce_de_Leche>>(instancia => { instancia.SetearContexto(new dbProd()); });


            container.Register<IBaseServicio<Produccion_Leche>, BaseServicio<Produccion_Leche>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Produccion_Leche>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Produccion_Crema>, BaseServicio<Produccion_Crema>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Produccion_Crema>>(instancia => { instancia.SetearContexto(new dbProd()); });


            container.Register<IBaseServicio<AccesoDatos.Contexto.Produccion.Productos>, BaseServicio<AccesoDatos.Contexto.Produccion.Productos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<AccesoDatos.Contexto.Produccion.Productos>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<AccesoDatos.Contexto.Ventas.Productos>, BaseServicio<AccesoDatos.Contexto.Ventas.Productos>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<AccesoDatos.Contexto.Ventas.Productos>>(instancia => { instancia.SetearContexto(new dbVentas()); });

            container.Register<IBaseServicio<Provee_Cta_Cte>, BaseServicio<Provee_Cta_Cte>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Provee_Cta_Cte>>(instancia => { instancia.SetearContexto(new dbGastos()); });

            container.Register<IBaseServicio<Queseros>, BaseServicio<Queseros>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Queseros>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Tipo_Proveedor>, BaseServicio<Tipo_Proveedor>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Tipo_Proveedor>>(instancia => { instancia.SetearContexto(new dbGastos()); });

            container.Register<IBaseServicio<Traspaso_Fabricas>, BaseServicio<Traspaso_Fabricas>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Traspaso_Fabricas>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Usuarios>, BaseServicio<Usuarios>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Usuarios>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Analisis_Microbiologico>, BaseServicio<Analisis_Microbiologico>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Analisis_Microbiologico>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Rango_Analisis_Microbiologico>, BaseServicio<Rango_Analisis_Microbiologico>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Rango_Analisis_Microbiologico>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Detalle_Resultado_Analisis>, BaseServicio<Detalle_Resultado_Analisis>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Detalle_Resultado_Analisis>>(instancia => { instancia.SetearContexto(new dbProd()); });

            container.Register<IBaseServicio<Resultado_Analisis_Microbiologico>, BaseServicio<Resultado_Analisis_Microbiologico>>(Lifestyle.Scoped);
            container.RegisterInitializer<BaseServicio<Resultado_Analisis_Microbiologico>>(instancia => { instancia.SetearContexto(new dbProd()); });

        }

    }
}
