﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Produccion.Listados
{
    public class ListadosModels
    {
        public class Detalle_Remito
        {
            [Display(Name = "Numero comprobante")]
            public int Numero_Comprobante { get; set; }

            [Display(Name = "Prefijo")]
            public int Prefijo { get; set; }


        }
        public class Remitos_Por_Producto
        {
            [Display(Name = "Producto")]
            public string Numero_Producto { get; set; }

            [Display(Name = "Lote")]
            public string Lote { get; set; }

            [Required]
            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Desde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Hasta { get; set; }
        }
        public class StockGeneralModel
        {
            [Display(Name = "Producto")]
            [Remote("ExisteProducto", "Listados")]
            public string Codigo_Producto { get; set; }

            [Display(Name = "Estado")]
            public int Codigo_Estado { get; set; }

            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }
        }
        public class StockProductosModel
        {
            /*[Display(Name = "Producto")]
            [Remote("ExisteProducto", "Listados")]
            public string Codigo_Producto { get; set; }*/
            [Display(Name = "Solo totales")]
            public bool Solo_Totales { get; set; }

            [Display(Name = "Excluir Vacios")]
            public bool ExcluirVacios { get; set; }

            [Display(Name = "Fabrica")]
            public int Codigo_Fabrica { get; set; }

            [Required]
            [Display(Name = "Fecha desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Desde { get; set; }

            [Required]
            [Display(Name = "Fecha hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Hasta { get; set; }
        }
        public class StockProductosDetalladoModel
        {
            [Display(Name = "Producto")]
            [Remote("ExisteProducto", "Listados")]
            public string Codigo_Producto { get; set; }

            [Display(Name = "Fábrica")]
            public int Codigo_Fabrica { get; set; }

            [Display(Name = "Vida Util(dias)")]
            public int Cantidad_Dias_Frescura { get; set; }

            [Required]
            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Desde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Hasta { get; set; }

            [Display(Name = "Tipo de Lotes")]
            public int tipo_lote { get; set; }
            public string estado { get; set; }
            [Display(Name = "Corte Por:")]
            public string agrupacion { get; set; }

            [Display(Name = "Línea")]
            public int Linea { get; set; }
            [Display(Name = "Rubro")]
            public int Rubro { get; set; }

            //Para filtrar en producciones detalladas
            [Display(Name = "Hora Acidificación")]
            public TimeSpan? Horas_Acificacion { get; set; }

            [Display(Name = "PH Final")]
            public string PH_Final { get; set; }

            [Display(Name = "Acidez Final")]
            public string Acidez_Final { get; set; }

            [Display(Name = "Grasa")]
            public decimal? Materia_Grasa { get; set; }

            [Display(Name = "Proteina")]
            public decimal? Proteina { get; set; }

            [Display(Name = "Humedad")]
            public decimal? Humedad { get; set; }
        }

        public class FacturaRemitoModel
        {
            public string Tipo_Factura { get; set; }
        }
        public class ListadoMermaModel
        {

            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            [Required]
            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }

            [Display(Name = "Producto")]
            public string Codigo_Producto { get; set; }
        }
        public class ResumenProdModel
        {
            [Display(Name = "Fábrica")]
            public int Codigo_Fabrica { get; set; }

            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            [Required]
            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }

            [Display(Name = "Producto")]
            public string Codigo_Producto { get; set; }
        }

        public class CambiosEstadoModel
        {
            [Display(Name = "Fábrica")]
            public int Codigo_Fabrica { get; set; }

            [Required]
            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Desde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Hasta { get; set; }

            [Display(Name = "Lote")]
            public string Lote { get; set; }
        }

        public class ProdDetalladoModel
        {
            [Display(Name = "Producto")]
            [Remote("ExisteProducto", "Listados")]
            public string Codigo_Producto { get; set; }

            [Display(Name = "Fabrica")]
            public int Codigo_Fabrica { get; set; }

            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }
        }

        public class StockInsumosModel
        {
            private List<string> _ordenamientoPor = new List<string> { "Código de artículo", "Nombre", "Moneda" };

            public StockInsumosModel()
            {

            }
            [Display(Name = "Insumo")]
            [Remote("ExisteInsumo", "Listados")]
            public string Codigo_Insumo { get; set; }

            [Display(Name = "Fabrica")]
            public int Codigo_Fabrica { get; set; }

            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }

            public string Codigo_Proveedor { get; set; }

            public List<string> OpcionesOrdenamiento => this._ordenamientoPor;

        }
        public class CalidadModel
        {
            [Display(Name = "Producto")]
            public string Numero_Producto { get; set; }

            [Display(Name = "Fabrica")]
            public int Codigo_Fabrica { get; set; }

            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Desde { get; set; }

            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Hasta { get; set; }

            [Display(Name = "Seleccionar Reporte")]
            public int Reporte { get; set; }
        }

        public class MermaProduccionesModel
        {
            [Display(Name = "Producto")]
            [Remote("ExisteInsumo", "Listados")]
            public string Codigo_Producto { get; set; }

            [Display(Name = "Fabrica")]
            public int Codigo_Fabrica { get; set; }

            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            [Required]
            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }
        }

        public class StockInsumos
        {
            [Display(Name = "Insumo")]
            [Remote("ExisteInsumo", "Listados")]
            public string Codigo_Insumo { get; set; }

            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }
        }

        public class StockLecheModel
        {
            [Display(Name = "Silo")]
            public int Codigo_Silo { get; set; }

            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }
        }

        public class DetalleFormulaModel
        {
            [Display(Name = "Producto")]
            [Remote("ExisteProducto", "Listados")]
            [Required]
            public string Codigo_Producto { get; set; }

            public Nullable<bool> Incluir_Costos_Indirectos { get; set; }

        }

        public class DetalleFormulaEnvasadoModel
        {
            [Display(Name = "Producto")]
            [Required]
            public string Numero_Producto_Ventas { get; set; }

            public Nullable<bool> Incluir_Costos_Indirectos { get; set; }

        }

        public class DuplicarFormulaModel
        {
            [Display(Name = "Producto Origen")]
            [Required]
            public string Producto_Origen { get; set; }

            [Display(Name = "Producto Destino")]
            [Required]
            public string Producto_Destino { get; set; }

        }

        public class ComprasPorProveedorModel
        {
            [Display(Name = "Insumo")]
            [Remote("ExisteInsumo", "Listados")]
            public string Codigo_Insumo { get; set; }

            [Display(Name = "Proveedor")]
            public int Codigo_Proveedor { get; set; }

            [Display(Name = "Fábrica")]
            public int Codigo_Fabrica { get; set; }
            [Display(Name = "Tipo Movimiento")]
            public string Tipo_Movimiento { get; set; }

            [Display(Name = "Ver ")]
            public string detalles_imputaciones { get; set; }

            [Required]
            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Desde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Hasta { get; set; }

            [Display(Name = "Línea")]
            public int Linea { get; set; }
        }

        public class CostosProductosModel
        {
            [Display(Name = "Producto")]
            [Remote("ExisteProducto", "Listados")]
            public string Codigo_Producto { get; set; }
            public bool Con_Costo_Envasado { get; set; }
        }

        public class ProduccionesPorProductoModel
        {
            [Display(Name = "Producto")]
            [Remote("ExisteProducto", "Listados")]
            public string Codigo_Producto { get; set; }

            [Display(Name = "Producto Ventas")]
            public string Numero_Producto_Ventas { get; set; }

            [Required]
            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }

        }

        public class ListadoRemitoTraspasoFabricasModel
        {
            [Required]
            [Display(Name = "FabricaOrigen")]
            public int FabricaOrigen { get; set; }

            [Required]
            [Display(Name = "FabricaDestino")]
            public int FabricaDestino { get; set; }

            [Required]
            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaDesde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FechaHasta { get; set; }


            [Display(Name = "Estado")]
            public int Estado { get; set; }


            public static SelectList TiposEstado()
            {
                return
                    new SelectList(
                        new List<Object> {
                            new { value = -1, text = "Todos"},
                            new { value = 0, text = "Pendiente Confirmación" },
                            new { value = 1, text = "Confirmado"},
                            new { value = 2, text = "Rechazado"}
                        }, "value", "text");
            }
        }

        public class HormasConsumidasModel
        {
            [Display(Name = "Producto")]
            //[Remote("ExisteProducto", "Listados")]
            public string Numero_Producto { get; set; }

            [Display(Name = "Fábrica")]
            public int Fabrica { get; set; }

            [Required]
            [Display(Name = "Fecha Desde")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Desde { get; set; }

            [Required]
            [Display(Name = "Fecha Hasta")]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Fecha_Hasta { get; set; }
        }
    }
}