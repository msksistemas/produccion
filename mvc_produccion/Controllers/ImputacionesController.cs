﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Gastos;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class ImputacionesController : Controller
    {
        private IBaseServicio<Imputaciones> ImputacionesServicio;

        public ImputacionesController(IBaseServicio<Imputaciones> imputacionesServicio)
        {
            this.ImputacionesServicio = imputacionesServicio;
        }
        // GET: Imputaciones
        public ActionResult Index()
        {
            return View(this.ImputacionesServicio.Obtener());
        }

        public ActionResult GetData()
        {
            var listado = this.ImputacionesServicio.Obtener((i)=> i.Produccion.Value && i.Activo == false).Select(i => new
            {
                Codigo = i.CODIGO,
                Descripcion = i.DESCRIPCION
            });

            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOne(string Codigo_Impu)
        {
            try
            {
                var imputacion = this.ImputacionesServicio.Obtener((i) => i.CODIGO == Codigo_Impu).Select(i => new
                {
                    i.CODIGO,
                    i.DESCRIPCION
                }).SingleOrDefault();

                if (imputacion == null)
                    return Json(new
                    {
                        success = false,
                        mensaje = "Error, no existe registro con el Código ingresado." + Codigo_Impu,
                        error = "Imputacion no encontrada"
                    }, JsonRequestBehavior.AllowGet);

                return Json(new { data = imputacion, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    mensaje = "Error, no existe registro con el Código ingresado." + Codigo_Impu
                                                ,
                    error = e.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
