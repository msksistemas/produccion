﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion.SP;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using System.Net.Http;

namespace MVC_Produccion.Controllers
{
    //TODO hacer ver
    public class ProduccionesLecheController : Controller
    {
        private IBaseServicio<Produccion_Leche> ProduccionLecheServicio;
        private IBaseServicio<Produccion> ProduccionServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Detalle_Produccion> DetalleProduccionServicio;
        private IProduccionServicio ProduccionServicioExtra;
        private dbProdSP db = new dbProdSP();

        public ProduccionesLecheController(IBaseServicio<Produccion_Leche> produccionLecheServicio,
                                           IBaseServicio<Produccion> produccionServicio,
                                           IBaseServicio<Formulas> formulasServicio,
                                           IBaseServicio<Detalle_Produccion> detalleProduccionServicio,
                                           IProduccionServicio produccionServicioExtra)
        {
            this.ProduccionLecheServicio = produccionLecheServicio;
            this.ProduccionServicio = produccionServicio;
            this.FormulasServicio = formulasServicio;
            this.DetalleProduccionServicio = detalleProduccionServicio;
            this.ProduccionServicioExtra = produccionServicioExtra;
    }

        // GET: ProduccionesLeche
        public ActionResult Index()
        {
            return View(this.ProduccionLecheServicio.Obtener(p => p.Produccion.Historico == false).ToList());
        }

        public ActionResult GetData(string Lote = "")
        {
            var listado = this.ProduccionLecheServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)
                                                                                && p.Produccion.Historico == false
                                                                                && p.Produccion.Codigo_Fabrica == (Sesion.UsuarioActual.Fabrica == -1 ? p.Produccion.Codigo_Fabrica : Sesion.UsuarioActual.Fabrica))
                                .Select(p => new
                                {
                                    Fecha = p.Produccion.Fecha,
                                    p.Lote,
                                    p.Litros_Reales,
                                    p.Litros_Teoricos,
                                    p.Fecha_Vencimiento,
                                    Kilos_Restantes = p.Litros_Reales ?? (p.Litros_Reales - this.ProduccionServicioExtra.ObtenerCantidadesEnvasadas(p.Lote)["kilos"])
                                });

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote = "")
        {
            if (Lote == "")
            {
                Produccion_Leche prod = new Produccion_Leche();
                prod.Produccion.Fecha = DateTime.Now;

                return View(prod);
            }
            else
            {
                return View(this.ProduccionLecheServicio.Obtener(x => x.Lote == Lote).FirstOrDefault());

            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(Produccion_Leche produccion, string Codigo_Producto)
        {
            produccion.Produccion.idUsuario = Sesion.UsuarioActual.idUsuario;
            if (produccion.Codigo_Interno_Produccion == 0)
            {
                if (this.ProduccionServicio.Obtener(p => p.Lote == produccion.Lote).FirstOrDefault() == null)
                {
                    Produccion_Leche produccionNuevo = new Produccion_Leche();
                    produccionNuevo.Lote = produccion.Lote;
                    produccionNuevo.Litros_Teoricos = produccion.Litros_Teoricos;
                    produccionNuevo.Litros_Reales = produccion.Litros_Reales;
                    produccionNuevo.Fecha_Vencimiento = produccion.Fecha_Vencimiento;

                    produccionNuevo.Produccion.Codigo_Fabrica = produccion.Produccion.Codigo_Fabrica;
                    produccionNuevo.Produccion.Lote = produccion.Produccion.Lote;
                    produccionNuevo.Produccion.Nro_Planilla = produccion.Produccion.Nro_Planilla;
                    produccionNuevo.Produccion.Codigo_Quesero = produccion.Produccion.Codigo_Quesero;
                    produccionNuevo.Produccion.Historico = true;

                    Formulas nuevaFormula = this.FormulasServicio.Obtener(f => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();

                    produccionNuevo.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;

                    /*GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS*/

                    foreach (var detalle in produccion.Produccion.Detalle_Produccion.ToList())
                    {
                        if (detalle != null)
                        {
                            detalle.Movimiento = "Producción";
                            detalle.Fecha = produccion.Produccion.Fecha;
                            detalle.Lote = produccion.Lote;

                            produccionNuevo.Produccion.Detalle_Produccion.Add(detalle);
                        }

                    }

                    this.ProduccionLecheServicio.Agregar(produccionNuevo);
                    db.Movimiento_Stock_Produccion(produccionNuevo.Produccion.Codigo_Interno_Produccion, false);

                    return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "Ya existe un registro con ese Lote." }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                Produccion_Leche produccionNuevo = this.ProduccionLecheServicio.Obtener(p => p.Lote == produccion.Lote).FirstOrDefault();

                if (produccionNuevo != null)
                {
                    produccionNuevo.Lote = produccion.Lote;
                    produccionNuevo.Litros_Teoricos = produccion.Litros_Teoricos;
                    produccionNuevo.Litros_Reales = produccion.Litros_Reales;
                    produccionNuevo.Fecha_Vencimiento = produccion.Fecha_Vencimiento;

                    produccionNuevo.Produccion.Codigo_Fabrica = produccion.Produccion.Codigo_Fabrica;
                    produccionNuevo.Produccion.Lote = produccion.Produccion.Lote;
                    produccionNuevo.Produccion.Nro_Planilla = produccion.Produccion.Nro_Planilla;
                    produccionNuevo.Produccion.Codigo_Quesero = produccion.Produccion.Codigo_Quesero;

                    /*TRAE EL PRODUCTO ASOCIADO A LA FÒRMULA PARA SABER SI ES UN INSUMO, PROD TERMINADO O AMBOS*/
                    Formulas nuevaFormula = this.FormulasServicio.Obtener(f => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                    produccionNuevo.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;

                    /*DETALLES DE LA PRODUCCIÓN A EDITAR*/
                    foreach (var detalle in this.DetalleProduccionServicio.Obtener((dp) => dp.Lote == produccionNuevo.Lote))
                    {
                        this.DetalleProduccionServicio.Borrar(detalle);
                    }
                    foreach (var detalle in produccionNuevo.Produccion.Detalle_Produccion)
                    {
                        detalle.Movimiento = "Producción";
                        if (detalle.Fecha == null)
                        {
                            detalle.Fecha = produccionNuevo.Produccion.Fecha;
                        }
                        detalle.Lote = produccionNuevo.Lote;
                        this.DetalleProduccionServicio.Agregar(detalle);
                    }


                    this.ProduccionLecheServicio.Editar(produccionNuevo);
                    this.ProduccionServicio.Editar(produccionNuevo.Produccion);
                    db.Movimiento_Stock_Produccion(produccionNuevo.Produccion.Codigo_Interno_Produccion, false);
                    return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "Error al actualizar registro." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
