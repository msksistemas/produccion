﻿using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using MVC_Produccion.Models;
using MVC_Produccion.Models.Utilidades;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Mvc;

namespace MVC_Produccion.Controllers
{
    public class ProduccionesDulceDeLecheController : Controller
    {
        private IBaseServicio<Produccion_Dulce_de_Leche> ProduccionDulceLecheServicio;
        private IBaseServicio<Detalle_Produccion> DetalleProduccionServicio;
        private IBaseServicio<Produccion> ProduccionServicio;
        private IBaseServicio<Envasado> EnvasadoServicio;
        private IBaseServicio<Detalle_Envasado> DetalleEnvasadoServicio;
        private IBaseServicio<Detalle_Insumo_Envasado> DetalleInsumoEnvasadoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Productos> ProductosServicio;
        private IBaseServicio<Familias> FamiliasServicio;
        private IBaseServicio<Mov_Ins_Asoc> MovInsAsocServicio;
        private IBaseServicio<Queseros> QueserosServicio;
        private IBaseServicio<Ajustes_Insumos> AjusteInsumosServicio;
        private IBaseServicio<Restantes_Envasado> RestanteEnvasadoServicio;
        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();
        private readonly HttpService httpClient;

        public ProduccionesDulceDeLecheController(IBaseServicio<Produccion_Dulce_de_Leche> produccionDulceLecheServicio,
                                            IBaseServicio<Ajustes_Insumos> ajusteInsumosServicio,
                                          IBaseServicio<Formulas> formulasServicio,
                                          IBaseServicio<Restantes_Envasado> restanteEnvasadoServicio,
                                          IBaseServicio<Queseros> queserosServicio,
                                          IBaseServicio<Mov_Ins_Asoc> movInsAsocServicio,
                                          IBaseServicio<Familias> familiasServicio,
                                          IBaseServicio<Detalle_Produccion> detalleProduccionServicio,
                                          IBaseServicio<Produccion> produccionServicio,
                                          IBaseServicio<Envasado> envasadoServicio,
                                          IBaseServicio<Detalle_Envasado> detalleEnvasadoServicio,
                                          IBaseServicio<Detalle_Insumo_Envasado> detalleInsumoEnvasadoServicio,
                                          IBaseServicio<Permisos> permisosServicio,
                                          IBaseServicio<Productos> productosServicio,
                                          IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                          HttpService httpClient)
        {
            this.RestanteEnvasadoServicio = restanteEnvasadoServicio;
            this.AjusteInsumosServicio = ajusteInsumosServicio;
            this.ProduccionDulceLecheServicio = produccionDulceLecheServicio;
            this.QueserosServicio = queserosServicio;
            this.MovInsAsocServicio = movInsAsocServicio;
            this.DetalleEnvasadoServicio = detalleEnvasadoServicio;
            this.DetalleInsumoEnvasadoServicio = detalleInsumoEnvasadoServicio;
            this.DetalleProduccionServicio = detalleProduccionServicio;
            this.EnvasadoServicio = envasadoServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.PermisosServicio = permisosServicio;
            this.ProduccionServicio = produccionServicio;
            this.FormulasServicio = formulasServicio;
            this.FamiliasServicio = familiasServicio;
            this.ProductosServicio = productosServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
            this.httpClient = httpClient;
        }

        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            else if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";
            ViewBag.configuraciones = this.Configuraciones;

            if (Sesion.UsuarioActual.Fabrica == 0 || Sesion.UsuarioActual.Fabrica == null && Sesion.UsuarioActual.Rol.ToLower() != "administrador")
                ViewBag.Error = "No tiene Fábrica asignada, por favor, solicite Permisos al Administrador.";

            var producciones = this.ProduccionServicio.Obtener();
            List<SelectListItem> ListadoHistoricos = new List<SelectListItem>()
                    {
                        new SelectListItem() {Text="No Históricos", Value="0"},
                        new SelectListItem() {Text="Históricos", Value="1"},
                        new SelectListItem() {Text="Todos", Value="-1"}
                    };
            ViewBag.ListadoHistoricos = ListadoHistoricos;
            return View(producciones);

        }

        private decimal CantidadEnvasada(string lote)
        {
            decimal resultado = 0;
            foreach (var item in this.DetalleEnvasadoServicio.Obtener((d)=> d.Envasado.Lote == lote))
            {
                resultado += item.Kilos.HasValue ? item.Kilos.Value : 0;
            }
            foreach (var item in this.RestanteEnvasadoServicio.Obtener((r)=> r.Lote == lote))
            {
                resultado += item.Kilos;
            }
            return resultado;
        }

        public ActionResult GetData(string Lote = "")
        {
            if ((Sesion.UsuarioActual.Fabrica != 0 && Sesion.UsuarioActual.Fabrica != null) || Sesion.UsuarioActual.Rol.ToLower() == "administrador" || !this.Configuraciones.Utiliza_Login)
            {
                var listado = this.ProduccionDulceLecheServicio.Obtener((p) =>
                              p.Lote == (Lote == "" ? p.Lote : Lote)
                              && p.Produccion.Historico == false
                              && p.Produccion.Codigo_Fabrica == (Sesion.UsuarioActual.Fabrica == -1 ? p.Produccion.Codigo_Fabrica : Sesion.UsuarioActual.Fabrica)
                            ).Select(p => new
                            {

                                p.Produccion.Fecha,
                                p.Codigo_Interno_Produccion,
                                p.Lote,
                                p.Acidez,
                                p.Ph,
                                p.Temperatura,
                                p.Densidad,
                                p.Litros,
                                p.Kilos_Reales,
                                p.Kilos_Teoricos,
                                p.Produccion.Formulas.Codigo_Producto,
                                p.Produccion.Formulas.Productos.Descripcion,
                                faltan_envasar = p.Kilos_Reales - CantidadEnvasada(p.Lote)

                            });
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { error = 1, Usuario = Sesion.UsuarioActual.Usuario, success = false }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetHistoricos(string Lote = "", int Historico = 0)
        {
            /* TODOS */
            if (Historico == -1)
            {
                var listado = this.ProduccionDulceLecheServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)).Select(p => new
                {
                    p.Produccion.Fecha,
                    p.Codigo_Interno_Produccion,
                    p.Lote,
                    p.Acidez,
                    p.Ph,
                    p.Temperatura,
                    p.Densidad,
                    p.Litros,
                    p.Kilos_Reales,
                    p.Kilos_Teoricos,
                    p.Produccion.Formulas.Codigo_Producto,
                    p.Produccion.Formulas.Productos.Descripcion,
                    faltan_envasar = p.Kilos_Reales - CantidadEnvasada(p.Lote)
                });
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var listado = this.ProduccionDulceLecheServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)
                                                                            && p.Produccion.Historico == (Historico == 1 ? true : false))
                                                                .Select(p => new
                                                                {
                                                                    p.Produccion.Fecha,
                                                                    p.Codigo_Interno_Produccion,
                                                                    p.Lote,
                                                                    p.Acidez,
                                                                    p.Ph,
                                                                    p.Temperatura,
                                                                    p.Densidad,
                                                                    p.Litros,
                                                                    p.Kilos_Reales,
                                                                    p.Kilos_Teoricos,
                                                                    p.Produccion.Formulas.Codigo_Producto,
                                                                    p.Produccion.Formulas.Productos.Descripcion,
                                                                    faltan_envasar = p.Kilos_Reales - CantidadEnvasada(p.Lote)
                                                                });
           
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote = "")
        {
            ViewBag.queseros = this.QueserosServicio.Obtener((q) => q.Activo == true).Select(q => new { q.Nombre, q.Codigo_Quesero });
            ViewBag.Codigo_Familia = this.FamiliasServicio.Obtener((f) => f.Nombre.ToLower() == "dulce de leche").FirstOrDefault().Codigo_Familia;
            ViewBag.Envasa_Sin_Kilos = this.Configuraciones.Envasa_Sin_Kilos;

            if (Lote == "")
            {
                Produccion_Dulce_de_Leche prod = new Produccion_Dulce_de_Leche();
                Produccion produccion = new Produccion();
                produccion.Fecha = DateTime.Now;
                if (!this.Configuraciones.NroPlanillaDigitable)
                    produccion.Nro_Planilla = (this.ProduccionDulceLecheServicio.Obtener().Count() + 1).ToString();
                else
                    produccion.Nro_Planilla = null;

                prod.Produccion = produccion;
                ViewBag.esDesglose = false;

                return View(prod);
            }
            else
            {
                var lote = this.ProduccionDulceLecheServicio.Obtener((x) => x.Produccion.Lote == Lote).FirstOrDefault();
                if (lote.Produccion.Lote_Padre != null && lote.Produccion.Lote_Padre != "undefined")
                {
                    ViewBag.esDesglose = true;
                }
                else
                {
                    ViewBag.esDesglose = false;
                }
                return View(lote);
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddOrEdit(Produccion_Dulce_de_Leche prod, List<Desglose_Produccion> desglose, string Codigo_Producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    prod.Lote = prod.Produccion.Lote;
                    prod.Produccion.idUsuario = Sesion.UsuarioActual.idUsuario;
                    if (prod.Codigo_Interno_Produccion == 0)
                    {
                        //if (httpClient.EsSanSatur())
                        //{
                        //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                        //        prod.Produccion.Detalle_Produccion.ToList(),
                        //        HttpService.ProcesoProduccion.Creacion,
                        //        prod.Produccion.Fecha ?? DateTime.Now,
                        //        httpClient.ObtenerPrimerNumProdVentas(Codigo_Producto)
                        //    );
                        //    if (!response.IsSuccessStatusCode)
                        //    {
                        //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                        //    }
                        //}
                        if (this.ProduccionServicio.Obtener((p) => p.Lote == prod.Lote).SingleOrDefault() != null)
                            return Json(new { success = false, message = "Ya existe un registro con ese Lote." }, JsonRequestBehavior.AllowGet);

                        prod.Produccion.Historico = false;
                        if (desglose != null)
                        {
                            prod.Produccion.Historico = true;
                            Produccion_Dulce_de_Leche pqDesglose;
                            Produccion prodDesglose;
                            Formulas formulaDesglose;
                            foreach (Desglose_Produccion d in desglose)
                            {
                                pqDesglose = new Produccion_Dulce_de_Leche();
                                pqDesglose.Acidez = prod.Acidez;
                                pqDesglose.Densidad= prod.Densidad;
                                pqDesglose.Kilos_Reales = d.Kilos;
                                pqDesglose.Lote = d.Lote;
                                pqDesglose.Ph = prod.Ph;
                                pqDesglose.Silo = prod.Silo;
                                pqDesglose.Temperatura= prod.Temperatura;
                                if (prod.Kilos_Teoricos.HasValue && prod.Kilos_Teoricos.Value != 0)
                                {
                                    var coeficienteDeRepresentacion = d.Kilos / prod.Kilos_Teoricos;
                                    pqDesglose.Litros= Convert.ToInt32(prod.Litros * coeficienteDeRepresentacion);
                                    pqDesglose.Kilos_Teoricos = Convert.ToInt32(prod.Kilos_Teoricos * coeficienteDeRepresentacion);

                                }

                                formulaDesglose = this.FormulasServicio.Obtener((x) => x.Codigo_Producto == d.Numero_Producto).FirstOrDefault();

                                prodDesglose = new Produccion();
                                prodDesglose.Lote = d.Lote;
                                prodDesglose.Lote_Padre = prod.Produccion.Lote;
                                prodDesglose.Historico = false;
                                prodDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                prodDesglose.Fecha = prod.Produccion.Fecha;
                                prodDesglose.Nro_Planilla = prod.Produccion.Nro_Planilla;
                                prodDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                prodDesglose.Codigo_Formula = formulaDesglose.Codigo_Formula;
                                prodDesglose.idUsuario = Sesion.UsuarioActual.idUsuario;
                                pqDesglose.Produccion = prodDesglose;

                                this.ProduccionDulceLecheServicio.Agregar(pqDesglose);
                            }
                        }

                        var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                        prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;
                        /*TRAE EL PRODUCTO ASOCIADO A LA FÒRMULA PARA SABER SI ES UN INSUMO, PROD TERMINADO O AMBOS*/
                        var prodNuevo = this.ProductosServicio.Obtener((p) => p.Codigo_Producto == nuevaFormula.Codigo_Producto).FirstOrDefault();
                        /*GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS*/
                        foreach (var detalle in prod.Produccion.Detalle_Produccion.ToList())
                        {
                            detalle.Movimiento = "Producción";
                            detalle.Fecha = prod.Produccion.Fecha;
                            detalle.Lote = prod.Lote;
                        }
                        /*GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS*/
                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                        {
                            movimiento.Lote = prod.Lote;
                        }
                        this.ProduccionDulceLecheServicio.Agregar(prod);
                        db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);

                        if (desglose != null)
                        {
                            foreach (Desglose_Produccion d in desglose)
                            {
                                int Codigo_Interno_Desglose = this.ProduccionServicio.Obtener((p) => p.Lote == d.Lote).SingleOrDefault().Codigo_Interno_Produccion;
                                db.Movimiento_Stock_Produccion(Codigo_Interno_Desglose, false);
                            }
                        }
                        scope.Complete();
                        return Json(new { success = true, envasar = desglose == null ? nuevaFormula.Productos.Salida_Directa : false, resultado = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        prod.Produccion.Historico = false;
                        if (desglose != null)
                        {
                            var lote = this.ProduccionServicio.Obtener((x) => x.Lote_Padre == prod.Lote).FirstOrDefault();
                            if (lote != null)
                            {
                                return Json(new { success = false, message = "Ya existen desgloses de esta producción." }, JsonRequestBehavior.AllowGet);
                            }
                            else if (prod.Produccion.Historico == true)
                            {
                                return Json(new { success = false, message = "No se puede desglosar una producción histórica." }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                prod.Produccion.Historico = true;
                                Produccion_Dulce_de_Leche pqDesglose;
                                Produccion prodDesglose;
                                Formulas formulaDesglose;
                                foreach (Desglose_Produccion d in desglose)
                                {
                                    pqDesglose = new Produccion_Dulce_de_Leche();
                                    pqDesglose.Acidez = prod.Acidez;
                                    pqDesglose.Densidad = prod.Densidad;
                                    pqDesglose.Kilos_Reales = d.Kilos;
                                    pqDesglose.Lote = d.Lote;
                                    pqDesglose.Ph = prod.Ph;
                                    pqDesglose.Silo = prod.Silo;
                                    pqDesglose.Temperatura = prod.Temperatura;
                                    if (prod.Kilos_Teoricos.HasValue && prod.Kilos_Teoricos.Value != 0)
                                    {
                                        var coeficienteDeRepresentacion = d.Kilos / prod.Kilos_Teoricos;
                                        pqDesglose.Litros = Convert.ToInt32(prod.Litros * coeficienteDeRepresentacion);
                                        pqDesglose.Kilos_Teoricos = prod.Kilos_Teoricos * coeficienteDeRepresentacion;

                                    }

                                    formulaDesglose = this.FormulasServicio.Obtener((x) => x.Codigo_Producto == d.Numero_Producto).FirstOrDefault();

                                    prodDesglose = new Produccion();
                                    prodDesglose.Lote = d.Lote;
                                    prodDesglose.Lote_Padre = prod.Produccion.Lote;
                                    prodDesglose.Historico = false;
                                    prodDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                    prodDesglose.Fecha = prod.Produccion.Fecha;
                                    prodDesglose.Nro_Planilla = prod.Produccion.Nro_Planilla;
                                    prodDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                    prodDesglose.Codigo_Formula = formulaDesglose.Codigo_Formula;
                                    prodDesglose.idUsuario = Sesion.UsuarioActual.idUsuario;
                                    pqDesglose.Produccion = prodDesglose;

                                    this.ProduccionDulceLecheServicio.Agregar(pqDesglose);
                                }
                            }
                        }

                        /*TRAE EL PRODUCTO ASOCIADO A LA FÒRMULA PARA SABER SI ES UN INSUMO, PROD TERMINADO O AMBOS*/
                        var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                        prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;
                        var prodNuevo = this.ProductosServicio.Obtener((p) => p.Codigo_Producto == Codigo_Producto).SingleOrDefault();

                        List<Detalle_Produccion> detalleAnterior = this.DetalleProduccionServicio.Obtener((dp) => dp.Lote == prod.Lote).ToList();

                        //if (httpClient.EsSanSatur())
                        //{
                        //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                        //        prod.Produccion.Detalle_Produccion.ToList(),
                        //        HttpService.ProcesoProduccion.Edicion,
                        //        Codigo_Producto,
                        //        detalleAnterior);
                        //    if (!response.IsSuccessStatusCode)
                        //    {
                        //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                        //    }
                        //}
                        /*BORRA LOS DETALLES DE LA PRODUCCIÓN A EDITAR*/
                        foreach (var detalle in detalleAnterior)
                        {
                            this.DetalleProduccionServicio.Borrar(detalle);
                        }
                        /*GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS*/
                        foreach (var detalle in prod.Produccion.Detalle_Produccion.ToList())
                        {
                            detalle.Movimiento = "Producción";
                            detalle.Fecha = prod.Produccion.Fecha;
                            detalle.Lote = prod.Lote;
                            this.DetalleProduccionServicio.Agregar(detalle);
                        }
                        /*BORRA TODOS LOS DETALLES DE PRODUCTOS ASOCIADOS*/
                        foreach (var movimiento in this.MovInsAsocServicio.Obtener((mia) => mia.Lote == prod.Produccion.Lote))
                        {
                            this.MovInsAsocServicio.Borrar(movimiento);
                        }
                        /*GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS*/
                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                        {
                            movimiento.Lote = prod.Produccion.Lote;
                            this.MovInsAsocServicio.Agregar(movimiento);
                        }
                        this.ProduccionDulceLecheServicio.Editar(prod);
                        this.ProduccionServicio.Editar(prod.Produccion);
                        db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);
                        if (desglose != null)
                        {
                            foreach (Desglose_Produccion d in desglose)
                            {
                                int Codigo_Interno_Desglose = this.ProduccionServicio.Obtener((p) => p.Lote == d.Lote).SingleOrDefault().Codigo_Interno_Produccion;
                                db.Movimiento_Stock_Produccion(Codigo_Interno_Desglose, false);
                            }
                        }
                        scope.Complete();
                        return Json(new { success = true, envasar = false, resultado = 2 }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        private void AgregarRestanteEnvasado(Envasado envasado, int codigoFabrica, List<Restantes_Envasado> restantes)
        {
            decimal kg_descarte = 0, hormas_descarte = 0;
            foreach (var restante in restantes)
            {
                restante.Lote = envasado.Lote;
                restante.Codigo_Interno_Envasado = envasado.Codigo_Interno_Envasado;
                kg_descarte += restante.Kilos;
                hormas_descarte += restante.Hormas;
                if (restante.Motivo == "Reproceso")
                {
                    Ajustes_Insumos ai = new Ajustes_Insumos();
                    ai.Comentario = "Descarte envasado lote " + envasado.Lote;
                    ai.Es_Traspaso = false;
                    ai.Fabrica = codigoFabrica;
                    ai.Fabrica_Origen = 0;
                    ai.Fecha = envasado.Fecha;
                    ai.Historico = true;
                    ai.Numero_Comprobante = envasado.Codigo_Interno_Envasado.ToString();
                    ai.Tipo_Movimiento = "Ingreso";
                    Detalle_Ajuste_Insumos dai = new Detalle_Ajuste_Insumos();
                    dai.Cantidad = restante.Hormas;
                    dai.Kilos_Litros = restante.Kilos;
                    dai.Lote = envasado.Lote;
                    dai.Codigo_Insumo = restante.Codigo_Insumo;

                    ai.Detalle_Ajuste_Insumos.Add(dai);

                    this.AjusteInsumosServicio.Agregar(ai);
                }
                this.RestanteEnvasadoServicio.Agregar(restante);
            }

            envasado.Kg_Descarte = kg_descarte;
            envasado.Hormas_Descarte = hormas_descarte;
            this.EnvasadoServicio.Editar(envasado);
        }

        //[HttpPost]
        //public ActionResult Envasar(Envasado envasado, List<Restantes_Envasado> restante, decimal? Kilos_Reales_Envasado = 0)
        //{
        //    try
        //    {
        //        Produccion_Dulce_de_Leche produccion = new Produccion_Dulce_de_Leche();
        //        produccion = this.ProduccionDulceLecheServicio.Obtener((p) => p.Lote == envasado.Lote).FirstOrDefault();
        //        decimal kgEnvasados = 0;
        //        decimal hormasEnvasadas = 0;
        //        //Suma todos los envasados anteriores
        //        foreach (var detalle in envasado.Detalle_Envasado.ToList())
        //        {
        //            kgEnvasados += detalle.Kilos.HasValue ? detalle.Kilos.Value : 0;
        //            hormasEnvasadas += detalle.Hormas.HasValue ? detalle.Hormas.Value : 0;
        //            detalle.Codigo_Interno_Envasado = envasado.Codigo_Interno_Envasado;
        //        }
        //        foreach (var detalle in this.DetalleEnvasadoServicio.Obtener((d)=> d.Envasado.Lote == envasado.Lote))
        //        {
        //            kgEnvasados += detalle.Kilos.HasValue ? detalle.Kilos.Value : 0;
        //            hormasEnvasadas += detalle.Hormas.HasValue ? detalle.Hormas.Value : 0;
        //        }
        //        if (restante != null)
        //            foreach (var r in restante)
        //            {
        //                kgEnvasados += r.Kilos;
        //                hormasEnvasadas += r.Hormas;
        //            }
        //        foreach (var r in this.RestanteEnvasadoServicio.Obtener((res)=> res.Lote == envasado.Lote))
        //        {
        //            kgEnvasados += r.Kilos;
        //            hormasEnvasadas += r.Hormas;
        //        }

        //        if (produccion.Kilos_Reales >= kgEnvasados)
        //        {
        //            if (produccion.Kilos_Reales == kgEnvasados)
        //                produccion.Produccion.Historico = true;
        //        }
        //        else
        //            return Json(new { success = false, message = "Se estan envasando mas kilos de los que tiene la produccion." }, JsonRequestBehavior.AllowGet);

            

        //        this.EnvasadoServicio.Agregar(envasado);

        //        var fabrica = produccion.Produccion.Codigo_Fabrica.HasValue ? produccion.Produccion.Codigo_Fabrica.Value : 0;
        //        if (restante != null)
        //            this.AgregarRestanteEnvasado(envasado, fabrica, restante);

        //        this.ProduccionDulceLecheServicio.Editar(produccion);

        //        db.Movimiento_Stock_Envasado(envasado.Lote);
        //        db.Liberado_Por_Calidad(envasado.Codigo_Interno_Envasado);
        //        return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
