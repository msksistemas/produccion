﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using MVC_Produccion.Sesiones;
using MVC_Produccion.Models.Utilidades;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion.SP;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Models;
using System.Net.Http;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class ProduccionesQuesosController : Controller
    {
        private IProduccionServicio ProduccionServicioExtra;
        private IBaseServicio<Produccion_Quesos> ProduccionQuesosServicio;
        private IBaseServicio<Acidificacion> AcidificacionServicio;
        private IBaseServicio<Detalle_Produccion> DetalleProduccionServicio;
        private IBaseServicio<Produccion> ProduccionServicio;
        private IBaseServicio<Envasado> EnvasadoServicio;
        private IBaseServicio<Detalle_Envasado> DetalleEnvasadoServicio;
        private IBaseServicio<Detalle_Insumo_Envasado> DetalleInsumoEnvasadoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Productos> ProductosServicio;
        private IBaseServicio<Ajustes_Insumos> AjusteInsumos;
        private IBaseServicio<Costos_Indirectos_Produccion> CostosIndirectosProduccion;
        private IBaseServicio<Mov_Ins_Asoc> MovInsAsocServicio;
        private IBaseServicio<Queseros> QueserosServicio;
        private IBaseServicio<Formulas_Envasado> FormulasEnvasadoServicio;
        private IBaseServicio<Estados> EstadosServicio;
        private IBaseServicio<Insumos> InsumosServicio;
        private IBaseServicio<Camaras> CamarasServicio;
        private IBaseServicio<Familias> FamiliasServicio;
        private IBaseServicio<Motivos_Retencion> MotivosRetencionServicio;
        private IBaseServicio<Restantes_Envasado> RestanteEnvasadoServicio;
        private IBaseServicio<Cambio_Estado> CambiosEstadoServicio;
        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();
        private readonly HttpService httpClient;

        public ProduccionesQuesosController(
                                 IProduccionServicio produccionServicioExtra,
                                 IBaseServicio<Acidificacion> acidificacionServicio,
                                 IBaseServicio<Estados> estadosServicio,
                                 IBaseServicio<Restantes_Envasado> restanteEnvasadoServicio,
                                 IBaseServicio<Camaras> camarasServicio,
                                 IBaseServicio<Motivos_Retencion> motivosRetencionServicio,
                                 IBaseServicio<Ajustes_Insumos> ajusteInsumos,
                                 IBaseServicio<Insumos> insumosServicio,
                                 IBaseServicio<Produccion_Quesos> produccionQuesosServicio,
                                 IBaseServicio<Ajustes_Insumos> ajusteInsumosServicio,
                                 IBaseServicio<Formulas> formulasServicio,
                                 IBaseServicio<Familias> familiasServicio,
                                 IBaseServicio<Queseros> queserosServicio,
                                 IBaseServicio<Mov_Ins_Asoc> movInsAsocServicio,
                                 IBaseServicio<Costos_Indirectos_Produccion> costosIndirectosProduccion,
                                 IBaseServicio<Detalle_Produccion> detalleProduccionServicio,
                                 IBaseServicio<Produccion> produccionServicio,
                                 IBaseServicio<Envasado> envasadoServicio,
                                 IBaseServicio<Detalle_Envasado> detalleEnvasadoServicio,
                                 IBaseServicio<Detalle_Insumo_Envasado> detalleInsumoEnvasadoServicio,
                                 IBaseServicio<Permisos> permisosServicio,
                                 IBaseServicio<Productos> productosServicio,
                                 IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                 IBaseServicio<Formulas_Envasado> formulasEnvasadoServicio,
                                 IBaseServicio<Cambio_Estado> cambiosEstadoServicio,
                                 HttpService httpClient)
        {
            this.ProduccionServicioExtra = produccionServicioExtra;
            this.RestanteEnvasadoServicio = restanteEnvasadoServicio;
            this.AcidificacionServicio = acidificacionServicio;
            this.AjusteInsumos = ajusteInsumos;
            this.InsumosServicio = insumosServicio;
            this.FormulasEnvasadoServicio = formulasEnvasadoServicio;
            this.ProduccionQuesosServicio = produccionQuesosServicio;
            this.EstadosServicio = estadosServicio;
            this.CamarasServicio = camarasServicio;
            this.QueserosServicio = queserosServicio;
            this.MovInsAsocServicio = movInsAsocServicio;
            this.DetalleEnvasadoServicio = detalleEnvasadoServicio;
            this.DetalleInsumoEnvasadoServicio = detalleInsumoEnvasadoServicio;
            this.DetalleProduccionServicio = detalleProduccionServicio;
            this.EnvasadoServicio = envasadoServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.PermisosServicio = permisosServicio;
            this.ProduccionServicio = produccionServicio;
            this.FormulasServicio = formulasServicio;
            this.ProductosServicio = productosServicio;
            this.AcidificacionServicio = acidificacionServicio;
            this.MotivosRetencionServicio = motivosRetencionServicio;
            this.CostosIndirectosProduccion = costosIndirectosProduccion;
            this.FamiliasServicio = familiasServicio;
            this.CambiosEstadoServicio = cambiosEstadoServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
            this.httpClient = httpClient;
        }

        public ActionResult Index(int resultado = 0)
        {
            if (Sesion.UsuarioActual.Fabrica == 0 || Sesion.UsuarioActual.Fabrica == null && Sesion.UsuarioActual.Rol.ToLower() != "administrador")
                ViewBag.Error = "No tiene Fábrica asignada, por favor, solicite Permisos al Administrador.";

            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";

            else if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";
            ViewBag.configuraciones = this.Configuraciones;

            List<SelectListItem> ListadoHistoricos = new List<SelectListItem>()
                        {
                            new SelectListItem() {Text="No Históricos", Value="0"},
                            new SelectListItem() {Text="Históricos", Value="1"},
                            new SelectListItem() {Text="Todos", Value="-1"}
                        };

            ViewBag.ListadoHistoricos = ListadoHistoricos;
            ViewBag.estados = this.EstadosServicio.Obtener().Select(e => new { e.Codigo_Estado, e.Descripcion });


            return View();
        }

        public ActionResult GetData(string Lote = "")
        {
            //TODO: CAMBIAR POR SP ObtenerProduccionesQuesos

            if ((Sesion.UsuarioActual.Fabrica != 0 && Sesion.UsuarioActual.Fabrica != null) || Sesion.UsuarioActual.Rol.ToLower() == "administrador")
            {
                var listado = db.Obtener_Producciones_Quesos(Lote, 0, 0, Sesion.UsuarioActual.Fabrica == null ? -1 : Sesion.UsuarioActual.Fabrica);
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { error = 1, Sesion.UsuarioActual.Usuario, success = false }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetHistoricos(string Lote = "", int Historico = -1, int Codigo = 0)
        {
            var listado = db.Obtener_Producciones_Quesos(Lote, Historico, Codigo, Sesion.UsuarioActual.Fabrica == null ? -1 : Sesion.UsuarioActual.Fabrica);
            var jsonResult = Json(new { data = listado }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote = "")
        {
            ViewBag.queseros = this.QueserosServicio.Obtener((q) => q.Activo == true).Select(q => new { q.Nombre, q.Codigo_Quesero });
            ViewBag.estados = this.EstadosServicio.Obtener().Select(e => new { e.Codigo_Estado, e.Descripcion });
            ViewBag.motivosRetencion = this.MotivosRetencionServicio.Obtener((m) => m.Tipo == "Produccion" && m.Activo).Select(m => new { m.Codigo_Motivo_Retencion, m.Descripcion });
            ViewBag.camaras = this.CamarasServicio.Obtener().Select(c => new { c.Codigo_Camara, c.Nombre });
            ViewBag.Codigo_Familia = this.FamiliasServicio.Obtener((f) => f.Nombre.ToLower() == "quesos").FirstOrDefault().Codigo_Familia;
            ViewBag.Envasa_Sin_Kilos = this.Configuraciones.Envasa_Sin_Kilos;
            if (Lote == "")
            {
                Produccion_Quesos prod = new Produccion_Quesos();
                Produccion produccion = new Produccion();
                prod.Codigo_Estado = 2;
                produccion.Fecha = DateTime.Now;
                //produccion.Fecha = prod.Produccion.Fecha;
                produccion.Historico = false;
                prod.Historico = false;
                prod.Produccion = produccion;
                if (this.Configuraciones.Utiliza_Login == true)
                    prod.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica;
                if (!this.Configuraciones.NroPlanillaDigitable)
                {
                    produccion.Nro_Planilla = (this.ProduccionQuesosServicio.Obtener().Count() + 1).ToString();
                    ViewBag.NroAuto = true;
                }
                else
                    ViewBag.NroAuto = false;

                ViewBag.esDesglose = false;
                return View(prod);
            }
            else
            {
                var lote = this.ProduccionQuesosServicio.Obtener((x) => x.Lote == Lote).FirstOrDefault();
                if (lote.Produccion.Lote_Padre != null && lote.Produccion.Lote_Padre != "undefined")
                {
                    ViewBag.esDesglose = true;
                }
                else
                {
                    ViewBag.esDesglose = false;
                }

                //Produccion produccion = new Produccion();

                //if (lote.Codigo_Fabrica == null)
                //{
                //    lote.Codigo_Fabrica = 2;
                //}
                return View(lote);
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddOrEdit(Produccion_Quesos prod, List<Desglose_Produccion> desglose, string Codigo_Producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    prod.Lote = prod.Produccion.Lote;
                    prod.Produccion.idUsuario = Sesion.UsuarioActual.idUsuario;
                    if (prod.Codigo_Interno_Produccion == 0)
                    {
                        //if (httpClient.EsSanSatur())
                        //{
                        //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                        //        prod.Produccion.Detalle_Produccion.ToList(),
                        //        HttpService.ProcesoProduccion.Creacion,
                        //        prod.Produccion.Fecha ?? DateTime.Now,
                        //        httpClient.ObtenerPrimerNumProdVentas(Codigo_Producto)
                        //    );
                        //    if (!response.IsSuccessStatusCode)
                        //    {
                        //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                        //    }
                        //}
                        if ((prod.Kilos_Teoricos.HasValue ? prod.Kilos_Teoricos.Value : 0) == 0)
                            return Json(new { success = false, message = "Los kilos no pueden ser 0." }, JsonRequestBehavior.AllowGet);

                        if (this.ProduccionServicio.Obtener((p) => p.Lote == prod.Produccion.Lote).Count != 0)
                            return Json(new { success = false, message = $"Lote {prod.Produccion.Lote} ya registrado" }, JsonRequestBehavior.AllowGet);

                        if (desglose != null)
                        {
                            foreach (Desglose_Produccion d in desglose)
                            {
                                Produccion produccion = this.ProduccionServicio.Obtener((p) => p.Lote == d.Lote).FirstOrDefault();

                                if (produccion != null)
                                    return Json(new { success = false, message = $"Lote {produccion.Lote} ya registrado" }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        prod.Kilos_Reales = prod.Kilos_Reales == null ? 0 : prod.Kilos_Reales;
                        //prod.Kilos_Envasado = this.Configuraciones.Envasa_Sin_Kilos ? 0 : prod.Kilos_Reales;
                        prod.Kilos_Envasado = 0;
                        prod.Produccion.Historico = false;
                        prod.Historico = false;
                        prod.Rendimiento = prod.Rendimiento.HasValue ? prod.Rendimiento : 0;
                        if (desglose != null)
                        {
                            var cantidadDesglosada = desglose.Sum(d => d.Cantidad);

                            if (prod.Piezas_Obtenidas.HasValue && cantidadDesglosada != prod.Piezas_Obtenidas.Value)
                            {
                                return Json(new { success = false, message = "La cantidad desglosada no es la misma que las piezas obtenidas." }, JsonRequestBehavior.AllowGet);
                            }

                            prod.Produccion.Historico = true;
                            prod.Historico = true;
                            Produccion_Quesos pqDesglose;
                            Produccion prodDesglose;
                            Formulas formulaDesglose;
                            foreach (Desglose_Produccion d in desglose)
                            {
                                pqDesglose = new Produccion_Quesos();
                                pqDesglose.Codigo_Estado = prod.Codigo_Estado;
                                pqDesglose.Codigo_Quesero = prod.Codigo_Quesero;
                                pqDesglose.Codigo_Tina = prod.Codigo_Tina;
                                pqDesglose.Codigo_Silo_1 = prod.Codigo_Silo_1;
                                pqDesglose.Grasa_Silo_1 = prod.Grasa_Silo_1;
                                pqDesglose.Proteina_Silo_1 = prod.Proteina_Silo_1; 
                                pqDesglose.Codigo_Silo_2 = prod.Codigo_Silo_2;
                                pqDesglose.Grasa_Silo_2 = prod.Grasa_Silo_2;
                                pqDesglose.Proteina_Silo_2 = prod.Proteina_Silo_2;
                                pqDesglose.Grasa = prod.Grasa;  
                                pqDesglose.Proteina = prod.Proteina;
                                pqDesglose.Factor = prod.Factor;
                                pqDesglose.Factor_Grasa_Proteina = prod.Factor_Grasa_Proteina;
                                pqDesglose.Grasa_Suero = prod.Grasa_Suero;
                                pqDesglose.Acidez = prod.Acidez;
                                pqDesglose.Test_Fosfatasa_Negativa = prod.Test_Fosfatasa_Negativa;
                                pqDesglose.Concentrado = prod.Concentrado;
                                pqDesglose.Temperatura_Coagulacion = prod.Temperatura_Coagulacion;
                                pqDesglose.TiempoCoagulacion = prod.TiempoCoagulacion;
                                pqDesglose.Temperatura_Coccion = prod.Temperatura_Coccion;
                                pqDesglose.TiempoCoccion = prod.TiempoCoccion;
                                pqDesglose.TiempoLirado = prod.TiempoLirado;
                                pqDesglose.HoraAgregados = prod.HoraAgregados;
                                pqDesglose.Horas_Acificacion = prod.Horas_Acificacion;
                                pqDesglose.PH_Final = prod.PH_Final;
                                pqDesglose.Acidez_Final = prod.Acidez_Final;
                                pqDesglose.Humedad = prod.Humedad;
                                pqDesglose.Piezas_Obtenidas = d.Cantidad;
                                pqDesglose.Kilos_Reales = d.Kilos;
                                pqDesglose.Codigo_Camara = prod.Codigo_Camara;
                                pqDesglose.Lote = d.Lote;
                                pqDesglose.Historico = false;
                                var coeficienteDeRepresentacion = prod.Piezas_Obtenidas.HasValue && prod.Piezas_Obtenidas != 0 ? d.Cantidad / prod.Piezas_Obtenidas : 0;
                                if ((!prod.Kilos_Reales.HasValue || prod.Kilos_Reales <= 0) && (prod.Kilos_Teoricos.HasValue && prod.Kilos_Teoricos.Value != 0))
                                {
                                    prod.Kilos_Reales = prod.Kilos_Teoricos;
                                }
                                pqDesglose.Kilos_Teoricos = prod.Kilos_Teoricos * coeficienteDeRepresentacion;
                                pqDesglose.Litros_Tina = Convert.ToInt32(prod.Litros_Tina * coeficienteDeRepresentacion);
                                pqDesglose.Litros_Silo_1 = Convert.ToInt32(prod.Litros_Silo_1 * coeficienteDeRepresentacion);
                                pqDesglose.Litros_Silo_2 = Convert.ToInt32(prod.Litros_Silo_2 * coeficienteDeRepresentacion);
                                //pqDesglose.Rendimiento = pqDesglose.Kilos_Reales.HasValue && pqDesglose.Litros_Tina.HasValue ? ((pqDesglose.Kilos_Reales / pqDesglose.Litros_Tina) * 100) : 0;
                                formulaDesglose = this.FormulasServicio.Obtener((x) => x.Codigo_Producto == d.Numero_Producto).FirstOrDefault();
                                prodDesglose = new Produccion();
                                prodDesglose.Lote = d.Lote;
                                prodDesglose.Lote_Padre = prod.Produccion.Lote;
                                prodDesglose.Historico = false;
                                prodDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                prodDesglose.Fecha = prod.Produccion.Fecha;
                                prodDesglose.Nro_Planilla = prod.Produccion.Nro_Planilla;
                                prodDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                prodDesglose.Codigo_Formula = formulaDesglose.Codigo_Formula;
                                prodDesglose.idUsuario = Sesion.UsuarioActual.idUsuario;
                                pqDesglose.Produccion = prodDesglose;

                                this.ProduccionQuesosServicio.Agregar(pqDesglose);
                            }
                        }

                        var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                        prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;
                        //GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS
                        foreach (var detalle in prod.Produccion.Detalle_Produccion.ToList())
                        {
                            detalle.Movimiento = "Producción";
                            detalle.Fecha = prod.Produccion.Fecha;
                            detalle.Lote = prod.Produccion.Lote;
                        }
                        foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion.ToList())
                        {
                            detalle.Lote = prod.Produccion.Lote;
                        }
                        foreach (var acidificacion in prod.Produccion.Acidificacion.ToList())
                        {
                            acidificacion.Lote = prod.Lote;
                        }
                        //GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS
                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                        {
                            movimiento.Lote = prod.Produccion.Lote;
                        }

                        this.ProduccionQuesosServicio.Agregar(prod);
                        db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);

                        if (desglose != null)
                        {
                            foreach (Desglose_Produccion d in desglose)
                            {
                                int Codigo_Interno_Desglose = this.ProduccionServicio.Obtener((p) => p.Lote == d.Lote).SingleOrDefault().Codigo_Interno_Produccion;
                                db.Movimiento_Stock_Produccion(Codigo_Interno_Desglose, false);

                                AgregarCambioEstado(prod, d.Lote);
                            }
                        }
                        else
                        {
                            AgregarCambioEstado(prod);
                        }
                        scope.Complete();
                        return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);

                    }
                    //Es una edición
                    else
                    {
                        try
                        {
                            prod.Lote = prod.Produccion.Lote;
                            if (desglose != null)
                            {
                                var lote = this.ProduccionServicio.Obtener((x) => x.Lote_Padre == prod.Lote).FirstOrDefault();
                                if (lote != null)
                                {
                                    return Json(new { success = false, message = "Ya existen desgloses de esta producción." }, JsonRequestBehavior.AllowGet);
                                }
                                else if (prod.Historico == true)
                                {
                                    return Json(new { success = false, message = "No se puede desglosar una producción histórica." }, JsonRequestBehavior.AllowGet);
                                }
                                else { 
                                    prod.Produccion.Historico = true;
                                    prod.Historico = true;
                                    Produccion_Quesos pqDesglose;
                                    Produccion prodDesglose;
                                    Formulas formulaDesglose;
                                    foreach (Desglose_Produccion d in desglose)
                                    {
                                        pqDesglose = new Produccion_Quesos();
                                        pqDesglose.Codigo_Estado = prod.Codigo_Estado;
                                        pqDesglose.Codigo_Quesero = prod.Codigo_Quesero;
                                        pqDesglose.Codigo_Tina = prod.Codigo_Tina;
                                        pqDesglose.Codigo_Silo_1 = prod.Codigo_Silo_1;
                                        pqDesglose.Grasa_Silo_1 = prod.Grasa_Silo_1;
                                        pqDesglose.Proteina_Silo_1 = prod.Proteina_Silo_1;
                                        pqDesglose.Codigo_Silo_2 = prod.Codigo_Silo_2;
                                        pqDesglose.Grasa_Silo_2 = prod.Grasa_Silo_2;
                                        pqDesglose.Proteina_Silo_2 = prod.Proteina_Silo_2;
                                        pqDesglose.Grasa = prod.Grasa;
                                        pqDesglose.Proteina = prod.Proteina;
                                        pqDesglose.Factor = prod.Factor;
                                        pqDesglose.Factor_Grasa_Proteina = prod.Factor_Grasa_Proteina;
                                        pqDesglose.Grasa_Suero = prod.Grasa_Suero;
                                        pqDesglose.Acidez = prod.Acidez;
                                        pqDesglose.Test_Fosfatasa_Negativa = prod.Test_Fosfatasa_Negativa;
                                        pqDesglose.Concentrado = prod.Concentrado;
                                        pqDesglose.Temperatura_Coagulacion = prod.Temperatura_Coagulacion;
                                        pqDesglose.TiempoCoagulacion = prod.TiempoCoagulacion;
                                        pqDesglose.Temperatura_Coccion = prod.Temperatura_Coccion;
                                        pqDesglose.TiempoCoccion = prod.TiempoCoccion;
                                        pqDesglose.TiempoLirado = prod.TiempoLirado;
                                        pqDesglose.HoraAgregados = prod.HoraAgregados;
                                        pqDesglose.Horas_Acificacion = prod.Horas_Acificacion;
                                        pqDesglose.PH_Final = prod.PH_Final;
                                        pqDesglose.Acidez_Final = prod.Acidez_Final;
                                        pqDesglose.Humedad = prod.Humedad;
                                        pqDesglose.Piezas_Obtenidas = d.Cantidad;
                                        pqDesglose.Kilos_Reales = d.Kilos;
                                        pqDesglose.Codigo_Camara = prod.Codigo_Camara;
                                        pqDesglose.Lote = d.Lote;
                                        pqDesglose.Historico = false;
                                        var coeficienteDeRepresentacion = prod.Piezas_Obtenidas.HasValue && prod.Piezas_Obtenidas != 0 ? d.Cantidad / prod.Piezas_Obtenidas : 0;
                                        if ((!prod.Kilos_Reales.HasValue || prod.Kilos_Reales <= 0) && (prod.Kilos_Teoricos.HasValue && prod.Kilos_Teoricos.Value != 0))
                                        {
                                            prod.Kilos_Reales = prod.Kilos_Teoricos;
                                        }
                                        pqDesglose.Kilos_Teoricos = prod.Kilos_Teoricos * coeficienteDeRepresentacion;
                                        pqDesglose.Litros_Tina = Convert.ToInt32(prod.Litros_Tina * coeficienteDeRepresentacion);
                                        pqDesglose.Litros_Silo_1 = Convert.ToInt32(prod.Litros_Silo_1 * coeficienteDeRepresentacion);
                                        pqDesglose.Litros_Silo_2 = Convert.ToInt32(prod.Litros_Silo_2 * coeficienteDeRepresentacion);
                                        //pqDesglose.Rendimiento = pqDesglose.Kilos_Reales.HasValue && pqDesglose.Litros_Tina.HasValue ? ((pqDesglose.Kilos_Reales / pqDesglose.Litros_Tina) * 100) : 0;
                                        formulaDesglose = this.FormulasServicio.Obtener((x) => x.Codigo_Producto == d.Numero_Producto).FirstOrDefault();
                                        prodDesglose = new Produccion();
                                        prodDesglose.Lote = d.Lote;
                                        prodDesglose.Lote_Padre = prod.Produccion.Lote;
                                        prodDesglose.Historico = false;
                                        prodDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                        prodDesglose.Fecha = prod.Produccion.Fecha;
                                        prodDesglose.Nro_Planilla = prod.Produccion.Nro_Planilla;
                                        prodDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                        prodDesglose.Codigo_Formula = formulaDesglose.Codigo_Formula;
                                        prodDesglose.idUsuario = Sesion.UsuarioActual.idUsuario;
                                        pqDesglose.Produccion = prodDesglose;
                                        this.ProduccionQuesosServicio.Agregar(pqDesglose);
                                    }
                                }
                            }

                            //TRAE EL PRODUCTO ASOCIADO A LA FÓRMULA PARA SABER SI ES UN INSUMO, PROD TERMINADO O AMBOS
                            var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                            prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;

                            //if (httpClient.EsSanSatur())
                            //{
                            //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                            //        prod.Produccion.Detalle_Produccion.ToList(),
                            //        HttpService.ProcesoProduccion.Edicion,
                            //        Codigo_Producto,
                            //        detalleAnterior);
                            //    if (!response.IsSuccessStatusCode)
                            //    {
                            //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                            //    }
                            //}

                            List<Detalle_Produccion> detalleAnterior = this.DetalleProduccionServicio.Obtener((dp) => dp.Lote == prod.Lote).ToList();
                            //BORRA LOS DETALLES DE LA PRODUCCIÓN A EDITAR
                            foreach (Detalle_Produccion detalle in detalleAnterior)
                            {
                                this.DetalleProduccionServicio.Borrar(detalle);
                            }

                            //GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS
                            foreach (var detalle in prod.Produccion.Detalle_Produccion.ToList())
                            {
                                detalle.Movimiento = "Producción";
                                detalle.Fecha = prod.Produccion.Fecha;
                                detalle.Lote = prod.Lote;

                                this.DetalleProduccionServicio.Agregar(detalle);
                            }

                            foreach (var detalle in this.CostosIndirectosProduccion.Obtener((ci) => ci.Lote == prod.Lote))
                            {
                                this.CostosIndirectosProduccion.Borrar(detalle);
                            }
                            foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion.ToList())
                            {
                                detalle.Lote = prod.Produccion.Lote;
                                this.CostosIndirectosProduccion.Agregar(detalle);
                            }
                            //BORRA TODOS LOS DETALLES DE PRODUCTOS ASOCIADOS
                            foreach (var movimiento in this.MovInsAsocServicio.Obtener((mia) => mia.Lote == prod.Lote))
                            {
                                this.MovInsAsocServicio.Borrar(movimiento);
                            }

                            //GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS
                            foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                            {
                                movimiento.Lote = prod.Lote;
                                this.MovInsAsocServicio.Agregar(movimiento);
                            }

                            foreach (var acidificacion in this.AcidificacionServicio.Obtener((a) => a.Lote == prod.Lote))
                            {
                                this.AcidificacionServicio.Borrar(acidificacion);
                            }
                            foreach (var acidificacion in prod.Produccion.Acidificacion.ToList())
                            {
                                acidificacion.Lote = prod.Lote;
                                this.AcidificacionServicio.Agregar(acidificacion);
                            }

                            this.ProduccionQuesosServicio.Editar(prod);
                            this.ProduccionServicio.Editar(prod.Produccion);
                            db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);
                            if (desglose != null)
                            {
                                foreach (Desglose_Produccion d in desglose)
                                {
                                    int Codigo_Interno_Desglose = this.ProduccionServicio.Obtener((p) => p.Lote == d.Lote).SingleOrDefault().Codigo_Interno_Produccion;
                                    db.Movimiento_Stock_Produccion(Codigo_Interno_Desglose, false);

                                    AgregarCambioEstado(prod, d.Lote);
                                }
                            }
                            else
                            {
                                Cambio_Estado estadoAnterior = this.CambiosEstadoServicio.Obtener((ce) => ce.Lote == prod.Lote).OrderByDescending((ce) => ce.Codigo_Cambio_Interno).FirstOrDefault();
                                if (estadoAnterior != null && (estadoAnterior.Codigo_Estado != prod.Codigo_Estado || estadoAnterior.Codigo_Camara != prod.Codigo_Camara))
                                {
                                    AgregarCambioEstado(prod);
                                }
                            }
                            scope.Complete();
                            return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return Json(new { success = false, message = e.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public void AgregarCambioEstado(Produccion_Quesos prod, string Lote = null)
        {
            Cambio_Estado estado = new Cambio_Estado();
            estado.Fecha = prod.Produccion.Fecha ?? DateTime.Now;
            estado.Codigo_Estado = prod.Codigo_Estado ?? 0;
            estado.Codigo_Camara = prod.Codigo_Camara;
            estado.Lote = Lote == null ? prod.Lote : Lote;
            estado.idUsuario = Sesion.UsuarioActual.idUsuario;
            this.CambiosEstadoServicio.Agregar(estado);
        }

        //TRAE TODOS LOS DETALLES ASOCIADOS A LA PRODUCCIÓN
        public ActionResult GetDetalles(string Lote)
        {
            if (Lote == "")
            {
                List<Detalle_Produccion> list = new List<Detalle_Produccion>();

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            var listado = this.DetalleProduccionServicio.Obtener((d) => d.Lote == Lote).Select(d => new
            {
                Codigo_Formula = d.Produccion.Codigo_Formula,
                Litros_Tina = d.Produccion.Formulas.Litros_Tina,
                Codigo_Insumo = d.Codigo_Insumo,
                Precio_Unitario = d.Precio_Unitario,
                Cantidad = d.Cantidad,
                Fecha = d.Fecha,
                Descripcion = this.InsumosServicio.Obtener((i) => d.Codigo_Insumo == i.Codigo_Insumo).SingleOrDefault() != null ?
                                this.InsumosServicio.Obtener((i) => d.Codigo_Insumo == i.Codigo_Insumo).SingleOrDefault().Descripcion : "",
                Precio_Lista_Insumo = this.InsumosServicio.Obtener((i) => d.Codigo_Insumo == i.Codigo_Insumo).SingleOrDefault() != null ?
                                        this.InsumosServicio.Obtener((i) => d.Codigo_Insumo == i.Codigo_Insumo).SingleOrDefault().Precio_Lista : 0,
                Precio_Por = this.InsumosServicio.Obtener((i) => d.Codigo_Insumo == i.Codigo_Insumo).SingleOrDefault() != null ?
                                this.InsumosServicio.Obtener((i) => d.Codigo_Insumo == i.Codigo_Insumo).SingleOrDefault().Precio_Por : "",
                Ajuste = d.Ajuste
            }).ToList();

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);

        }

        /*OBTIENE LAS PRODUCCIONES SIN KILOS REALES*/
        public ActionResult GetPesadas(string Humedad_Kilos= "kilos")
        {

            if ((Sesion.UsuarioActual.Fabrica != 0 && Sesion.UsuarioActual.Fabrica != null) || Sesion.UsuarioActual.Rol.ToLower() == "administrador")
            {
                var listado = this.db.Get_Pesadas(Sesion.UsuarioActual.Fabrica,Humedad_Kilos).ToList();
               
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { error = 1, Usuario = Sesion.UsuarioActual.Usuario, success = false }, JsonRequestBehavior.AllowGet);


        }

        public ActionResult InformePesadas(int resultado = 0)
        {
            if (resultado == 1)
            {
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            }
            else if (resultado == 2)
            {
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";
            }
            ViewBag.configuraciones = this.Configuraciones;
            List<SelectListItem> ListadoInformes = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="Kilos Reales", Value="kilos"},
                                new SelectListItem() {Text="Humedad", Value="humedad"}
                            };

            ViewBag.ListadoInformes = ListadoInformes;

            return View();
        }

        [HttpGet]
        public ActionResult AddOrEditPesadas(string Lote = "")
        {
            if (Lote == "")
                return View(new Produccion_Quesos());
            else
            {
                var prod = this.ProduccionQuesosServicio.Obtener((p) => p.Lote == Lote).FirstOrDefault();
                ViewBag.estados = this.EstadosServicio.Obtener().Select(e => new { e.Codigo_Estado, e.Descripcion });
                ViewBag.camaras = this.CamarasServicio.Obtener().Select(c => new { c.Codigo_Camara, c.Nombre });
                return View(prod);
            }
        }

        [HttpPost]
        public ActionResult AddOrEditPesadas(string Lote, decimal? Kilos_Reales, decimal? Humedad, decimal Piezas_Obtenidas,
                                            decimal? Grasa, decimal? Proteina, int? Codigo_Estado, int? Codigo_Camara, DateTime? Fecha_Pesada)
        {
            try
            {
                var prod = this.ProduccionQuesosServicio.Obtener((p) => p.Lote == Lote).SingleOrDefault();
                prod.Kilos_Reales = Kilos_Reales;
                prod.Humedad = Humedad;
                prod.Piezas_Obtenidas = Piezas_Obtenidas;
                prod.Grasa = Grasa;
                prod.Proteina = Proteina;
                prod.Codigo_Estado = Codigo_Estado.HasValue ? Codigo_Estado : 0;
                prod.Codigo_Camara = Codigo_Camara.HasValue ? Codigo_Camara : 0;
                prod.Fecha_Pesada = Fecha_Pesada.HasValue ?  Fecha_Pesada.Value.Date: DateTime.Now;
                this.ProduccionQuesosServicio.Editar(prod);

                return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        private void AgregarRestanteEnvasado(Envasado envasado, int codigoFabrica, List<Restantes_Envasado> restantes)
        {
            decimal kg_descarte = 0, hormas_descarte = 0;
            foreach (var restante in restantes)
            {
                restante.Lote = envasado.Lote;
                restante.Codigo_Interno_Envasado = envasado.Codigo_Interno_Envasado;
                kg_descarte += restante.Kilos;
                hormas_descarte += restante.Hormas;
                if (restante.Motivo == "Reproceso")
                {
                    Ajustes_Insumos ai = new Ajustes_Insumos();
                    ai.Comentario = "Descarte envasado lote " + envasado.Lote;
                    ai.Es_Traspaso = false;
                    ai.Fabrica = codigoFabrica;
                    ai.Fabrica_Origen = 0;
                    ai.Fecha = envasado.Fecha;
                    ai.Historico = true;
                    ai.Numero_Comprobante = envasado.Codigo_Interno_Envasado.ToString();
                    ai.Tipo_Movimiento = "Ingreso";
                    Detalle_Ajuste_Insumos dai = new Detalle_Ajuste_Insumos();
                    dai.Cantidad = restante.Hormas;
                    dai.Kilos_Litros = restante.Kilos;
                    dai.Lote = envasado.Lote;
                    dai.Codigo_Insumo = restante.Codigo_Insumo;

                    ai.Detalle_Ajuste_Insumos.Add(dai);

                    this.AjusteInsumos.Agregar(ai);
                }
                this.RestanteEnvasadoServicio.Agregar(restante);
            }

            envasado.Kg_Descarte = kg_descarte;
            envasado.Hormas_Descarte = hormas_descarte;
            this.EnvasadoServicio.Editar(envasado);
        }

        public ActionResult ObtenerRestantes(string Lote = "")
        {
            decimal? kilosEnvasados = 0;
            decimal? piezasEnvasadas = 0;
            decimal? kilosRestantes = 0;
            decimal? piezasRestantes = 0;
            decimal hormas_detalle;
            foreach (var env in this.EnvasadoServicio.Obtener((e) => e.Lote == Lote).ToList())
            {
                kilosEnvasados += env.Kg_Descarte == null ? 0 : env.Kg_Descarte;
                piezasEnvasadas += env.Hormas_Descarte == null ? 0 : env.Hormas_Descarte;
                if (env != null && env.Detalle_Envasado.Count() > 0)
                {
                    foreach (var detalle in env.Detalle_Envasado.ToList())
                    {
                        if (detalle != null)
                        {
                            kilosEnvasados += detalle.Kilos == null ? 0 : detalle.Kilos;
                            hormas_detalle = detalle.Hormas == null ? 0 : detalle.Hormas.Value;
                            if (hormas_detalle != 0)
                                piezasEnvasadas += hormas_detalle;
                            else
                                piezasEnvasadas += (detalle.Cantidad == null ? 0 : detalle.Cantidad) / detalle.Formulas_Envasado.Obtenidas_Por_Envasada;
                        }
                    }
                }
            }
            var prod = this.ProduccionQuesosServicio.Obtener((p) => p.Lote == Lote).FirstOrDefault();
            if (prod != null)
            {
                try
                {
                    kilosRestantes = prod.Kilos_Reales - kilosEnvasados;
                    piezasRestantes = prod.Piezas_Obtenidas - piezasEnvasadas;
                }
                catch { }
            }
            return Json(new { success = true, kilosRestantes = kilosRestantes == null ? 0 : kilosRestantes, piezasRestantes = piezasRestantes == null ? 0 : piezasRestantes }, JsonRequestBehavior.AllowGet);
        }

        private decimal PiezasEnvasadas(string lote)
        {
            decimal piezasEnvasadas = 0;
            decimal hormas_detalle;
            foreach (var env in this.EnvasadoServicio.Obtener((e) => e.Lote == lote).ToList())
            {
                piezasEnvasadas += env.Hormas_Descarte == null ? 0 : env.Hormas_Descarte.Value;
                if (env != null && env.Detalle_Envasado.Count() > 0)
                {
                    foreach (var detalle in env.Detalle_Envasado.ToList())
                    {
                        if (detalle != null)
                        {
                            hormas_detalle = detalle.Hormas == null ? 0 : detalle.Hormas.Value;
                            if (hormas_detalle != 0)
                                piezasEnvasadas += hormas_detalle;
                            else
                                piezasEnvasadas += (detalle.Cantidad == null ? 0 : detalle.Cantidad.Value) / (detalle.Formulas_Envasado.Obtenidas_Por_Envasada == null ? 0 : detalle.Formulas_Envasado.Obtenidas_Por_Envasada.Value);
                        }

                    }
                }
            }
            return piezasEnvasadas;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Controla que el usuario tenga permisos
        /// </summary>
        /// <returns>0:Correcto, 1: No esta Logeado, 2: No tiene permiso</returns>
        private int ControlaPermisos()
        {
            if (this.Configuraciones.Utiliza_Login == true)
            {
                if (Sesion.UsuarioActual == null)
                    return 1;
                var permisosUsuario = this.PermisosServicio.Obtener((p) => p.idUsuario == Sesion.UsuarioActual.idUsuario).SingleOrDefault();
                if (!permisosUsuario.Elaboracion_Queso)
                    return 2;
            }
            return 0;
        }
    }
}