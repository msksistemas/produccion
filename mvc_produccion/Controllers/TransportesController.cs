﻿using Servicios.Interfaces;
using AccesoDatos.Contexto.Ventas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Produccion.Controllers
{
    public class TransportesController : Controller
    {
        private IBaseServicio<Transportes> TransportesServicio;

        public TransportesController(IBaseServicio<Transportes> transportesServicio)
        {
            this.TransportesServicio = transportesServicio;
        }

        public ActionResult Index()
        {
            return View(this.TransportesServicio.Obtener().ToList());
        }

        public ActionResult GetData()
        {
            var listado = this.TransportesServicio.Obtener().Select(t => new
            {
                Codigo_Transporte = t.CODIGO,
                Razon_Social = t.RAZON_SOCIAL
            });

            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOne(int Codigo_Transporte)
        {
            if (Codigo_Transporte > 0)
            {
                try
                {
                    var transporteNuevo = this.TransportesServicio.Obtener((i) => i.CODIGO == Codigo_Transporte).Select(t => new
                    {
                        t.CODIGO,
                        t.RAZON_SOCIAL
                    }).FirstOrDefault();

                    return Json(new { data = transporteNuevo, success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, mensaje = "Error, no existe registro con el Código ingresado." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, mensaje = "Error, no existe registro con Código negativo." }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}