﻿using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Gastos;
using AccesoDatos.Contexto.Ventas;
using AccesoDatos.Contexto.Produccion.SP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    //TODO Revisar Crud
    public class OrdenesDeCompraController : Controller
    {
        private IBaseServicio<Orden_Compra> OrdenCompraServicio;
        private IBaseServicio<Detalle_Orden_Compra> DetalleOrdenCompraServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Provee_Cta_Cte> ProveedoresServicio;
        private IBaseServicio<Transportes> TransportesServicio;
        private IBaseServicio<Fabricas> FabricaServicio;


        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();
        public OrdenesDeCompraController(  IBaseServicio<Orden_Compra> ordenCompraServicio,
                                           IBaseServicio<Detalle_Orden_Compra> detalleOrdenCompraServicio,
                                           IBaseServicio<Permisos> permisosServicio,
                                           IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                           IBaseServicio<Provee_Cta_Cte> proveedoresServicio,
                                           IBaseServicio<Fabricas> fabricaServicio,
                                           IBaseServicio<Transportes> transportesServicio)
        {
            this.OrdenCompraServicio = ordenCompraServicio;
            this.TransportesServicio = transportesServicio;
            this.ProveedoresServicio = proveedoresServicio;
            this.DetalleOrdenCompraServicio = detalleOrdenCompraServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.FabricaServicio = fabricaServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }



        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";

            if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="Activa", Value="Activa"},
                                new SelectListItem() {Text="Inactiva", Value="Inactiva"},
                                new SelectListItem() {Text="Cumplida", Value="Cumplida"},
                                new SelectListItem() {Text="Completa", Value="Completa"},
                                new SelectListItem() {Text="Incompleta", Value="Incompleta"},
                                new SelectListItem() {Text="Todas", Value=""}
                            };

            ViewBag.ListadoEstados = ListadoEstados;

            return View(this.OrdenCompraServicio.Obtener());

        }

        public ActionResult GetData(int Codigo_Interno_Orden = 0, string estado = "")
        {
            var listado = db.GetOrdenesCompra(Codigo_Interno_Orden, estado).ToList();
            
            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDetalles(int Codigo_Interno_Orden = 0)
        {
            if (Codigo_Interno_Orden == 0)
            {
                List<Detalle_Orden_Compra> list = new List<Detalle_Orden_Compra>();

                return Json(new { data = list, success = true, Cantidad = list.Count }, JsonRequestBehavior.AllowGet);
            }

            var listado = db.GetDetallesOrdenCompra(Codigo_Interno_Orden).ToList();
            var orden = this.OrdenCompraServicio.Obtener((oc) => oc.Codigo_Interno_Orden == Codigo_Interno_Orden).SingleOrDefault();
            var estado = "";
            var prov = 0;
            if (orden == null)
                estado = "NoExiste";
            else
            {
                prov = orden.Codigo_Proveedor.HasValue ? orden.Codigo_Proveedor.Value : 0;
                estado = orden.Estado;
            }

            return Json(new
            {
                data = listado,
                success = true,
                Estado = estado,
                proveedor = prov
            }, JsonRequestBehavior.AllowGet);


        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
            {
                Orden_Compra orden = new Orden_Compra();
                orden.Fecha = DateTime.Now;
                if (this.OrdenCompraServicio.Obtener().Count() != 0)
                    orden.Numero_Comprobante = this.OrdenCompraServicio.Obtener().Max(x => x.Numero_Comprobante) + 1;
                ViewBag.descProv = "";
                ViewBag.descTransporte = "";

                return View(orden);
            }
            var OC = this.OrdenCompraServicio.Obtener((x) => x.Codigo_Interno_Orden == id).FirstOrDefault();
            var prov = this.ProveedoresServicio.Obtener((p) => p.CODIGO == OC.Codigo_Proveedor).SingleOrDefault();
            var transporte = this.TransportesServicio.Obtener((t) => t.CODIGO == OC.Codigo_Transporte).SingleOrDefault();
            var fabrica = this.FabricaServicio.Obtener((t) => t.Codigo_Fabrica == OC.Codigo_Fabrica).SingleOrDefault();
            ViewBag.descProv = prov != null ? prov.RAZON_SOCIAL : "";
            ViewBag.descTransporte = transporte != null ? transporte.RAZON_SOCIAL :  "";
            ViewBag.descFabrica = fabrica != null ? fabrica.Descripcion : "";
            return View(OC);

        }

        [HttpPost]
        public ActionResult AddOrEdit(Orden_Compra ordenCompra, List<Detalle_Orden_Compra> detalleOrdenCompra)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                if (ordenCompra.Codigo_Interno_Orden < 0)
                    return Json(new { success = false, message = "Error, por favor ingrese un Código mayor a 0." }, JsonRequestBehavior.AllowGet);

                // NUEVO REGISTRO
                if (ordenCompra.Codigo_Interno_Orden == 0)
                {
                    ordenCompra.Estado = "Activa";
                    this.OrdenCompraServicio.Agregar(ordenCompra);
               
                    // GUARDA TODOS LOS DETALLES ASOCIADOS
                    foreach (var detalle in detalleOrdenCompra)
                    {
                        detalle.Codigo_Interno_Orden = ordenCompra.Codigo_Interno_Orden;
                        this.DetalleOrdenCompraServicio.Agregar(detalle);
                    }
                    scope.Complete();
                    return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
                }

                //REGISTRO A EDITAR
                ordenCompra.Estado = "Activa";

                // BORRA TODOS LOS DETALLES ASOCIADOS
                foreach (var detalle in this.DetalleOrdenCompraServicio.Obtener((doc)=> doc.Codigo_Interno_Orden == ordenCompra.Codigo_Interno_Orden))
                {
                    this.DetalleOrdenCompraServicio.Borrar(detalle);
                }

                // GUARDA TODOS LOS DETALLES ASOCIADOS
                foreach (var detalle in detalleOrdenCompra)
                {
                    detalle.Codigo_Interno_Orden = ordenCompra.Codigo_Interno_Orden;
                    this.DetalleOrdenCompraServicio.Agregar(detalle);
                }

                this.OrdenCompraServicio.Editar(ordenCompra);
                scope.Complete();
                return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);          
            }
        }

        public ActionResult Delete(int Codigo_Interno_Orden = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Orden_Compra est = this.OrdenCompraServicio.Obtener((oc) => oc.Codigo_Interno_Orden == Codigo_Interno_Orden).SingleOrDefault();
                    bool Incompleta = false;
                    foreach (var item in est.Detalle_Orden_Compra)
                    {
                        if (item.Recibidos != 0)
                            Incompleta = true;
                    }

                    if (Incompleta)
                        est.Estado = "Incompleta";
                    else
                        est.Estado = "Inactiva";

                    this.OrdenCompraServicio.Editar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, message = "Error al eliminar, corrobore si el registro está en uso." }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        public ActionResult Inactivate(int Codigo_Interno_Orden = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Orden_Compra est = this.OrdenCompraServicio.Obtener((oc) => oc.Codigo_Interno_Orden == Codigo_Interno_Orden).SingleOrDefault();
                    est.Estado = "Inactiva";
                    this.OrdenCompraServicio.Editar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro inactivado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, message = "Error al inactivar, corrobore si el registro está en uso." }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        
        public ActionResult Fulfill(int Codigo_Interno_Orden = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Orden_Compra est = this.OrdenCompraServicio.Obtener((oc) => oc.Codigo_Interno_Orden == Codigo_Interno_Orden).SingleOrDefault();
                    if(est.Estado == "Cumplida")
                    {
                        est.Estado = "Activa";
                    }
                    else
                    {
                        est.Estado = "Cumplida";
                    }
                    this.OrdenCompraServicio.Editar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, message = "Error al inactivar, corrobore si el registro está en uso." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}