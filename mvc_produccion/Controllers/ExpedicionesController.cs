﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using AccesoDatos.Contexto.Ventas;
using MVC_Produccion.Sesiones;
using MVC_Produccion.Utilidades;
using MVC_Produccion.Backend;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class ExpedicionesController : Controller
    {
        private IBaseServicio<Expedicion> ExpedicionServicio;
        private IBaseServicio<Permisos> PermisoServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Detalle_Expedicion> DetalleExpedicionServicio;
        private IBaseServicio<Formulas_Envasado> FormulasEnvasadoServicio;
        private IBaseServicio<Sucursales_Propias> SucursalesPropiasServicio;
        private IBandejaServicio BandejaServicio;

        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();
        private dbVentas dbVentas = new dbVentas();

        public ExpedicionesController(IBaseServicio<Expedicion> expedicionServicio,
                                      IBaseServicio<Permisos> permisoServicio,
                                      IBaseServicio<Sucursales_Propias> sucursalesPropiasServicios,
                                      IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                      IBaseServicio<Detalle_Expedicion> detalleExpedicionServicio,
                                      IBaseServicio<Formulas_Envasado> formulasEnvasadoServicio,
                                      IBandejaServicio bandejaServicio)
        {
            this.BandejaServicio = bandejaServicio;
            this.ExpedicionServicio = expedicionServicio;
            this.FormulasEnvasadoServicio = formulasEnvasadoServicio;
            this.DetalleExpedicionServicio = detalleExpedicionServicio;
            this.PermisoServicio = permisoServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.SucursalesPropiasServicio = sucursalesPropiasServicios;

            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Ingresos_Pendientes()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Confirmar_Ingreso_Pendiente(int Codigo_Interno_Expedicion)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    db.Confirmar_Ingreso_Pendiente(Codigo_Interno_Expedicion, Sesion.UsuarioActual.idUsuario);
                    db.Movimiento_Stock_Venta_Expedicion(Codigo_Interno_Expedicion);
                    scope.Complete();
                    return Json(new { message = "OK", success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { message = e.ToString(), success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Rechazar_Ingreso_Pendiente(int Codigo_Interno_Expedicion, string Motivo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Expedicion expedicion = this.ExpedicionServicio.Obtener((e) => e.Codigo_Interno_Expedicion == Codigo_Interno_Expedicion).SingleOrDefault();
                    expedicion.Motivo_Rechazo = Motivo;
                    expedicion.Historico = false;
                    expedicion.Recepcion_Confirmada = false;
                    expedicion.Egreso_Ingreso = "Egreso";
                    this.ExpedicionServicio.Editar(expedicion);

                    db.Movimiento_Stock_Venta_Expedicion(expedicion.Codigo_Interno_Expedicion);
                    scope.Complete();
                    return Json(new { message = "OK", success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { message = e.ToString(), success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult GetData(int Codigo_Interno_Expedicion = 0)
        {
            int Codigo_Fabrica = -1;
            if (this.Configuraciones.Utiliza_Login)
                Codigo_Fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : -1;

            if (Codigo_Interno_Expedicion == -1)
            {
                var listado = db.GetExpediciones(Codigo_Interno_Expedicion, Codigo_Fabrica);
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var listado = db.GetExpediciones(Codigo_Interno_Expedicion, Codigo_Fabrica);
                var detalles = db.GetDetallesExpedicion(Codigo_Interno_Expedicion);

                var detallesExp = new List<object>();

                foreach (var detalle in detalles)
                {

                    int? tipo = 1;

                    var detalleModel = this.DetalleExpedicionServicio.Obtener((d) => d.Codigo_Interno_Detalle == detalle.Codigo_Interno_Detalle && d.Codigo_Interno_Expedicion == detalle.Codigo_Interno_Expedicion).FirstOrDefault();

                    if (detalleModel != null)
                    {
                        tipo = detalleModel.Tipo;
                    }

                    else
                    {
                        var formula = this.FormulasEnvasadoServicio.Obtener((f) => f.Codigo_Producto == detalle.Codigo_Producto_Produccion).FirstOrDefault();

                        if (formula != null)
                        {
                            tipo = formula.Es_Insumo_Prod_Ventas.GetValueOrDefault() ? 3 : formula.Es_Insumo.GetValueOrDefault() ? 1 : 2;
                        }
                    }

                    detallesExp.Add(new
                    {
                        detalle.Codigo_Interno_Detalle,
                        detalle.Codigo_Interno_Expedicion,
                        detalle.Numero_Producto_Ventas,
                        detalle.Lote,
                        detalle.Descripcion_Producto,
                        detalle.Kilos,
                        detalle.Cantidad,
                        detalle.Codigo_Producto_Produccion,
                        detalle.Descripcion_Producto_Produccion,
                        Tipo = tipo
                    });

                }

                return Json(new { data = listado, detalles = detallesExp, success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetIngresosPendientes()
        {
            int sucursal = 0;
            if (this.Configuraciones.Utiliza_Login)
                sucursal = Sesion.UsuarioActual.Fabrica.HasValue ? Sesion.UsuarioActual.Fabrica.Value : 0;

            var listado = this.ExpedicionServicio.Obtener((e) => e.Historico == true && e.Recepcion_Confirmada == false && e.Sucursal == (sucursal == -1 ? e.Sucursal : sucursal))
                                                   .Select(e => new
                                                   {
                                                       e.Codigo_Interno_Expedicion,
                                                       e.Prefijo,
                                                       e.Numero_Comprobante,
                                                       e.Fecha,
                                                       e.FechaRecepcion,
                                                       e.Egreso_Ingreso

                                                   });


            return Json(new { data = listado.ToList(), success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetIngresosPendientes(string id, string FechaRecepcion)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    CampoEditable<DateTime> campo = new CampoEditable<DateTime>(id, FechaRecepcion);
                    var exp = this.ExpedicionServicio.Obtener((e) => e.Codigo_Interno_Expedicion == campo.id).FirstOrDefault();
                    if (exp != null)
                    {
                        exp.FechaRecepcion = campo.valor;
                        this.ExpedicionServicio.Editar(exp);
                        scope.Complete();
                        return Json(new { message = "UPDATED" });
                    }
                    else
                    {
                        return Json(new { message = "FAIL" });
                    }
                }
                catch
                {
                    return Json(new { message = "FAIL" });
                }
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int Codigo_Interno_Expedicion = 0)
        {
            ViewBag.UsaBandejas = this.Configuraciones.UtilizaBandejas;

            int origen = 0;
            if (this.Configuraciones.Utiliza_Login)
                origen = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;

            List<SelectListItem> TipoExpedicion = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Egreso", Value="Egreso"},
                new SelectListItem() {Text="Ingreso", Value="Ingreso"}
            };
            ViewBag.TipoExpedicion = TipoExpedicion;
            ViewBag.Sucursal = "";

            Expedicion expedicion = new Expedicion();
            if (Codigo_Interno_Expedicion == 0)
            {
                expedicion.Fecha = DateTime.Now;
                if (origen != 0)
                    expedicion.Origen = origen;
            }
            else
                expedicion = this.ExpedicionServicio.Obtener((x) => x.Codigo_Interno_Expedicion == Codigo_Interno_Expedicion).FirstOrDefault();
            if (expedicion.Origen != null)
            {
                if (expedicion.Origen != 0)
                    try
                    {
                        var sp = this.SucursalesPropiasServicio.Obtener((s) => s.Numero == expedicion.Origen).SingleOrDefault();
                        ViewBag.Origen = sp.Nombre;
                    }
                    catch (Exception e)
                    {
                    }
            }
            if (expedicion.Sucursal != null)
            {
                if (expedicion.Sucursal != 0)
                    try
                    {
                        var sp = this.SucursalesPropiasServicio.Obtener((s) => s.Numero == expedicion.Sucursal).SingleOrDefault();
                        ViewBag.Sucursal = sp.Nombre;
                    }
                    catch (Exception e)
                    {
                    }
            }
            return View(expedicion);
        }

        [HttpGet]
        public ActionResult VerDetalle(int Codigo_Interno_Expedicion = 0)
        {
            ViewBag.UsaBandejas = this.Configuraciones.UtilizaBandejas;
            List<SelectListItem> TipoExpedicion = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Egreso", Value="Egreso"},
                new SelectListItem() {Text="Ingreso", Value="Ingreso"}
            };

            ViewBag.TipoExpedicion = TipoExpedicion;
            ViewBag.Sucursal = "";

            Expedicion expedicion = new Expedicion();

            if (Codigo_Interno_Expedicion == 0)
                expedicion.Fecha = DateTime.Now;

            else
            {
                expedicion = this.ExpedicionServicio.Obtener((x) => x.Codigo_Interno_Expedicion == Codigo_Interno_Expedicion).FirstOrDefault();
                if (expedicion.Sucursal != null)
                {
                    try
                    {
                        var sp = this.SucursalesPropiasServicio.Obtener((s) => s.Numero == expedicion.Sucursal).FirstOrDefault();

                        ViewBag.Sucursal = sp != null ? sp.Nombre : "Desconocida";
                    }
                    catch { }
                }
            }

            return View(expedicion);
        }

        [HttpPost]
        public ActionResult AddOrEdit(Expedicion expedicion, List<DetallesMovimientosBandejas> bandeja)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var stock = new List<ObtenerLotesLiberados_Result>();
                    //CONTROLA STOCK
                    if(expedicion.Codigo_Interno_Expedicion != 0)
                    {
                        List<Detalle_Expedicion> detalleActual = this.DetalleExpedicionServicio.Obtener((de) => de.Codigo_Interno_Expedicion == expedicion.Codigo_Interno_Expedicion).ToList();
                        foreach (Detalle_Expedicion item in expedicion.Detalle_Expedicion)
                        {
                            stock = db.ObtenerLotesLiberados(item.Numero_Producto_Ventas, expedicion.Origen).ToList();
                            var stockLote = stock.SingleOrDefault(s => s.Lote == item.Lote);

                            Detalle_Expedicion detalle = detalleActual.Where((de) => de.Lote == item.Lote && de.Numero_Producto_Ventas == item.Numero_Producto_Ventas).FirstOrDefault();

                            decimal cantidadDisponible = (stockLote == null ? 0 : stockLote.Cantidad) + (detalle == null ? 0 : detalle.Cantidad) ?? 0;
                            decimal kilosDisponibles = (stockLote == null ? 0 : stockLote.Kilos) + (detalle == null ? 0 : detalle.Kilos) ?? 0;

                            if (cantidadDisponible < item.Cantidad || kilosDisponibles < item.Kilos)
                            {
                                var mensaje = $"Se quiere expedir de más en el lote: {item.Lote}";
                                mensaje += $"<br>Cantidad Disponible: {cantidadDisponible}";
                                mensaje += $"<br>Kilos Disponibles: {kilosDisponibles}";
                                return Json(new { success = false, message = mensaje }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in expedicion.Detalle_Expedicion)
                        {
                            stock = db.ObtenerLotesLiberados(item.Numero_Producto_Ventas, expedicion.Origen).ToList();
                            var stockLote = stock.SingleOrDefault(s => s.Lote == item.Lote);
                            if (stockLote == null)
                                return Json(new { success = false, message = item.Lote + " Sin stock" }, JsonRequestBehavior.AllowGet);

                            if (stockLote.Cantidad < item.Cantidad || stockLote.Kilos < item.Kilos)
                            {
                                var mensaje = $"Se quiere expedir de mas en el lote: {item.Lote} ";
                                mensaje += $"\n Cantidad Disponible: {stockLote.Cantidad}";
                                mensaje += $"\n Kilos Disponible: {stockLote.Kilos}";
                                return Json(new { success = false, message = mensaje }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    if (expedicion.Codigo_Interno_Expedicion == 0)
                    {
                            expedicion.Historico = false;
                            expedicion.idUsuario = Sesion.UsuarioActual.idUsuario;
                            expedicion.Recepcion_Confirmada = false;

                            if (this.ExpedicionServicio.Obtener().Count() > 0)
                                expedicion.Codigo_Interno_Expedicion = this.ExpedicionServicio.Obtener().Max(e => e.Codigo_Interno_Expedicion + 1);
                            else
                                expedicion.Codigo_Interno_Expedicion = 1;

                            foreach (var detalle in expedicion.Detalle_Expedicion)
                            {
                                if (detalle != null)
                                {
                                    detalle.Codigo_Interno_Expedicion = expedicion.Codigo_Interno_Expedicion;
                                    //this.DetalleExpedicionServicio.Agregar(detalle);
                                }
                            }
                            this.ExpedicionServicio.Agregar(expedicion);
                            if (bandeja != null && this.Configuraciones.UtilizaBandejas)
                            {
                                var movimientoBandeja = new MovimientosBandejas()
                                {
                                    Origen = expedicion.Origen.Value,
                                    Destino = expedicion.Sucursal,
                                    Fecha = expedicion.Fecha.HasValue ? expedicion.Fecha.Value : DateTime.Today,
                                    Codigo_Interno_Expedicion = expedicion.Codigo_Interno_Expedicion,
                                    Historico = false,
                                    Prefijo = expedicion.Prefijo,
                                    NroComprobante = expedicion.Numero_Comprobante
                                };
                                movimientoBandeja.Detalles = new List<DetallesMovimientosBandejas>();
                                foreach (var item in bandeja)
                                {
                                    movimientoBandeja.Detalles.Add(item);
                                }

                                this.BandejaServicio.Agregar(movimientoBandeja);
                            }
                            scope.Complete();
                            return Json(new { success = true, message = "Registro guardado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        expedicion.idUsuario = Sesion.UsuarioActual.idUsuario;

                        foreach (var detalle in this.DetalleExpedicionServicio.Obtener((de) => de.Codigo_Interno_Expedicion == expedicion.Codigo_Interno_Expedicion))
                        {
                            if (detalle != null)
                                this.DetalleExpedicionServicio.Borrar(detalle);
                        }

                        foreach (var detalle in expedicion.Detalle_Expedicion.ToList())
                        {
                            if (detalle != null)
                            {
                                detalle.Codigo_Interno_Expedicion = expedicion.Codigo_Interno_Expedicion;
                                this.DetalleExpedicionServicio.Agregar(detalle);
                            }
                        }

                        this.ExpedicionServicio.Editar(expedicion);
                        if (this.Configuraciones.UtilizaBandejas)
                        {
                            var destino = expedicion.Sucursal.HasValue ? expedicion.Sucursal.Value : 0;
                            this.BandejaServicio.EditarMovimientoExpedicion(expedicion.Codigo_Interno_Expedicion, destino, bandeja);
                        }
                        scope.Complete();
                        return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Interno_Expedicion = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Expedicion est = this.ExpedicionServicio.Obtener((x) => x.Codigo_Interno_Expedicion == Codigo_Interno_Expedicion).FirstOrDefault();
                    this.ExpedicionServicio.Borrar(est);
                    db.Movimiento_Stock_Venta_Expedicion(Codigo_Interno_Expedicion);
                    if (this.Configuraciones.UtilizaBandejas)
                    {
                        this.BandejaServicio.BorrarPorIdExpedicion(Codigo_Interno_Expedicion);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Pasar_A_Historico(int Codigo_Interno_Expedicion = 0)
        {
            try
            {
                Expedicion exp = this.ExpedicionServicio.Obtener((x) => x.Codigo_Interno_Expedicion == Codigo_Interno_Expedicion).FirstOrDefault();
                var suc_destino = this.SucursalesPropiasServicio.Obtener((sp) => sp.Numero == exp.Sucursal).SingleOrDefault();
                using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    try
                    {
                        exp.Historico = true;
                        exp.Recepcion_Confirmada = suc_destino != null ? suc_destino.Confirmar_Recepcion_Automaticamente : exp.Recepcion_Confirmada;
                        exp.Motivo_Rechazo = "";
                        exp.Egreso_Ingreso = "Ingreso";
                        exp.FechaRecepcion = DateTime.Now;

                        this.ExpedicionServicio.Editar(exp);
                        if (exp.Recepcion_Confirmada.HasValue && exp.Recepcion_Confirmada.Value == true)
                        {
                            int idUsuario = -1;
                            if (Configuraciones.Utiliza_Login)
                            {
                                idUsuario = Sesion.UsuarioActual.idUsuario;
                            }
                            db.Confirmar_Ingreso_Pendiente(exp.Codigo_Interno_Expedicion, idUsuario);
                            db.Movimiento_Stock_Venta_Expedicion(exp.Codigo_Interno_Expedicion);
                        }
                        if (this.Configuraciones.UtilizaBandejas)
                        {
                            this.BandejaServicio.PasarHistorico(0, Codigo_Interno_Expedicion);
                        }
                        scope.Complete();
                        return Json(new { success = true, message = "Actualizado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex) 
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }        
        }

        public ActionResult Existe(string prefijo, string numero_comprobante)
        {
            Expedicion e = this.ExpedicionServicio.Obtener((x) => x.Prefijo == prefijo && numero_comprobante == x.Numero_Comprobante).FirstOrDefault();
            if (e == null)
                return Json(new { data = false, success = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { data = true, success = true }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                dbVentas.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
