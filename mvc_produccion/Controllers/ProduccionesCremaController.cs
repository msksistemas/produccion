﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using System.Threading.Tasks;
using MVC_Produccion.Models;
using System.Net.Http;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class ProduccionesCremaController : Controller
    {
        private IBaseServicio<Produccion_Crema> ProduccionCremaServicio;
        private IBaseServicio<Detalle_Produccion> DetalleProduccionServicio;
        private IBaseServicio<Produccion> ProduccionServicio;
        private IBaseServicio<Envasado> EnvasadoServicio;
        private IBaseServicio<Detalle_Envasado> DetalleEnvasadoServicio;
        private IBaseServicio<Detalle_Insumo_Envasado> DetalleInsumoEnvasadoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Productos> ProductosServicio;
        private IBaseServicio<Costos_Indirectos_Produccion> CostosIndirectosProduccion;
        private IBaseServicio<Familias> FamiliaServicio;
        private IBaseServicio<Mov_Ins_Asoc> MovInsAsocServicio;
        private IBaseServicio<Queseros> QueserosServicio;
        private IProduccionServicio ProduccionServicioExtra;
        private dbProdSP db = new dbProdSP();
        private OpcionesTrabajo Configuraciones;
        private readonly HttpService httpClient;

        public ProduccionesCremaController(IBaseServicio<Produccion_Crema> produccionCremaServicio,
                                            IBaseServicio<Costos_Indirectos_Produccion> costosIndirectosProduccion,
                                           IBaseServicio<Formulas> formulasServicio,
                                           IProduccionServicio produciconServicioExtra,
                                           IBaseServicio<Queseros> queserosServicio,
                                           IBaseServicio<Familias> familiaServicio,
                                           IBaseServicio<Mov_Ins_Asoc> movInsAsocServicio,
                                           IBaseServicio<Costos_Indirectos_Produccion> CostosIndirectosProduccion,
                                           IBaseServicio<Detalle_Produccion> detalleProduccionServicio,
                                           IBaseServicio<Produccion> produccionServicio,
                                           IBaseServicio<Envasado> envasadoServicio,
                                           IBaseServicio<Detalle_Envasado> detalleEnvasadoServicio,
                                           IBaseServicio<Detalle_Insumo_Envasado> detalleInsumoEnvasadoServicio,
                                           IBaseServicio<Permisos> permisosServicio,
                                           IBaseServicio<Productos> productosServicio,
                                           IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                           HttpService httpClient)
        {
            this.CostosIndirectosProduccion = costosIndirectosProduccion;
            this.ProduccionServicioExtra = produciconServicioExtra;
            this.QueserosServicio = queserosServicio;
            this.MovInsAsocServicio = movInsAsocServicio;
            this.FamiliaServicio = familiaServicio;
            this.DetalleEnvasadoServicio = detalleEnvasadoServicio;
            this.DetalleInsumoEnvasadoServicio = detalleInsumoEnvasadoServicio;
            this.DetalleProduccionServicio = detalleProduccionServicio;
            this.EnvasadoServicio = envasadoServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.PermisosServicio = permisosServicio;
            this.ProduccionCremaServicio = produccionCremaServicio;
            this.ProduccionServicio = produccionServicio;
            this.FormulasServicio = formulasServicio;
            this.ProductosServicio = productosServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
            this.httpClient = httpClient;
        }

        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            else if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";
            ViewBag.configuraciones = this.Configuraciones;


            if (Sesion.UsuarioActual.Fabrica == 0 || Sesion.UsuarioActual.Fabrica == null && Sesion.UsuarioActual.Rol.ToLower() != "administrador")
            {
                ViewBag.Error = "No tiene Fábrica asignada, por favor, solicite Permisos al Administrador.";
            }

            var producciones = this.ProduccionServicio.Obtener();

            List<SelectListItem> ListadoHistoricos = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="No Históricos", Value="0"},
                                    new SelectListItem() {Text="Históricos", Value="1"},
                                    new SelectListItem() {Text="Todos", Value="-1"}
                                };

            ViewBag.ListadoHistoricos = ListadoHistoricos;

            return View(producciones);
        }

        public ActionResult GetData(string Lote = "")
        {
            if ((Sesion.UsuarioActual.Fabrica != 0 && Sesion.UsuarioActual.Fabrica != null) || Sesion.UsuarioActual.Rol.ToLower() == "administrador")
            {
                var listado = this.ProduccionCremaServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)
                                                                        && p.Produccion.Historico == false
                                                                        && p.Produccion.Codigo_Fabrica == (Sesion.UsuarioActual.Fabrica == -1 ? p.Produccion.Codigo_Fabrica : Sesion.UsuarioActual.Fabrica)
                                                                ).Select(p => new
                                                                {
                                                                    Fecha = p.Produccion.Fecha,
                                                                    p.Codigo_Interno_Produccion,
                                                                    p.Materia_Grasa,
                                                                    p.Acidez,
                                                                    p.Lote,
                                                                    p.Ph,
                                                                    p.Litros_Teoricos,
                                                                    p.Litros_Reales,
                                                                    p.Fecha_Vencimiento,
                                                                    p.Litros_Crema,
                                                                    p.Piezas_Obtenidas,
                                                                    Restan_Envasar = p.Litros_Reales -  this.ProduccionServicioExtra.ObtenerCantidadesEnvasadas(p.Lote)["kilos"]
                                                                }).ToList();
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = 1, Usuario = Sesion.UsuarioActual.Usuario, success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GetHistoricos(string Lote = "", int Historico = 0)
        {
            /* TODOS */
            if (Historico == -1)
            {
                var listado = this.ProduccionCremaServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)).Select(p => new
                {
                    Fecha = p.Produccion.Fecha,
                    p.Codigo_Interno_Produccion,
                    p.Materia_Grasa,
                    p.Acidez,
                    p.Lote,
                    p.Ph,
                    p.Litros_Teoricos,
                    p.Litros_Reales,
                    p.Fecha_Vencimiento,
                    p.Litros_Crema
                });
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var listado = this.ProduccionCremaServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)
                                                                        && p.Produccion.Historico == (Historico == 1 ? true : false))
                                                          .Select(p => new
                                                          {
                                                              Fecha = p.Produccion.Fecha,
                                                              p.Codigo_Interno_Produccion,
                                                              p.Materia_Grasa,
                                                              p.Acidez,
                                                              p.Lote,
                                                              p.Ph,
                                                              p.Litros_Teoricos,
                                                              p.Litros_Reales,
                                                              p.Litros_Crema
                                                          });

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote = "")
        {
            ViewBag.queseros = this.QueserosServicio.Obtener((q) => q.Activo == true).Select(q => new { q.Nombre, q.Codigo_Quesero });
            ViewBag.Codigo_Familia = this.FamiliaServicio.Obtener((f) => f.Nombre.ToLower()== "crema").FirstOrDefault().Codigo_Familia;
            if (Lote == "")
            {
                Produccion_Crema prod = new Produccion_Crema();

                Produccion produccion = new Produccion();
                produccion.Fecha = DateTime.Now;

                prod.Produccion = produccion;

                if (!this.Configuraciones.NroPlanillaDigitable)
                {
                    produccion.Nro_Planilla = (this.ProduccionCremaServicio.Obtener().Count() + 1).ToString();
                    ViewBag.NroAuto = true;
                }
                else
                {
                    ViewBag.NroAuto = false;
                }

                return View(prod);
            }
            else
            {
                var prod = this.ProduccionCremaServicio.Obtener((x) => x.Lote == Lote).FirstOrDefault();
                return View(prod);
            }

        }

        [HttpPost]
        public async Task<ActionResult> AddOrEdit(Produccion_Crema prod, string Codigo_Producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    prod.Lote = prod.Produccion.Lote;
                    prod.Produccion.idUsuario = Sesion.UsuarioActual.idUsuario;
                    if (prod.Codigo_Interno_Produccion == 0)
                    {
                        //if (httpClient.EsSanSatur())
                        //{
                        //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                        //        prod.Produccion.Detalle_Produccion.ToList(),
                        //        HttpService.ProcesoProduccion.Creacion,
                        //        prod.Produccion.Fecha ?? DateTime.Now,
                        //        httpClient.ObtenerPrimerNumProdVentas(Codigo_Producto)
                        //    );
                        //    if (!response.IsSuccessStatusCode)
                        //    {
                        //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                        //    }
                        //}
                        if (this.ProduccionServicio.Obtener((p) => p.Lote == prod.Lote).SingleOrDefault() != null)
                            return Json(new { success = false, message = "Ya existe un registro con ese Lote." }, JsonRequestBehavior.AllowGet);

                        var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                        prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;
                        /*GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS*/
                        foreach (var detalle in prod.Produccion.Detalle_Produccion)
                        {
                            detalle.Movimiento = "Producción";
                            detalle.Fecha = prod.Produccion.Fecha;
                            detalle.Lote = prod.Lote;
                        }
                        foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion)
                        {
                            detalle.Lote = prod.Produccion.Lote;
                        }
                        /*GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS*/
                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                        {
                            movimiento.Lote = prod.Lote;
                        }
                        prod.Produccion.Historico = false;
                        this.ProduccionCremaServicio.Agregar(prod);
                        db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);
                        scope.Complete();
                        return Json(new { success = true, /*envasar = nuevaFormula.Productos.Salida_Directa,*/ resultado = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        /*TRAE EL PRODUCTO ASOCIADO A LA FÒRMULA PARA SABER SI ES UN INSUMO, PROD TERMINADO O AMBOS*/
                        var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                        prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;
                        List<Detalle_Produccion> detalleAnterior = this.DetalleProduccionServicio.Obtener((dp) => dp.Lote == prod.Lote).ToList();

                        //if (httpClient.EsSanSatur())
                        //{
                        //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                        //        prod.Produccion.Detalle_Produccion.ToList(),
                        //        HttpService.ProcesoProduccion.Edicion,
                        //        Codigo_Producto,
                        //        detalleAnterior);
                        //    if (!response.IsSuccessStatusCode)
                        //    {
                        //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                        //    }
                        //}

                        /*BORRA LOS DETALLES DE LA PRODUCCIÓN A EDITAR*/
                        foreach (var detalle in detalleAnterior)
                        {
                            this.DetalleProduccionServicio.Borrar(detalle);
                        }
                        /*GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS*/
                        foreach (var detalle in prod.Produccion.Detalle_Produccion.ToList())
                        {
                            detalle.Movimiento = "Producción";
                            detalle.Fecha = prod.Produccion.Fecha;
                            detalle.Lote = prod.Lote;
                            this.DetalleProduccionServicio.Agregar(detalle);
                        }
                        foreach (var detalle in this.CostosIndirectosProduccion.Obtener((p) => p.Lote == prod.Produccion.Lote))
                        {
                            this.CostosIndirectosProduccion.Borrar(detalle);
                        }

                        foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion.ToList())
                        {
                            detalle.Lote = prod.Produccion.Lote;
                            this.CostosIndirectosProduccion.Agregar(detalle);
                        }
                        /*BORRA TODOS LOS DETALLES DE PRODUCTOS ASOCIADOS*/
                        foreach (var movimiento in this.MovInsAsocServicio.Obtener((mia) => mia.Lote == prod.Produccion.Lote))
                        {
                            this.MovInsAsocServicio.Borrar(movimiento);
                        }
                        /*GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS*/
                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                        {
                            movimiento.Lote = prod.Produccion.Lote;
                            this.MovInsAsocServicio.Agregar(movimiento);
                        }

                        this.ProduccionCremaServicio.Editar(prod);
                        this.ProduccionServicio.Editar(prod.Produccion);
                        db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);
                        scope.Complete();
                        return Json(new { success = true, envasar = false, resultado = 2 }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult ObtenerRestantes(string Lote = "")
        {
            decimal? litrosEnvasados = 0;
            decimal? piezasEnvasadas = 0;

            decimal? litrosRestantes = 0;
            decimal? piezasRestantes = 0;

            Produccion_Crema prod = new Produccion_Crema();
            prod = this.ProduccionCremaServicio.Obtener((p) => p.Lote == Lote).FirstOrDefault();

            foreach (var env in this.EnvasadoServicio.Obtener((e) => e.Lote == Lote).ToList())
            {
                if (env != null && env.Detalle_Envasado.Count() > 0)
                {
                    foreach (var detalle in env.Detalle_Envasado.ToList())
                    {
                        if (detalle != null)
                        {
                            litrosEnvasados += detalle.Kilos;
                            piezasEnvasadas += detalle.Cantidad;
                        }
                    }

                }
            }

            if (prod != null)
            {
                try
                {
                    litrosRestantes = prod.Litros_Reales - litrosEnvasados;
                    piezasRestantes = prod.Piezas_Obtenidas - piezasEnvasadas;
                }
                catch { }
            }

            return Json(new
            {
                success = true,
                kilosRestantes = litrosRestantes,
                piezasRestantes = piezasRestantes
            }, JsonRequestBehavior.AllowGet);

        }

        //[HttpPost]
        //public ActionResult Envasar(Envasado envasado, decimal? Litros_Reales_Envasado = 0)
        //{
        //    try
        //    {
        //        Produccion_Crema prod = new Produccion_Crema();
        //        prod = this.ProduccionCremaServicio.Obtener((p) => p.Lote == envasado.Lote).FirstOrDefault();
        //        prod.Litros_Reales = Litros_Reales_Envasado;
        //        prod.Produccion.Historico = true;
        //        this.ProduccionCremaServicio.Editar(prod);
        //        foreach (var detalle in envasado.Detalle_Envasado.ToList())
        //        {
        //            detalle.Codigo_Interno_Envasado = envasado.Codigo_Interno_Envasado;
        //        }
        //        this.EnvasadoServicio.Agregar(envasado);
        //        db.Movimiento_Stock_Envasado(envasado.Lote);
        //        db.Liberado_Por_Calidad(envasado.Codigo_Interno_Envasado);
        //        return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
