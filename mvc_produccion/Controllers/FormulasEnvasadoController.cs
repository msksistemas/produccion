﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using System.Transactions;

namespace MVC_Formulas_Envasado.Controllers
{
    class DetalleFormulaEnvasadoResult
    {
        public int Codigo_Interno_Detalle { get; set; }
        public string Codigo_Insumo { get; set; }
        public string Insumo { get; set; }
        public decimal? Precio_Costo { get; set; }
        public decimal Cantidad { get; set; }
        public decimal? Total { get; set; }
        public bool Marca { get; set; }
        public decimal? Ajuste { get; set; }
    } 
    public class FormulasEnvasadoController : Controller
    {
        private IBaseServicio<Familias> FamiliasServicio;
        private IBaseServicio<Productos> ProductosServicios;
        private IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> ProductosVentasServicios;
        private IBaseServicio<Formulas_Envasado> FormulasEnvasadoServicio;
        private IBaseServicio<Detalle_Formula_Envasado> DetalleFormulaEnvasado;
        private IBaseServicio<Costos_Indirectos_Formula_Envasado> CostosIndirectosFormulaEnvasado;
        private IBaseServicio<Monedas> MonedasServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<Insumos> InsumosServicio;
        private IBaseServicio<Costos_Indirectos> CostosIndirectosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private OpcionesTrabajo Configuraciones;
        public FormulasEnvasadoController(IBaseServicio<Detalle_Formula_Envasado> detalleFormulaEnvasadoServicio,
                                           IBaseServicio<Familias> familiasServicio,
                                           IBaseServicio<Costos_Indirectos> costosIndirectosServicio,
                                           IBaseServicio<Insumos> insumosServicios,
                                           IBaseServicio<Formulas_Envasado> formulasEnvasadoServicio,
                                           IBaseServicio<Permisos> permisosServicio,
                                           IBaseServicio<Productos> productosServicio,
                                           IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> productosVentasServicio,
                                           IBaseServicio<Costos_Indirectos_Formula_Envasado> costosIndirectosFormulasEnvasado,
                                           IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                           IBaseServicio<Monedas> monedasServicio)
        {
            this.ProductosServicios = productosServicio;
            this.ProductosVentasServicios = productosVentasServicio;
            this.FamiliasServicio = familiasServicio;
            this.PermisosServicio = permisosServicio;
            this.CostosIndirectosFormulaEnvasado = costosIndirectosFormulasEnvasado;
            this.DetalleFormulaEnvasado = detalleFormulaEnvasadoServicio;
            this.CostosIndirectosServicio = costosIndirectosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.FormulasEnvasadoServicio = formulasEnvasadoServicio;
            this.InsumosServicio = insumosServicios;
            this.MonedasServicio = monedasServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }
        private dbProdSP db = new dbProdSP();

        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            else if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";

            var formulas_Envasado = this.FormulasEnvasadoServicio.Obtener();

            return View(formulas_Envasado.ToList());
        }
        public ActionResult GetData(string Lote = "", string Fecha_Envasado = "", string Codigo_Producto = "")
        {
            try
            {
                DateTime fecha = DateTime.Now;
                if (Fecha_Envasado != "")
                    fecha = Convert.ToDateTime(Fecha_Envasado);
                var listado = db.GetFormulasEnvasado(Lote, fecha, Codigo_Producto).ToList();
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        // TRAE TODOS LOS DETALLES ASOCIADOS A LA FÒRMULA
        public ActionResult GetDetalles(int Codigo_Interno_Formula = 0)
        {
            if (Codigo_Interno_Formula == 0)
            {
                List<Detalle_Formula_Envasado> list = new List<Detalle_Formula_Envasado>();
                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            var listado = this.DetalleFormulaEnvasado.Obtener((dfe) => dfe.Codigo_Interno_Formula == Codigo_Interno_Formula && dfe.Insumos.Materia_Prima);

            List<DetalleFormulaEnvasadoResult> listReturn = new List<DetalleFormulaEnvasadoResult>();

            foreach(var det in listado)
            {
                var detalleFormulaAgregar = new DetalleFormulaEnvasadoResult();

                JOIN_insumo_proveedores_Result insumo = db.JOIN_insumo_proveedores(det.Codigo_Insumo, true).FirstOrDefault();

                detalleFormulaAgregar.Codigo_Interno_Detalle = det.Codigo_Interno_Detalle;
                detalleFormulaAgregar.Codigo_Insumo = det.Codigo_Insumo;
                detalleFormulaAgregar.Insumo = insumo.Descripcion;
                detalleFormulaAgregar.Precio_Costo = (decimal)insumo.Precio_Unitario;
                detalleFormulaAgregar.Cantidad = det.Cantidad;
                detalleFormulaAgregar.Total = det.Cantidad * (decimal)insumo.Precio_Unitario;
                detalleFormulaAgregar.Marca = det.Marca;
                detalleFormulaAgregar.Ajuste = det.Ajuste;

                listReturn.Add(detalleFormulaAgregar);
            }

            return Json(new { data = listReturn, success = true }, JsonRequestBehavior.AllowGet);
        }
        // TRAE TODOS LOS DETALLES ASOCIADOS A LA FÒRMULA
        public ActionResult GetInsumosEnvasado(string Codigo_Producto)
        {
            if (string.IsNullOrEmpty(Codigo_Producto))
            {
                List<Detalle_Formula_Envasado> list = new List<Detalle_Formula_Envasado>();
                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            var listado = this.DetalleFormulaEnvasado.Obtener((dfe) => dfe.Formulas_Envasado.Codigo_Producto == Codigo_Producto && dfe.Insumos.Materia_Prima)
                                           .Select(d => new
                                           {
                                               d.Formulas_Envasado.Numero_Producto_Ventas,
                                               d.Formulas_Envasado.Obtenidas_Por_Envasada,
                                               d.Formulas_Envasado.Cantidad_Envasada,
                                               d.Insumos.Codigo_Insumo,
                                               d.Insumos.Descripcion,
                                               d.Insumos.Precio_Por,
                                               d.Cantidad,
                                               Ajuste = 0,
                                               d.Insumos.Unidad_Medida
                                           }).ToList();
            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int Codigo_Interno_Formula = 0)
        {
            if (Codigo_Interno_Formula > 0)
            {
                try
                {
                    var formulaNueva = this.FormulasEnvasadoServicio.Obtener((f) => f.Codigo_Interno_Formula == Codigo_Interno_Formula)
                                                                    .Select(fe => new
                                                                    {

                                                                        fe.Codigo_Interno_Formula,
                                                                        fe.Codigo_Producto,
                                                                        Producto = fe.Productos.Descripcion,
                                                                        fe.Cantidad_Envasada,
                                                                        fe.Numero_Producto_Ventas,
                                                                        fe.Obtenidas_Por_Envasada
                                                                    });
                    return Json(new { data = formulaNueva, success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, mensaje = "Error, no existe registro con el Código ingresado." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, mensaje = "Error, no existe registro con Código negativo." }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCostosIndirectos(int Codigo_Interno_Formula = 0)
        {
            if (Codigo_Interno_Formula == 0)
            {
                List<Detalle_Formula_Envasado> list = new List<Detalle_Formula_Envasado>();
                return Json(new { data = list, success = true }, JsonRequestBehavior.AllowGet);
            }
            var listado = this.CostosIndirectosFormulaEnvasado
                              .Obtener((cife) => cife.Codigo_Interno_Formula == Codigo_Interno_Formula)
                              .Select(cife => new
                              {
                                  cife.Codigo_Costo_Indirecto_Interno,
                                  cife.Codigo_Interno_Formula,
                                  cife.Codigo_Costo_Indirecto,
                                  cife.Costos_Indirectos.Descripcion,
                                  Precio = cife.Costos_Indirectos.Precio * (decimal)(cife.Costos_Indirectos.Monedas != null ? cife.Costos_Indirectos.Monedas.intercambio : 1),
                                  cife.Cantidad,
                                  cife.Costos_Indirectos.Precio_Fijo,
                                  cife.Costos_Indirectos.Porcentaje,
                                  cife.Marca,
                                  Total = cife.Cantidad * cife.Costos_Indirectos.Precio * (decimal)(cife.Costos_Indirectos.Monedas != null ? cife.Costos_Indirectos.Monedas.intercambio : 1)
                              });
            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            ViewBag.Codigo_Familia_Quesos = this.FamiliasServicio.Obtener((f) => f.Nombre == "Quesos").SingleOrDefault().Codigo_Familia;
            if (id == 0)
            {
                Formulas_Envasado formula = new Formulas_Envasado();
                formula.Obtenidas_Por_Envasada = 1;
                formula.Cantidad_Envasada = 1;
                formula.Es_Insumo = false;
                return View(formula);
            }
            else
            {
                var formula = this.FormulasEnvasadoServicio.Obtener((x) => x.Codigo_Interno_Formula == id).FirstOrDefault();
                var prodVenta = this.ProductosVentasServicios.Obtener((pv) => pv.Numero_de_Producto == formula.Numero_Producto_Ventas).FirstOrDefault();
                ViewBag.descProducto = prodVenta != null ? prodVenta.Descripcion : "";
                return View(formula);
            }
        }
        [HttpPost]
        public ActionResult AddOrEdit(Formulas_Envasado formula)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var mostrarPrecios = Sesion.UsuarioActual.Muestra_Costos.HasValue ? Sesion.UsuarioActual.Muestra_Costos.Value : false;
                    var codigo_familia_quesos = this.FamiliasServicio.Obtener((f) => f.Nombre == "Quesos").SingleOrDefault().Codigo_Familia;
                    if (formula.Codigo_Interno_Formula < 0)
                        return Json(new { success = false, message = "Error, por favor ingrese un Código mayor a 0." }, JsonRequestBehavior.AllowGet);
                    if (formula.Codigo_Interno_Formula == 0)
                    {
                        Formulas_Envasado formulaExistente = this.FormulasEnvasadoServicio.Obtener((f) => f.Codigo_Producto == formula.Codigo_Producto && f.Numero_Producto_Ventas == formula.Numero_Producto_Ventas).FirstOrDefault();
                        if (formulaExistente != null)
                            return Json(new { success = false, message = "Ya existe una fórmula con el mismo producto desnudo y producto de ventas." }, JsonRequestBehavior.AllowGet);
                        Dictionary<string, decimal> cantities = new Dictionary<string, decimal>();
                        if (this.FormulasEnvasadoServicio.Obtener((f) => f.Codigo_Interno_Formula == formula.Codigo_Interno_Formula).FirstOrDefault() != null)
                            return Json(new { success = false, message = "Error, ya existe un registro con ese Código." }, JsonRequestBehavior.AllowGet);
                        foreach (var detalle in formula.Detalle_Formula_Envasado.ToList())
                        {
                            cantities.Add(detalle.Codigo_Insumo, detalle.Cantidad);
                        }
                        foreach (var detalle in formula.Costos_Indirectos_Formula_Envasado.ToList())
                        {
                            if (!mostrarPrecios)
                            {
                                var costo = this.CostosIndirectosServicio.Obtener((c) => c.Codigo_Costo_Indirecto == detalle.Codigo_Costo_Indirecto).SingleOrDefault();
                                if (costo.Porcentaje != null)
                                    detalle.Porcentaje = costo.Porcentaje.HasValue ? costo.Porcentaje.Value : 0;
                                if (costo.Precio != null)
                                    detalle.Precio = costo.Precio.HasValue ? costo.Precio.Value : 0;

                                if (detalle.Precio_Fijo == null)
                                    detalle.Precio_Fijo = false;
                                if (costo.Precio_Fijo != null)
                                {
                                    detalle.Precio = costo.Precio.HasValue ? costo.Precio.Value : 0;
                                    detalle.Precio_Fijo = true;
                                }

                            }

                            detalle.Codigo_Interno_Formula = formula.Codigo_Interno_Formula;
                    
                            formula.Costos_Indirectos_Formula_Envasado.Add(detalle);
                        }
                        if (!formula.Obtenidas_Por_Envasada.HasValue)
                            formula.Obtenidas_Por_Envasada = 1;

                        this.FormulasEnvasadoServicio.Agregar(formula);
                        foreach (var detalle in formula.Detalle_Formula_Envasado.ToList())
                        {
                            // Cambio de capa
                            using (var db = new Data.ProdEntities())
                            {
                                Data.Detalle_Formula_Envasado det = db.Detalle_Formula_Envasado.Find(detalle.Codigo_Interno_Formula, detalle.Codigo_Insumo);
                                db.Detalle_Formula_Envasado.Attach(det);

                                det.Cantidad = cantities[det.Codigo_Insumo];
                                db.SaveChanges();
                            }
                        }
                        scope.Complete();
                        return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        int idDetalle = 0;
                        if (!formula.Obtenidas_Por_Envasada.HasValue)
                            formula.Obtenidas_Por_Envasada = 1;
                        /*BORRA TODOS LOS DETALLES ASOCIADOS*/
                        foreach (var detalle in this.DetalleFormulaEnvasado.Obtener((df)=> df.Codigo_Interno_Formula== formula.Codigo_Interno_Formula))
                        {
                            this.DetalleFormulaEnvasado.Borrar(detalle);
                        }
                        /*GUARDA TODOS LOS DETALLES ASOCIADOS*/
                        foreach (var detalle in formula.Detalle_Formula_Envasado.ToList())
                        {
                            // Cambio de capa
                            using (var db = new Data.ProdEntities())
                            {
                                Data.Detalle_Formula_Envasado det = new Data.Detalle_Formula_Envasado();
                                det.Cantidad = detalle.Cantidad;
                                det.Codigo_Interno_Formula = formula.Codigo_Interno_Formula;
                                det.Codigo_Insumo = detalle.Codigo_Insumo;
                                det.Marca = detalle.Marca;
                                det.Ajuste = detalle.Ajuste;

                                det = db.Detalle_Formula_Envasado.Add(det);
                                db.SaveChanges();
                                idDetalle = det.Codigo_Interno_Detalle;
                            }

                            //detalle.Codigo_Interno_Formula = formula.Codigo_Interno_Formula;
                            //this.DetalleFormulaEnvasado.Agregar(detalle);
                        }
                        /*BORRA TODOS LOS COSTOS ASOCIADOS ASOCIADOS*/
                        foreach (var detalle in this.CostosIndirectosFormulaEnvasado.Obtener((ci)=> ci.Codigo_Interno_Formula == formula.Codigo_Interno_Formula))
                        {
                            this.CostosIndirectosFormulaEnvasado.Borrar(detalle);
                        }
                        /*GUARDA TODOS LOS COSTOS INDIRECTOS ASOCIADOS*/
                        foreach (var detalle in formula.Costos_Indirectos_Formula_Envasado.ToList())
                        {
                            if (!mostrarPrecios)
                            {
                                var costo = this.CostosIndirectosServicio.Obtener((c) => c.Codigo_Costo_Indirecto == detalle.Codigo_Costo_Indirecto).SingleOrDefault();
                                if (costo.Porcentaje != null)
                                    detalle.Porcentaje = costo.Porcentaje.HasValue ? costo.Porcentaje.Value : 0;
                                if (costo.Precio != null)
                                    detalle.Precio = costo.Precio.HasValue ? costo.Precio.Value : 0;

                                if (detalle.Precio_Fijo == null)
                                    detalle.Precio_Fijo = false;
                                if (costo.Precio_Fijo != null)
                                {
                                    detalle.Precio = costo.Precio.HasValue ? costo.Precio.Value : 0;
                                    detalle.Precio_Fijo = true;
                                }
                            }

                            detalle.Codigo_Interno_Formula = formula.Codigo_Interno_Formula;
                            this.CostosIndirectosFormulaEnvasado.Agregar(detalle);
                        }
                        ICollection<Detalle_Formula_Envasado> updatedDetalle = this.DetalleFormulaEnvasado.Obtener((detalleFormula) => detalleFormula.Codigo_Interno_Detalle == 0);
                        formula.Detalle_Formula_Envasado = updatedDetalle;
                        this.FormulasEnvasadoServicio.Editar(formula);
                        scope.Complete();
                        return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);
                    }
                } catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public ActionResult Delete(int Codigo_Interno_Formula = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Formulas_Envasado est = this.FormulasEnvasadoServicio.Obtener((fe) => fe.Codigo_Interno_Formula == Codigo_Interno_Formula).SingleOrDefault();
                    this.FormulasEnvasadoServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, message = "Error al eliminar, corrobore si el registro está en uso." }, JsonRequestBehavior.AllowGet);
                }

            }

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
