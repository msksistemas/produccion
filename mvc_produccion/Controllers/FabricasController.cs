﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class FabricasController : Controller
    {
        private IBaseServicio<Fabricas> FabricaServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajaoServicio;

        private OpcionesTrabajo Configuraciones;

        public FabricasController(IBaseServicio<Fabricas> fabricasServicio,
                                  IBaseServicio<Permisos> permisosServicio,
                                  IBaseServicio<OpcionesTrabajo> opcionesTrabajo)
        {
            this.FabricaServicio = fabricasServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajaoServicio = opcionesTrabajo;

            this.Configuraciones = this.OpcionesTrabajaoServicio.Obtener().FirstOrDefault();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData(int Codigo_Fabrica = -1)
        {
            var resultado = this.FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica || Codigo_Fabrica == -1).Select(fa => new
            {
                fa.Codigo_Interno_Fabrica,
                fa.Codigo_Fabrica,
                fa.Descripcion,
                fa.Nro_Planilla
            }).ToList();

            if (Codigo_Fabrica != -1 && resultado.Count == 0)
                return Json(new { message = "Error, no existe registro con el Código ingresado.", success = false }, JsonRequestBehavior.AllowGet);

            return Json(new { data = resultado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new Fabricas());
            else
                return View(this.FabricaServicio.Obtener((x) => x.Codigo_Fabrica == id).SingleOrDefault());

        }

        [HttpPost]
        public ActionResult AddOrEdit(Fabricas fabrica)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var mensaje = "Registro Modificado Satisfactoriamente.";
                    if (fabrica.Codigo_Interno_Fabrica == 0)
                    {
                        var existe = this.FabricaServicio.Obtener((fa) => fa.Codigo_Fabrica == fabrica.Codigo_Fabrica).SingleOrDefault();
                        if (existe != null)
                            return Json(new { success = false, message = "Ya existe un registro con ese Código." }, JsonRequestBehavior.AllowGet);

                        this.FabricaServicio.Agregar(fabrica);

                        mensaje = "Registro Guardado Satisfactoriamente.";
                    }
                    else
                        this.FabricaServicio.Editar(fabrica);
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Fabrica = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Fabricas fabrica = this.FabricaServicio.Obtener((fa) => fa.Codigo_Fabrica == Codigo_Fabrica).SingleOrDefault();

                    if (fabrica != null)
                        this.FabricaServicio.Borrar(fabrica);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message}, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = this.FabricaServicio.ObtenerConRawSql("SELECT TOP 1 * FROM Fabricas ORDER BY Codigo_Fabrica DESC").FirstOrDefault();

                return Json(new { data = ultimoRegistro.Codigo_Fabrica, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
