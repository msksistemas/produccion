﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class RubrosController : Controller
    {
        private IBaseServicio<Rubros> RubrosServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicios;
        private OpcionesTrabajo Configuraciones;

        public RubrosController(IBaseServicio<Rubros> rubrosServicio,
                                 IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                 IBaseServicio<Permisos> permisosServicio)
        {
            this.RubrosServicio = rubrosServicio;
            this.OpcionesTrabajoServicios = opcionesTrabajoServicio;
            this.PermisosServicio = permisosServicio;

            this.Configuraciones = this.OpcionesTrabajoServicios.Obtener().SingleOrDefault();
        }

        public ActionResult Index()
        {
            return View(this.RubrosServicio.Obtener());
        }

        public ActionResult GetData(int Codigo_Rubro = -1)
        {
            var listado = this.RubrosServicio.Obtener((r) => r.Codigo_Rubro == Codigo_Rubro || Codigo_Rubro == -1);

            if (Codigo_Rubro != -1)
            {
                var rubro = listado.SingleOrDefault();
                if (rubro != null)
                    return Json(new { data = rubro, success = true }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "Error, No existe Rubro." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new Rubros());
            else
                return View(this.RubrosServicio.Obtener((r) => r.Codigo_Rubro == id).SingleOrDefault());
           
        }

        [HttpPost]
        public ActionResult AddOrEdit(Rubros rubro)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var mensaje = "Registro Modificado satisfactoriamente.";
                try
                {
                    if (rubro.Codigo_Interno_Rubro == 0)
                    {
                        var existe = this.RubrosServicio.Obtener((r) => r.Codigo_Rubro == rubro.Codigo_Rubro).SingleOrDefault();
                        if (existe != null)
                            return Json(new { success = false, message = "Ya existe un registro con ese Código." }, JsonRequestBehavior.AllowGet);

                        this.RubrosServicio.Agregar(rubro);
                        mensaje = "Registro Guardado satisfactoriamente.";
                    }
                    else
                        this.RubrosServicio.Editar(rubro);
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = this.RubrosServicio.Obtener().Max(r => r.Codigo_Rubro);

                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Rubro)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Rubros eliminar =this.RubrosServicio.Obtener((r) => r.Codigo_Rubro == Codigo_Rubro).SingleOrDefault();
                    this.RubrosServicio.Borrar(eliminar);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
