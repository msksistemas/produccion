﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class TraspasosInsumosController : Controller
    {
        private IBaseServicio<Ajustes_Insumos> AjusteInsumosServicio;
        private IBaseServicio<Detalle_Ajuste_Insumos> DetalleAjusteInsumosServicio;

        private IBaseServicio<OpcionesTrabajo> OpcionesDeTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;

        private OpcionesTrabajo Configuraciones { get; set; }

        public TraspasosInsumosController(IBaseServicio<Ajustes_Insumos> ajusteInsumosServicio,
                                        IBaseServicio<OpcionesTrabajo> opcionesDeTrabajoServicio,
                                        IBaseServicio<Permisos> permisosServicio,
                                        IBaseServicio<Detalle_Ajuste_Insumos> detalleAjusteInsumosServicio)
        {
            this.AjusteInsumosServicio = ajusteInsumosServicio;
            this.DetalleAjusteInsumosServicio = detalleAjusteInsumosServicio;
            this.OpcionesDeTrabajoServicio = opcionesDeTrabajoServicio;
            this.PermisosServicio = permisosServicio;

            this.Configuraciones = this.OpcionesDeTrabajoServicio.Obtener().SingleOrDefault();
        }

        public ActionResult Traspasos_Insumos()
        {
            int fabrica = -1;

            fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : 0;

            ViewBag.Fabrica_Usuario = fabrica;

            return View();
        }

        public ActionResult Traspasos_Pendientes()
        {
            int fabrica = -1;
            fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : 0;
            ViewBag.Fabrica_Usuario = fabrica;

            return View();
        }

        public ActionResult Actualizar_Traspaso(List<int> codigos_traspasos)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                foreach (var codigo in codigos_traspasos)
                {
                    var traspaso = this.AjusteInsumosServicio.Obtener((aj) => aj.Codigo_Interno_Ajuste == codigo).SingleOrDefault();
                    traspaso.Estado = "Pendiente_Confirmacion";
                    this.AjusteInsumosServicio.Editar(traspaso);
                }
                scope.Complete();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Rechazar_Traspaso_Pendiente(int codigo_traspaso, string motivo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var traspaso = this.AjusteInsumosServicio.Obtener((aj) => aj.Codigo_Interno_Ajuste == codigo_traspaso).SingleOrDefault();
                traspaso.Estado = "Rechazado";
                traspaso.Motivo_Rechazo = motivo;
                this.AjusteInsumosServicio.Editar(traspaso);
                scope.Complete();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Obtener_Traspasos(bool pendiente_confirmacion)
        {
            int fabrica = -1;
            fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : 0;
            var listado = this.AjusteInsumosServicio.Obtener((ai) => ai.Historico == false
                              && ai.Es_Traspaso == true
                              && ((ai.Estado == "Pendiente_Confirmacion" && pendiente_confirmacion)
                                    || (ai.Estado != "Pendiente_Confirmacion" && pendiente_confirmacion == false))
                              && ((ai.Fabrica == (fabrica == -1 ? ai.Fabrica : fabrica) && pendiente_confirmacion)
                                    || (ai.Fabrica_Origen == (fabrica == -1 ? ai.Fabrica_Origen : fabrica) && pendiente_confirmacion == false)))
                              .Select(ai => new
                              {
                                  ai.Codigo_Interno_Ajuste,
                                  ai.Fecha,
                                  ai.Comentario,
                                  ai.Numero_Comprobante,
                                  ai.Descuento,
                                  ai.Tipo_Movimiento,
                                  ai.idUsuario,
                                  ai.Fabrica,
                                  ai.Fabrica_Origen,
                                  ai.Estado,
                                  ai.Motivo_Rechazo
                              });

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetData(int Codigo_Interno_Ajuste = 0)
        {
            int fabrica = -1;
            fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : 0;


            if (Codigo_Interno_Ajuste == -1)
            {
                var listado = this.AjusteInsumosServicio.Obtener((ai) =>
                                  ai.Historico == false
                                  && ai.Es_Traspaso == true
                                  && ai.Estado != "Pendiente_Confirmacion"
                                  && (ai.Fabrica == (fabrica == -1 ? ai.Fabrica : fabrica)
                                    || ai.Fabrica_Origen == (fabrica == -1 ? ai.Fabrica_Origen : fabrica))
                                ).Select(ai => new
                                {
                                    ai.Codigo_Interno_Ajuste,
                                    ai.Fecha,
                                    ai.Comentario,
                                    ai.Numero_Comprobante,
                                    ai.Descuento,
                                    ai.Tipo_Movimiento,
                                    ai.idUsuario,
                                    ai.Fabrica,
                                    ai.Fabrica_Origen
                                });
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (Codigo_Interno_Ajuste == 0)
                    return Json(new { data = new List<Ajustes_Insumos>(), detalles = new List<Detalle_Ajuste_Insumos>(), success = true }, JsonRequestBehavior.AllowGet);

                var lista = this.AjusteInsumosServicio.Obtener((ai) => ai.Codigo_Interno_Ajuste == Codigo_Interno_Ajuste);
                var listado = lista.Select(ai => new
                {
                    ai.Codigo_Interno_Ajuste,
                    ai.Fecha,
                    ai.Comentario,
                    ai.Tipo_Movimiento,
                    ai.Numero_Comprobante,
                    ai.Descuento,
                    ai.idUsuario,
                    ai.Fabrica,
                    ai.Fabrica_Origen,
                    ai.Estado,
                    ai.Motivo_Rechazo
                });
                var detalles = lista.SingleOrDefault().Detalle_Ajuste_Insumos.Select(dai =>
                                new
                                {
                                    dai.Codigo_Interno_Detalle,
                                    dai.Codigo_Insumo,
                                    Descripcion_Insumo = dai.Insumos.Descripcion,
                                    dai.Cantidad,
                                    dai.Kilos_Litros,
                                    dai.Codigo_Interno_Ajuste,
                                    Lote = string.IsNullOrEmpty(dai.Lote) ? " " : dai.Lote
                                });
                return Json(new { data = listado, detalles = detalles, success = true }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult AddOrEdit(int id = 0, bool solo_lectura = false)
        {
            int fabrica = 0;
            fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : 0;
            
            List<SelectListItem> TiposMovimiento = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Ingreso Mercadería", Value="Ingreso Mercadería"},
                new SelectListItem() {Text="Factura Cuenta Corriente", Value="Factura Cuenta Corriente"},
                new SelectListItem() {Text="N/C Por Devolución", Value="N/C Por Devolución"},
                new SelectListItem() {Text="Factura Contado", Value="Factura Contado"},
                new SelectListItem() {Text="Mercadería S/Cargo", Value="Mercadería S/Cargo"},
                new SelectListItem() {Text="Devolución Al Proveedor", Value="Devolución Al Proveedor"},
                new SelectListItem() {Text="Baja Por Deterioro", Value="Baja Por Deterioro"},
                new SelectListItem() {Text="Salida Por Producción", Value="Salida Por Producción"},
                new SelectListItem() {Text="Salida Por Sucursal", Value="Salida Por Sucursal"},
                new SelectListItem() {Text="Ajuste +", Value="Ajuste +"},
                new SelectListItem() {Text="Ajuste -", Value="Ajuste -"}
            };
            ViewBag.TiposMovimiento = TiposMovimiento;
            ViewBag.solo_lectura = solo_lectura;
            Ajustes_Insumos ajustes;
            if (id == 0)
            {
                ViewBag.Fabrica_Enabled = fabrica != -1 ? "disabled" : "enabled";
                ajustes = new Ajustes_Insumos();
                ajustes.Fecha = DateTime.Now;
                if (fabrica != -1)
                    ajustes.Fabrica_Origen = fabrica;
            }
            else
            {
                ajustes = this.AjusteInsumosServicio.Obtener((ai)=> ai.Codigo_Interno_Ajuste == id).SingleOrDefault();
                ViewBag.Fabrica_Enabled = ajustes.Fabrica_Origen != -1 ? "disabled" : "enabled";
            }
            return View(ajustes);
        }
        [HttpPost]
        public ActionResult AddOrEdit(Ajustes_Insumos Ajustes_Insumos)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    string mensaje = "Registro modificado satisfactoriamente";
                    if (Ajustes_Insumos.Codigo_Interno_Ajuste == 0)
                    {
                        bool existe = this.AjusteInsumosServicio.Obtener((ai) => ai.Es_Traspaso == true && ai.Numero_Comprobante == Ajustes_Insumos.Numero_Comprobante
                                                                           && ai.Prefijo == Ajustes_Insumos.Prefijo).SingleOrDefault() != null ? true : false;
                        if (existe)
                            return Json(new { success = false, message = "Ya existe Traspaso con ese N.Comprob y Prefijo." }, JsonRequestBehavior.AllowGet);

                        Ajustes_Insumos.Historico = false;
                        Ajustes_Insumos.Es_Traspaso = true;
                        Ajustes_Insumos.Tipo_Movimiento = "Traspaso";
                        Ajustes_Insumos.Estado = "Pendiente_Actualizacion";

                        this.AjusteInsumosServicio.Agregar(Ajustes_Insumos);
                        mensaje = "Registro guardado satisfactoriamente.";
                    }
                    else
                    {
                        var detallesViejos = this.DetalleAjusteInsumosServicio.Obtener((detViejo) => detViejo.Codigo_Interno_Ajuste == Ajustes_Insumos.Codigo_Interno_Ajuste);

                        foreach (var detalleViejo in detallesViejos)
                        {
                            this.DetalleAjusteInsumosServicio.Borrar(detalleViejo);
                        }
                        foreach (var detalleAgregar in Ajustes_Insumos.Detalle_Ajuste_Insumos)
                        {
                            this.DetalleAjusteInsumosServicio.Agregar(detalleAgregar);
                        }
                        Ajustes_Insumos.Es_Traspaso = true;
                        Ajustes_Insumos.Tipo_Movimiento = "Traspaso";

                        this.AjusteInsumosServicio.Editar(Ajustes_Insumos);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Ajuste)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Ajustes_Insumos est = this.AjusteInsumosServicio.Obtener((x) => x.Codigo_Interno_Ajuste == Codigo_Ajuste).FirstOrDefault();
                    this.AjusteInsumosServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        public ActionResult Pasar_A_Historico(int Codigo_Interno_Ajuste = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Ajustes_Insumos ajuste = this.AjusteInsumosServicio.Obtener((x) => x.Codigo_Interno_Ajuste == Codigo_Interno_Ajuste).FirstOrDefault();
                    ajuste.Historico = true;
                    ajuste.Estado = "Confirmado";
                    this.AjusteInsumosServicio.Editar(ajuste);
                    scope.Complete();
                    return Json(new { success = true, message = "Actualizado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
