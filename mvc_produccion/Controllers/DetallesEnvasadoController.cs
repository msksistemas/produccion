﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    /// <summary>
    /// VER QUE NO SE ESTA USANDO
    /// </summary>
    public class DetallesEnvasadoController : Controller
    {
        private IBaseServicio<Detalle_Envasado> DetalleEnvasadoServicio;

        public DetallesEnvasadoController(IBaseServicio<Detalle_Envasado> detalleEnvasadoServicio)
        {
            this.DetalleEnvasadoServicio = detalleEnvasadoServicio;
        }
        public ActionResult GetData(string Lote = "")
        {
            var listado = this.DetalleEnvasadoServicio.Obtener().Select(de => new
            {
                Codigo_Interno_Detalle = de.Codigo_Interno_Detalle,
                Lote = de.Envasado.Lote,
                Codigo_Interno_Formula = de.Codigo_Interno_Formula,
                Kilos = de.Kilos,
                Cantidad = de.Cantidad,
                Fecha = de.Envasado.Fecha
            });

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);         
        }
    }
}
