﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using AccesoDatos.Contexto.Gastos;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class ParametrosStockInsmos
    {
        public int Fabrica { get; set; }
        public string[] Codigos { get; set; }
        public string Lote { get; set; }

    }
    public class InsumosController : Controller
    {
        private IBaseServicio<Insumos> InsumosServicio;
        private IBaseServicio<Provee_Cta_Cte> ProveedoresServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Imputaciones> ImputacionesServicio;
        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();


        public InsumosController(IBaseServicio<Insumos> insumosServicio,
                                 IBaseServicio<Permisos> permisosServicio,
                                 IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                 IBaseServicio<Provee_Cta_Cte> proveedoresServicios,
                                 IBaseServicio<Imputaciones> imputacionesServicios)
        {
            this.InsumosServicio = insumosServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.ProveedoresServicio = proveedoresServicios;
            this.ImputacionesServicio = imputacionesServicios;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            var insumos = this.InsumosServicio.Obtener((i) => i.Activo == true).ToList();
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="Activo", Value="true"},
                                new SelectListItem() {Text="Inactivo", Value="false"}
                            };

            ViewBag.ListadoEstados = ListadoEstados;

            return View(insumos);
        }

        public ActionResult GetInsumo(string Codigo_Insumo)
        {
            if (Codigo_Insumo != "")
            {
                try
                {
                    var insumos = db.JOIN_insumo_proveedores(Codigo_Insumo, true).ToList();
                    return Json(new { data = insumos }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, mensaje = "Error, no existe registro con el Código ingresado." }, JsonRequestBehavior.AllowGet);
                }
            }

            Insumos insumoNuevo = new Insumos();
            insumoNuevo.Codigo_Insumo = (this.InsumosServicio.Obtener().Count + 1).ToString("0000");

            return Json(new { data = insumoNuevo, success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOne(string Codigo_Insumo)
        {
            if (Codigo_Insumo != "")
            {
                try
                {
                    var insumo = db.JOIN_insumo_proveedores(Codigo_Insumo, true).ToList();
                    if (insumo.Count == 0 || insumo == null)
                    {
                        return Json(new { mensaje = "No se encontró el insumo especificado.", success = false }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { data = insumo, success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false, mensaje = "Por favor, ingrese un Código." }, JsonRequestBehavior.AllowGet);
        }

        // TRAE TODOS LOS REGISTROS
        public ActionResult GetData(bool estado = true)
        {
            var listado = db.JOIN_insumo_proveedores("", estado).ToList();

            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLeche(string Codigo_Insumo="")
        {
            if (Codigo_Insumo == "")
            {
                var insumos = InsumosServicio.Obtener((i) => i.Leche == true && i.Activo == true).Select(i => new { i.Codigo_Insumo, i.Descripcion, i.Precio_Ultima_Compra });

                return Json(new { data = insumos }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var insumo = InsumosServicio.Obtener((i) => i.Codigo_Insumo == Codigo_Insumo && i.Leche == true && i.Activo == true).FirstOrDefault();
                
                return Json(new { success = insumo != null }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetDataPrima()
        {
            var listado = db.JOIN_insumo_proveedores("", true).ToList();

            listado = listado.Where(insumo => insumo.Materia_Prima).ToList();
            bool puedeEditar = Sesion.UsuarioActual.Rol.ToLower() == "administrador" ? true : false;

            return Json(new { data = listado, puedeEditar }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(string id = "", bool EsModal = false)
        {
            string ultimoRegistro = "";
            Insumos insumo = new Insumos();
            insumo.Fecha_Ultima_Compra = DateTime.Now;
            insumo.Codigo_Insumo = ultimoRegistro;
            insumo.Precio_Por = "K";
            insumo.Moneda = "Pesos";
            insumo.Unidad_Medida = "K";
            ViewBag.ImputarCompras = this.Configuraciones.ImputarCompras;
            ViewBag.EsModal = EsModal;

            if (id == "")
                return View(insumo);

            Insumos InsumoNuevo = new Insumos();
            InsumoNuevo = this.InsumosServicio.Obtener((x) => x.Codigo_Insumo == id).SingleOrDefault();
            try
            {
                Provee_Cta_Cte Prove_Habitual = this.ProveedoresServicio.Obtener((p) => p.CODIGO == InsumoNuevo.Codigo_Proveedor).FirstOrDefault();
                Provee_Cta_Cte Prove_Alternativo = this.ProveedoresServicio.Obtener((p) => p.CODIGO == InsumoNuevo.Codigo_Proveedor_Alternativo).FirstOrDefault();
                Provee_Cta_Cte Prove_Ultimo = this.ProveedoresServicio.Obtener((p) => p.CODIGO == InsumoNuevo.Codigo_Ultimo_Proveedor).FirstOrDefault();
                Imputaciones Imputacion = this.ImputacionesServicio.Obtener((i) => i.CODIGO == InsumoNuevo.Codigo_Imputacion).FirstOrDefault();

                if (Imputacion != null)
                    ViewBag.Imputacion = Imputacion.DESCRIPCION;

                if (Prove_Habitual != null)
                    ViewBag.Prove_Habitual = Prove_Habitual.RAZON_SOCIAL;

                if (Prove_Alternativo != null)
                    ViewBag.Prove_Alternativo = Prove_Alternativo.RAZON_SOCIAL;

                if (Prove_Ultimo != null)
                    ViewBag.Prove_Ultimo = Prove_Ultimo.RAZON_SOCIAL;


            }
            catch { }

            return View(InsumoNuevo);

        }

        [HttpPost]
        public ActionResult AddOrEdit(Insumos insumo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    insumo.Precio_Ultima_Compra = insumo.Precio_Ultima_Compra == null ? 0 : insumo.Precio_Ultima_Compra;
                    insumo.Precio_Lista = insumo.Precio_Lista == null ? 0 : insumo.Precio_Lista;
                    insumo.Punto_Pedido = insumo.Punto_Pedido == null ? 0 : insumo.Punto_Pedido;
               
                    insumo.Activo = true;
                    if (insumo.Codigo_Interno_Insumos == 0)
                    {
                        insumo.Activo = true;
                        this.InsumosServicio.Agregar(insumo);
                    }
                    else
                        this.InsumosServicio.Editar(insumo);
                    scope.Complete();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    var mensaje = e.Message;
                    var excepcion = e.InnerException;
                    while (excepcion != null)
                    {
                        mensaje = excepcion.Message;
                        excepcion = excepcion.InnerException;
                    }
                    mensaje = "Ya existe un Artículo con ese código";
                    return Json(new { success = false, error = mensaje }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = this.InsumosServicio.Obtener().Count;
                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(string Codigo_Insumo = "")
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Insumos insumo = this.InsumosServicio.Obtener((x) => x.Codigo_Insumo == Codigo_Insumo).FirstOrDefault<Insumos>();
                    var existe_df = insumo.Detalle_Formula.FirstOrDefault();
                    if (existe_df != null)
                        return Json(new { success = false, message = "Este insumo se esta usando en la formula: " + existe_df.Codigo_Formula }, JsonRequestBehavior.AllowGet);

                    var existe_dfe = insumo.Detalle_Formula_Envasado.FirstOrDefault();

                    if (existe_dfe != null)
                        return Json(new { success = false, message = "Este insumo se esta usando en la formula de envasado: " + existe_dfe.Codigo_Interno_Formula }, JsonRequestBehavior.AllowGet);

                    this.InsumosServicio.Borrar(insumo);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }

            }

        }

        public ActionResult CambiarEstado(string Codigo_Insumo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == Codigo_Insumo).SingleOrDefault();
                if (insumo.Activo == true)
                {
                    var existe_df = insumo.Detalle_Formula.FirstOrDefault();
                    if (existe_df != null)
                        return Json(new { success = false, message = "Este insumo se esta usando en la formula: " + existe_df.Codigo_Formula }, JsonRequestBehavior.AllowGet);

                    var existe_dfe = insumo.Detalle_Formula_Envasado.FirstOrDefault();

                    if (existe_dfe != null)
                        return Json(new { success = false, message = "Este insumo se esta usando en la formula de envasado: " + existe_dfe.Codigo_Interno_Formula }, JsonRequestBehavior.AllowGet);

                }
                insumo.Activo = !insumo.Activo;
                this.InsumosServicio.Editar(insumo);
                scope.Complete();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ObtenerCantidadesStock(List<string> Codigos, int? Fabrica = -1, string Lote = "")
        {
            List<Obtener_Stock_Lote_Insumo_Result> cantidades = new List<Obtener_Stock_Lote_Insumo_Result>();
            foreach (var codigo in Codigos)
            {
                List<Obtener_Stock_Lote_Insumo_Result> cantidadParcial = db.Obtener_Stock_Lote_Insumo(Fabrica, DateTime.Today, codigo, Lote).ToList();
                cantidades.AddRange(cantidadParcial);
            }
            return Json(new { cantidades }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ControlStockPuntoPedido()
        {
            int fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : 0;
            var resultado = db.Control_Stock_Insumo(fabrica, "", 0, null);
            return Json(new { resultado = resultado }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
