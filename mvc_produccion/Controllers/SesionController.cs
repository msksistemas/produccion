﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC_Produccion.ContextoProduccion;
using MVC_Produccion.Sesiones;
using MVC_Produccion.ViewModels;
using System.Threading;

namespace MVC_Produccion.Controllers
{
    public class SesionController : Controller
    {/*
        public static Usuarios user = new Usuarios();
        Entities db = new Entities();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LogOff()
        {
            try
            {
                if (Sesion.UsuarioActual.Rol.ToLower() != "administrador")
                {
                    ContextoProduccion.Sesiones sesionActual = null;

                    Usuarios usuarioActual = Sesion.UsuarioActual;
                    int idActual = usuarioActual.idUsuario;

                    sesionActual = db.Sesiones.Where(s => s.idUsuario == idActual).FirstOrDefault();

                    if(sesionActual != null)
                    {
                        db.Sesiones.Remove(sesionActual);
                        db.SaveChanges();
                    }
                }

                Sesion.UsuarioActual = null;

                return Json(new { message = "Deslogueado.", success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { message = ex.ToString(), success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult LoginModal()
        {
            LoginViewModel model = new LoginViewModel();
            return View(model);
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult LoginModal(LoginViewModel model)
        {
            string message = "";
            bool success = false;
            int error = 0;
            Usuarios usuario = new Usuarios();
            usuario = db.Usuarios.Where(u => u.Usuario == model.Usuario).FirstOrDefault();
            try
            {
                if (db.Usuarios.Where(u => u.Usuario == model.Usuario).FirstOrDefault() != null)
                {
                    if (usuario.Contraseña != model.Password)
                    {
                        message = "Contraseña incorrecta, vuelva a intentarlo.";
                        error = 1;
                    }
                    else
                    {
                        //if(db.Sesiones.Where(s => s.idUsuario == usuario.idUsuario).FirstOrDefault() == null)
                       // {
                            success = true;

                            //if(usuario.Rol.ToLower() != "administrador")
                            //{
                            //    ContextoProduccion.Sesiones sesionNueva = new ContextoProduccion.Sesiones();

                            //    sesionNueva.idUsuario = usuario.idUsuario;
                            //    sesionNueva.Fecha = DateTime.Now;

                            //    db.Sesiones.Add(sesionNueva);

                            //    db.SaveChanges();

                            //    ThreadStart delegado = new ThreadStart(DesloguearUsuario);

                            //    Thread hilo = new Thread(delegado);
                            //    hilo.Start();

                            //    user = usuario;
                            //}  
                       // }
                       // else
                       // {
                       //     message = "El usuario ya está logueado en el sistema, solicite al Administrador cierre su sesión.";
                       //     error = 3;
                       // }
                    }
                }
                else
                {
                    message = "No existe el usuario ingresado.";
                    error = 2;
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }
            return Json(new { message, success, error }, JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        public ActionResult Register()
        {
            RegisterViewModel model = new RegisterViewModel();
            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            string message = "";
            bool success = false;
            try
            {
                if (db.Usuarios.Where(u => u.Usuario == model.Usuario).FirstOrDefault() != null)
                {
                    message = "Ya existe un usuario con el nombre de usuario ingresado.";
                }
                else
                {

                    Usuarios usuario = new Usuarios();
      
                    usuario.Usuario = model.Usuario;
                    usuario.Contraseña = model.Password;
                    usuario.Rol = "Usuario";
                    usuario.Fabrica = 0;
                    usuario.Muestra_Costos = false;

                    db.Usuarios.Add(usuario);

                    db.SaveChanges();

                    Permisos permisoNuevo = new Permisos();

                    Usuarios user = new Usuarios();
                    user = db.Usuarios.Where(u => u.Usuario == model.Usuario).FirstOrDefault();

                    permisoNuevo.idUsuario = user.idUsuario;

                    db.Permisos.Add(permisoNuevo);
          
                    db.SaveChanges();

                    //if (usuario.Rol.ToLower() != "administrador")
                    //{
                    //    ContextoProduccion.Sesiones sesionNueva = new ContextoProduccion.Sesiones();

                    //    sesionNueva.idUsuario = usuario.idUsuario;
                    //    sesionNueva.Fecha = DateTime.Now;

                    //    db.Sesiones.Add(sesionNueva);

                    //    db.SaveChanges();
                    //}

                    success = true;
                    Sesion.UsuarioActual = usuario;

                    message = "Se ha registrado satisfactoriamente.";

                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }

            return Json(new { message = message, success = success }, JsonRequestBehavior.AllowGet);
        }
        public void DesloguearUsuario()
        {
            while (true)
            {
                Thread.Sleep(10800000);
                if (user.Rol.ToLower() != "administrador")
                {
                    ContextoProduccion.Sesiones sesionActual = new ContextoProduccion.Sesiones();
                    sesionActual = db.Sesiones.Where(s => s.idUsuario == user.idUsuario).FirstOrDefault();

                    if(sesionActual != null)
                    {
                        db.Sesiones.Remove(sesionActual);

                        db.SaveChanges();

                        user = null;
                    }   
                }
            }           
        }      */
    }
}