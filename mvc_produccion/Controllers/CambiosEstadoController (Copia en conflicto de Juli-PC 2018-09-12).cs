﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_Produccion.ContextoProduccion;
using MVC_Produccion.Sesiones;

namespace MVC_Produccion.Controllers
{
    public class CambiosEstadoController : Controller
    {
        private Entities db = new Entities();

        // GET: Cambio_Estado
        public ActionResult Index()
        {
            var cambio_Estado = db.Cambio_Estado.Include(c => c.Camaras).Include(c => c.Estados).Include(c => c.Produccion).Include(c => c.Usuarios);
            return View(cambio_Estado.ToList());
        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote)
        {
            Cambio_Estado cambioEstado = new Cambio_Estado();
            cambioEstado.Lote = Lote;
            cambioEstado.Fecha = DateTime.Now;

            return View(cambioEstado);
        }

        [HttpPost]
        public ActionResult AddOrEdit(Cambio_Estado cambio_estado)
        {
            try
            {
                Produccion_Quesos prod = new Produccion_Quesos();
                prod = db.Produccion_Quesos.Where(p => p.Lote == cambio_estado.Lote).FirstOrDefault();

                prod.Codigo_Estado = cambio_estado.Codigo_Estado;

                db.SaveChanges();

                return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
