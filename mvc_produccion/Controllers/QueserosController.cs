﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class QueserosController : Controller
    {
        private IBaseServicio<Queseros> QueserosServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private OpcionesTrabajo Configuraciones;

        public QueserosController(IBaseServicio<Queseros> queserosServicio,
                                   IBaseServicio<Permisos> permisosServicio,
                                   IBaseServicio<OpcionesTrabajo> opcionesTrabajo)
        {
            this.QueserosServicio = queserosServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajo;

            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="Activo", Value="true"},
                                new SelectListItem() {Text="Inactivo", Value="false"}
                            };
            ViewBag.ListadoEstados = ListadoEstados;

            return View();
        }

        public ActionResult GetData(bool estado = true)
        {
            var listado = this.QueserosServicio.Obtener((q) => q.Activo == estado).Select(q => new
            {
                q.Codigo_Quesero,
                q.Codigo_Quesero_Interno,
                q.Activo,
                q.Nombre
            });

            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult AddOrEdit(int Codigo_Quesero, string Nombre)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var existe = this.QueserosServicio.Obtener((q) => q.Codigo_Quesero == Codigo_Quesero).SingleOrDefault();
                try
                {
                    string mensaje = "Registro Modificado satisfactoriamente.";

                    if (existe == null)
                    {
                        existe = new Queseros
                        {
                            Nombre = Nombre,
                            Codigo_Quesero = Codigo_Quesero,
                            Activo = true
                        };
                        this.QueserosServicio.Agregar(existe);
                        mensaje = "Registro guardado satisfactoriamente.";
                    }
                    else
                    {
                        existe.Nombre = Nombre;
                        this.QueserosServicio.Editar(existe);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = 1;
                var queseros = this.QueserosServicio.Obtener();
                if (queseros.Count != 0)
                    ultimoRegistro = queseros.Max(q => q.Codigo_Quesero);

                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Quesero)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var est = this.QueserosServicio.Obtener((x) => x.Codigo_Quesero == Codigo_Quesero).SingleOrDefault();
                    this.QueserosServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message =ex.Message}, JsonRequestBehavior.AllowGet);
                }
            }          
        }

        public ActionResult CambiarEstado(int codigo_quesero)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var quesero =this.QueserosServicio.Obtener((q) => q.Codigo_Quesero == codigo_quesero).SingleOrDefault();
                quesero.Activo = !quesero.Activo;
                this.QueserosServicio.Editar(quesero);
                scope.Complete();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
