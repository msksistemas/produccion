﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion;
using Business.Models;
using Business.Models.Schemes;
using Business.DataTransferObjects;
using System.IO;
using Newtonsoft.Json;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class PermisosController : Controller
    {
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private OpcionesTrabajo Configuraciones;

        public PermisosController(IBaseServicio<Permisos> permisosServicio,
                                    IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio)
        {
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
            {
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            }
            else if (resultado == 2)
            {
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";
            }

            return View();
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new Permisos());
            else
            {
                var permisos = this.PermisosServicio.Obtener((x) => x.idUsuario == id).FirstOrDefault();
                // permisos.ProductoVenta = permisos.ProductoVenta.HasValue ? permisos.ProductoVenta.Value : false;
                // permisos.Proveedores = permisos.Proveedores.HasValue ? permisos.Proveedores.Value : false;

                return View(permisos);
            }
        }

        [HttpGet]
        public ActionResult AddOrEditV2(int id = 0)
        {
            // Necesitamos el userId en la view
            ViewData["UserId"] = id;
            Dictionary<string, Access_Scheme> modules = new Dictionary<string, Access_Scheme>();

            if (id == 0)
                return View(modules);

            User user = Business.Models.User.Get_UserByID(id);
            
            // Transforma el array de modulos en el formato que utiliza el listado
            foreach (Access_Scheme access in user.AccessScheme)
            {
                if (!modules.ContainsKey(access.Module.Name))
                {
                    modules.Add(access.Module.Name, access);
                }
            }

            return View(modules);
        }
        [HttpPost]
        public ActionResult AddOrEditV2()
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                Request.InputStream.Position = 0;
                var req = Request.InputStream;
                var json = new StreamReader(req).ReadToEnd();
                User_DTO result = JsonConvert.DeserializeObject<User_DTO>(json);
            
                try
                {
                    result.saveAccessScheme();
                    scope.Complete();
                    return Json(new { success = true, message = "Registro modificado satisfactoriamente" });
                }
                catch (Exception e)
                {
                    return Json(new { success = false, message = e.Message });
                }

            }
            
        }

        [HttpPost]
        public ActionResult AddOrEdit(Permisos Permiso)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                if (Permiso.Camaras || Permiso.Costos_Indirectos || Permiso.Estados
                        || Permiso.Insumos || Permiso.Proveedores || Permiso.Fabricas
                        || Permiso.Lineas || Permiso.Productos || Permiso.ProductoVenta
                        || Permiso.Queseros || Permiso.Rubros || Permiso.Silos
                        || Permiso.Sucursales || Permiso.Tinas || Permiso.Motivos_Retencion)
                    Permiso.Menu_Maestros = true;
                else
                    Permiso.Menu_Maestros = false;

                if (Permiso.Formulas || Permiso.Formulas_Envasado)
                    Permiso.Menu_Costos = true;
                else
                    Permiso.Menu_Costos = false;

                if (Permiso.Elaboracion_Queso || Permiso.Elaboracion_Queso_Fundido || Permiso.Elaboracion_Leche_Polvo || Permiso.Elaboracion_Ricota
                    || Permiso.Elaboracion_Dulce_De_Leche || Permiso.Elaboracion_Muzzarela)
                    Permiso.Planillas_Elaboracion = true;
                else
                    Permiso.Planillas_Elaboracion = false;

                if (Permiso.Elaboracion_Queso || Permiso.Elaboracion_Queso_Fundido || Permiso.Elaboracion_Muzzarela || Permiso.Informe_Pesadas || Permiso.Elaboracion_Dulce_De_Leche
                    || Permiso.Liberar_Por_Calidad == true)
                    Permiso.Menu_Novedades = true;
                else
                    Permiso.Menu_Novedades = false;

                if (Permiso.Ingreso_Factura || Permiso.Ingreso_Leche || Permiso.Ajustes_Insumos)
                    Permiso.Menu_Insumos = true;
                else
                    Permiso.Menu_Insumos = false;


                if (Permiso.Opciones_Trabajo || Permiso.Datos_Empresa || Permiso.Duplicar_Formula)
                    Permiso.Menu_Utilidades = true;
                else
                    Permiso.Menu_Utilidades = false;

                if (Permiso.Stock_Insumos || Permiso.Compras_Proveedor || Permiso.Variacio_Precios || Permiso.Insumos)
                    Permiso.Listados_Insumos = true;
                else
                    Permiso.Listados_Insumos = false;

                if (Permiso.Producido_Entre_Fechas || Permiso.Detallado_Formula || Permiso.Detallado_Formula_Envasado
                   || Permiso.Detallado_Produccion || Permiso.Resumen_Produccion || Permiso.Producciones_Por_Producto
                   || Permiso.Seguimiento_Lote || Permiso.Listado_Merma)
                    Permiso.Listados_Produccion = true;
                else
                    Permiso.Listados_Produccion = false;

                if (Permiso.Costos_Productos || Permiso.Stock_Productos || Permiso.Stock_Productos_Detallado || Permiso.Stock_Resumido)
                    Permiso.Listados_Productos = true;
                else
                    Permiso.Listados_Productos = false;

                if (Permiso.Listados_Productos || Permiso.Listados_Produccion || Permiso.Listados_Insumos || Permiso.Stock_Leche)
                    Permiso.Menu_Listados = true;
                else
                    Permiso.Menu_Listados = false;



                if (Permiso.Expedicion || Permiso.Ajustes_Productos)
                    Permiso.Menu_Stock = true;
                else
                    Permiso.Menu_Stock = false;

                if (Permiso.Carga_Traspasos_Insumos || Permiso.Pendientes_Traspasos_Insumos)
                    Permiso.Traspasos_Insumos = true;
                else
                    Permiso.Traspasos_Insumos = false;

                Permiso.Menu_Orden_Compra = Permiso.Ingreso_Orden_Compra;
                this.PermisosServicio.Editar(Permiso);

                scope.Complete();
                return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
            }

        }
    }

}
