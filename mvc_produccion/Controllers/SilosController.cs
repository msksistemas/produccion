﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class SilosController : Controller
    {
        private IBaseServicio<Silos> SilosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;

        public SilosController( IBaseServicio<Silos> silosServicio,
                                IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio)
        {
            this.SilosServicio = silosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetOne(int Codigo_Silo)
        {
            try
            {
                Silos silo = this.SilosServicio.Obtener((s) => s.Codigo_Silo == Codigo_Silo).FirstOrDefault();
                if (silo == null)
                {
                    return Json(new { success = false, message = "Error, no existe un Silo con el Código ingresado." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, data = silo }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult GetData()
        {
            var listado = this.SilosServicio.Obtener().Select(s => new
            {
                s.Codigo_Interno_Silo,
                s.Codigo_Silo,
                s.Ecomilk,
                s.Nombre
            });

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new Silos());
            else
                return View(this.SilosServicio.Obtener((x) => x.Codigo_Silo == id).SingleOrDefault());
          
        }

        [HttpPost]
        public ActionResult AddOrEdit(int Codigo_Silo, int? Ecomilk, string Nombre)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var existe = this.SilosServicio.Obtener((s) => s.Codigo_Silo == Codigo_Silo).SingleOrDefault();
                    var mensaje = "Registro guardado satisfactoriamente.";
                    if (existe == null)
                    {
                        existe = new Silos
                        {
                            Nombre = Nombre,
                            Codigo_Silo = Codigo_Silo,
                            Ecomilk = Ecomilk
                        };

                        this.SilosServicio.Agregar(existe);
                    }
                    else
                    {
                        existe.Nombre = Nombre;
                        existe.Ecomilk = Ecomilk;
                        mensaje = "Registro Modificado satisfactoriamente.";
                        this.SilosServicio.Editar(existe);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message}, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = 1;
                var silos = this.SilosServicio.Obtener();
                if (silos.Count != 0)
                    ultimoRegistro = silos.Max(s => s.Codigo_Silo);
                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Silo = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var silo = this.SilosServicio.Obtener((s) => s.Codigo_Silo == Codigo_Silo).SingleOrDefault();
                    this.SilosServicio.Borrar(silo);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, message = "Error al eliminar, corrobore si el registro está en uso." }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
