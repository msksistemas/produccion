﻿using System.Linq;
using System.Web.Mvc;
using MVC_Produccion.ContextoProduccion;
using MVC_Produccion.Sesiones;
using Microsoft.AspNet.Identity;

namespace MVC_Produccion.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private Entities db = new Entities();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        public ActionResult Report(string url)
        {
            ViewBag.Url = url;
            return View();
        }
    }
}