﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using MVC_Produccion.ContextoProduccion;
using MVC_Produccion.Sesiones;

namespace MVC_Produccion.Controllers
{
    //TODO hacer ver
    public class ProduccionesYogurController : Controller
    {
        private Entities db = new Entities();

        // GET: ProduccionesYogur
        public ActionResult Index()
        {
            return View(db.Produccion_Yogur.ToList());
        }

        public ActionResult GetData(string Lote = "")
        {

            var listado = from p in db.Produccion_Yogur
                          join f in db.Formulas
                          on p.Produccion.Codigo_Formula equals f.Codigo_Formula into Formulas
                          from f in Formulas.DefaultIfEmpty()

                          join q in db.Queseros
                          on p.Produccion.Codigo_Quesero equals q.Codigo_Quesero into Queseros
                          from q in Queseros.DefaultIfEmpty()

                          join prod in db.Productos
                          on f.Codigo_Producto equals prod.Codigo_Producto into Productos
                          from prod in Productos.DefaultIfEmpty()

                          where p.Lote == (Lote == "" ? p.Lote : Lote)
                          && p.Produccion.Historico == false

                          select new
                          {
                              p,
                              Codigo_Producto = f.Codigo_Producto,
                              Producto = prod.Descripcion,
                              Quesero = q.Nombre,
                          };

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote = "")
        {
            if (Lote == "")
            {
                Produccion_Yogur prod = new Produccion_Yogur();
                prod.Produccion.Fecha = DateTime.Now;

                return View(prod);
            }
            else
            {

                return View(db.Produccion_Crema.Where(x => x.Lote == Lote).FirstOrDefault<Produccion_Crema>());

            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(Produccion_Yogur produccion, string Codigo_Producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                produccion.Produccion.idUsuario = Sesion.UsuarioActual.idUsuario;
                if (produccion.Codigo_Interno_Produccion == 0)
                {
                    if (db.Produccion.Find(produccion.Lote) == null)
                    {
                        Produccion_Yogur produccionNuevo = new Produccion_Yogur();
                        produccionNuevo.Lote = produccion.Lote;
                        produccionNuevo.Ph = produccion.Ph;
                        produccionNuevo.Fecha_Vencimiento = produccion.Fecha_Vencimiento;
                        produccionNuevo.Lote = produccion.Lote;

                        produccionNuevo.Produccion.Codigo_Fabrica = produccion.Produccion.Codigo_Fabrica;
                        produccionNuevo.Produccion.Lote = produccion.Produccion.Lote;
                        produccionNuevo.Produccion.Nro_Planilla = produccion.Produccion.Nro_Planilla;
                        produccionNuevo.Produccion.Codigo_Quesero = produccion.Produccion.Codigo_Quesero;
                        produccionNuevo.Produccion.Historico = true;

                        Formulas nuevaFormula = db.Formulas.Where(f => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();

                        produccionNuevo.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;

                        /*GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS*/

                        foreach (var detalle in produccion.Produccion.Detalle_Produccion.ToList())
                        {
                            if (detalle != null)
                            {
                                detalle.Movimiento = "Producción";
                                detalle.Fecha = produccion.Produccion.Fecha;
                                detalle.Lote = produccion.Lote;

                                produccionNuevo.Produccion.Detalle_Produccion.Add(detalle);
                            }

                        }

                        foreach (var acidificacion in produccion.Produccion.Acidificacion.ToList())
                        {
                            if (acidificacion != null)
                            {
                                acidificacion.Lote = produccion.Lote;

                                db.Acidificacion.Add(acidificacion);
                            }

                        }

                        db.Produccion_Yogur.Add(produccionNuevo);
                        db.SaveChanges();
                        //int Codigo_Interno_Produccion = db.Produccion_Yogur.Where(p => p.Lote == produccionNuevo.Lote).FirstOrDefault().Codigo_Interno_Produccion;
                        //db.Movimiento_Stock_Produccion(Codigo_Interno_Produccion);
                        scope.Complete();
                        return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, message = "Ya existe un registro con ese Lote." }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    Produccion_Yogur produccionNuevo = db.Produccion_Yogur.Where(p => p.Lote == produccion.Lote).FirstOrDefault();

                    if (produccionNuevo != null)
                    {
                        produccionNuevo.Lote = produccion.Lote;
                        produccionNuevo.Ph = produccion.Ph;
                        produccionNuevo.Fecha_Vencimiento = produccion.Fecha_Vencimiento;
                        produccionNuevo.Lote = produccion.Lote;

                        produccionNuevo.Produccion.Codigo_Fabrica = produccion.Produccion.Codigo_Fabrica;
                        produccionNuevo.Produccion.Lote = produccion.Produccion.Lote;
                        produccionNuevo.Produccion.Nro_Planilla = produccion.Produccion.Nro_Planilla;
                        produccionNuevo.Produccion.Codigo_Quesero = produccion.Produccion.Codigo_Quesero;

                        /*TRAE EL PRODUCTO ASOCIADO A LA FÒRMULA PARA SABER SI ES UN INSUMO, PROD TERMINADO O AMBOS*/
                        Formulas nuevaFormula = db.Formulas.Where(f => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();

                        produccionNuevo.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;

                        /*BORRA LOS DETALLES DE LA PRODUCCIÓN A EDITAR*/

                        foreach (var detalle in produccionNuevo.Produccion.Detalle_Produccion.ToList())
                        {
                            if (detalle != null)
                            {
                                db.Detalle_Produccion.Remove(detalle);
                            }

                        }

                        /*GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS*/
                        foreach (var detalle in produccion.Produccion.Detalle_Produccion.ToList())
                        {
                            if (detalle != null)
                            {
                                detalle.Movimiento = "Producción";
                                detalle.Fecha = produccion.Produccion.Fecha;
                                detalle.Lote = produccion.Lote;
                            }

                            produccionNuevo.Produccion.Detalle_Produccion.Add(detalle);

                        }

                        db.SaveChanges();
                        //db.Movimiento_Stock_Produccion(produccionNuevo.Codigo_Interno_Produccion);
                        scope.Complete();
                        return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, message = "Error al actualizar registro." }, JsonRequestBehavior.AllowGet);
                    }

                }

            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
