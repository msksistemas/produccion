﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using Servicios.Interfaces;
using MVC_Produccion.Models.Utilidades;
using System.Net.Http;
using MVC_Produccion.Models;
using System.Threading.Tasks;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class ProduccionesController : Controller
    {
        private IBaseServicio<Produccion> ProduccionServicio;
        private IBaseServicio<Detalle_Produccion> DetalleProduccionServicio;
        private IBaseServicio<Costos_Indirectos_Produccion> CostosIndirectosProduccionServicio;
        private IBaseServicio<Produccion_Quesos> ProduccionQuesosServicio;
        private IBaseServicio<Produccion_Quesos_Fundido> ProduccionQuesosFundidoServicio;
        private IBaseServicio<Produccion_Crema> ProduccionCremaServicio;
        private IBaseServicio<Produccion_Leche_Polvo> ProduccionLechePolvoServicio;
        private IBaseServicio<Produccion_Ricota> ProduccionRicotaServicio;
        private IBaseServicio<Produccion_Muzzarela> ProduccionMuzzarelaServicio;
        private IBaseServicio<Insumos> InsumosServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Produccion_Dulce_de_Leche> ProduccionDulceLecheServicio;
        private IBaseServicio<Envasado> EnvasadoServicio;
        private IBaseServicio<Detalle_Expedicion> DetalleExpedicionServicio;
        private IBaseServicio<Detalle_Traspaso_Fabricas> DetalleTraspasoFabricaServicio;
        private IBaseServicio<Detalle_Formula> DetalleFormulaServicio;
        private IProduccionServicio ProduccionServicioExtra;
        private dbProdSP db = new dbProdSP();
        private readonly HttpService httpClient;

        public ProduccionesController(IBaseServicio<Produccion> produccionServicio,
                                        IProduccionServicio produccionServicioExtra,
                                        IBaseServicio<Formulas> formulasServicio,
                                        IBaseServicio<Detalle_Formula> detalleFormulaServicio,
                                        IBaseServicio<Detalle_Produccion> detalleProduccionServicio,
                                        IBaseServicio<Costos_Indirectos_Produccion> costosIndirectosProduccionServicio,
                                        IBaseServicio<Produccion_Quesos> produccionQuesoServicio,
                                        IBaseServicio<Produccion_Quesos_Fundido> produccionQuesosFundidoServicio,
                                        IBaseServicio<Produccion_Crema> produccionCremaServicio,
                                        IBaseServicio<Produccion_Leche_Polvo> produccionLechePolvoServicio,
                                        IBaseServicio<Produccion_Ricota> produccionRicotaServicio,
                                        IBaseServicio<Produccion_Muzzarela> produccionMuzzarellaServicio,
                                        IBaseServicio<Produccion_Dulce_de_Leche> produccionDulceLecheServicio,
                                        IBaseServicio<Envasado> EnvasadoServicio,
                                        IBaseServicio<Detalle_Expedicion> detalleExpedicionServicio,
                                        IBaseServicio<Detalle_Traspaso_Fabricas> detalleTraspasoFabricasServicio,
                                        IBaseServicio<Insumos> insumosServicio,
                                        HttpService httpClient)
        {
            this.DetalleFormulaServicio = detalleFormulaServicio;
            this.ProduccionServicioExtra = produccionServicioExtra;
            this.FormulasServicio = formulasServicio;
            this.ProduccionCremaServicio = produccionCremaServicio;
            this.ProduccionLechePolvoServicio = produccionLechePolvoServicio;
            this.ProduccionDulceLecheServicio = produccionDulceLecheServicio;
            this.ProduccionMuzzarelaServicio = produccionMuzzarellaServicio;
            this.ProduccionQuesosServicio = produccionQuesoServicio;
            this.ProduccionQuesosFundidoServicio = produccionQuesosFundidoServicio;
            this.ProduccionRicotaServicio = produccionRicotaServicio;
            this.DetalleProduccionServicio = detalleProduccionServicio;
            this.EnvasadoServicio = EnvasadoServicio;
            this.DetalleExpedicionServicio = detalleExpedicionServicio;
            this.DetalleTraspasoFabricaServicio= detalleTraspasoFabricasServicio;
            this.InsumosServicio = insumosServicio;
            this.ProduccionServicio = produccionServicio;
            this.CostosIndirectosProduccionServicio = costosIndirectosProduccionServicio;
            this.httpClient = httpClient;
        }

        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
            {
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            }
            else if (resultado == 2)
            {
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";
            }

            var producciones = this.ProduccionServicio.Obtener();

            return View(producciones);
        }

        public ActionResult GetData(string Lote = "")
        {
            try
            {
                var listado = this.ProduccionServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote))
                                .Select(p => new
                                {
                                    Codigo_Interno_Produccion = p.Codigo_Interno_Produccion,
                                    Codigo_Formula = p.Codigo_Formula,
                                    Codigo_Producto = p.Formulas != null ? p.Formulas.Codigo_Producto : "",
                                    Producto = p.Formulas != null ? p.Formulas.Productos != null ? p.Formulas.Productos.Descripcion : "" : "",
                                    Fecha = p.Fecha,
                                    Nro_Planilla = p.Nro_Planilla,
                                    Codigo_Quesero = p.Codigo_Quesero,
                                    Quesero = p.Queseros != null ? p.Queseros.Nombre : "",
                                    Lote = p.Lote,
                                    Codigo_Fabrica = p.Codigo_Fabrica

                                }).ToList();

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ObtenerRestantes(string Lote = "")
        {
            var cantidadesEnvasadas = this.ProduccionServicioExtra.ObtenerCantidadesEnvasadas(Lote);
            var cantidadesReales = this.ProduccionServicioExtra.ObtenerCantidadesReales(Lote);

            return Json(new
            {
                success = true,
                kilosRestantes = cantidadesReales["kilos"] - cantidadesEnvasadas["kilos"],
                piezasRestantes = cantidadesReales["piezas"] - cantidadesEnvasadas["piezas"]
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenerProduccion(string Lote = "")
        {
            var cantidadesReales = this.ProduccionServicioExtra.ObtenerCantidadesReales(Lote);
            var cantidades = this.ProduccionServicioExtra.ObtenerCantidadesEnvasadas(Lote);

            var prod = this.ProduccionServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote))
                            .Select(p => new
                            {
                                Codigo_Interno_Produccion = p.Codigo_Interno_Produccion,
                                Codigo_Formula = p.Codigo_Formula,
                                Codigo_Producto = p.Formulas != null ? p.Formulas.Codigo_Producto : "",
                                Producto = p.Formulas != null ? p.Formulas.Productos != null ? p.Formulas.Productos.Descripcion : "" : "",
                                Fecha = p.Fecha,
                                Nro_Planilla = p.Nro_Planilla,
                                Codigo_Quesero = p.Codigo_Quesero,
                                Quesero = p.Queseros != null ? p.Queseros.Nombre : "",
                                Lote = p.Lote,
                                Codigo_Fabrica = p.Codigo_Fabrica,
                                Vencimiento_Envasado = p.Formulas != null ? p.Formulas.Productos != null ? p.Formulas.Productos.Vencimiento_Envasado : false : false,
                                Kilos_Reales = cantidadesReales["kilos"],
                                Kilos_Envasado = cantidades["kilos"],
                                Piezas_Obtenidas = cantidadesReales["piezas"]
                            }).SingleOrDefault();

            return Json(new { data = prod, success = true }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ObtenerTodas(string Lote = "", int Codigo_Estado = 0)
        {
            var listado = this.ProduccionServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote))
                           .Select(p => new
                           {
                               Codigo_Interno_Produccion = p.Codigo_Interno_Produccion,
                               Codigo_Formula = p.Codigo_Formula,
                               Codigo_Producto = p.Formulas.Codigo_Producto,
                               Producto = p.Formulas.Productos.Descripcion,
                               Fecha = p.Fecha,
                               Codigo_Quesero = p.Codigo_Quesero,
                               Quesero = p.Queseros.Nombre,
                               Lote = p.Lote
                           }).ToList();

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        //TRAE TODOS LOS DETALLES ASOCIADOS A LA PRODUCCIÓN
        public ActionResult GetDetalles(string Lote)
        {
            if (Lote == "")
            {
                List<Detalle_Produccion> list = new List<Detalle_Produccion>();

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            var listado = this.DetalleProduccionServicio.Obtener((dp) => dp.Lote == Lote).Select(d => new
            {
                d.Produccion.Formulas.Codigo_Formula,
                d.Produccion.Formulas.Litros_Tina,
                d.Codigo_Insumo,
                d.Precio_Unitario,
                d.Cantidad,
                d.Cantidad_Unidades,
                d.Fecha,
                this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == d.Codigo_Insumo).SingleOrDefault().Descripcion,
                Precio_Lista_Insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == d.Codigo_Insumo).SingleOrDefault().Precio_Lista,
                this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == d.Codigo_Insumo).SingleOrDefault().Precio_Por,
                d.Ajuste,
                d.Lote_Insumo,
                this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == d.Codigo_Insumo).SingleOrDefault().Masa
            });
            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCostosIndirectos(string Lote = "")
        {
            if (Lote == "")
            {
                List<Costos_Indirectos_Produccion> list = new List<Costos_Indirectos_Produccion>();

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var listado = this.CostosIndirectosProduccionServicio.Obtener((cip) => cip.Lote == Lote).Select(cip => new
                {
                    cip.Codigo_Interno_Detalle,
                    cip.Lote,
                    cip.Codigo_Costo_Indirecto,
                    cip.Precio,
                    cip.Cantidad,
                    cip.Precio_Fijo,
                    cip.Porcentaje

                });
                
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetUltimo(int Codigo_Fabrica)
        {
            try
            {
                var ultimoRegistro = 1;

                if (this.ProduccionServicio.Obtener((p) => p.Codigo_Fabrica == Codigo_Fabrica).Count() > 0)
                {
                    ultimoRegistro = int.Parse(this.ProduccionServicio.Obtener((p) => p.Codigo_Fabrica == Codigo_Fabrica).Max(p => p.Nro_Planilla)) + 1;
                }

                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(string Lote)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try 
                {
                    Produccion est = this.ProduccionServicio.Obtener((x) => x.Lote == Lote).FirstOrDefault();

                    //if (httpClient.EsSanSatur())
                    //{
                    //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                    //        est.Detalle_Produccion.ToList(),
                    //        HttpService.ProcesoProduccion.Eliminacion);
                    //    if (!response.IsSuccessStatusCode)
                    //    {
                    //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                    //    }
                    //}

                    var prodDulce = this.ProduccionDulceLecheServicio.Obtener((pd) => pd.Lote == Lote).SingleOrDefault();
                    var prodMuzza = this.ProduccionMuzzarelaServicio.Obtener((pd) => pd.Lote == Lote).SingleOrDefault();
                    var prodCrema = this.ProduccionCremaServicio.Obtener((pd) => pd.Lote == Lote).SingleOrDefault();
                    var prodRicota = this.ProduccionRicotaServicio.Obtener((pd) => pd.Lote == Lote).SingleOrDefault();
                    var prodQuesoFundido = this.ProduccionQuesosFundidoServicio.Obtener((pd) => pd.Lote == Lote).SingleOrDefault();
                    var prodLechePolvo = this.ProduccionLechePolvoServicio.Obtener((pd) => pd.Lote == Lote).SingleOrDefault();
                    var prodQuesos = this.ProduccionQuesosServicio.Obtener((pd) => pd.Lote == Lote).SingleOrDefault();
                    var Envasado = this.EnvasadoServicio.Obtener((de) => de.Lote == Lote).ToList();
                    var detalleExpedicion = this.DetalleExpedicionServicio.Obtener((de) => de.Lote == Lote).ToList();
                    var detalleTraspasoFabricas = this.DetalleTraspasoFabricaServicio.Obtener((de) => de.Lote == Lote).ToList();

                    if (est.Historico == true)
                    {
                        db.Borrar_Produccion_Historica(Lote);
                    } 
                    else 
                    {
                        if (prodCrema != null)
                            this.ProduccionCremaServicio.Borrar(prodCrema);
                        if (prodDulce != null)
                            this.ProduccionDulceLecheServicio.Borrar(prodDulce);
                        if (prodMuzza != null)
                            this.ProduccionMuzzarelaServicio.Borrar(prodMuzza);
                        if (prodRicota != null)
                            this.ProduccionRicotaServicio.Borrar(prodRicota);
                        if (prodQuesoFundido != null)
                            this.ProduccionQuesosFundidoServicio.Borrar(prodQuesoFundido);
                        if (prodLechePolvo != null)
                            this.ProduccionLechePolvoServicio.Borrar(prodLechePolvo);
                        if (prodQuesos != null)
                            this.ProduccionQuesosServicio.Borrar(prodQuesos);

                        db.Movimiento_Stock_Produccion(est.Codigo_Interno_Produccion, true);

                        string lotePadre = null;
                        if(est.Lote_Padre != null)
                        {
                            lotePadre = est.Lote_Padre;
                        }

                        this.ProduccionServicio.Borrar(est);
                        foreach (var e in Envasado)
                        { 
                            this.EnvasadoServicio.Borrar(e);
                        }
                        foreach (var de in detalleExpedicion)
                        {
                            this.DetalleExpedicionServicio.Borrar(de);
                        }
                        foreach (var dtp in detalleTraspasoFabricas)
                        {
                            this.DetalleTraspasoFabricaServicio.Borrar(dtp);
                        }
                        if (lotePadre != null)
                        {
                            var prodPadre = this.ProduccionServicio.Obtener((p) => p.Lote == lotePadre).FirstOrDefault();
                            var prodHijas = this.ProduccionServicio.Obtener((p) => p.Lote_Padre == lotePadre);
                            if(prodHijas.Count == 0)
                            {
                                this.ProduccionServicio.Borrar(prodPadre);
                            }
                        }
                    }
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex) 
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        //OBTIENE TODOS LOS PRODUCTOS ASOCIADOS DE UNA PRODUCCIÓN
        public ActionResult Get_Mov_Ins_Asoc(string Lote = "")
        {
            if (Lote == "")
            {
                List<Mov_Ins_Asoc> list = new List<Mov_Ins_Asoc>();

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    Produccion produccion = this.ProduccionServicio.Obtener((p) => p.Lote == Lote).SingleOrDefault();

                    if (produccion.Mov_Ins_Asoc.Count() > 0)
                    {
                        var insumos = produccion.Mov_Ins_Asoc.Select(p => new
                        {
                            Codigo_Interno_Insumos = p.Codigo_Interno_Movimiento,
                            p.Insumos.Codigo_Insumo,
                            p.Insumos.Descripcion,
                            p.Lote,
                            p.Litros
                        });
                        return Json(new { data = insumos, success = true }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var insumos = from f in produccion.Formulas.Insumos

                                      select new
                                      {
                                          f.Codigo_Interno_Insumos,
                                          f.Codigo_Insumo,
                                          f.Descripcion,
                                          produccion.Lote,
                                          Litros = 0
                                      };
                        return Json(new { data = insumos, success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception)
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult ExisteProduccion(Produccion Produccion)
        {

            if (Produccion.Codigo_Interno_Produccion == 0)
            {

                if (this.ProduccionServicio.Obtener((p) => p.Lote == Produccion.Lote).SingleOrDefault() != null)
                {
                    return Json("Ya existe.", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExisteFormula(string Producto)
        {
            Formulas formula = this.FormulasServicio.Obtener((x) => x.Codigo_Producto == Producto).FirstOrDefault();
            if (formula == null)
                return Json(false, JsonRequestBehavior.AllowGet);
            else
                return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenerInsumosConsumidos(string producto, decimal litros, string tipoProd, string lote = "")
        {
            List<InsumoConsumidoProduccion> lista = new List<InsumoConsumidoProduccion>();
            if (lote != "")
            {
                foreach (var item in this.DetalleProduccionServicio.Obtener((d)=> d.Lote == lote))
                {
                    var i = this.InsumosServicio.Obtener((insumo) => insumo.Codigo_Insumo == item.Codigo_Insumo).SingleOrDefault();
                    if (i != null)
                    {
                        lista.Add(new InsumoConsumidoProduccion {
                            CodigoInsumo = item.Codigo_Insumo,
                            DescripcionInsumo = i.Descripcion,
                            Cantidad = item.Cantidad.HasValue ? item.Cantidad.Value : 0,
                            Ajuste = item.Ajuste.HasValue ? item.Ajuste.Value : 0,
                            Lote = item.Lote_Insumo,
                            PrecioPor = i.Precio_Por,
                            PrecioUnitario = item.Precio_Unitario.HasValue ? item.Precio_Unitario.Value : 0,
                            Fecha_Vencimiento = item.Fecha_Vencimiento,
                            Ph = item.Ph,
                            Humedad = item.Humedad,
                            Rendimiento = item.Rendimiento,
                            EsLeche = i.Leche,
                        });
                    }
                }

                return PartialView("ObtenerInsumosConsumidos", lista);
            }
            var formula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == producto).FirstOrDefault();
            if (formula == null || formula.Detalle_Formula == null)
                return PartialView("ObtenerInsumosConsumidos", lista);

            foreach (var item in formula.Detalle_Formula.Where((df) => df.Marca))
            {
                var insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == item.Codigo_Insumo).SingleOrDefault();
                var consumido = new InsumoConsumidoProduccion
                {
                    CodigoInsumo = item.Codigo_Insumo,
                };
                switch (tipoProd)
                {
                    case "ricota":
                        consumido.Cantidad = (item.Cantidad * litros) / (formula.Litros_Suero.HasValue ? formula.Litros_Suero.Value : 1);
                        break;
                    case "quesos":
                        consumido.Cantidad = formula.Litros_Tina == 0 ? 0 : ((item.Cantidad * litros) / formula.Litros_Tina);
                        break;
                    case "quesos reproceso":
                        consumido.Cantidad = (item.Cantidad * litros) / formula.Kilos_Reproceso.Value;
                        break;
                    case "muzzarela":
                        consumido.Cantidad = (item.Cantidad * litros) / (formula.Kilos_Masa.HasValue ? formula.Kilos_Masa.Value : 1);
                        break;
                    case "dulceLeche":
                        consumido.Cantidad = (item.Cantidad * litros) / formula.Litros_Tina;
                        break;
                    case "crema":
                        consumido.Cantidad = (item.Cantidad * litros) / (formula.Litros_Crema.HasValue ? formula.Litros_Crema.Value : 1);
                        break;
                    default:
                        break;
                }
                if (insumo != null)
                {
                    consumido.PrecioUnitario = insumo.Precio_Ultima_Compra.HasValue ? insumo.Precio_Ultima_Compra.Value : 0;
                    consumido.DescripcionInsumo = insumo.Descripcion;
                    consumido.PrecioPor = insumo.Precio_Por;
                }
                consumido.EsLeche = insumo.Leche;
                consumido.EsMasa = insumo.Masa;

                lista.Add(consumido);
            }
            
            return PartialView("ObtenerInsumosConsumidos", lista);
        }

        public ActionResult ObtenerInsumosConsumidosMuzza(string producto, decimal litros, string tipoProd, string lote = "")
        {
            List<InsumoConsumidoProduccion> lista = new List<InsumoConsumidoProduccion>();
            if (lote != "")
            {
                foreach (var item in this.DetalleProduccionServicio.Obtener((d) => d.Lote == lote))
                {
                    var i = this.InsumosServicio.Obtener((insumo) => insumo.Codigo_Insumo == item.Codigo_Insumo).SingleOrDefault();
                    if (i != null)
                    {
                        lista.Add(new InsumoConsumidoProduccion
                        {
                            CodigoInsumo = item.Codigo_Insumo,
                            DescripcionInsumo = i.Descripcion,
                            Cantidad = item.Cantidad.HasValue ? item.Cantidad.Value : 0,
                            Cantidad_Unidades = item.Cantidad_Unidades.HasValue ? item.Cantidad_Unidades.Value : 0,
                            Ajuste = item.Ajuste.HasValue ? item.Ajuste.Value : 0,
                            Lote = item.Lote_Insumo,
                            PrecioPor = i.Precio_Por,
                            PrecioUnitario = item.Precio_Unitario.HasValue ? item.Precio_Unitario.Value : 0,
                            Fecha = item.Fecha,
                            Fecha_Vencimiento = item.Fecha_Vencimiento,
                            Ph = item.Ph,
                            Humedad = item.Humedad,
                            Rendimiento = item.Rendimiento,
                            EsMasa = i.Masa
                        });
                    }
                }

                return PartialView("ObtenerInsumosConsumidosMuzza", lista);
            }
            var formula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == producto).FirstOrDefault();
            if (formula == null || formula.Detalle_Formula == null)
                return PartialView("ObtenerInsumosConsumidosMuzza", lista);

            foreach (var item in formula.Detalle_Formula.Where((df) => df.Marca))
            {
                var insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == item.Codigo_Insumo).SingleOrDefault();
                var consumido = new InsumoConsumidoProduccion
                {
                    CodigoInsumo = item.Codigo_Insumo,
                };
                switch (tipoProd)
                {
                    case "ricota":
                        consumido.Cantidad = (item.Cantidad * litros) / (formula.Litros_Suero.HasValue ? formula.Litros_Suero.Value : 1);
                        break;
                    case "quesos":
                        consumido.Cantidad = (item.Cantidad * litros) / formula.Litros_Tina;
                        break;
                    case "quesos reproceso":
                        consumido.Cantidad = (item.Cantidad * litros) / formula.Kilos_Reproceso.Value;
                        break;
                    case "muzzarela":
                        consumido.Cantidad = (item.Cantidad * litros) / (formula.Kilos_Masa.HasValue ? formula.Kilos_Masa.Value : 1);
                        break;
                    case "dulceLeche":
                        consumido.Cantidad = (item.Cantidad * litros) / formula.Litros_Tina;
                        break;
                    case "crema":
                        consumido.Cantidad = (item.Cantidad * litros) / (formula.Litros_Crema.HasValue ? formula.Litros_Crema.Value : 1);
                        break;
                    default:
                        break;
                }
                if (insumo != null)
                {
                    consumido.PrecioUnitario = insumo.Precio_Ultima_Compra.HasValue ? insumo.Precio_Ultima_Compra.Value : 0;
                    consumido.DescripcionInsumo = insumo.Descripcion;
                    consumido.PrecioPor = insumo.Precio_Por;
                    consumido.EsMasa = insumo.Masa;
                }

                lista.Add(consumido);
            }
            return PartialView("ObtenerInsumosConsumidosMuzza", lista);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
