﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Ventas;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    class DetalleFormulaResult
    {
        public int Codigo_Formula { get; set; }
        public decimal? Kilos_Teoricos { get; set; }
        public int Litros_Tina { get; set; }
        public decimal? Kilos_Masa { get; set; }
        public decimal? Litros_Crema { get; set; }
        public decimal? Litros_Suero { get; set; }
        public string Codigo_Insumo { get; set; }
        public string Descripcion { get; set; }
        public decimal? Precio_Lista_Insumo { get; set; }
        public string Precio_Por { get; set; }
        public decimal Cantidad { get; set; }
        public decimal? Total { get; set; }
        public decimal? Precio_Unitario { get; set; }
        public bool Marca { get; set; }
        public decimal? Factor { get; set; }
        public float IVA { get; set; }
    }

    public class FormulasController : Controller
    {
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<AccesoDatos.Contexto.Produccion.Productos> ProductoProduccionServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Detalle_Formula> DetalleFormulaServicio;
        private IBaseServicio<Impuestos> ImpuestosServicio;
        private IBaseServicio<Monedas> MonedasServicio;
        private IBaseServicio<Familias> FamiliasServicio;
        private IBaseServicio<Costos_Indirectos_Formula> CostosIndirectosFormulaServicio;
        private IBaseServicio<Insumos_Formulas_Recuperado> InsumosFormulasRecuperadoServicio;
        private AccesoDatos.Contexto.Produccion.SP.dbProdSP db = new AccesoDatos.Contexto.Produccion.SP.dbProdSP();
        private OpcionesTrabajo Configuraciones;

        public FormulasController(IBaseServicio<Permisos> permisosServicio,
                                    IBaseServicio<AccesoDatos.Contexto.Produccion.Productos> productosServicio,
                                    IBaseServicio<Monedas> monedasServicio,
                                    IBaseServicio<Familias> familiasServicio,
                                    IBaseServicio<Insumos_Formulas_Recuperado> insumosFormulasRecuperadoServicio,
                                    IBaseServicio<Costos_Indirectos_Formula> costosIndirectosFormulaServicio,
                                    IBaseServicio<Impuestos> impuestosServicio,
                                    IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                    IBaseServicio<Formulas> formulasServicio,
                                    IBaseServicio<Detalle_Formula> detalleFormulaServicio)
        {
            this.CostosIndirectosFormulaServicio = costosIndirectosFormulaServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.FormulasServicio = formulasServicio;
            this.DetalleFormulaServicio = detalleFormulaServicio;
            this.ImpuestosServicio = impuestosServicio;
            this.MonedasServicio = monedasServicio;
            this.FamiliasServicio = familiasServicio;
            this.InsumosFormulasRecuperadoServicio = insumosFormulasRecuperadoServicio;
            this.ProductoProduccionServicio = productosServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        // GET: Fórmulas
        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";

            return View();
        }

        // TRAE TODOS LOS REGISTROS
        public ActionResult GetData(int nohistoricas = 0)
        {
            try
            {
                var listado = this.FormulasServicio.Obtener((f) => nohistoricas == 0
                                                                || ((f.Historico == false) || (f.Historico == null)))
                                                  .Select(f => new
                                                  {
                                                      Codigo_Formula = f.Codigo_Formula,
                                                      Codigo_Producto = f.Codigo_Producto,
                                                      Descripcion_Producto = f.Productos.Descripcion,
                                                      Litros_Kilos = f.Litros_Tina > 0 ? f.Litros_Tina : (f.Litros_Crema > 0 ? f.Litros_Crema : (f.Litros_Suero > 0 ? f.Litros_Suero : (f.Kilos_Masa > 0 ? f.Kilos_Masa : null))),
                                                      f.Litros_Crema,
                                                      f.Litros_Suero,
                                                      f.Kilos_Masa,
                                                      Factor = f.Factor,
                                                      Kilos_Teoricos = f.Kilos_Teoricos,
                                                      Unidades_Esperadas = f.Unidades_Esperadas,
                                                      Comentario = f.Comentario
                                                  });
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            } catch (Exception ex)
            {
                return Json(new { message = ex.Message, success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        // TRAE TODOS LOS REGISTROS
        public ActionResult GetFormulasSegunUsaProducto(string utiliza, string familia, int nohistoricas = 0)
        {
            var listado = db.GetFormulasSegunUsaProducto(familia, false).ToList();
            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOne(int Codigo_Formula, string Codigo_Producto)
        {
            if (Codigo_Formula > 0)
            {
                try
                {
                    var formula = this.FormulasServicio.Obtener((f) => f.Codigo_Formula == Codigo_Formula).Select(f => new
                    {
                        Codigo_Formula = f.Codigo_Formula,
                        Codigo_Producto = f.Codigo_Producto,
                        Producto = f.Productos.Descripcion,
                        Litros_Tina = f.Litros_Tina,
                        Litros_Crema = f.Litros_Crema,
                        Factor = f.Factor,
                        Kilos_Teoricos = f.Kilos_Teoricos,
                        AptoParaEnvasar = f.Productos.Formulas_Envasado.Count > 0 ? true : false,
                        f.Productos.Salida_Directa,
                        f.Productos.Vencimiento_Envasado,
                        f.Productos.Dias_Frescura,
                        f.Productos.Codigo_Familia,
                        f.Kilos_Masa
                    });
                    return Json(new { data = formula, success = true }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }

            if (Codigo_Producto != "")
            {
                try
                {
                    var formula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).Select(f => new
                    {
                        Codigo_Formula = f.Codigo_Formula,
                        Codigo_Producto = f.Codigo_Producto,
                        Producto = f.Productos.Descripcion,
                        Litros_Tina = f.Litros_Tina,
                        Litros_Crema = f.Litros_Crema,
                        Factor = f.Factor,
                        Kilos_Teoricos = f.Kilos_Teoricos,
                        AptoParaEnvasar = f.Productos.Formulas_Envasado.Count > 0 ? true : false,
                        f.Productos.Salida_Directa,
                        f.Productos.Vencimiento_Envasado,
                        f.Productos.Dias_Frescura,
                        f.Productos.Codigo_Familia,
                        f.Kilos_Reproceso,
                        f.Kilos_Masa
                    });

                    return Json(new { data = formula, success = true }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false, mensaje = "Codigo Formula y producto vacio" }, JsonRequestBehavior.AllowGet);

        }

        // TRAE TODOS LOS DETALLES ASOCIADOS A LA FÓRMULA
        public ActionResult GetDetalles(string Codigo_Producto = "", int Codigo_Formula = -1)
        {
            if (Codigo_Producto == "")
            {
                List<Detalle_Formula> list = new List<Detalle_Formula>();
                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            List<DetalleFormulaResult> lest = new List<DetalleFormulaResult>();

            foreach (var formula in this.FormulasServicio.Obtener((f) => (Codigo_Formula >= 0 ? Codigo_Formula == f.Codigo_Formula : true)))
            {
                foreach (var detalle in formula.Detalle_Formula)
                {
                    if (formula.Codigo_Producto == Codigo_Producto && detalle.Insumos.Materia_Prima)
                    {
                        var detalleFormulaAgregar = new DetalleFormulaResult();
                        AccesoDatos.Contexto.Produccion.SP.JOIN_insumo_proveedores_Result insumo = db.JOIN_insumo_proveedores(detalle.Codigo_Insumo, true).FirstOrDefault();

                        detalleFormulaAgregar.Codigo_Formula = formula.Codigo_Formula;
                        detalleFormulaAgregar.Kilos_Teoricos = formula.Kilos_Teoricos;
                        detalleFormulaAgregar.Litros_Tina = formula.Litros_Tina;
                        detalleFormulaAgregar.Kilos_Masa = formula.Kilos_Masa;
                        detalleFormulaAgregar.Litros_Crema = formula.Litros_Crema;
                        detalleFormulaAgregar.Litros_Suero = formula.Litros_Suero;
                        detalleFormulaAgregar.Codigo_Insumo = detalle.Codigo_Insumo;
                        detalleFormulaAgregar.Descripcion = insumo.Descripcion;
                        detalleFormulaAgregar.Precio_Lista_Insumo = insumo.Precio_Lista;
                        detalleFormulaAgregar.Precio_Por = insumo.Precio_Por;
                        detalleFormulaAgregar.Cantidad = detalle.Cantidad;
                        detalleFormulaAgregar.Total = detalle.Cantidad * (decimal)insumo.Precio_Unitario;
                        detalleFormulaAgregar.Precio_Unitario = (decimal)insumo.Precio_Unitario;
                        detalleFormulaAgregar.Marca = detalle.Marca;
                        detalleFormulaAgregar.Factor = formula.Factor;
                        detalleFormulaAgregar.IVA = (float)(insumo.IVA ?? 0);

                        lest.Add(detalleFormulaAgregar);
                    }
                }
            }

            return Json(new { data = lest, success = true }, JsonRequestBehavior.AllowGet);
        }

        //OBTIENE TODOS LOS INSUMOS DE UNA FÓRMULA
        public ActionResult GetInsumos(int Codigo_Formula = 0)
        {
            if (Codigo_Formula == 0)
            {
                List<Insumos> list = new List<Insumos>();

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                Formulas formula = this.FormulasServicio.Obtener((f) => f.Codigo_Formula == Codigo_Formula).FirstOrDefault();
                var insumos = this.InsumosFormulasRecuperadoServicio.Obtener((ifr) => ifr.Codigo_Formula == formula.Codigo_Formula
                                                                                    && ifr.Insumos.Materia_Prima)
                                                                .Select(ifr => new
                                                                {
                                                                    Codigo_Interno_Insumos = ifr.Insumos.Codigo_Interno_Insumos,
                                                                    Codigo_Insumo = ifr.Insumos.Codigo_Insumo,
                                                                    Descripcion = ifr.Insumos.Descripcion,
                                                                    Costo = ifr.Insumos.Precio_Ultima_Compra,
                                                                    CantidadRecuperada = ifr.CantidadRecuperada,
                                                                    Moneda = ifr.Insumos.Moneda,
                                                                    Codigo_Iva = ifr.Insumos.Codigo_Iva,
                                                                    Materia_Prima = ifr.Insumos.Materia_Prima
                                                                });
                List<object> dataInsumos = new List<object>();

                foreach (var insumo in insumos)
                {
                    var moneda = this.MonedasServicio.Obtener((m) => m.nombre == insumo.Moneda).FirstOrDefault();
                    decimal cambio = 1;

                    if (moneda != null)
                        cambio = (decimal)moneda.intercambio;

                    decimal precio = insumo.Costo.HasValue ? insumo.Costo.Value * cambio : 0;

                    var iva = this.ImpuestosServicio.Obtener((impuesto) => impuesto.Numero == insumo.Codigo_Iva).FirstOrDefault();
                    float impuestoValor = iva != null ? iva.Porcentaje_IVA_Inscripto.HasValue ? (float)iva.Porcentaje_IVA_Inscripto.Value : 0 : 0;

                    if (this.Configuraciones.MostrarIVAFormulas)
                        precio = precio + ((precio * ((decimal)impuestoValor)) / 100);

                    var precioUnitario = new Nullable<decimal>(precio);

                    if (insumo.Materia_Prima)
                    {
                        dataInsumos.Add(new
                        {
                            Codigo_Interno_Insumos = insumo.Codigo_Interno_Insumos,
                            Codigo_Insumo = insumo.Codigo_Insumo,
                            Descripcion = insumo.Descripcion,
                            Costo = precioUnitario,
                            CantidadRecuperada = insumo.CantidadRecuperada,
                            Total = insumo.CantidadRecuperada * precioUnitario
                        });
                    }
                }

                return Json(new { data = dataInsumos, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, msg = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //OBTIENE TODOS LOS COSTOS INDIRECTOS DE UNA FÓRMULA
        public ActionResult GetCostosIndirectos(int Codigo_Formula = 0)
        {
            if (Codigo_Formula == 0)
            {
                List<Costos_Indirectos> list = new List<Costos_Indirectos>();

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var listado = this.CostosIndirectosFormulaServicio.Obtener((c) => c.Codigo_Formula == Codigo_Formula).Select(c => new
                {
                    Codigo_Costo_Indirecto = c.Codigo_Costo_Indirecto,
                    Codigo_Formula = c.Codigo_Formula,
                    Descripcion = c.Costos_Indirectos.Descripcion,
                    Precio = c.Costos_Indirectos.Precio * (decimal)(c.Costos_Indirectos.Monedas != null ? c.Costos_Indirectos.Monedas.intercambio : 1),
                    Cantidad = c.Cantidad,
                    Precio_Fijo = c.Costos_Indirectos.Precio_Fijo,
                    Porcentaje = c.Costos_Indirectos.Porcentaje,
                    Marca = c.Marca,
                    Fijo = c.Precio_Fijo == true ? c.Costos_Indirectos.Precio : null,
                    Total = c.Costos_Indirectos.Precio_Fijo ?? (c.Cantidad * c.Costos_Indirectos.Precio * (decimal)(c.Costos_Indirectos.Monedas != null ? c.Costos_Indirectos.Monedas.intercambio : 1))
                }).ToList();

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ObtenerNumeroProducionesAsociadas(int Codigo_Formula = 0)
        {
            try
            {
                var listado = this.FormulasServicio.Obtener((f) => f.Codigo_Formula == Codigo_Formula).Select(f => new
                {
                    Producciones = f.Produccion.Count
                });

                return Json(new { Numero_Producciones = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            ViewBag.MostrarIVAFormulas = this.Configuraciones.MostrarIVAFormulas;
            ViewBag.Productos = this.ProductoProduccionServicio.Obtener();

            if (id == 0)
                return View(new Formulas());
            else
            {
                var formula = this.FormulasServicio.Obtener((x) => x.Codigo_Formula == id).FirstOrDefault();

                if (formula != null)
                {
                    var materias = formula.Insumos.Where(insumo => insumo.Materia_Prima);
                    var filtrado = new List<Insumos>();

                    foreach (var materia in materias)
                    {
                        filtrado.Add(materia);
                    }

                    formula.Insumos = filtrado;

                }

                return View(formula);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(Formulas formula)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {

                AccesoDatos.Contexto.Produccion.Productos producto = this.ProductoProduccionServicio.Obtener((p) => p.Codigo_Producto == formula.Codigo_Producto).SingleOrDefault();

                if (producto == null)
                    return Json(new { success = false, message = "No se puede registrar la fórmula sin un Producto existente." }, JsonRequestBehavior.AllowGet);

                if (producto.Que_Usa.ToLower() == "masa")
                    foreach (var costoIndirecto in formula.Costos_Indirectos_Formula)
                    {
                        if (formula.Kilos_Masa != costoIndirecto.Cantidad)
                            costoIndirecto.Cantidad = Convert.ToInt32(formula.Kilos_Masa);
                    }
            
                if (formula.Codigo_Formula == 0)
                {
                    formula.Fecha_Alta = DateTime.Now;

                    if (this.FormulasServicio.Obtener((f) => f.Codigo_Producto == formula.Codigo_Producto).FirstOrDefault() != null)
                        return Json(new { success = false, message = "Error, ya existe un registro con ese Código." }, JsonRequestBehavior.AllowGet);
             
                    foreach (var item in formula.Insumos.ToList())
                    {
                        formula.Insumos.Remove(item);
                    }
                    this.FormulasServicio.Agregar(formula);
                    scope.Complete();
                    return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                
                    /*BORRA TODOS LOS DETALLES ASOCIADOS*/
                    foreach (var detalle in this.DetalleFormulaServicio.Obtener((df) => df.Codigo_Formula == formula.Codigo_Formula))
                    {
                        this.DetalleFormulaServicio.Borrar(detalle);
                    }
                    foreach (var detalle in formula.Detalle_Formula)
                    {
                        //Produccion_Ricota objRicota;
                        //objRicota = new Produccion_Ricota();

                        detalle.Codigo_Formula = formula.Codigo_Formula;
                        //detalle.Formulas.Litros_Tina = (int)objRicota.Litros_Tina;
                        this.DetalleFormulaServicio.Agregar(detalle);
                    }

                    /*PRODUCTOS ASOCIADOS*/
                    foreach (var insumo in this.InsumosFormulasRecuperadoServicio.Obtener((f) => f.Codigo_Formula == formula.Codigo_Formula))
                    {
                        this.InsumosFormulasRecuperadoServicio.Borrar(insumo);
                    }

                    foreach (var insumo in formula.Insumos_Formulas_Recuperado)
                    {
                        insumo.Codigo_Formula = formula.Codigo_Formula;
                        this.InsumosFormulasRecuperadoServicio.Agregar(insumo);
                    }

                    /*COSTOS INDIRECTOS ASOCIADOS*/

                    foreach (var costo in this.CostosIndirectosFormulaServicio.Obtener((c) => c.Codigo_Formula == formula.Codigo_Formula).ToList())
                    {
                        this.CostosIndirectosFormulaServicio.Borrar(costo);
                    }
                    foreach (var costo in formula.Costos_Indirectos_Formula.ToList())
                    {
                        costo.Codigo_Formula = formula.Codigo_Formula;
                        this.CostosIndirectosFormulaServicio.Agregar(costo);
                    }

                    this.FormulasServicio.Editar(formula);
                    scope.Complete();
                    return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Formula)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (var ifr in this.InsumosFormulasRecuperadoServicio.Obtener((i) => i.Codigo_Formula == Codigo_Formula))
                    {
                        this.InsumosFormulasRecuperadoServicio.Borrar(ifr);
                    }
                    foreach (var item in this.CostosIndirectosFormulaServicio.Obtener((c) => c.Codigo_Formula == Codigo_Formula))
                    {
                        this.CostosIndirectosFormulaServicio.Borrar(item);
                    }
                    Formulas est = this.FormulasServicio.Obtener((x) => x.Codigo_Formula == Codigo_Formula).FirstOrDefault<Formulas>();
                    this.FormulasServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult Copiar(int Codigo_Formula)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var formula = this.FormulasServicio.Obtener((f) => f.Codigo_Formula == Codigo_Formula).SingleOrDefault();

                bool success = false;
                string message = "";

                if (formula != null)
                {

                    var nuevaFormula = CopiarFormula(formula);
                    this.FormulasServicio.Agregar(nuevaFormula);
                    success = true;

                    message = "Fórmula copiada correctamente.";

                }
                else
                {
                    message = "La Fórmula no existe";
                }
                scope.Complete();
                return Json(new { success = success, message = message }, JsonRequestBehavior.AllowGet);
            }

        }

        private Formulas CopiarFormula(Formulas formula)
        {
            int maxID = 1;
            if (this.FormulasServicio.Obtener().Count != 0)
                maxID = this.FormulasServicio.Obtener().Count + 1;

            var formulaNueva = new Formulas();

            formulaNueva.Historico = false;

            formulaNueva.Codigo_Formula = maxID + 1;
            formulaNueva.Fecha_Alta = DateTime.Now;
            formulaNueva.Codigo_Producto = formula.Codigo_Producto;
            formulaNueva.Comentario = "[COPIA DE FÓRMULA " + formula.Codigo_Formula.ToString() + "]  " + formula.Comentario;
            formulaNueva.Factor = formula.Factor;
            formulaNueva.Kilos_Teoricos = formula.Kilos_Teoricos;
            formulaNueva.Litros_Tina = formula.Litros_Tina;
            formulaNueva.Kilos_Masa = formula.Kilos_Masa;
            formulaNueva.Litros_Suero = formula.Litros_Suero;
            formulaNueva.Litros_Crema = formula.Litros_Crema;

            /*GUARDA TODOS LOS DETALLES ASOCIADOS*/
            foreach (var detalle in formula.Detalle_Formula.ToList())
            {
                if (detalle != null)
                {
                    var nuevoDetalle = new Detalle_Formula();

                    nuevoDetalle.Cantidad = detalle.Cantidad;
                    nuevoDetalle.Codigo_Formula = formulaNueva.Codigo_Formula;
                    nuevoDetalle.Codigo_Insumo = detalle.Codigo_Insumo;
                    nuevoDetalle.Codigo_Interno_Formula = formulaNueva.Codigo_Formula;
                    nuevoDetalle.Costo = detalle.Costo;
                    nuevoDetalle.Marca = detalle.Marca;
                    nuevoDetalle.Precio_Unitario = detalle.Precio_Unitario;
                    nuevoDetalle.Insumos = detalle.Insumos;

                    formulaNueva.Detalle_Formula.Add(nuevoDetalle);
                }
            }

            /*GUARDA TODOS LOS COSTOS INDIRECTOS ASOCIADOS EN COSTOS_INDIRECTOS_FORMULA*/
            foreach (var costo in formula.Costos_Indirectos_Formula.ToList())
            {
                if (costo != null)
                {

                    var nuevoCosto = new Costos_Indirectos_Formula();

                    nuevoCosto.Cantidad = costo.Cantidad;
                    nuevoCosto.Codigo_Formula = formulaNueva.Codigo_Formula;
                    nuevoCosto.Costos_Indirectos = costo.Costos_Indirectos;
                    nuevoCosto.Marca = costo.Marca;
                    nuevoCosto.Porcentaje = costo.Porcentaje;
                    nuevoCosto.Precio = costo.Precio;
                    nuevoCosto.Precio_Fijo = costo.Precio_Fijo;

                    formulaNueva.Costos_Indirectos_Formula.Add(nuevoCosto);

                }

            }

            foreach (var insumo in formula.Insumos_Formulas_Recuperado.ToList())
            {
                var nuevoInsumo = new Insumos_Formulas_Recuperado
                {
                    CantidadRecuperada = insumo.CantidadRecuperada,
                    Codigo_Formula = formulaNueva.Codigo_Formula,
                    Codigo_Insumo = insumo.Codigo_Insumo
                };
                formulaNueva.Insumos_Formulas_Recuperado.Add(nuevoInsumo);
            }

            formula.Historico = true;

            return formulaNueva;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
