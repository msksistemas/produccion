﻿using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace MVC_Produccion.Controllers
{
    public class AcidificacionesController : Controller
    {
        private IBaseServicio<Acidificacion> AcidificacionServicio;
        public AcidificacionesController(IBaseServicio<Acidificacion> acidificacionServicio)
        {
            this.AcidificacionServicio = acidificacionServicio;
        }

        public ActionResult Index()
        {
            return View(AcidificacionServicio.Obtener());
        }

        public ActionResult GetData(string Lote = "")
        {
            var acidificacion = this.AcidificacionServicio.Obtener((a) => a.Lote == Lote).Select(ac => new
            {
                ac.Codigo_Interno_Acidificacion,
                ac.Lote,
                Fecha = ac.Fecha.ToString(),
                ac.Valor_Ph,
                ac.Valor_Acidez,
                Hora = ac.Hora.ToString()
            });

            if (acidificacion.Count() < 0)
                return Json(new { message = "Error, no existen registro con el Lote ingresado.", success = false }, JsonRequestBehavior.AllowGet);

            return Json(new { data = acidificacion, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = AcidificacionServicio.ObtenerConRawSql("SELECT top 1 Codigo_Interno_Acidificacion, " +
                                                                                    "Lote, " +
                                                                                    "Fecha, " +
                                                                                    "Valor_Ph, " +
                                                                                    "Valor_Acidez, " +
                                                                                    "Hora " +
                                                                            "FROM Acidificacion " +
                                                                            "ORDER BY Codigo_Interno_Acidificacion DESC").FirstOrDefault();

                return Json(new { data = ultimoRegistro.Codigo_Interno_Acidificacion, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Acidificacion = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            { 
                try
                    {
                    var acidificacion = this.AcidificacionServicio.Obtener((ac) => ac.Codigo_Interno_Acidificacion == Codigo_Acidificacion).SingleOrDefault();

                    if (acidificacion != null)
                        this.AcidificacionServicio.Borrar(acidificacion);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    scope.Complete();
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }

        }
  }
}