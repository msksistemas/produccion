﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class ProductosController : Controller
    {
        private dbProdSP db = new dbProdSP();
        private IBaseServicio<Productos> ProductosServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Familias> FamiliasServicio;
        private IBaseServicio<Formulas> FormulasServicio;

        private OpcionesTrabajo Configuraciones;

        public ProductosController(IBaseServicio<Productos> productosServicios,
                                    IBaseServicio<Permisos> permisosServicios,
                                    IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicios,
                                    IBaseServicio<Familias> familiasServicio,
                                    IBaseServicio<Formulas> formulasServicio)
        {
            this.ProductosServicio = productosServicios;
            this.PermisosServicio = permisosServicios;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicios;
            this.FamiliasServicio = familiasServicio;
            this.FormulasServicio = formulasServicio;

            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        // GET: Productos
        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            else if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";

            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="Activo", Value="true"},
                                new SelectListItem() {Text="Inactivo", Value="false"}
                            };
            ViewBag.ListadoEstados = ListadoEstados;

            var productos = this.ProductosServicio.Obtener();

            return View(productos);
        }

        public ActionResult GetData(bool estado = true, string familia = "")
        {
            var listado = db.JOIN_productos_productosVentas("", estado, familia);
            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProducto(string Codigo_Producto)
        {
            if (Codigo_Producto != "")
            {
                try
                {
                    var productoNuevo = db.JOIN_productos_productosVentas(Codigo_Producto, true, "").ToList();

                    return Json(new { data = productoNuevo, success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                ContextoProduccion.Productos productoNuevo = new ContextoProduccion.Productos();
                string max = "1";
                if (this.ProductosServicio.Obtener().Count != 0)
                    max = this.ProductosServicio.Obtener().Max(p => p.Codigo_Producto);

                productoNuevo.Codigo_Producto = max;

                productoNuevo.Codigo_Producto = (int.Parse(productoNuevo.Codigo_Producto) + 1).ToString(new string('0', this.Configuraciones.NumeroDigitos));
                return Json(new { data = productoNuevo, success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetOne(string Codigo_Producto)
        {
            if (Codigo_Producto != "")
            {
                try
                {
                    var productoNuevo = db.JOIN_productos_productosVentas(Codigo_Producto, true, "").ToList();

                    return Json(new { data = productoNuevo, success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, message = "Error, no existe registro con Código negativo." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult modalAddOrEdit(string id = "", int ProductoInsumo = 0)
        {
            List<SelectListItem> ListadoUsa = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="Crema", Value="Crema"},
                                    new SelectListItem() {Text="Masa", Value="Masa"},
                                    new SelectListItem() {Text="Leche", Value="Leche"},
                                    new SelectListItem() {Text="Suero", Value="Suero"}
                                };

            ViewBag.ListadoUsa = ListadoUsa;

            List<SelectListItem> ListadoFamilia = new List<SelectListItem>();
            ListadoFamilia.Add(new SelectListItem()
            {
                Text = "Seleccionar",
                Value = ""
            });
            foreach (var item in this.FamiliasServicio.Obtener())
            {
                ListadoFamilia.Add(new SelectListItem()
                {
                    Text = item.Nombre,
                    Value = item.Codigo_Familia.ToString()
                });
            }
            ViewBag.ListadoFamilia = ListadoFamilia;


            ViewBag.ProductoInsumo = ProductoInsumo;

            if (id == "")
            {
                string ultimoRegistro = "";

                ContextoProduccion.Productos productoNuevo = new ContextoProduccion.Productos();
                var productos = this.ProductosServicio.Obtener();
                if (productos.Count() > 0)
                {
                    ultimoRegistro = productos.Max(p => p.Codigo_Producto);
                    productoNuevo.Codigo_Producto = (int.Parse(ultimoRegistro) + 1).ToString(new string('0', this.Configuraciones.NumeroDigitos));
                }
                else
                {
                    productoNuevo.Codigo_Producto = (1).ToString(new string('0', this.Configuraciones.NumeroDigitos));
                }

                return View(productoNuevo);
            }
            else
            {
                var productoNuevo = this.ProductosServicio.Obtener((p) => p.Codigo_Producto == id).SingleOrDefault();

                return View(productoNuevo);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(string id = "", int modal = 0)
        {
            List<SelectListItem> ListadoUsa = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="Crema", Value="Crema"},
                                    new SelectListItem() {Text="Masa", Value="Masa"},
                                    new SelectListItem() {Text="Leche", Value="Leche"},
                                    new SelectListItem() {Text="Suero", Value="Suero"}
                                };

            ViewBag.ListadoUsa = ListadoUsa;

            List<SelectListItem> ListadoFamilia = new List<SelectListItem>();
            ListadoFamilia.Add(new SelectListItem()
            {
                Text = "Seleccionar",
                Value = ""
            });
            foreach (var item in this.FamiliasServicio.Obtener())
            {
                ListadoFamilia.Add(new SelectListItem()
                {
                    Text = item.Nombre,
                    Value = item.Codigo_Familia.ToString()
                });
            }
            ViewBag.ListadoFamilia = ListadoFamilia;


            if (id == "")
            {

                string ultimoRegistro = "";

                Productos productoNuevo = new Productos();
                var productos = this.ProductosServicio.Obtener();
                if (productos.Count() > 0)
                {
                    ultimoRegistro = productos.Max(p => p.Codigo_Producto);
                    productoNuevo.Codigo_Producto = (int.Parse(ultimoRegistro) + 1).ToString(new string('0', this.Configuraciones.NumeroDigitos));
                }
                else
                {
                    productoNuevo.Codigo_Producto = (1).ToString(new string('0', this.Configuraciones.NumeroDigitos));
                }

                return View(productoNuevo);
            }
            else
            {
                var producto = this.ProductosServicio.Obtener((x) => x.Codigo_Producto == id).SingleOrDefault();

                return View(producto);
            }
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = "1";
                var productos = this.ProductosServicio.Obtener();
                if (productos.Count() > 0)
                    ultimoRegistro = productos.Max(p => p.Codigo_Producto);

                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        private string ObtenerQueUsa(Familias familia)
        {
            string queUsa = string.Empty;
            if (familia.Nombre.ToLower() == "quesos")
                queUsa = "Leche";

            if (familia.Nombre.ToLower() == "leche en polvo")
                queUsa = "Leche en Polvo";

            if (familia.Nombre.ToLower() == "queso fundido")
                queUsa = "quesos reproceso";

            if (familia.Nombre.ToLower() == "dulce de leche")
                queUsa = "Leche";

            if (familia.Nombre.ToLower() == "dulce de leche reprocesado")
                queUsa = "Dulce de leche";

            if (familia.Nombre.ToLower() == "crema")
                queUsa = "Crema";

            if (familia.Nombre.ToLower() == "ricota")
                queUsa = "Suero";

            if (familia.Nombre.ToLower() == "muzzarella")
                queUsa = "Masa";

            if (familia.Nombre.ToLower() == "yogurt")
                queUsa = "Leche";

            //if (familia.Nombre.ToLower() == "leche fluida")
            //    queUsa = "Leche";

            //if (familia.Nombre.ToLower() == "manteca")
            //    queUsa = "Leche";

            return queUsa;
        }

        [HttpPost]
        public ActionResult AddOrEdit(Productos producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var familia = this.FamiliasServicio.Obtener((f) => f.Codigo_Familia == producto.Codigo_Familia).SingleOrDefault();
                    producto.Activo = true;
                    producto.Que_Usa = ObtenerQueUsa(familia);

                    if (producto.Codigo_Interno_Producto == 0)
                    {
                        Productos prod = this.ProductosServicio.Obtener((p) => p.Codigo_Producto == producto.Codigo_Producto).FirstOrDefault();
                        if (prod != null)
                            return Json(new { success = false, message = $"El producto {prod.Descripcion.Trim()} está registrado con el código {prod.Codigo_Producto}" }, JsonRequestBehavior.AllowGet);
                        this.ProductosServicio.Agregar(producto);
                        scope.Complete();
                        return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        this.ProductosServicio.Editar(producto);
                        scope.Complete();
                        return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);
                    }

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpPost]
        public ActionResult Delete(Productos producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    this.ProductosServicio.Borrar(producto);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult CambiarEstado(string codigo_producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var producto = this.ProductosServicio.Obtener((p) => p.Codigo_Producto == codigo_producto).SingleOrDefault();
                producto.Activo = producto.Activo.Value ? false : true;
                this.ProductosServicio.Editar(producto);

                scope.Complete();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
