﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Globalization;
using MVC_Produccion.Sesiones;
using MVC_Produccion.Utilidades;
using System.Collections.Generic;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using Servicios.Interfaces;
using Business.Services.DetalleProduccion;
using Business.Services;
using System.Net.Http;
using System.Threading.Tasks;
using MVC_Produccion.Models;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class InfoRetencionEnvasado
    {
        public int CodigoDetalle { get; set; }
        public string MotivoRetencion { get; set; }
        public decimal Kilos { get; set; }
        public decimal Cantidad { get; set; }
        public int IdUsuario { get; set; }
        public DateTime Fecha { get; set; }
        public string ComentarioRetencion { get; set; }
        public string InsumoReproceso { get; set; }
        public string LoteInsumoReproceso { get; set; }
    }

    public class EnvasadosController : Controller
    {
        private IBaseServicio<Envasado> EnvasadoServicio;
        private IBaseServicio<Detalle_Envasado> DetalleEnvasadoServicio;
        private IBaseServicio<Detalle_Expedicion> DetalleExpedicionServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<Produccion_Quesos> ProduccionQuesosServicio;
        private IBaseServicio<Produccion_Quesos_Fundido> ProduccionQuesosFundidoServicio;
        private IBaseServicio<Produccion_Crema> ProduccionCremaServicio;
        private IBaseServicio<Produccion_Dulce_de_Leche> ProduccionDulceDeLecheServicio;
        private IBaseServicio<Produccion_Muzzarela> ProduccionMuzzarelaServicio;
        private IBaseServicio<Produccion_Ricota> ProduccionRicotaServicio;
        private IBaseServicio<AccesoDatos.Contexto.Produccion.Produccion> ProduccionServicio;
        private IBaseServicio<Ajustes_Insumos> AjusteInsumosServicio;
        private IBaseServicio<Formulas_Envasado> FormulasEnvasadoServicio;
        private IBaseServicio<Motivos_Retencion> MotivosRetencionServicio;
        private IBaseServicio<Restantes_Envasado> RestanteEnvasadoServicio;
        private IBaseServicio<Detalle_Ajuste_Insumos> DetalleAjusteInsumoServicio;
        private IBaseServicio<Insumos> InsumosServicio;
        private IBaseServicio<Detalle_Insumo_Envasado> DetalleInsumoEnvasadoServicio;
        private IBaseServicio<Mov_Stock> MovStockServicio;
        private IBaseServicio<Mov_Stock_Prod_Ventas> MovStockProdVentasServicio;
        private IProduccionServicio ProduccionServicioExtra;
        private OpcionesTrabajo Configuraciones;


        private ProduccionService pService = new ProduccionService();
        private ProduccionCremaService pcService = new ProduccionCremaService();
        private ProduccionQuesoService pqService = new ProduccionQuesoService();
        private ProduccionDulceLecheService pdlService = new ProduccionDulceLecheService();
        private ProduccionLecheService plService = new ProduccionLecheService();
        private ProduccionLechePolvoService plpService = new ProduccionLechePolvoService();
        private ProduccionMuzzarellaService pmService = new ProduccionMuzzarellaService();
        private ProduccionQuesoFundidoService pqfService = new ProduccionQuesoFundidoService();
        private ProduccionRicotaService prService = new ProduccionRicotaService();
        private ProduccionYogurService pyService = new ProduccionYogurService();

        private readonly HttpService httpClient;

        public EnvasadosController(IBaseServicio<Envasado> envasadoServicio,
                                    IBaseServicio<Produccion_Quesos_Fundido> produccionQuesoFundido,
                                    IBaseServicio<Produccion_Ricota> produccionRicotaServicio,
                                    IBaseServicio<Produccion_Muzzarela> produccionMuzzarelaServicio,
                                    IBaseServicio<Produccion_Crema> produccionCremaServicio,
                                    IBaseServicio<Produccion_Dulce_de_Leche> produccionDulceLecheServicio,
                                    IProduccionServicio produccionServicioExtra,
                                    IBaseServicio<Detalle_Ajuste_Insumos> detalleAjusteInsumoServicio,
                                    IBaseServicio<Restantes_Envasado> restanteEnvasadoServicio,
                                   IBaseServicio<Motivos_Retencion> motivosRetencionServicio,
                                   IBaseServicio<Formulas_Envasado> formulasEnvasadoServicio,
                                   IBaseServicio<Detalle_Envasado> detalleEnvasadoServicio,
                                   IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                   IBaseServicio<Permisos> permisosServicio,
                                   IBaseServicio<Detalle_Expedicion> detalleExpedicion,
                                   IBaseServicio<Produccion_Quesos> produccionQuesosServicios,
                                   IBaseServicio<Produccion> produccionServicio,
                                   IBaseServicio<Ajustes_Insumos> ajusteInsumoServicio,
                                   IBaseServicio<Insumos> insumosServicio,
                                   IBaseServicio<Detalle_Insumo_Envasado> detalleInsumoEnvasadoServicio,
                                   IBaseServicio<Mov_Stock> movStockServicio,
                                   IBaseServicio<Mov_Stock_Prod_Ventas> movStockProdVentasServicio,
                                   HttpService httpClient)
        {
            this.ProduccionQuesosFundidoServicio = produccionQuesoFundido;
            this.ProduccionCremaServicio = produccionCremaServicio;
            this.ProduccionDulceDeLecheServicio = produccionDulceLecheServicio;
            this.ProduccionMuzzarelaServicio = produccionMuzzarelaServicio;
            this.ProduccionRicotaServicio = produccionRicotaServicio;
            this.DetalleAjusteInsumoServicio = detalleAjusteInsumoServicio;
            this.RestanteEnvasadoServicio = restanteEnvasadoServicio;
            this.ProduccionServicioExtra = produccionServicioExtra;
            this.MotivosRetencionServicio = motivosRetencionServicio;
            this.FormulasEnvasadoServicio = formulasEnvasadoServicio;
            this.AjusteInsumosServicio = ajusteInsumoServicio;
            this.EnvasadoServicio = envasadoServicio;
            this.DetalleEnvasadoServicio = detalleEnvasadoServicio;
            this.ProduccionQuesosServicio = produccionQuesosServicios;
            this.ProduccionServicio = produccionServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.DetalleExpedicionServicio = detalleExpedicion;
            this.InsumosServicio = insumosServicio;
            this.DetalleInsumoEnvasadoServicio = detalleInsumoEnvasadoServicio;
            this.MovStockServicio = movStockServicio;
            this.MovStockProdVentasServicio = movStockProdVentasServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
            this.httpClient = httpClient;
        }
        private dbProdSP db = new dbProdSP();

        enum ProcesoEnvasado
        {
            Decomiso,
            Reproceso,
            Retencion
        }

        public ActionResult Index()
        {
            bool liberacion = false;
            ViewBag.Liberacion_Condicional = liberacion;
            ViewBag.configuraciones = this.Configuraciones;

            return View();
        }

        public ActionResult Liberados()
        {
            ViewBag.Liberacion_Condicional = true;
            ViewBag.configuraciones = this.Configuraciones;
            return View();
        }

        [HttpGet]
        public ActionResult LiberarPorCalidad()
        {
            ViewBag.Liberacion_Condicional = true;

            return View();
        }

        [HttpGet]
        public ActionResult Liberados_Condicional()
        {
            ViewBag.Liberacion_Condicional = true;

            return View();
        }

        [HttpGet]
        public ActionResult GetData(bool Historico = false, bool Liberado_Condicional = false)
        {
            var listado = db.GetDetallesEnvasado(Sesion.UsuarioActual.Fabrica == null ? -1 : Sesion.UsuarioActual.Fabrica, Historico, Liberado_Condicional);
            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetData(string id, string Humedad)
        {
            string humedadComa = Humedad.Replace('.', ',');

            var Codigo_Fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : -1;

            CampoEditable<decimal> campo = new CampoEditable<decimal>(id, humedadComa);

            var exp = this.DetalleEnvasadoServicio.Obtener((detalle) => detalle.Codigo_Interno_Detalle == campo.id).FirstOrDefault();
            var env = this.EnvasadoServicio.Obtener((envasado) => envasado.Codigo_Interno_Envasado == exp.Codigo_Interno_Envasado).FirstOrDefault();

            TipoProduccion tipoProd = pService.Get_TipoProduccion(env.Lote);

            if (env != null)
            {
                try
                {
                    // Guarda la humedad en la respectiva tabla
                    if (tipoProd == TipoProduccion.Queso)
                        pqService.Update_Humedad(env.Lote, campo.valor);
                    else if (tipoProd == TipoProduccion.Crema)
                        pcService.Update_Humedad(env.Lote, campo.valor);
                    else if (tipoProd == TipoProduccion.DulceLeche)
                        pdlService.Update_Humedad(env.Lote, campo.valor);
                    else if (tipoProd == TipoProduccion.Leche)
                        plService.Update_Humedad(env.Lote, campo.valor);
                    else if (tipoProd == TipoProduccion.LechePolvo)
                        plpService.Update_Humedad(env.Lote, campo.valor);
                    else if (tipoProd == TipoProduccion.Muzzarella)
                        pmService.Update_Humedad(env.Lote, campo.valor);
                    else if (tipoProd == TipoProduccion.QuesoFundido)
                        pqfService.Update_Humedad(env.Lote, campo.valor);
                    else if (tipoProd == TipoProduccion.Ricota)
                        prService.Update_Humedad(env.Lote, campo.valor);
                    else if (tipoProd == TipoProduccion.Yogur)
                        pyService.Update_Humedad(env.Lote, campo.valor);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                return Json(new { message = "UPDATED" });
            }
            else
            {
                return Json(new { message = "FAIL" });
            }
        }

        [HttpPost]
        public ActionResult DetalleEnvasadoLote(string id, string Lote)
        {

            string lote = Lote.Trim();

            var Codigo_Fabrica = Sesion.UsuarioActual.Fabrica != null ? Sesion.UsuarioActual.Fabrica.Value : -1;

            CampoEditable<string> campo = new CampoEditable<string>(id, lote);

            var exp = this.DetalleEnvasadoServicio.Obtener((detalle) => detalle.Codigo_Interno_Detalle == campo.id).FirstOrDefault();

            if (exp != null)
            {
                try
                {
                    exp.Lote = campo.valor;
                    this.DetalleEnvasadoServicio.Editar(exp);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                };

                return Json(new { message = "UPDATED" });
            }
            else
            {
                return Json(new { message = "FAIL" });
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int Codigo_Interno_Envasado = 0, string Lote = "", string SiguienteUrl = "")
        {
            ViewBag.Envasa_Sin_Kilos = this.Configuraciones.Envasa_Sin_Kilos ? 1 : 0;
            ViewBag.Fabrica_Deposito = this.Configuraciones.Fabrica_Deposito == "Fábrica" ? "Fabrica" : "Fabrica/Deposito";
            ViewBag.siguiente_url = SiguienteUrl;
            ViewBag.Kg_Desnudo = this.Configuraciones.Kg_Desnudo ? 1 : 0;
            ViewBag.TipoProd = this.ProduccionServicioExtra.ObtenerTipoProduccion(Lote);
            var prod = this.ProduccionServicio.Obtener((p) => p.Lote == Lote).SingleOrDefault();
            ViewBag.CodProducto = prod.Formulas.Codigo_Producto;
            ViewBag.Fabrica = prod.Codigo_Fabrica;
            if (Codigo_Interno_Envasado == 0)
            {
                Envasado envasado = new Envasado();
                envasado.Lote = Lote;
                envasado.Fecha = DateTime.Now;
                return View(envasado);
            }
            else
            {
                Envasado envasadoAEditar = new Envasado();
                envasadoAEditar = this.EnvasadoServicio.Obtener((envasado) => envasado.Codigo_Interno_Envasado == Codigo_Interno_Envasado).SingleOrDefault();
                return View(envasadoAEditar);
            }
        }
        [HttpGet]
        public ActionResult EditarEnvasado(int Codigo_Interno_Detalle = 0, string Lote = "")
        {
            if (Codigo_Interno_Detalle == 0)
            {
                Envasado envasado = new Envasado();
                envasado.Lote = Lote;
                envasado.Fecha = DateTime.Now;

                return View(envasado);
            }
            else
            {
                Detalle_Envasado detalle = new Detalle_Envasado();
                detalle = this.DetalleEnvasadoServicio.Obtener((de) => de.Codigo_Interno_Detalle == Codigo_Interno_Detalle).FirstOrDefault();
                int Codigo_Interno_Envasado = 0;
                Codigo_Interno_Envasado = detalle.Codigo_Interno_Envasado;
                Envasado envasadoAEditar = new Envasado();
                envasadoAEditar = this.EnvasadoServicio.Obtener((envasado) => envasado.Codigo_Interno_Envasado == Codigo_Interno_Envasado).FirstOrDefault();

                return View("AddOrEdit", envasadoAEditar);
            }
        }
        [HttpPost]
        public ActionResult AddOrEdit(Envasado envasado, decimal Kilos_Reales_Envasado = 0)
        {
            ViewBag.Envasa_Sin_Kilos = this.Configuraciones.Envasa_Sin_Kilos ? 1 : 0;

            try
            {
                //Envasado envasadoActual = new Envasado();
                //envasadoActual = db.Envasado.Where(e => e.Codigo_Interno_Envasado == envasado.Codigo_Interno_Envasado).FirstOrDefault();
                //envasadoActual.Humedad = envasado.Humedad;
                //envasadoActual.Fecha = envasado.Fecha;

                foreach (var detalle in envasado.Detalle_Envasado)
                {
                    detalle.Lote = envasado.Lote;
                }
                this.EnvasadoServicio.Editar(envasado);
                return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        private void AgregarRestanteEnvasado(Envasado envasado, int codigoFabrica, List<Restantes_Envasado> restantes)
        {
            decimal kg_descarte = 0, hormas_descarte = 0;
            foreach (Restantes_Envasado restante in restantes)
            {
                restante.Lote = envasado.Lote;
                restante.Codigo_Interno_Envasado = envasado.Codigo_Interno_Envasado;
                kg_descarte += restante.Kilos;
                hormas_descarte += restante.Hormas;
                if (restante.Motivo == "Reproceso")
                {
                    Ajustes_Insumos ai = new Ajustes_Insumos();
                    ai.Comentario = "Reproceso envasado lote " + envasado.Lote;
                    ai.Es_Traspaso = false;
                    ai.Fabrica = codigoFabrica;
                    ai.Fabrica_Origen = 0;
                    ai.Fecha = envasado.Fecha;
                    ai.Historico = true;
                    ai.Numero_Comprobante = envasado.Codigo_Interno_Envasado.ToString();
                    ai.Tipo_Movimiento = "Ingreso";
                    Detalle_Ajuste_Insumos dai = new Detalle_Ajuste_Insumos();
                    dai.Cantidad = restante.Hormas;
                    dai.Kilos_Litros = restante.Kilos;
                    dai.Lote = envasado.Lote;
                    dai.Codigo_Insumo = restante.Codigo_Insumo;
                    ai.Detalle_Ajuste_Insumos.Add(dai);

                    this.AjusteInsumosServicio.Agregar(ai);
                }
                this.RestanteEnvasadoServicio.Agregar(restante);
            }

            envasado.Kg_Descarte = kg_descarte;
            envasado.Hormas_Descarte = hormas_descarte;
            this.EnvasadoServicio.Editar(envasado);
        }

        [HttpPost]
        public async Task<ActionResult> Envasar(Envasado envasado, List<Restantes_Envasado> restante, decimal? Kilos_Reales_Envasado = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var envasaSinKilosReales = this.Configuraciones.Envasa_Sin_Kilos;
                    var tipoProd = this.ProduccionServicioExtra.ObtenerTipoProduccion(envasado.Lote);
                    var cantidades = this.ProduccionServicioExtra.ObtenerCantidadesEnvasadas(envasado.Lote);
                    decimal kilosEnvasados = cantidades["kilos"];
                    decimal hormasEnvasadas = cantidades["piezas"];
                    bool salida_directa = false;

                    if (restante != null)
                    {
                        restante.RemoveAll(rest => rest.Hormas == 0);
                        foreach (Restantes_Envasado r in restante)
                        {
                            if (r.Kilos == 0 && envasaSinKilosReales)
                            {
                                return Json(new { success = false, message = "Se encontraron restantes con Unidades cargadas pero sin kilos, revise el detalle" }, JsonRequestBehavior.AllowGet);
                            }

                            kilosEnvasados += r.Kilos;
                            hormasEnvasadas += r.Hormas;
                        }
                    }

                    for (int i = envasado.Detalle_Envasado.Count - 1; i >= 0; i--)
                    {
                        Detalle_Envasado detalle = envasado.Detalle_Envasado.ElementAt(i);
                        if (detalle.Cantidad == 0)
                        {
                            envasado.Detalle_Envasado.Remove(detalle);
                        }
                        else
                        {
                            if (detalle.Kilos == 0 && envasaSinKilosReales)
                                return Json(new { success = false, message = "Se encontraron productos con Piezas cargadas pero sin kilos, revise el detalle" }, JsonRequestBehavior.AllowGet);

                            if (detalle.Hormas == 0)
                                envasado.Detalle_Envasado.ElementAt(i).Hormas = detalle.Cantidad / (detalle.Formulas_Envasado.Obtenidas_Por_Envasada == null
                                    || detalle.Formulas_Envasado.Obtenidas_Por_Envasada == 0
                                    ? 1 : detalle.Formulas_Envasado.Obtenidas_Por_Envasada);

                            envasado.Detalle_Envasado.ElementAt(i).Lote = envasado.Lote;
                            envasado.Detalle_Envasado.ElementAt(i).Historico = false;

                            kilosEnvasados += detalle.Kilos ?? 0;
                            hormasEnvasadas += detalle.Hormas ?? 0;
                        }
                    }

                    var Reales = this.ProduccionServicioExtra.ObtenerCantidadesReales(envasado.Lote);
                    var kilosReales = Reales["kilos"];
                    var hormasReales = Reales["piezas"];
                    if (kilosEnvasados > kilosReales && !envasaSinKilosReales)
                        return Json(new { success = false, message = "Se envasan " + (kilosEnvasados - kilosReales) + " kilos más de los reales." }, JsonRequestBehavior.AllowGet);

                    if (hormasEnvasadas > hormasReales && tipoProd != "dulceleche" && hormasReales != 0)
                        return Json(new { success = false, message = "Se envasan " + (hormasEnvasadas - hormasReales) + " unidades más de los reales." }, JsonRequestBehavior.AllowGet);

                    if (tipoProd != "dulceleche")
                    {
                        if (hormasEnvasadas == hormasReales)
                            if (kilosEnvasados == kilosReales || envasaSinKilosReales)
                                this.ProduccionServicioExtra.PasarProduccionHistorica(envasado.Lote);
                            else
                                return Json(new { success = false, message = "Debe informar todos los kilos." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (kilosEnvasados == kilosReales || envasaSinKilosReales)
                            this.ProduccionServicioExtra.PasarProduccionHistorica(envasado.Lote);
                    }

                    //if (httpClient.EsSanSatur())
                    //{
                    //    List<IMovimientoInsumo> detallesInsumos = new List<IMovimientoInsumo>();
                    //    foreach (Detalle_Envasado det in envasado.Detalle_Envasado)
                    //    {
                    //        foreach(Detalle_Insumo_Envasado detalleInsumo in DetalleInsumoEnvasadoServicio.Obtener((detIns) => detIns.Codigo_Interno_Detalle == det.Codigo_Interno_Detalle))
                    //        {
                    //            detallesInsumos.Add(new IMovimientoInsumo
                    //            {
                    //                Codigo_Insumo = detalleInsumo.Codigo_Insumo.Trim(),
                    //                Cantidad = (int)detalleInsumo.Cantidad,
                    //                Kilos_Litros = (float)detalleInsumo.Kilos
                    //            });
                    //        }
                    //    }

                    //    HttpResponseMessage responseInsumos = await httpClient.AjusteStockInsumos(
                    //                    detallesInsumos,
                    //                    DateTime.Now);
                    //    if (!responseInsumos.IsSuccessStatusCode)
                    //    {
                    //        return Json(new { success = false, message = responseInsumos.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                    //    }
                    //}
                    if (tipoProd == "queso")
                    {
                        var prodQueso = this.ProduccionQuesosServicio.Obtener((p) => p.Lote == envasado.Lote).SingleOrDefault();
                        if (prodQueso != null)
                            prodQueso.Kilos_Envasado = kilosEnvasados;
                    }
                    if (envasaSinKilosReales)
                    {
                        switch (tipoProd)
                        {
                            case "queso":
                                var prodQueso = this.ProduccionQuesosServicio.Obtener((p) => p.Lote == envasado.Lote).SingleOrDefault();
                                if (prodQueso != null)
                                {
                                    // La humedad pasa a guardarse en la tabla Detalle Produccion
                                    prodQueso.Humedad = envasado.Humedad;
                                    //prodQueso.Kilos_Reales = kilosEnvasados;
                                    //Si en la producción de queso NO se digita kilo real, éste debe tomarse del envasado y luego colocar el valor en el campo rendimiento de la producción de queso ya calculado (dividiéndolo por litros tina y luego multiplicando por 100)
                                    prodQueso.Rendimiento = (prodQueso.Kilos_Envasado / prodQueso.Litros_Tina) * 100;                                
                                    this.ProduccionQuesosServicio.Editar(prodQueso);
                                }
                                else
                                {
                                    var prodFundido = pqfService.Get_ByLote(envasado.Lote);

                                    // La humedad pasa a guardarse en la tabla Detalle Produccion
                                    prodFundido.Humedad = envasado.Humedad;
                                    prodFundido.Kilos_Reales = kilosEnvasados;

                                    pqfService.Save(prodFundido);
                                }
                                break;
                            case "ricota":
                                var prodRicota = prService.Get_ByLote(envasado.Lote);

                                prodRicota.Kilos_Reales = kilosEnvasados;
                                prodRicota.Humedad = envasado.Humedad;

                                prService.Save(prodRicota);

                                break;
                            case "crema":
                                var prodCrema = pcService.Get_ByLote(envasado.Lote);

                                prodCrema.Litros_Reales = kilosEnvasados;
                                prodCrema.Humedad = envasado.Humedad;

                                pcService.Save(prodCrema);

                                break;
                            case "muzzarella":
                                var prodMuzzarella = pmService.Get_ByLote(envasado.Lote);

                                prodMuzzarella.Kilos_Reales = kilosEnvasados;
                                prodMuzzarella.Humedad = envasado.Humedad;

                                pmService.Save(prodMuzzarella);

                                break;
                            case "dulceleche":
                                var prodDulceLeche = pdlService.Get_ByLote(envasado.Lote);

                                prodDulceLeche.Kilos_Reales = kilosEnvasados;
                                prodDulceLeche.Humedad = envasado.Humedad;

                                pdlService.Save(prodDulceLeche);

                                break;
                            default:
                                break;
                        }

                        // Previene que se guarde la humedad en Envasado
                        envasado.Humedad = null;
                    }
                    salida_directa = pService.Get_Producto(envasado.Lote).Salida_Directa ?? false; // if null then false;
                    envasado.Historico = false;
                    this.EnvasadoServicio.Agregar(envasado);
                    if (restante != null)
                    {
                        var fabrica = this.ProduccionServicioExtra.ObtenerFabrica(envasado.Lote);
                        this.AgregarRestanteEnvasado(envasado, fabrica, restante);
                    }

                    db.Movimiento_Stock_Envasado(envasado.Codigo_Interno_Envasado);
                    foreach (Detalle_Envasado detalle in envasado.Detalle_Envasado)
                    {
                        db.Movimiento_Stock_Venta_Envasado(detalle.Codigo_Interno_Detalle);
                    }

                    if (salida_directa)
                    {
                        List<int> listaCodigos = new List<int>();
                        listaCodigos.Add(envasado.Detalle_Envasado.FirstOrDefault().Codigo_Interno_Detalle);
                        await this.LiberarPorCalidad(listaCodigos, DateTime.Today.ToString());
                    }

                    scope.Complete();
                    return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = "Err:" + ex.ToString() + ", Inner: " + ex.InnerException.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public async Task<ActionResult> LiberarPorCalidad(List<int> codigos, string date = "")
        {
            ViewBag.Liberacion_Condicional = true;
            try
            {
                if (httpClient.EsSanSatur())
                {
                    List<MovimientoStockZulu> requestContent = new List<MovimientoStockZulu>();

                    DateTime fechaLiberacion = date != "" ? DateTime.Parse(date) : DateTime.Now;
                    List<Detalle_Envasado> detallesEnvasado = DetalleEnvasadoServicio.Obtener((det) => codigos.Contains(det.Codigo_Interno_Detalle)).ToList();

                    foreach (Detalle_Envasado det in detallesEnvasado)
                    {
                        DateTime fechaProduccion = this.ProduccionServicio.Obtener((p) => p.Lote == det.Envasado.Lote).FirstOrDefault().Fecha ?? DateTime.Now;
                        Formulas_Envasado formula = FormulasEnvasadoServicio.Obtener((fe) => fe.Codigo_Interno_Formula == det.Codigo_Interno_Formula).FirstOrDefault();
                        
                        MovimientoStockZulu movimiento = new MovimientoStockZulu(
                            httpClient.FechaComprobante(fechaLiberacion),
                            new List<DetalleMovimientoStock> {
                                new DetalleMovimientoStock(
                                    det.Formulas_Envasado.Numero_Producto_Ventas.Trim(),
                                    (float)(det.Kilos ?? 0),
                                    (int)(det.Cantidad ?? 0)
                                )
                        });

                        if ((formula.Es_Insumo ?? false) == true)
                        {
                            movimiento.tipoDescarte = 1;
                            movimiento.observaciones = $"Liberación por calidad de lote {det.Lote} para reproceso";
                        }
                        else
                        {
                            movimiento.tipoDescarte = 0;
                            movimiento.observaciones = $"Liberación por calidad de lote {det.Lote}";
                        }

                        requestContent.Add(movimiento);
                    }

                    HttpResponseMessage response = await httpClient.PostData(requestContent);
                    if (!response.IsSuccessStatusCode)
                    {
                        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                    }
                }
                using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    string fabrica_deposito = this.Configuraciones.Fabrica_Deposito.ToLower();

                    List<Detalle_Envasado> detallesEnvasado = this.DetalleEnvasadoServicio.Obtener(de => codigos.Contains(de.Codigo_Interno_Detalle)).ToList();
                    if (!detallesEnvasado.Any())
                        return Json(new { success = false, message = "No se encontraron registros para los códigos proporcionados." }, JsonRequestBehavior.AllowGet);

                    foreach (Detalle_Envasado detalle in detallesEnvasado)
                    {
                        if (detalle == null) continue;
                        DateTime fechaLiberado;
                        if (!DateTime.TryParse(date, CultureInfo.CurrentCulture, DateTimeStyles.None, out fechaLiberado))
                        {
                            fechaLiberado = DateTime.Now;
                        }

                        detalle.Fecha_Liberado = fechaLiberado;
                        detalle.Id_Usuario_Libera = Sesion.UsuarioActual.idUsuario;
                        detalle.Liberado_Condicional = false;
                        detalle.Historico = true;

                        DetalleEnvasadoServicio.Editar(detalle);

                        db.Movimiento_Stock_Venta_Envasado(detalle.Codigo_Interno_Detalle);
                        db.Liberado_Por_Calidad(detalle.Codigo_Interno_Detalle);
                    }

                    List<Envasado> envasados = EnvasadoServicio.Obtener(
                        e => e.Detalle_Envasado.Any(de => codigos.Contains(de.Codigo_Interno_Detalle))
                    ).ToList();

                    foreach (Envasado envasado in envasados)
                    {
                        if (!envasado.Detalle_Envasado.Any(de => !de.Historico))
                        {
                            envasado.Historico = true;
                            EnvasadoServicio.Editar(envasado);
                        }
                    }

                    scope.Complete();
                    return Json(new { success = true, message = "Registro/s liberado/s satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Decomisar(InfoRetencionEnvasado info)
        {
            try
            {
                Detalle_Envasado detalle;

                using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    detalle = ProcesarEnvasado(info, ProcesoEnvasado.Decomiso);
                    scope.Complete();
                }

                if (httpClient.EsSanSatur())
                {
                    List<MovimientoStockZulu> requestContent = new List<MovimientoStockZulu>
                    {
                        new MovimientoStockZulu(httpClient.FechaComprobante(info.Fecha), new List<DetalleMovimientoStock>
                        {
                            new DetalleMovimientoStock(
                                detalle.Formulas_Envasado.Numero_Producto_Ventas.Trim(),
                                (float)(detalle.Kilos ?? 0),
                                (int)(detalle.Cantidad ?? 0)
                            )
                        },
                        observaciones: $"Liberación para scrap de lote {detalle.Lote}",
                        tipoDescarte: 2)
                    };

                    HttpResponseMessage response = await httpClient.PostData(requestContent);
                    if (!response.IsSuccessStatusCode)
                    {
                        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new { success = true, message = "Registro decomisado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Reprocesar(InfoRetencionEnvasado info)
        {
            try
            {
                Detalle_Envasado detalle;
                Envasado env;

                using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    detalle = ProcesarEnvasado(info, ProcesoEnvasado.Reproceso);
                    env = detalle.Envasado;

                    if (env != null)
                    {
                        Produccion prod = this.ProduccionServicio.Obtener((x) => x.Lote == env.Lote).FirstOrDefault();

                        Ajustes_Insumos ai = new Ajustes_Insumos
                        {
                            Comentario = $"Envasado reprocesado - Lote {env.Lote}", // Cuidado con este comentario, se usa en el método Delete
                            Es_Traspaso = false,
                            Fabrica = prod.Codigo_Fabrica,
                            Fabrica_Origen = 0,
                            Fecha = info.Fecha,
                            Historico = true,
                            Numero_Comprobante = env.Codigo_Interno_Envasado.ToString(),
                            Tipo_Movimiento = "Ingreso",
                            Detalle_Ajuste_Insumos = new List<Detalle_Ajuste_Insumos>
                            {
                                new Detalle_Ajuste_Insumos
                                {
                                    Cantidad = info.Cantidad,
                                    Kilos_Litros = info.Kilos,
                                    Lote = info.LoteInsumoReproceso,
                                    Codigo_Insumo = info.InsumoReproceso
                                }
                            }
                        };

                        this.AjusteInsumosServicio.Agregar(ai);
                        scope.Complete();
                    }
                    else
                    {
                        return Json(new { success = false, message = "Hubo un problema con el reproceso de este envasado, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                    }
                }

                // Si es San Satur, ejecutar la solicitud HTTP después de la transacción
                if (httpClient.EsSanSatur())
                {
                    List<MovimientoStockZulu> requestContent = new List<MovimientoStockZulu>
                    {
                        new MovimientoStockZulu(httpClient.FechaComprobante(info.Fecha), new List<DetalleMovimientoStock>
                        {
                            new DetalleMovimientoStock(
                                detalle.Formulas_Envasado.Numero_Producto_Ventas.Trim(),
                                (float)(detalle.Kilos ?? 0),
                                (int)(detalle.Cantidad ?? 0)
                            )
                        },
                        observaciones: $"Liberación para reproceso de lote {env.Lote}",
                        tipoDescarte: 1)
                    };

                    HttpResponseMessage response = await httpClient.PostData(requestContent);
                    if (!response.IsSuccessStatusCode)
                    {
                        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new { success = true, message = "Registro reprocesado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Retener(InfoRetencionEnvasado info)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    ProcesarEnvasado(info, ProcesoEnvasado.Retencion);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro retenido satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Rehabilitar(int Codigo_Interno_Detalle)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Detalle_Envasado detalle = this.DetalleEnvasadoServicio.Obtener((de) => de.Codigo_Interno_Detalle == Codigo_Interno_Detalle).SingleOrDefault();
                    detalle.Codigo_Motivo_Retencion = null;
                    detalle.Fecha_Retenido = null;
                    detalle.Comentario_Retencion = null;

                    this.DetalleEnvasadoServicio.Editar(detalle);
                    Mov_Stock_Prod_Ventas movimiento_retencion = MovStockProdVentasServicio.Obtener((mspv) => mspv.Codigo_Interno_Detalle_Envasado == detalle.Codigo_Interno_Detalle
                                                                                                                   && mspv.Estado == "Envasado Retenido").FirstOrDefault();
                    if (movimiento_retencion != null)
                    {
                        MovStockProdVentasServicio.Borrar(movimiento_retencion);
                    }

                    scope.Complete();
                    return Json(new { success = true, message = "Registro rehabilitado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { success = false, message = "Error al rehabilitar el envasado." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Interno_Detalle = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Detalle_Envasado detalle = this.DetalleEnvasadoServicio.Obtener((de) => de.Codigo_Interno_Detalle == Codigo_Interno_Detalle).FirstOrDefault();
                    Envasado envasado = EnvasadoServicio.Obtener((e) => e.Codigo_Interno_Envasado == detalle.Codigo_Interno_Envasado).FirstOrDefault();
                    string lote = detalle.Envasado.Lote;
                    string productoVenta = detalle.Formulas_Envasado.Numero_Producto_Ventas;
                    decimal? Kilos_Envasados = 0;
                    if (detalle.Historico)
                    {
                        decimal? cantidad_expedida = this.DetalleExpedicionServicio.Obtener((x) => x.Lote.Equals(lote) && x.Numero_Producto_Ventas.Equals(productoVenta)).Sum(x => x.Cantidad);
                        decimal? cantidad_envasada = this.DetalleEnvasadoServicio.Obtener((x) => x.Formulas_Envasado.Numero_Producto_Ventas.Equals(productoVenta) && x.Envasado.Lote.Equals(lote) && x.Historico == true).Sum(x => x.Cantidad);

                        cantidad_expedida = cantidad_expedida == null ? 0 : cantidad_expedida;
                        cantidad_envasada = cantidad_envasada == null ? 0 : cantidad_envasada;

                        if (cantidad_envasada - detalle.Cantidad < cantidad_expedida)
                            return Json(new { success = false, message = "No puede eliminar el envasado porque ha sido expedido." }, JsonRequestBehavior.AllowGet);
                    }

                    List<Restantes_Envasado> restantes_envasado = RestanteEnvasadoServicio.Obtener((r) => r.Lote == lote).ToList();
                    foreach (Restantes_Envasado item in restantes_envasado)
                    {
                        if (item.Motivo == "Reproceso")
                        {
                            int Codigo_Interno_Ajuste = this.DetalleAjusteInsumoServicio.Obtener((da) => da.Lote == item.Lote && da.Cantidad == item.Hormas && item.Kilos == da.Kilos_Litros).FirstOrDefault().Codigo_Interno_Ajuste;
                            Ajustes_Insumos ajustePorRestante = this.AjusteInsumosServicio.Obtener((a) => a.Codigo_Interno_Ajuste == Codigo_Interno_Ajuste).SingleOrDefault();
                            AjusteInsumosServicio.Borrar(ajustePorRestante);
                        }
                    }

                    //Borra los movimientos de reproceso generados por ese detalle
                    Ajustes_Insumos ajustePorReproceso = AjusteInsumosServicio.Obtener((a) => a.Numero_Comprobante == envasado.Codigo_Interno_Envasado.ToString()).FirstOrDefault();
                    if(ajustePorReproceso != null && ajustePorReproceso.Comentario == $"Envasado reprocesado - Lote {lote}")
                    {
                        AjusteInsumosServicio.Borrar(ajustePorReproceso);
                    }

                    //Borra los movimientos por liberación de un producto que tiene un insumo relacionado
                    Productos producto = detalle.Formulas_Envasado.Productos;
                    if(producto.Codigo_Insumo != null && producto.Codigo_Insumo != "")
                    {
                        Insumos insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == producto.Codigo_Insumo).FirstOrDefault();
                        if (insumo != null && insumo.Codigo_Insumo != null && insumo.Codigo_Insumo != "")
                        {
                            Detalle_Ajuste_Insumos detalleAjuste = DetalleAjusteInsumoServicio.Obtener((dai) => dai.Codigo_Insumo == insumo.Codigo_Insumo
                                                                                                        && dai.Cantidad == detalle.Cantidad
                                                                                                        && dai.Kilos_Litros == detalle.Kilos).FirstOrDefault();
                            if (detalleAjuste != null)
                            {
                                Ajustes_Insumos ajustePorLiberacion = AjusteInsumosServicio.Obtener((ai) => ai.Codigo_Interno_Ajuste == detalleAjuste.Codigo_Interno_Ajuste).FirstOrDefault();
                                if(ajustePorLiberacion.Comentario == "Ingreso por producción")
                                this.AjusteInsumosServicio.Borrar(ajustePorLiberacion);
                            }
                        }
                    }

                    Produccion_Quesos prodQuesos = this.ProduccionQuesosServicio.Obtener((p) => p.Lote.Equals(lote)).FirstOrDefault();
                    Produccion prod = this.ProduccionServicio.Obtener((p) => p.Lote.Equals(lote)).FirstOrDefault();

                    if (prodQuesos != null)
                        prodQuesos.Historico = false;

                    if (prod != null)
                        prod.Historico = false;

                    db.Eliminar_Envasado(Codigo_Interno_Detalle);
                    this.DetalleEnvasadoServicio.Borrar(detalle);

                    foreach (Envasado env in this.EnvasadoServicio.Obtener((e) => e.Lote == lote).ToList())
                    {
                        foreach (Detalle_Envasado det in env.Detalle_Envasado.ToList())
                        {
                            env.Kg_Descarte = env.Kg_Descarte == null ? 0 : env.Kg_Descarte;
                            Kilos_Envasados = Kilos_Envasados == null ? 0 : Kilos_Envasados;
                            Kilos_Envasados = Kilos_Envasados + det.Kilos + env.Kg_Descarte;
                        }
                    }
                    if (prodQuesos != null)
                    {
                        prodQuesos.Kilos_Envasado = Kilos_Envasados;
                    }                
                    if (prod != null)
                    {
                        this.ProduccionServicio.Editar(prod);
                    }
                    if (prodQuesos != null)
                    {
                        this.ProduccionQuesosServicio.Editar(prodQuesos);                
                    }
                    if (!envasado.Detalle_Envasado.Any())
                    {
                        List<int> codigos_restantes = restantes_envasado.Select(re => re.Codigo_Restantes_Envasado).ToList();
                        List<Mov_Stock> movimientos_stock_restante = MovStockServicio.Obtener(ms => 
                            codigos_restantes.Contains(ms.Codigo_Interno_Restante_Envasado ?? 0))
                            .ToList();

                        if (movimientos_stock_restante.Any())
                        {
                            foreach (Mov_Stock mov in movimientos_stock_restante)
                            {
                                MovStockServicio.Borrar(mov);
                            }
                        }

                        EnvasadoServicio.Borrar(envasado);
                    }

                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { success = false, message = "Error al eliminar " + e.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        private Detalle_Envasado ProcesarEnvasado(InfoRetencionEnvasado info, ProcesoEnvasado proceso)
        {
            Detalle_Envasado detalle = this.DetalleEnvasadoServicio.Obtener((d) => d.Codigo_Interno_Detalle == info.CodigoDetalle).FirstOrDefault();
            detalle.Historico = false;
            int Codigo_Detalle = 0;

            Motivos_Retencion motivo = VerificarMotivo(info);
            Detalle_Envasado nuevo_detalle = new Detalle_Envasado();

            if (detalle.Kilos == info.Kilos && detalle.Cantidad == info.Cantidad)
            {
                switch(proceso)
                {
                    case ProcesoEnvasado.Decomiso:
                        detalle.Historico = true;
                        detalle.Decomisado = true;
                        detalle.Id_Usuario_Decomisa = Sesion.UsuarioActual.idUsuario;
                        detalle.Codigo_Motivo_Decomiso = motivo.Codigo_Motivo_Retencion;
                        detalle.Fecha_Decomiso = info.Fecha;
                        detalle.Comentario_Decomiso = info.ComentarioRetencion;
                        break;
                    case ProcesoEnvasado.Reproceso:
                        detalle.Historico = true;
                        detalle.Codigo_Insumo_Reproceso = info.InsumoReproceso;
                        detalle.Id_Usuario_Reprocesa = Sesion.UsuarioActual.idUsuario;
                        detalle.Codigo_Motivo_Reproceso = motivo.Codigo_Interno_Motivo;
                        detalle.Fecha_Reproceso = info.Fecha;
                        detalle.Comentario_Reproceso = info.ComentarioRetencion;
                        break;
                    case ProcesoEnvasado.Retencion:
                        detalle.Historico = false;
                        detalle.Id_Usuario_Retiene = Sesion.UsuarioActual.idUsuario;
                        detalle.Codigo_Motivo_Retencion = motivo.Codigo_Motivo_Retencion;
                        detalle.Fecha_Retenido = info.Fecha;
                        detalle.Comentario_Retencion = info.ComentarioRetencion;
                        break;
                }

                Codigo_Detalle = detalle.Codigo_Interno_Detalle;
            }
            else
            {
                detalle.Cantidad -= info.Cantidad;
                detalle.Kilos -= info.Kilos;

                nuevo_detalle.Cantidad = info.Cantidad;
                nuevo_detalle.Kilos = info.Kilos;
                nuevo_detalle.Codigo_Interno_Formula = detalle.Codigo_Interno_Formula;
                nuevo_detalle.Lote = detalle.Lote;
                nuevo_detalle.Codigo_Interno_Envasado = detalle.Codigo_Interno_Envasado;
                nuevo_detalle.Vencimiento = detalle.Vencimiento;
                nuevo_detalle.Envasado = detalle.Envasado;

                switch (proceso)
                {
                    case ProcesoEnvasado.Decomiso:
                        nuevo_detalle.Historico = true;
                        nuevo_detalle.Decomisado = true;
                        nuevo_detalle.Id_Usuario_Decomisa = Sesion.UsuarioActual.idUsuario;
                        nuevo_detalle.Codigo_Motivo_Decomiso = motivo.Codigo_Motivo_Retencion;
                        nuevo_detalle.Fecha_Decomiso = info.Fecha;
                        nuevo_detalle.Comentario_Decomiso = info.ComentarioRetencion;
                        break;
                    case ProcesoEnvasado.Reproceso:
                        nuevo_detalle.Historico = true;
                        nuevo_detalle.Codigo_Insumo_Reproceso = info.InsumoReproceso;
                        nuevo_detalle.Id_Usuario_Reprocesa = Sesion.UsuarioActual.idUsuario;
                        nuevo_detalle.Codigo_Motivo_Reproceso = motivo.Codigo_Interno_Motivo;
                        nuevo_detalle.Fecha_Reproceso = info.Fecha;
                        nuevo_detalle.Comentario_Reproceso = info.ComentarioRetencion;
                        break;
                    case ProcesoEnvasado.Retencion:
                        nuevo_detalle.Historico = false;
                        nuevo_detalle.Id_Usuario_Retiene = Sesion.UsuarioActual.idUsuario;
                        nuevo_detalle.Codigo_Motivo_Retencion = motivo.Codigo_Motivo_Retencion;
                        nuevo_detalle.Fecha_Retenido = info.Fecha;
                        nuevo_detalle.Comentario_Retencion = info.ComentarioRetencion;
                        break;
                }

                this.DetalleEnvasadoServicio.Agregar(nuevo_detalle);

                Codigo_Detalle = nuevo_detalle.Codigo_Interno_Detalle;
            }
            this.DetalleEnvasadoServicio.Editar(detalle);

            db.Movimiento_Stock_Venta_Envasado(Codigo_Detalle);

            return nuevo_detalle.Codigo_Interno_Detalle != 0 ? nuevo_detalle : detalle;
        }

        private Motivos_Retencion VerificarMotivo(InfoRetencionEnvasado info)
        {
            // verifico si existe algun motivo ya creado igual
            Motivos_Retencion motivoRetencion = this.MotivosRetencionServicio.Obtener((motivo) => motivo.Descripcion == info.MotivoRetencion).FirstOrDefault();

            // si no existe motivo con la descripcion brindada
            // creo uno para asignar al detalle retenido
            if (motivoRetencion == null)
            {
                // compruebo si ya existe alguno
                int motivoid = 1;
                if (this.MotivosRetencionServicio.Obtener().Count != 0)
                    motivoid = this.MotivosRetencionServicio.Obtener().Max(motivor => motivor.Codigo_Motivo_Retencion) + 1;

                motivoRetencion = new Motivos_Retencion()
                {
                    Codigo_Motivo_Retencion = motivoid,
                    Descripcion = info.MotivoRetencion,
                    Tipo = "Calidad",
                    Activo = true
                };
                this.MotivosRetencionServicio.Agregar(motivoRetencion);
            }

            return motivoRetencion;
        }

        public ActionResult GetDetallesEnvasado(int Codigo_Interno_Envasado = 0)
        {
            var listado = db.GET_Detalles_Envasado(Codigo_Interno_Envasado);
            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenerProductosLiberadosPorCalidad()
        {
            try
            {
                var listado = db.ObtenerProductosLiberadosPorCalidad();
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ObtenerLotesLiberados(int Codigo_Fabrica = -1, string Numero_Producto_Ventas = "")
        {
            try
            {
                var listado = db.ObtenerLotesLiberados(Numero_Producto_Ventas, Codigo_Fabrica);
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ObtenerRestantePorSalidaMercaderia(int Codigo_Fabrica, string Lote = "", string Numero_Producto_Ventas = "")
        {
            try
            {
                var objeto = db.ObtenerLotesLiberados(Numero_Producto_Ventas, Codigo_Fabrica).SingleOrDefault(l => l.Lote == Lote);
                var formula = this.FormulasEnvasadoServicio.Obtener((f) => f.Numero_Producto_Ventas == Numero_Producto_Ventas).FirstOrDefault();
                object nuevoObjeto = null;

                if (objeto != null && formula != null)
                {
                    nuevoObjeto = new
                    {
                        Cantidad_Restante = objeto.Cantidad,
                        Codigo_Producto_Produccion = formula.Codigo_Producto,
                        Descripcion_Producto_Produccion = formula.Productos.Descripcion,
                        Existe = true,
                        Kilos_Restantes = objeto.Kilos,
                        Tipo = formula.Es_Insumo_Prod_Ventas.GetValueOrDefault() ? 3 : formula.Es_Insumo.GetValueOrDefault() ? 1 : 2
                    };
                }

                return Json(new { objeto = (nuevoObjeto != null) ? nuevoObjeto : objeto, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ObtenerRestantePorSalidaMercaderiaLotes(int Codigo_Fabrica, List<Dictionary<string, string>> Productos)
        {
            try
            {
                List<object> lotes = new List<object>{ };
                foreach(var prod in Productos)
                {
                    string Numero_Producto_Ventas = prod["Numero_Producto_Ventas"];
                    string Lote = prod["Lote"];
                    int Cantidad = int.Parse(prod["Cantidad"], CultureInfo.InvariantCulture);
                    float Kilos = float.Parse(prod["Kilos"], CultureInfo.InvariantCulture);
                    var objeto = db.ObtenerLotesLiberados(Numero_Producto_Ventas, Codigo_Fabrica).SingleOrDefault(l => l.Lote == Lote);
                    var formula = this.FormulasEnvasadoServicio.Obtener((f) => f.Numero_Producto_Ventas == Numero_Producto_Ventas).FirstOrDefault();
                    if (formula != null)
                    {
                        lotes.Add(new
                        {
                            Lote,
                            Numero_Producto_Ventas,
                            Cantidad,
                            Kilos,
                            Codigo_Producto_Produccion = formula.Codigo_Producto,
                            Descripcion_Producto_Produccion = formula.Productos.Descripcion,
                            Existe = true,
                            Cantidad_Restante = objeto == null ? 0 : objeto.Cantidad,
                            Kilos_Restantes = objeto == null ? 0 : objeto.Kilos,
                            Tipo = formula.Es_Insumo_Prod_Ventas.GetValueOrDefault() ? 3 : formula.Es_Insumo.GetValueOrDefault() ? 1 : 2
                        });
                    }
                }

                return Json(new { data = lotes, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
