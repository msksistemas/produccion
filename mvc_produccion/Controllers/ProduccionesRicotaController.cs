﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using MVC_Produccion.Sesiones;
using MVC_Produccion.Models.Utilidades;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion.SP;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Models;
using System.Threading.Tasks;
using System.Net.Http;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class ProduccionesRicotaController : Controller
    {
        private IProduccionServicio ProduccionServicioExtra;
        private IBaseServicio<Ajustes_Insumos> AjusteInsumosServicio;
        private IBaseServicio<Restantes_Envasado> RestanteEnvasadoServicio;
        private IBaseServicio<Produccion_Ricota> ProduccionRicotaServicio;
        private IBaseServicio<Detalle_Produccion> DetalleProduccionServicio;
        private IBaseServicio<Produccion> ProduccionServicio;
        private IBaseServicio<Envasado> EnvasadoServicio;
        private IBaseServicio<Detalle_Envasado> DetalleEnvasadoServicio;
        private IBaseServicio<Detalle_Insumo_Envasado> DetalleInsumoEnvasadoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<Familias> FamiliasServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Productos> ProductosServicio;
        private IBaseServicio<Costos_Indirectos_Produccion> CostosIndirectosProduccion;
        private IBaseServicio<Mov_Ins_Asoc> MovInsAsocServicio;
        private IBaseServicio<Queseros> QueserosServicio;
        private IBaseServicio<Formulas_Envasado> FormulasEnvasadoServicio;
        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();
        private readonly HttpService httpClient;

        public ProduccionesRicotaController(
                                         IProduccionServicio produccionServicioExtra,
                                         IBaseServicio<Produccion_Ricota> produccionRicotaServicio,
                                         IBaseServicio<Ajustes_Insumos> ajusteInsumosServicio,
                                         IBaseServicio<Formulas> formulasServicio,
                                         IBaseServicio<Restantes_Envasado> restanteEnvasadoServicio,
                                         IBaseServicio<Queseros> queserosServicio,
                                         IBaseServicio<Familias> familiasServicio,
                                         IBaseServicio<Mov_Ins_Asoc> movInsAsocServicio,
                                         IBaseServicio<Costos_Indirectos_Produccion> costosIndirectosProduccion,
                                         IBaseServicio<Detalle_Produccion> detalleProduccionServicio,
                                         IBaseServicio<Produccion> produccionServicio,
                                         IBaseServicio<Envasado> envasadoServicio,
                                         IBaseServicio<Detalle_Envasado> detalleEnvasadoServicio,
                                         IBaseServicio<Detalle_Insumo_Envasado> detalleInsumoEnvasadoServicio,
                                         IBaseServicio<Permisos> permisosServicio,
                                         IBaseServicio<Productos> productosServicio,
                                         IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                         IBaseServicio<Formulas_Envasado> formulasEnvasadoServicio,
                                         HttpService httpClient)
        {
            this.ProduccionServicioExtra = produccionServicioExtra;
            this.AjusteInsumosServicio = ajusteInsumosServicio;
            this.RestanteEnvasadoServicio = restanteEnvasadoServicio;
            this.FormulasEnvasadoServicio = formulasEnvasadoServicio;
            this.ProduccionRicotaServicio = produccionRicotaServicio;
            this.QueserosServicio = queserosServicio;
            this.MovInsAsocServicio = movInsAsocServicio;
            this.DetalleEnvasadoServicio = detalleEnvasadoServicio;
            this.DetalleInsumoEnvasadoServicio = detalleInsumoEnvasadoServicio;
            this.DetalleProduccionServicio = detalleProduccionServicio;
            this.EnvasadoServicio = envasadoServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.PermisosServicio = permisosServicio;
            this.FamiliasServicio = familiasServicio;
            this.ProduccionServicio = produccionServicio;
            this.FormulasServicio = formulasServicio;
            this.CostosIndirectosProduccion = costosIndirectosProduccion;
            this.ProductosServicio = productosServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
            this.httpClient = httpClient;
        }

        public ActionResult Index(int resultado = 0)
        {
            if (Sesion.UsuarioActual.Fabrica == 0 || Sesion.UsuarioActual.Fabrica == null && Sesion.UsuarioActual.Rol.ToLower() != "administrador")
                ViewBag.Error = "No tiene Fábrica asignada, por favor, solicite Permisos al Administrador.";

            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";

            else if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";

            ViewBag.configuraciones = this.Configuraciones;
            ViewBag.Envasa_Sin_Kilos = this.Configuraciones.Envasa_Sin_Kilos;

            List<SelectListItem> ListadoHistoricos = new List<SelectListItem>()
                        {
                            new SelectListItem() {Text="No Históricos", Value="0"},
                            new SelectListItem() {Text="Históricos", Value="1"},
                            new SelectListItem() {Text="Todos", Value="-1"}
                        };

            ViewBag.ListadoHistoricos = ListadoHistoricos;

            return View();
        }

        public ActionResult GetData(string Lote = "")
        {
            if ((Sesion.UsuarioActual.Fabrica != 0 && Sesion.UsuarioActual.Fabrica != null) || Sesion.UsuarioActual.Rol.ToLower() == "administrador")
            {
                var listado = this.ProduccionRicotaServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)
                              && p.Produccion.Historico == false
                              && p.Produccion.Codigo_Fabrica == (Sesion.UsuarioActual.Fabrica == -1 ? p.Produccion.Codigo_Fabrica : Sesion.UsuarioActual.Fabrica))
                              .Select(p => new
                              {
                                  p.Produccion.Fecha,
                                  p.Codigo_Interno_Produccion,
                                  p.Acidez_Inicial,
                                  p.Acidez_Final,
                                  p.Lote,
                                  p.Tina,
                                  p.Codigo_Silo,
                                  p.Litros_Suero,
                                  p.Porcentaje_Solidos,
                                  p.Factor_Concentracion,
                                  p.Hora_Inicio,
                                  p.Hora_Final,
                                  p.Temperatura_Corte,
                                  p.Kilos_Reales,
                                  p.Kilos_Teoricos,
                                  p.Produccion.Formulas.Codigo_Producto,
                                  p.Produccion.Formulas.Productos.Descripcion,
                                  p.Piezas_Obtenidas
                              }).OrderByDescending(p => p.Fecha).ToList(); ;
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { error = 1, Usuario = Sesion.UsuarioActual.Usuario, success = false }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetHistoricos(string Lote = "", int Historico = 0)
        {
            var listado = this.ProduccionRicotaServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)
                                                                        && (Historico == -1 || p.Produccion.Historico == (Historico == 1 ? true : false))).Select(p => new
                                                                        {
                                                                            p.Produccion.Fecha,
                                                                            p.Codigo_Interno_Produccion,
                                                                            p.Acidez_Inicial,
                                                                            p.Acidez_Final,
                                                                            p.Lote,
                                                                            p.Tina,
                                                                            p.Codigo_Silo,
                                                                            p.Litros_Suero,
                                                                            p.Porcentaje_Solidos,
                                                                            p.Factor_Concentracion,
                                                                            p.Hora_Inicio,
                                                                            p.Hora_Final,
                                                                            p.Temperatura_Corte,
                                                                            p.Kilos_Reales,
                                                                            p.Kilos_Teoricos,
                                                                            Codigo_Producto = p.Produccion.Formulas != null ? p.Produccion.Formulas.Codigo_Producto : "",
                                                                            Descripcion = p.Produccion.Formulas != null ? p.Produccion.Formulas.Productos != null ? p.Produccion.Formulas.Productos.Descripcion : "" : "",
                                                                            p.Piezas_Obtenidas,
                                                                            Kilos_Restantes = this.ObtenerRestantes(p.Lote,true)
                                                                        }).OrderByDescending(p => p.Fecha).ToList();
            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote = "")
        {
            ViewBag.queseros = this.QueserosServicio.Obtener((q) => q.Activo == true).Select(q => new { q.Nombre, q.Codigo_Quesero }).OrderBy(p => p.Nombre);
            ViewBag.Codigo_Familia = this.FamiliasServicio.Obtener((f) => f.Nombre.ToLower() == "ricota").FirstOrDefault().Codigo_Familia;
            ViewBag.Envasa_Sin_Kilos = this.Configuraciones.Envasa_Sin_Kilos;

            if (!string.IsNullOrEmpty(Lote))
            {
                var lote = this.ProduccionRicotaServicio.Obtener((x) => x.Produccion.Lote == Lote).FirstOrDefault();
                if (lote.Produccion.Lote_Padre != null && lote.Produccion.Lote_Padre != "undefined")
                {
                    ViewBag.esDesglose = true;
                }
                else
                {
                    ViewBag.esDesglose = false;
                }
                return View(lote);
            }

            Produccion_Ricota prod = new Produccion_Ricota();
            Produccion produccion = new Produccion();
            produccion.Fecha = DateTime.Now;
            prod.Produccion = produccion;

            if (!this.Configuraciones.NroPlanillaDigitable)
            {
                var plantilla = "1";
                if (this.ProduccionRicotaServicio.Obtener().Count > 0)
                {
                    try
                    {
                        plantilla = this.ProduccionRicotaServicio.Obtener().Max(p => Convert.ToInt32(p.Produccion.Nro_Planilla)).ToString();
                    }
                    catch (Exception ex)
                    {
                        plantilla = (this.ProduccionRicotaServicio.Obtener().Count + 1).ToString();
                    }

                    prod.Produccion.Nro_Planilla = plantilla;
                }
                ViewBag.NroAuto = true;
            }
            else
                ViewBag.NroAuto = false;

            ViewBag.esDesglose = false;
            return View(prod);
        }

        [HttpPost]
        public async Task<ActionResult> AddOrEdit(Produccion_Ricota prod, List<Desglose_Produccion> desglose, string Codigo_Producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //if (httpClient.EsSanSatur())
                    //{
                    //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                    //        prod.Produccion.Detalle_Produccion.ToList(),
                    //        HttpService.ProcesoProduccion.Creacion,
                    //        prod.Produccion.Fecha ?? DateTime.Now,
                    //        httpClient.ObtenerPrimerNumProdVentas(Codigo_Producto)
                    //    );
                    //    if (!response.IsSuccessStatusCode)
                    //    {
                    //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                    //    }
                    //}

                    prod.Lote = prod.Produccion.Lote;
                    prod.Produccion.idUsuario = Sesion.UsuarioActual.idUsuario;
                    if (prod.Codigo_Interno_Produccion == 0)
                    {
                        if (this.ProduccionServicio.Obtener((p) => p.Lote == prod.Lote).SingleOrDefault() != null)
                            return Json(new { success = false, message = $"Lote {prod.Lote} ya registrado." }, JsonRequestBehavior.AllowGet);

                        prod.Produccion.Historico = false;

                        if (desglose != null)
                        {
                            var cantidadDesglosada = desglose.Sum(d => d.Cantidad);

                            if (prod.Piezas_Obtenidas.HasValue && cantidadDesglosada != prod.Piezas_Obtenidas.Value)
                            {
                                return Json(new { success = false, message = "La cantidad desglosada no es la misma que las piezas obtenidas." }, JsonRequestBehavior.AllowGet);
                            }

                            prod.Produccion.Historico = true;
                            Produccion_Ricota prDesglose;
                            Produccion prodDesglose;
                            Formulas formulaDesglose;
                            foreach (Desglose_Produccion d in desglose)
                            {
                                prDesglose = new Produccion_Ricota();
                                prDesglose.Piezas_Obtenidas = Decimal.ToInt32(d.Cantidad);
                                prDesglose.Kilos_Reales = d.Kilos;
                                prDesglose.Lote = d.Lote;
                                prDesglose.Kilos_Teoricos = prod.Kilos_Teoricos;
                                prDesglose.Litros_Suero = prod.Litros_Suero;
                                prDesglose.Litros_Tina = prod.Litros_Tina;
                                prDesglose.Fecha_Vencimiento = prod.Fecha_Vencimiento;
                                prDesglose.Codigo_Silo = prod.Codigo_Silo;
                                prDesglose.Tina = prod.Tina;
                                prDesglose.Porcentaje_Solidos = prod.Porcentaje_Solidos;
                                prDesglose.Factor_Concentracion = prod.Factor_Concentracion;
                                prDesglose.Temperatura_Corte = prod.Temperatura_Corte;
                                prDesglose.Acidez_Inicial = prod.Acidez_Inicial;
                                prDesglose.Acidez_Final = prod.Acidez_Final;
                                prDesglose.Hora_Inicio = prod.Hora_Inicio;
                                prDesglose.Hora_Final = prod.Hora_Final;

                                formulaDesglose = this.FormulasServicio.Obtener((x) => x.Codigo_Producto == d.Numero_Producto).FirstOrDefault();

                                prodDesglose = new Produccion();
                                prodDesglose.Lote = d.Lote;
                                prodDesglose.Lote_Padre = prod.Produccion.Lote;
                                prodDesglose.Historico = false;
                                prodDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                prodDesglose.Fecha = prod.Produccion.Fecha;
                                prodDesglose.Nro_Planilla = prod.Produccion.Nro_Planilla;
                                prodDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                prodDesglose.Codigo_Formula = formulaDesglose.Codigo_Formula;
                                prodDesglose.idUsuario = Sesion.UsuarioActual.idUsuario;
                                prDesglose.Produccion = prodDesglose;

                                this.ProduccionRicotaServicio.Agregar(prDesglose);
                            }
                        }

                        var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();

                        prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;

                        //detalle Prod
                        foreach (var detalle in prod.Produccion.Detalle_Produccion.ToList())
                        {
                            detalle.Movimiento = "Producción";
                            detalle.Fecha = prod.Produccion.Fecha;
                            detalle.Lote = prod.Lote;
                        }
                        foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion.ToList())
                        {
                            detalle.Lote = prod.Produccion.Lote;
                        }
                        /*GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS*/
                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                        {
                            movimiento.Lote = prod.Lote;
                        }

                        List<string> errors = new List<string>();

                        try
                        {
                            this.ProduccionRicotaServicio.Agregar(prod);
                            db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);

                            if (desglose != null)
                            {
                                foreach (Desglose_Produccion d in desglose)
                                {
                                    int Codigo_Interno_Desglose = this.ProduccionServicio.Obtener((p) => p.Lote == d.Lote).SingleOrDefault().Codigo_Interno_Produccion;
                                    db.Movimiento_Stock_Produccion(Codigo_Interno_Desglose, false);
                                }
                            }
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var error in e.EntityValidationErrors)
                            {
                                foreach (var validation in error.ValidationErrors)
                                {
                                    errors.Add("Error en campo " + validation.PropertyName + ": " + validation.ErrorMessage);
                                }
                            }
                        }
                        if (errors.Count == 0)
                        {
                            scope.Complete();
                            return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, errs = errors }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        if (desglose != null)
                        {
                            var lote = this.ProduccionServicio.Obtener((x) => x.Lote_Padre == prod.Lote).FirstOrDefault();
                            if (lote != null)
                            {
                                return Json(new { success = false, message = "Ya existen desgloses de esta producción." }, JsonRequestBehavior.AllowGet);
                            }
                            else if (prod.Produccion.Historico == true)
                            {
                                return Json(new { success = false, message = "No se puede desglosar una producción histórica." }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                prod.Produccion.Historico = true;
                                Produccion_Ricota prDesglose;
                                Produccion prodDesglose;
                                Formulas formulaDesglose;
                                foreach (Desglose_Produccion d in desglose)
                                {
                                    prDesglose = new Produccion_Ricota();
                                    prDesglose.Piezas_Obtenidas = Decimal.ToInt32(d.Cantidad);
                                    prDesglose.Kilos_Reales = d.Kilos;
                                    prDesglose.Lote = d.Lote;
                                    prDesglose.Kilos_Teoricos = prod.Kilos_Teoricos;
                                    prDesglose.Litros_Suero = prod.Litros_Suero;
                                    prDesglose.Litros_Tina = prod.Litros_Tina;
                                    prDesglose.Fecha_Vencimiento = prod.Fecha_Vencimiento;
                                    prDesglose.Codigo_Silo = prod.Codigo_Silo;
                                    prDesglose.Tina = prod.Tina;
                                    prDesglose.Porcentaje_Solidos = prod.Porcentaje_Solidos;
                                    prDesglose.Factor_Concentracion = prod.Factor_Concentracion;
                                    prDesglose.Temperatura_Corte = prod.Temperatura_Corte;
                                    prDesglose.Acidez_Inicial = prod.Acidez_Inicial;
                                    prDesglose.Acidez_Final = prod.Acidez_Final;
                                    prDesglose.Hora_Inicio = prod.Hora_Inicio;
                                    prDesglose.Hora_Final = prod.Hora_Final;

                                    formulaDesglose = this.FormulasServicio.Obtener((x) => x.Codigo_Producto == d.Numero_Producto).FirstOrDefault();

                                    prodDesglose = new Produccion();
                                    prodDesglose.Lote = d.Lote;
                                    prodDesglose.Lote_Padre = prod.Produccion.Lote;
                                    prodDesglose.Historico = false;
                                    prodDesglose.Codigo_Fabrica = prod.Produccion.Codigo_Fabrica;
                                    prodDesglose.Fecha = prod.Produccion.Fecha;
                                    prodDesglose.Nro_Planilla = prod.Produccion.Nro_Planilla;
                                    prodDesglose.Codigo_Quesero = prod.Produccion.Codigo_Quesero;
                                    prodDesglose.Codigo_Formula = formulaDesglose.Codigo_Formula;
                                    prodDesglose.idUsuario = Sesion.UsuarioActual.idUsuario;
                                    prDesglose.Produccion = prodDesglose;

                                    this.ProduccionRicotaServicio.Agregar(prDesglose);
                                }
                            }
                        }

                        /*TRAE EL PRODUCTO ASOCIADO A LA FÒRMULA PARA SABER SI ES UN INSUMO, PROD TERMINADO O AMBOS*/
                        var nuevaFormula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                        prod.Produccion.Codigo_Formula = nuevaFormula.Codigo_Formula;

                        List<Detalle_Produccion> detalleAnterior = this.DetalleProduccionServicio.Obtener((dp) => dp.Lote == prod.Lote).ToList();

                        //if (httpClient.EsSanSatur())
                        //{
                        //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                        //        prod.Produccion.Detalle_Produccion.ToList(),
                        //        HttpService.ProcesoProduccion.Edicion,
                        //        Codigo_Producto,
                        //        detalleAnterior);
                        //    if (!response.IsSuccessStatusCode)
                        //    {
                        //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                        //    }
                        //}

                        /*BORRA LOS DETALLES DE LA PRODUCCIÓN A EDITAR*/
                        foreach (var detalle in detalleAnterior)
                        {
                            this.DetalleProduccionServicio.Borrar(detalle);
                        }
                        /*GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS*/
                        foreach (var detalle in prod.Produccion.Detalle_Produccion.ToList())
                        {
                            detalle.Movimiento = "Producción";
                            detalle.Fecha = prod.Produccion.Fecha;
                            detalle.Lote = prod.Lote;
                            this.DetalleProduccionServicio.Agregar(detalle);
                        }
                        foreach (var detalle in this.CostosIndirectosProduccion.Obtener((ci) => ci.Lote == prod.Lote))
                        {
                            this.CostosIndirectosProduccion.Borrar(detalle);
                        }
                        foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion.ToList())
                        {
                            detalle.Lote = prod.Produccion.Lote;
                            this.CostosIndirectosProduccion.Agregar(detalle);
                        }
                        /*BORRA TODOS LOS DETALLES DE PRODUCTOS ASOCIADOS*/
                        foreach (var movimiento in this.MovInsAsocServicio.Obtener((mia) => mia.Lote == prod.Lote))
                        {
                            this.MovInsAsocServicio.Borrar(movimiento);
                        }
                        /*GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS*/
                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                        {
                            if (movimiento != null)
                            {
                                movimiento.Lote = prod.Produccion.Lote;
                                var detalleInsumo = prod.Produccion.Detalle_Produccion.Where(detalle => detalle.Codigo_Insumo == movimiento.Codigo_Insumo).FirstOrDefault();
                                if (detalleInsumo != null)
                                {
                                    var ajuste = detalleInsumo.Ajuste.HasValue ? detalleInsumo.Ajuste.Value : 0;
                                    if (ajuste != 0)
                                        this.MovInsAsocServicio.Agregar(movimiento);
                                }
                            }
                        }
                        this.ProduccionRicotaServicio.Editar(prod);
                        this.ProduccionServicio.Editar(prod.Produccion);
                        db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);

                        if (desglose != null)
                        {
                            foreach (Desglose_Produccion d in desglose)
                            {
                                int Codigo_Interno_Desglose = this.ProduccionServicio.Obtener((p) => p.Lote == d.Lote).SingleOrDefault().Codigo_Interno_Produccion;
                                db.Movimiento_Stock_Produccion(Codigo_Interno_Desglose, false);
                            }
                        }
                        scope.Complete();
                        return Json(new { success = true, resultado = 2 }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        private decimal ObtenerEnvasados_Kilos(string Lote = "")
        {
            decimal kilosEnvasados = 0;

            foreach (var env in this.EnvasadoServicio.Obtener((e) => e.Lote == Lote).ToList())
            {
                foreach (var restante in env.Restantes_Envasados)
                {
                    kilosEnvasados += restante.Kilos;

                }
                foreach (var detalle in env.Detalle_Envasado.ToList())
                {
                    kilosEnvasados += detalle.Kilos.HasValue ? detalle.Kilos.Value : 0;
                }

            }

           
            return kilosEnvasados;
        }
        private decimal ObtenerRestantes(string Lote ="", bool kilos = true)
        {
            decimal? kilosEnvasados = 0;
            decimal? piezasEnvasadas = 0;

            decimal? kilosRestantes = 0;
            decimal? piezasRestantes = 0;

            var prod = this.ProduccionRicotaServicio.Obtener((p) => p.Lote == Lote).FirstOrDefault();

            foreach (var env in this.EnvasadoServicio.Obtener((e) => e.Lote == Lote).ToList())
            {
                foreach (var restante in env.Restantes_Envasados)
                {
                    kilosEnvasados += restante.Kilos;
                    piezasEnvasadas += Convert.ToInt32(restante.Hormas);

                }
                foreach (var detalle in env.Detalle_Envasado.ToList())
                {
                    kilosEnvasados += detalle.Kilos;
                    piezasEnvasadas += detalle.Cantidad;
                }

            }

            if (prod != null)
            {
                try
                {
                    kilosRestantes = prod.Kilos_Reales - kilosEnvasados;
                    piezasRestantes = prod.Piezas_Obtenidas - piezasEnvasadas;
                }
                catch { }
            }

            return kilos ? (kilosRestantes.HasValue ? kilosRestantes.Value : 0) : (piezasRestantes.HasValue ? piezasRestantes.Value : 0);
        }
        public ActionResult ObtenerRestantes(string Lote = "")
        {
            decimal? kilosEnvasados = 0;
            decimal? piezasEnvasadas = 0;

            decimal? kilosRestantes = 0;
            decimal? piezasRestantes = 0;

            var prod = this.ProduccionRicotaServicio.Obtener((p) => p.Lote == Lote).FirstOrDefault();

            foreach (var env in this.EnvasadoServicio.Obtener((e) => e.Lote == Lote).ToList())
            {
                foreach (var restante in env.Restantes_Envasados)
                {
                    kilosEnvasados += restante.Kilos;
                    piezasEnvasadas += Convert.ToInt32(restante.Hormas);

                }
                foreach (var detalle in env.Detalle_Envasado.ToList())
                {
                    kilosEnvasados += detalle.Kilos;
                    piezasEnvasadas += detalle.Cantidad;
                }

            }

            if (prod != null)
            {
                try
                {
                    kilosRestantes = prod.Kilos_Reales - kilosEnvasados;
                    piezasRestantes = prod.Piezas_Obtenidas - piezasEnvasadas;
                }
                catch { }
            }

            return Json(new
            {
                success = true,
                kilosRestantes = kilosRestantes,
                piezasRestantes = piezasRestantes
            }, JsonRequestBehavior.AllowGet);

        }

        private void AgregarRestanteEnvasado(Envasado envasado, int codigoFabrica, List<Restantes_Envasado> restantes)
        {
            decimal kg_descarte = 0, hormas_descarte = 0;
            foreach (var restante in restantes)
            {
                restante.Lote = envasado.Lote;
                restante.Codigo_Interno_Envasado = envasado.Codigo_Interno_Envasado;
                kg_descarte += restante.Kilos;
                hormas_descarte += restante.Hormas;
                if (restante.Motivo == "Reproceso")
                {
                    Ajustes_Insumos ai = new Ajustes_Insumos();
                    ai.Comentario = "Descarte envasado lote " + envasado.Lote;
                    ai.Es_Traspaso = false;
                    ai.Fabrica = codigoFabrica;
                    ai.Fabrica_Origen = 0;
                    ai.Fecha = envasado.Fecha;
                    ai.Historico = true;
                    ai.Numero_Comprobante = envasado.Codigo_Interno_Envasado.ToString();
                    ai.Tipo_Movimiento = "Ingreso";
                    Detalle_Ajuste_Insumos dai = new Detalle_Ajuste_Insumos();
                    dai.Cantidad = restante.Hormas;
                    dai.Kilos_Litros = restante.Kilos;
                    dai.Lote = envasado.Lote;
                    dai.Codigo_Insumo = restante.Codigo_Insumo;

                    ai.Detalle_Ajuste_Insumos.Add(dai);

                    this.AjusteInsumosServicio.Agregar(ai);
                }
                this.RestanteEnvasadoServicio.Agregar(restante);
            }

            envasado.Kg_Descarte = kg_descarte;
            envasado.Hormas_Descarte = hormas_descarte;
            this.EnvasadoServicio.Editar(envasado);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }

    }
}
