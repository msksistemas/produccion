﻿using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Ventas;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class ImpuestosController : Controller
    {
        private IBaseServicio<Impuestos> ImpuestosServicio;
        public ImpuestosController(IBaseServicio<Impuestos> impuestosServicio)
        {
            this.ImpuestosServicio = impuestosServicio;
        }


        public ActionResult GetData()
        {
            var listado = this.ImpuestosServicio.Obtener().Select(i => new
            {
                Codigo = i.Numero,
                Descripcion = i.Porcentaje_IVA_Inscripto
            });

            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult obtener(int numero = 0)
        {
            if (numero != 0)
            {
                var alicuota = this.ImpuestosServicio.Obtener((i) => i.Numero == numero).SingleOrDefault();

                if (alicuota != null)
                    return Json(new { success = true, alicuota }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, mensaje = "No existe la alicuota" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var alicuotas = this.ImpuestosServicio.Obtener().ToList();

                return Json(new { success = true, alicuotas }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: Alicuota/Create
        [HttpPost]
        public JsonResult Add(decimal valor)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var existe = this.ImpuestosServicio.Obtener((i) => i.Porcentaje_IVA_Inscripto == valor).SingleOrDefault();

                    if (existe == null)
                    {
                        Int16 sig = 0;
                        if (this.ImpuestosServicio.Obtener().Count != 0)
                            sig = Convert.ToInt16(this.ImpuestosServicio.Obtener().Max(i => i.Numero) + 1);

                        Impuestos nuevo = new Impuestos
                        {
                            Porcentaje_IVA_Inscripto = valor,
                            Numero = sig
                        };

                        this.ImpuestosServicio.Agregar(nuevo);
                        scope.Complete();
                        return Json(new { success = true, nuevo }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, mensaje = "Ya existe un Impuesto con ese valor", error = "" }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception Ex)
                {
                    return Json(new { success = false, mensaje = "Ocurrio un Error", error = Ex }, JsonRequestBehavior.AllowGet);

                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int numero)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var eliminar = this.ImpuestosServicio.Obtener((i) => i.Numero == numero).SingleOrDefault();
                    this.ImpuestosServicio.Borrar(eliminar);
                    scope.Complete();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = "Ocurrio un error", consola = ex }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
