﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static MVC_Produccion.Listados.ListadosModels;
using MVC_Produccion.Models.Utilidades;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class UtilidadesController : Controller
    {
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<Monedas> MonedasServicio;
        private IBaseServicio<Datos_Empresa> DatosEmpresaServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Productos> ProductosServicio;
        private IBaseServicio<Formulas_Envasado> FormulasEnvasadoServicio;
        private IBaseServicio<Produccion> ProduccionServicio;

        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();

        public UtilidadesController(IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                    IBaseServicio<Permisos> permisosServicio,
                                    IBaseServicio<Monedas> monedasServicio,
                                    IBaseServicio<Datos_Empresa> datosEmpresaServicio,
                                    IBaseServicio<Formulas> formulasServicio,
                                    IBaseServicio<Productos> productosServicio,
                                    IBaseServicio<Produccion> produccionServicio,
                                    IBaseServicio<Formulas_Envasado> formulasEnvasadoServicio)
        {
            this.FormulasEnvasadoServicio = formulasEnvasadoServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.PermisosServicio = permisosServicio;
            this.MonedasServicio = monedasServicio;
            this.DatosEmpresaServicio = datosEmpresaServicio;
            this.FormulasServicio = formulasServicio;
            this.ProductosServicio = productosServicio;
            this.ProduccionServicio = produccionServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }
        [HttpGet]
        public ActionResult OpcionesTrabajo()
        {
            List<SelectListItem> FabricaDeposito = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Fábrica", Value="Fábrica"},
                new SelectListItem() {Text="Fábrica/Depósito", Value="Fábrica/Depósito"}
            };
            ViewBag.Fabrica_Deposito = FabricaDeposito;

            List<SelectListItem> Imputar_Total_O_Neto = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Total", Value="true"},
                new SelectListItem() {Text="Neto", Value="false"}
            };
            ViewBag.Total_O_Neto = Imputar_Total_O_Neto;

            return View(this.Configuraciones);
        }

        [HttpGet]
        public JsonResult Monedas()
        {
            try
            {
                var monedas = this.MonedasServicio.Obtener((m) => m.nacional == false).Select(m => new { m.nombre, m.intercambio }).ToList();
                return Json(new { error = false, data = monedas }, JsonRequestBehavior.AllowGet);

            } catch (Exception e)
            {
                return Json(new { error = true, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ActualizarTinas()
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                string response = "ok";

                if (this.db != null)
                {
                    try
                    {
                        this.db.ActualizarLitrosQuesos();
                    }

                    catch (Exception e)
                    {
                        response = e.Message;
                    }
                }
                scope.Complete();
                return Json(new { message = response }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Monedas(string nombre, string intercambio)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                string valor = intercambio.Replace('.', ',');
                double valorMoneda = 1;

                try
                {
                    valorMoneda = double.Parse(valor);
                }

                catch { };

                if (valorMoneda < 0)
                    valorMoneda = 0;

                Monedas moneda = this.MonedasServicio.Obtener((m) => m.nombre == nombre).SingleOrDefault();

                if (moneda != null)
                {
                    moneda.intercambio = valorMoneda;
                    this.MonedasServicio.Editar(moneda);
                    return Json(new { message = "ok" });
                }
                scope.Complete();
                return Json(new { message = "fail" });
            }
        }

        [HttpPost]
        public JsonResult ActualizarMaestroProductos(FiltroActualizarMaestroProductos filtro)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    this.db.RPT_AJUSTE_PRECIOS_PRODUCTOS(
                        filtro.FechaDesde1,
                        filtro.FechaDesde2,
                        filtro.FechaHasta1,
                        filtro.FechaHasta2
                    );
                    scope.Complete();
                    return Json(new { message = "ok" }, JsonRequestBehavior.AllowGet);
                }

                catch (Exception e)
                {
                    return Json(new { message = "ERROR AL ACTUALIZAR: " + e.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult OpcionesTrabajo(OpcionesTrabajo OpcionesTrabajo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    this.OpcionesTrabajoServicio.Borrar(this.Configuraciones);
                    this.OpcionesTrabajoServicio.Agregar(OpcionesTrabajo);
                    this.Configuraciones = OpcionesTrabajo;
                    scope.Complete();
                    return Json(new { success = true, message = "Opciones de Trabajo modificadas satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult DatosEmpresa()
        {
            var datos_empresa = this.DatosEmpresaServicio.Obtener().FirstOrDefault();

            var nombre = Path.GetFileName(datos_empresa.Imagen);
            var direccion = Path.Combine("DatosEmpresa/", nombre);
            datos_empresa.Imagen = direccion;

            return View(datos_empresa);
        }

        [HttpPost]
        public ActionResult DatosEmpresa(Datos_Empresa Configuraciones, HttpPostedFileBase file)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var datosViejos = this.DatosEmpresaServicio.Obtener().FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        var ruta = Server.MapPath("~/DatosEmpresa/");
                        Directory.CreateDirectory(ruta);
                        var nombre = Path.GetFileName("Logo.jpg");
                        var direccion = Path.Combine(ruta, nombre);
                        file.SaveAs(direccion);
                        Configuraciones.Imagen = nombre;
                    }
                    else
                        Configuraciones.Imagen = datosViejos.Imagen;
                    this.DatosEmpresaServicio.Borrar(datosViejos);
                    this.DatosEmpresaServicio.Agregar(Configuraciones);
                    scope.Complete();
                    return Json(new { success = true, message = "Datos guardados satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult ObtenerConfiguraciones()
        {
            try
            {
                return Json(new { data = this.Configuraciones, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult Duplicar_Formula()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Duplicar_Formula(DuplicarFormulaModel modelo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Formulas Formula_A_Duplicar = new Formulas();
                    Formula_A_Duplicar = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == modelo.Producto_Origen.Trim()).FirstOrDefault();

                    Formulas FormulaNueva = new Formulas();
                    FormulaNueva.Codigo_Producto = modelo.Producto_Destino;
                    FormulaNueva.Comentario = Formula_A_Duplicar.Comentario;
                    FormulaNueva.Factor = Formula_A_Duplicar.Factor;

                    Productos prod = new Productos();
                    prod = this.ProductosServicio.Obtener((p) => p.Codigo_Producto == modelo.Producto_Destino.Trim()).FirstOrDefault();

                    if (prod.Que_Usa.ToLower() == "leche")
                    {
                        FormulaNueva.Litros_Tina = Formula_A_Duplicar.Litros_Tina;
                    }
                    else if (prod.Que_Usa.ToLower() == "crema")
                    {
                        FormulaNueva.Litros_Crema = Formula_A_Duplicar.Litros_Crema;
                    }
                    else if (prod.Que_Usa.ToLower() == "masa")
                    {
                        FormulaNueva.Kilos_Masa = Formula_A_Duplicar.Kilos_Masa;
                    }
                    else if (prod.Que_Usa.ToLower() == "suero")
                    {
                        FormulaNueva.Litros_Suero = Formula_A_Duplicar.Litros_Suero;
                    }

                    FormulaNueva.Kilos_Teoricos = Formula_A_Duplicar.Kilos_Teoricos;

                    foreach (var detalle in Formula_A_Duplicar.Detalle_Formula.ToList())
                    {
                        Detalle_Formula detalleNuevo = new Detalle_Formula();
                        detalleNuevo.Codigo_Formula = FormulaNueva.Codigo_Formula;
                        detalleNuevo.Codigo_Insumo = detalle.Codigo_Insumo;
                        detalleNuevo.Cantidad = detalle.Cantidad;
                        detalleNuevo.Costo = detalle.Costo;
                        detalleNuevo.Precio_Unitario = detalle.Precio_Unitario;
                        detalleNuevo.Marca = detalle.Marca;

                        FormulaNueva.Detalle_Formula.Add(detalleNuevo);
                    }

                    foreach (var costo in Formula_A_Duplicar.Costos_Indirectos_Formula.ToList())
                    {
                        Costos_Indirectos_Formula costoNuevo = new Costos_Indirectos_Formula();
                        costoNuevo.Codigo_Formula = FormulaNueva.Codigo_Formula;
                        costoNuevo.Codigo_Costo_Indirecto = costo.Codigo_Costo_Indirecto;
                        costoNuevo.Precio = costo.Precio;
                        costoNuevo.Cantidad = costo.Cantidad;
                        costoNuevo.Precio_Fijo = costo.Precio_Fijo;
                        costoNuevo.Porcentaje = costo.Porcentaje;
                        costoNuevo.Marca = costo.Marca;

                        FormulaNueva.Costos_Indirectos_Formula.Add(costoNuevo);
                    }

                    this.FormulasServicio.Agregar(FormulaNueva);
                    scope.Complete();
                    return Json(new { success = true, message = "Fórmula duplicada satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult Duplicar_Formula_Envasado()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Duplicar_Formula_Envasado(DuplicarFormulaModel modelo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var ProdVenta = modelo.Producto_Destino;
                var CodFormula = Convert.ToInt64(modelo.Producto_Origen);

                try
                {
                    var Formula = this.FormulasEnvasadoServicio.Obtener((f) => f.Codigo_Interno_Formula == CodFormula).SingleOrDefault();
                    var existe = this.FormulasEnvasadoServicio.Obtener((f) => f.Codigo_Producto == Formula.Codigo_Producto
                                                                            && f.Numero_Producto_Ventas == ProdVenta).SingleOrDefault();
                    if (existe != null)
                        return Json(new { success = false, message = "Ya existe esa formula" }, JsonRequestBehavior.AllowGet);

                    var NuevaFormula = new Formulas_Envasado();

                    NuevaFormula.Numero_Producto_Ventas = ProdVenta;
                    NuevaFormula.Codigo_Producto = Formula.Codigo_Producto;
                    NuevaFormula.Cantidad_Envasada = Formula.Cantidad_Envasada;
                    NuevaFormula.Costos_Indirectos_Formula_Envasado = Formula.Costos_Indirectos_Formula_Envasado;
                    NuevaFormula.Es_Insumo = Formula.Es_Insumo;
                    NuevaFormula.Es_Insumo_Prod_Ventas = Formula.Es_Insumo_Prod_Ventas;
                    NuevaFormula.Obtenidas_Por_Envasada = Formula.Obtenidas_Por_Envasada;
                    NuevaFormula.Unidad_Medida = Formula.Unidad_Medida;


                    foreach (var item in Formula.Detalle_Formula_Envasado.ToList())
                    {
                        var nuevoDetalle = new Detalle_Formula_Envasado();
                        nuevoDetalle.Cantidad = item.Cantidad;
                        nuevoDetalle.Marca = item.Marca;
                        nuevoDetalle.Ajuste = item.Ajuste;
                        nuevoDetalle.Codigo_Insumo = item.Codigo_Insumo;
                        nuevoDetalle.Codigo_Interno_Formula = NuevaFormula.Codigo_Interno_Formula;

                        NuevaFormula.Detalle_Formula_Envasado.Add(nuevoDetalle);
                    }

                    this.FormulasEnvasadoServicio.Agregar(NuevaFormula);
                    scope.Complete();
                    return Json(new { success = true, message = "Fórmula duplicada satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message, error = ex }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public FileResult Exportar_EXCEL(string NombreArchivo = "")
        {
            string file = this.Configuraciones.Ruta_EXCELS + NombreArchivo;
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            return File(file, contentType, Path.GetFileName(file));
        }

        [HttpGet]
        public ActionResult ModalModificarLoteProduccion()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ModificarLoteProduccion(string Lote_Anterior, string Lote_Nuevo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {

                    if (Lote_Nuevo.Length > 13)
                        return Json(new { success = false, message = "El Lote nuevo no puede tener más de 13 caracteres." }, JsonRequestBehavior.AllowGet);

                    if (this.ProduccionServicio.Obtener((p) => p.Lote == Lote_Nuevo).SingleOrDefault() != null)
                        return Json(new { success = false, message = "Ya existe un registro con ese Lote." }, JsonRequestBehavior.AllowGet);

                    db.ActualizarLoteProduccion(Lote_Anterior, Lote_Nuevo);
                    scope.Complete();
                    return Json(new { success = true, message = "Lote modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
