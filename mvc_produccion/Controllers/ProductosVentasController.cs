﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Ventas;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class ProductosVentasController : Controller
    {
        private IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> ProductosVentasServicio;
        private IBaseServicio<Permisos> PermisosServicios;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicios;

        private OpcionesTrabajo Configuraciones;

        public ProductosVentasController(IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> productosVentasServicio,
                                         IBaseServicio<Permisos> permisosServicio,
                                         IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicios)
        {
            this.OpcionesTrabajoServicios = opcionesTrabajoServicios;
            this.ProductosVentasServicio = productosVentasServicio;
            this.PermisosServicios = permisosServicio;

            this.Configuraciones = this.OpcionesTrabajoServicios.Obtener().FirstOrDefault();
        }
        public ActionResult GetData(int estado = 0)
        {
            var listado = this.ProductosVentasServicio.Obtener((p) => p.Baja == estado).Select(p => new
            {
                p.Numero_de_Producto,
                p.Descripcion
            });

            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetOne(string Codigo_de_Producto = "")
        {
            if (Codigo_de_Producto != "")
            {
                try
                {
                    var producto = this.ProductosVentasServicio.Obtener((i) => i.Numero_de_Producto == Codigo_de_Producto).FirstOrDefault();
                    if (producto == null) throw new Exception();

                    AccesoDatos.Contexto.Ventas.Productos productoNuevo = new AccesoDatos.Contexto.Ventas.Productos();

                    productoNuevo.Codigo_de_Producto = producto.Numero_de_Producto;
                    productoNuevo.Descripcion = producto.Descripcion;

                    return Json(new { data = productoNuevo, success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, message = "Error, no existe registro con el Código ingresado." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, message = "Error, no existe registro con el Código ingresado." }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult Inicio()
        {
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="Activo", Value="0"},
                                new SelectListItem() {Text="Inactivo", Value="1"}
                            };
            ViewBag.ListadoEstados = ListadoEstados;

            return View();
        }



        [HttpGet]
        public ActionResult ObtenerProdVentas(int estado = 0)
        {
            var listado = this.ProductosVentasServicio.Obtener((p) => p.Baja == estado).Select(p => new
            {
                Numero_de_Producto = p.Numero_de_Producto.Trim(),
                p.Descripcion,
                p.Baja
            });

            return Json(new
            {
                success = true,
                mensaje = "",
                data = listado
            }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Delete(string Numero_de_Producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var eliminar = this.ProductosVentasServicio.Obtener((p) => p.Numero_de_Producto == Numero_de_Producto).SingleOrDefault();

                    this.ProductosVentasServicio.Borrar(eliminar);
                    scope.Complete();
                    return Json(new
                    {
                        success = true,
                        message = "Se elimino Correctamente",
                    }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        success = false,
                        message = "A ocurrido un error",
                        consola = ex.Message,
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(string Numero_de_Producto, string Descripcion)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    string mensaje = "Se modifico Correctamente";
                    var existe = this.ProductosVentasServicio.Obtener((p) => p.Numero_de_Producto == Numero_de_Producto).SingleOrDefault();
                    if (existe == null)
                    {
                        AccesoDatos.Contexto.Ventas.Productos nuevo = new AccesoDatos.Contexto.Ventas.Productos
                        {
                            Numero_de_Producto = Numero_de_Producto,
                            Descripcion = Descripcion,
                            Codigo_Impuesto_Interno = 0,
                            Baja = 0
                        };
                        this.ProductosVentasServicio.Agregar(nuevo);
                        mensaje = "Se Agrego Correctamente";
                    }
                    else
                    {
                        existe.Descripcion = Descripcion;
                        this.ProductosVentasServicio.Editar(existe);
                    }
                    scope.Complete();
                    return Json(new
                    {
                        success = true,
                        message = mensaje,
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        success = false,
                        message = "A ocurrido un Error",
                        consola = ex.Message
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        public ActionResult CambiarEstado(string codigo_producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var producto = this.ProductosVentasServicio.Obtener((p) => p.Numero_de_Producto == codigo_producto).SingleOrDefault();
                if (producto.Baja == 1)
                    producto.Baja = 0;
                else
                    producto.Baja = 1;
                this.ProductosVentasServicio.Editar(producto);
                scope.Complete();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
