﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class CambiosEstadoController : Controller
    {
        private IBaseServicio<Cambio_Estado> CambioEstadoServicio;
        private IBaseServicio<Estados> EstadoServicio;
        private IBaseServicio<Produccion_Quesos> ProduccionQuesosServicio;
        private IBaseServicio<Produccion_Quesos_Fundido> ProduccionQuesosFundidoServicio;
        private IBaseServicio<Produccion_Leche_Polvo> ProduccionLechePolvoServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;

        public CambiosEstadoController(IBaseServicio<Cambio_Estado> cambioEstadoServicio,
                                       IBaseServicio<Produccion_Quesos> produccionQuesosServicio,
                                       IBaseServicio<Produccion_Quesos_Fundido> _produccionQuesoFundidoServicio,
                                       IBaseServicio<Produccion_Leche_Polvo> _produccionLechePolvoServicio,
                                       IBaseServicio<Estados> estadoServicio,
                                       IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio)
        {
            this.ProduccionQuesosServicio = produccionQuesosServicio;
            this.EstadoServicio = estadoServicio;
            this.CambioEstadoServicio = cambioEstadoServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.ProduccionLechePolvoServicio = _produccionLechePolvoServicio;
            this.ProduccionQuesosFundidoServicio = _produccionQuesoFundidoServicio;
        }
        // GET: Cambio_Estado
        public ActionResult Index()
        {
            var cambio_Estado = this.CambioEstadoServicio.Obtener().ToList();
            return View(cambio_Estado);
        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote)
        {
            Cambio_Estado cambioEstado = new Cambio_Estado();
            cambioEstado.Lote = Lote;
            cambioEstado.Fecha = DateTime.Now;

            var prodQueso = this.ProduccionQuesosServicio.Obtener((p) => p.Lote == Lote).FirstOrDefault();
            var prodFundido = this.ProduccionQuesosFundidoServicio.Obtener((p) => p.Lote == Lote).FirstOrDefault();
            var prodLeche = this.ProduccionLechePolvoServicio.Obtener((p) => p.Lote == Lote).FirstOrDefault();
            if (prodQueso != null)
            {
                cambioEstado.Codigo_Estado = (int)prodQueso.Codigo_Estado;
                cambioEstado.Codigo_Camara = prodQueso.Codigo_Camara;
            } else if (prodFundido != null)
            {
                cambioEstado.Codigo_Estado = (int)prodFundido.Codigo_Estado;
                cambioEstado.Codigo_Camara = prodFundido.Codigo_Camara;
            } else if (prodLeche != null)
            {
                cambioEstado.Codigo_Estado = (int)prodLeche.Codigo_Estado;
                cambioEstado.Codigo_Camara = prodLeche.Codigo_Camara;
            }

            List<SelectListItem> listadoCambiosEstado = new List<SelectListItem>();
            foreach (var item in this.EstadoServicio.Obtener())
            {
                listadoCambiosEstado.Add(new SelectListItem()
                {
                    Text = item.Descripcion,
                    Value = item.Codigo_Estado.ToString()
                });
            }
            ViewBag.listadoCambiosEstado = listadoCambiosEstado;
            ViewBag.usaStockCamara = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault().Usa_Stock_Camara;

            return View(cambioEstado);
        }

        [HttpPost]
        public ActionResult AddOrEdit(Cambio_Estado cambio_estado)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var prodQueso = this.ProduccionQuesosServicio.Obtener((p) => p.Lote == cambio_estado.Lote).FirstOrDefault();
                    var prodFundido = this.ProduccionQuesosFundidoServicio.Obtener((p) => p.Lote == cambio_estado.Lote).FirstOrDefault();
                    var prodLeche = this.ProduccionLechePolvoServicio.Obtener((p) => p.Lote == cambio_estado.Lote).FirstOrDefault();
                    if (prodQueso == null && prodFundido == null && prodLeche == null)
                        return Json(new { success = false, message = "No se encuentra producción con ese lote." }, JsonRequestBehavior.AllowGet);

                    cambio_estado.idUsuario = Sesion.UsuarioActual.idUsuario;
                    this.CambioEstadoServicio.Agregar(cambio_estado);

                    if (prodQueso != null)
                    {
                        prodQueso.Codigo_Estado = cambio_estado.Codigo_Estado;
                        if (cambio_estado.Codigo_Camara != null)
                            prodQueso.Codigo_Camara = cambio_estado.Codigo_Camara;

                        this.ProduccionQuesosServicio.Editar(prodQueso);
                    }
                    if (prodFundido != null)
                    {
                        prodFundido.Codigo_Estado = cambio_estado.Codigo_Estado;
                        if (cambio_estado.Codigo_Camara != null)
                            prodFundido.Codigo_Camara = cambio_estado.Codigo_Camara;

                        this.ProduccionQuesosFundidoServicio.Editar(prodFundido);
                    }
                    if (prodLeche != null)
                    {
                        prodLeche.Codigo_Estado = cambio_estado.Codigo_Estado;
                        if (cambio_estado.Codigo_Camara != null)
                            prodLeche.Codigo_Camara = cambio_estado.Codigo_Camara;

                        this.ProduccionLechePolvoServicio.Editar(prodLeche);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

    }
}
