﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using MVC_Produccion.ContextoProduccion;
using MVC_Produccion.Sesiones;

namespace MVC_Produccion.Controllers
{
    //TODO ver que hacer
    public class IniciosSesionController : Controller
    {
        private Entities db = new Entities();

        public ActionResult Index()
        {
            return View(db.Sesiones.ToList());
        }

        public ActionResult GetData()
        {
            using (Entities dbContext = new Entities())
            {
                OpcionesTrabajo configuraciones = new OpcionesTrabajo();
                configuraciones = db.OpcionesTrabajo.FirstOrDefault();

                if (configuraciones.Utiliza_Login == true)
                {
                    if (Sesion.UsuarioActual != null)
                    {
                        if(Sesion.UsuarioActual.Rol.ToLower() == "administrador")
                        {
                            var listado = from s in db.Sesiones
                                          join u in db.Usuarios on s.idUsuario equals u.idUsuario
                                          select new
                                          {
                                              s.idUsuario,
                                              u.Usuario,
                                              u.Contraseña,
                                              s.Fecha
                                          };

                            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, message = "No posee permisos para ver las Sesiones." }, JsonRequestBehavior.AllowGet);
                        }   
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account");

                    }
                }
                else
                {
                    var listado = from s in db.Sesiones
                                  join u in db.Usuarios on s.idUsuario equals u.idUsuario
                                  select new
                                  {
                                      s.idUsuario,
                                      u.Usuario,
                                      u.Contraseña,
                                      s.Fecha
                                  };

                    return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(ContextoProduccion.Sesiones sesion)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (Entities dbContext = new Entities())
                {
                    OpcionesTrabajo configuraciones = new OpcionesTrabajo();
                    configuraciones = db.OpcionesTrabajo.FirstOrDefault();

                    if (configuraciones.Utiliza_Login == true)
                    {
                        if (Sesion.UsuarioActual != null)
                        {
                            if (Sesion.UsuarioActual.Rol.ToLower() == "administrador")
                            {
                                try
                                {
                                    ContextoProduccion.Sesiones sesionActual = new ContextoProduccion.Sesiones();
                                    sesionActual = db.Sesiones.Where(s => s.idUsuario == sesion.idUsuario).FirstOrDefault();

                                    db.Sesiones.Remove(sesionActual);

                                    db.SaveChanges();
                                    scope.Complete();
                                    return Json(new { message = "Sesión cerrada exitosamente.", success = true }, JsonRequestBehavior.AllowGet);
                                }
                                catch (Exception ex)
                                {
                                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { success = false, message = "No posee permisos para ver las Sesiones." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return RedirectToAction("Login", "Account");

                        }
                    }
                    else
                    {
                        try
                        {
                            ContextoProduccion.Sesiones sesionActual = new ContextoProduccion.Sesiones();
                            sesionActual = db.Sesiones.Where(s => s.idUsuario == sesion.idUsuario).FirstOrDefault();

                            db.Sesiones.Remove(sesionActual);

                            db.SaveChanges();
                            scope.Complete();
                            return Json(new { message = "Sesión cerrada exitosamente.", success = true }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
