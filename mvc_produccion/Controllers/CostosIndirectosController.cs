﻿using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class CostosIndirectosController : Controller
    {
        private IBaseServicio<Costos_Indirectos> CostosIndirectosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Monedas> MonedasServicio;
        private OpcionesTrabajo Configuraciones;

        public CostosIndirectosController(IBaseServicio<Costos_Indirectos> costosIndirectosServicio,
                                          IBaseServicio<Permisos> permisosServicio,
                                          IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                          IBaseServicio<Monedas> monedasServicio)
        {
            this.CostosIndirectosServicio = costosIndirectosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.MonedasServicio = monedasServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            var listado = this.CostosIndirectosServicio.Obtener().Select(ci => new
            {
                ci.Codigo_Costo_Indirecto,
                ci.Codigo_Interno_Costo_Indirecto,
                ci.Descripcion,
                ci.Porcentaje,
                ci.Porcentaje_Aplica,
                Precio = ci.Precio * (decimal)(ci.Monedas != null ? ci.Monedas.intercambio : 1),
                ci.Precio_Fijo,
                ci.Moneda
            }).ToList();

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOne(string Codigo_Costo_Indirecto)
        {
            try
            {
                var costo = this.CostosIndirectosServicio.Obtener((c) => (c).Codigo_Costo_Indirecto == Codigo_Costo_Indirecto).Select(c => new
                {
                    c.Codigo_Costo_Indirecto,
                    c.Codigo_Interno_Costo_Indirecto,
                    c.Descripcion,
                    c.Porcentaje,
                    c.Porcentaje_Aplica,
                    Precio = c.Precio * (decimal)(c.Monedas != null ? c.Monedas.intercambio : 1),
                    c.Precio_Fijo,
                    c.Moneda
                }).SingleOrDefault();
                if (costo == null)
                    return Json(new { success = false, mensaje = "Error, no se pudo obtener el costo." }, JsonRequestBehavior.AllowGet);

                return Json(new { data = costo, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult AddOrEdit(string Codigo_Costo_Indirecto = "")
        {
            ViewBag.Monedas = MonedasServicio.Obtener().Select(m => new { m.nombre }).ToList();
            if (Codigo_Costo_Indirecto == "")
            {
                string ultimoRegistro = "";

                Costos_Indirectos costo = new Costos_Indirectos();

                if (this.CostosIndirectosServicio.Obtener().Count() > 0)
                {
                    ultimoRegistro = this.CostosIndirectosServicio.Obtener().Max(c => c.Codigo_Costo_Indirecto);
                    costo.Codigo_Costo_Indirecto = (int.Parse(ultimoRegistro) + 1).ToString(new string('0', this.Configuraciones.NumeroDigitos));
                }
                else
                    costo.Codigo_Costo_Indirecto = (1).ToString(new string('0', this.Configuraciones.NumeroDigitos));

                costo.Porcentaje_Aplica = "Insumos";

                return View(costo);
            }

            else
                return View(this.CostosIndirectosServicio.Obtener((x) => (x).Codigo_Costo_Indirecto == Codigo_Costo_Indirecto).SingleOrDefault());
        }

        [HttpPost]
        public ActionResult AddOrEdit(Costos_Indirectos Costo_Indirecto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                string mensaje = "Registro modificado satisfactoriamente.";

                if (Costo_Indirecto.Codigo_Interno_Costo_Indirecto == 0)
                {
                    var existeCosto = this.CostosIndirectosServicio.Obtener((e) => (e).Codigo_Costo_Indirecto == Costo_Indirecto.Codigo_Costo_Indirecto).SingleOrDefault();
                    if (existeCosto != null)
                        return Json(new { success = false, message = "Ya existe un registro con ese Código." }, JsonRequestBehavior.AllowGet);

                    this.CostosIndirectosServicio.Agregar(Costo_Indirecto);
                    mensaje = "Registro guardado satisfactoriamente.";
                }
                else
                {
                    this.CostosIndirectosServicio.Editar(Costo_Indirecto);
                    //Costos_Indirectos objCostosIndirectos = new Costos_Indirectos();
                    //Detalle_Formula objDetalleFormula = new Detalle_Formula();
                }
                scope.Complete();
                return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimo = this.CostosIndirectosServicio.ObtenerConRawSql("SELECT top 1 * FROM Costos_Indirectos ORDER BY Codigo_Costo_Indirecto DESC").FirstOrDefault();

                return Json(new { data = ultimo.Codigo_Costo_Indirecto, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }
        private string GetFullExceptionMessage(Exception ex)
        {
            if (ex == null) return string.Empty;
            return $"{ex.Message} {GetFullExceptionMessage(ex.InnerException)}";
        }


        [HttpPost]
        public ActionResult Delete(string Codigo_Costo_Indirecto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Costos_Indirectos est = this.CostosIndirectosServicio.Obtener((x) => (x).Codigo_Costo_Indirecto == Codigo_Costo_Indirecto).SingleOrDefault();
                    if (est == null)
                        return Json(new { success = false, message = "Error al eliminar, no se encontro el registro." }, JsonRequestBehavior.AllowGet);
                    this.CostosIndirectosServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var fullExceptionMessage = GetFullExceptionMessage(ex);
                    return Json(new { success = false, message = ex.Message, fullExceptionMessage = fullExceptionMessage }, JsonRequestBehavior.AllowGet);
                }

            }
        }
    }
}
