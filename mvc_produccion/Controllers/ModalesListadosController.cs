﻿using MVC_Produccion.ContextoGastos;
using MVC_Produccion.ContextoProduccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Produccion.Controllers
{
    // TODO ver que hacer
    public class ModalesListadosController : Controller
    {
        private GastosEntities dbGastos = new GastosEntities();
        private Entities db = new Entities();
        private IBaseServicio<AccesoDatos.Contexto.Produccion.OpcionesTrabajo> OpcionesDeTrabajoServicio;
        private IBaseServicio<AccesoDatos.Contexto.Produccion.Permisos> PermisosServicio;
        private AccesoDatos.Contexto.Produccion.OpcionesTrabajo Configuraciones;
        public ModalesListadosController(IBaseServicio<AccesoDatos.Contexto.Produccion.OpcionesTrabajo> opcionesDeTrabajoServicio,
                                        IBaseServicio<AccesoDatos.Contexto.Produccion.Permisos> permisosServicio)
        {
            this.PermisosServicio = permisosServicio;
            this.OpcionesDeTrabajoServicio = opcionesDeTrabajoServicio;
            this.Configuraciones = this.OpcionesDeTrabajoServicio.Obtener().FirstOrDefault();
        }

        // GET: ModalesListados
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Modal_Stock_Insumo_Lote(string insumo, int fabrica)
        {
            var lotes = db.Stock_Insumo_Por_Lote(insumo, fabrica).Select(l => new
            {
                Lote = l.Lote,
                Cantidad = l.Cantidad,
                Kilos_Litros = l.Kilos_Litros
            }).ToList();
            ViewBag.descripcion = db.Insumos.SingleOrDefault(i => i.Codigo_Insumo == insumo).Descripcion;
            ViewBag.lotes = lotes;
            ViewBag.codInsumo = insumo;
            return View();
        }


        [HttpGet]
        public ActionResult Modal_Sucursales()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Modal_Rubros()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Modal_Lineas()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Modal_Lavados()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Modal_Proveedores_Listado()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Modal_Proveedores()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Modal_Tipos_Proveedores()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Costos_Indirectos()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Codigos_Comprobante()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Insumos(string loadfrom = "")
        {            
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Productos()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Productos_Ventas()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Silos()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Envasados()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Fabricas()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_LotesSinAnalizar()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Analisis()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Modal_Formulas()
        {
            ViewBag.Utiliza_Login = this.Configuraciones.Utiliza_Login;
            return View();
        }
        [HttpGet]
        public ActionResult Modal_Formulas_Envasado()
        {
            return View();
        }

        public ActionResult GetCodigosComprobantes(string Codigo_Comprobante = "")
        {
            if (Codigo_Comprobante == "")
            {
                var listado = from cc in dbGastos.Codigo_Comprobante
                              orderby cc.DESCRIPCION
                              select new
                              {
                                  Codigo_Comprobante = cc.CODIGO,
                                  Descripcion = cc.DESCRIPCION

                              };

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (int.Parse(Codigo_Comprobante) < 0)
                {
                    return Json(new { success = false, message = "Error, no existe registro con Código negativo." }, JsonRequestBehavior.AllowGet);
                }

                else
                {
                    if (dbGastos.Codigo_Comprobante.Where(l => l.CODIGO == Codigo_Comprobante).FirstOrDefault() == null)
                    {
                        return Json(new { message = "Error, no existe registro con el Código ingresado.", success = false }, JsonRequestBehavior.AllowGet);
                    }

                    var Comprobante = from cc in dbGastos.Codigo_Comprobante
                                      where cc.CODIGO == Codigo_Comprobante
                                      orderby cc.DESCRIPCION
                                      select new
                                      {
                                          Codigo_Comprobante = cc.CODIGO,
                                          Descripcion = cc.DESCRIPCION

                                      };

                    return Json(new { data = Comprobante, success = true }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}