﻿using System.Data;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Ventas;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class SucursalesController : Controller
    {
        private IBaseServicio<Sucursales_Propias> SucursalesServicio;

        public SucursalesController(IBaseServicio<Sucursales_Propias> sucursalesServicio)
        {
            this.SucursalesServicio = sucursalesServicio;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetData()
        {
            var listado = this.SucursalesServicio.Obtener().Select(s => new
            {
                s.Numero,
                s.Nombre,
                s.Confirmar_Recepcion_Automaticamente
            });
            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddOrEdit(int numero, string nombre, bool confirmar_recepcion_automaticamente)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var mensaje = "Se agrego correctamente";
                    var existe = this.SucursalesServicio.Obtener((s) => s.Numero == numero).SingleOrDefault();
                    if (existe != null)
                    {
                    
                        existe.Numero = numero;
                        existe.Nombre = nombre;
                        existe.Confirmar_Recepcion_Automaticamente = confirmar_recepcion_automaticamente;
                        this.SucursalesServicio.Editar(existe);
                        mensaje = "Se modifico correctamente";
                    }
                    else
                        this.SucursalesServicio.Agregar(new Sucursales_Propias()
                        {
                            Nombre = nombre,
                            Numero = numero,
                            Confirmar_Recepcion_Automaticamente = confirmar_recepcion_automaticamente
                        });
                    scope.Complete();
                    return Json(new
                    {
                        message = mensaje,
                        success = true
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (System.Exception ex)
                {
                    return Json(new
                    {
                        message = "Ocurrio un error",
                        error = ex,
                        success = false
                    }, JsonRequestBehavior.AllowGet);
                }

            }

        }

        public ActionResult Eliminar(int numero)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var eliminar = this.SucursalesServicio.Obtener((s) => s.Numero == numero).SingleOrDefault();
                    this.SucursalesServicio.Borrar(eliminar);
                    scope.Complete();
                    return Json(new
                    {
                        message = "Se elimino Correctamente",
                        success = true
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (System.Exception ex)
                {
                    return Json(new
                    {
                        message = "Ocurrio un error",
                        error = ex,
                        success = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        
    }
}
