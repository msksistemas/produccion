﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Gastos;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using MVC_Produccion.Models;
using System.Threading.Tasks;
using AccesoDatos.Contexto.Produccion.SP;
using System.Net.Http;
using System.Globalization;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class AjustesInsumosController : Controller
    {
        private IBaseServicio<Ajustes_Insumos> AjusteInsumosServicio;
        private IBaseServicio<Detalle_Ajuste_Insumos> DetalleAjusteInsumosServicio;
        private IBaseServicio<Insumos> InsumosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesDeTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<Provee_Cta_Cte> ProveedorServicio;
        private IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> ProductosVentasServicio;
        private OpcionesTrabajo Configuraciones { get; set; }
        private readonly HttpService httpClient;

        public AjustesInsumosController(IBaseServicio<Ajustes_Insumos> ajusteInsumosServicio,
                                        IBaseServicio<OpcionesTrabajo> opcionesDeTrabajoServicio,
                                        IBaseServicio<Insumos> insumosServicio,
                                        IBaseServicio<Permisos> permisosServicio,
                                        IBaseServicio<Detalle_Ajuste_Insumos> detalleAjusteInsumosServicio,
                                        IBaseServicio<Provee_Cta_Cte> ProveedorServicio,
                                        IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> productosVentasServicio,
                                        HttpService httpClient)
        {
            this.AjusteInsumosServicio = ajusteInsumosServicio;
            this.DetalleAjusteInsumosServicio = detalleAjusteInsumosServicio;
            this.OpcionesDeTrabajoServicio = opcionesDeTrabajoServicio;
            this.PermisosServicio = permisosServicio;
            this.InsumosServicio = insumosServicio;
            this.ProveedorServicio = ProveedorServicio;
            this.Configuraciones = this.OpcionesDeTrabajoServicio.Obtener().SingleOrDefault();
            this.ProductosVentasServicio = productosVentasServicio;
            this.httpClient = httpClient;
        }

        dbProdSP dbAccesoDatos = new dbProdSP();
        public ActionResult Index()
        {
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="No Histórica", Value="false"},
                                new SelectListItem() {Text="Histórica", Value="true"}
                            };

            ViewBag.ListadoEstados = ListadoEstados;

            return View();
        }

        public ActionResult GetData(int Codigo_Interno_Ajuste = 0, bool estado = false)
        {
            if (Codigo_Interno_Ajuste == -1)
            {
                var listado = this.AjusteInsumosServicio.Obtener((ai) => ai.Es_Traspaso == false && ai.Historico == estado).Select(l => new
                {
                    l.Codigo_Interno_Ajuste,
                    l.Comentario,
                    l.Descuento,
                    l.Estado,
                    l.Es_Traspaso,
                    l.Fabrica,
                    l.Fabrica_Origen,
                    l.Fecha,
                    l.Historico,
                    l.idUsuario,
                    l.Motivo_Rechazo,
                    l.Numero_Comprobante,
                    l.Tipo_Movimiento
                }).ToList();

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var listado = this.AjusteInsumosServicio.Obtener((ai) => ai.Codigo_Interno_Ajuste == Codigo_Interno_Ajuste);
                var ajuste = listado.SingleOrDefault();
                if (ajuste != null)
                {
                    var listadoEnviar = listado.Select(l => new
                    {
                        l.Codigo_Interno_Ajuste,
                        l.Comentario,
                        l.Descuento,
                        l.Estado,
                        l.Es_Traspaso,
                        l.Fabrica,
                        l.Fabrica_Origen,
                        l.Fecha,
                        l.Historico,
                        l.idUsuario,
                        l.Motivo_Rechazo,
                        l.Numero_Comprobante,
                        l.Tipo_Movimiento
                    });
                    var detalles = ajuste.Detalle_Ajuste_Insumos.Select(det => new
                    {
                        det.Codigo_Interno_Detalle,
                        det.Codigo_Insumo,
                        Descripcion_Insumo = det.Insumos.Descripcion,
                        det.Numero_Producto,
                        Descripcion_Producto_Venta = ProductosVentasServicio.Obtener((pv) => pv.Numero_de_Producto == det.Numero_Producto)
                                .Select(pv => pv.Descripcion)
                                .FirstOrDefault() ?? "",
                        det.Cantidad,
                        det.Kilos_Litros,
                        det.Codigo_Interno_Ajuste,
                        det.Lote,
                        det.Fecha_Vencimiento,
                        det.Ph,
                        det.Humedad,
                        det.Precio_Compra,
                        det.Codigo_Proveedor,
                        det.Insumos.Precio_Por,
                        det.Insumos.Unidad_Medida,
                        det.Insumos.Masa
                    }).ToList();
                    return Json(new { data = listadoEnviar, detalles, success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { data = listado, detalles = 0, success = true }, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult GetOneProveedor_AI(int codigoProveedor)
        {
            if (codigoProveedor > 0)
            {
                var Proveedor = this.ProveedorServicio.Obtener((i) => i.CODIGO == codigoProveedor).FirstOrDefault();

                if (Proveedor != null)
                {
                    return Json(new
                    {
                        data = new
                        {
                            Proveedor.CODIGO,
                            Proveedor.RAZON_SOCIAL
                        },
                        success = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, mensaje = "Error, el proveedor no puede ser nulo." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, mensaje = "Error, no existe un proveedor con Código negativo." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            int fabrica = 0;

            if (Sesion.UsuarioActual.Fabrica != null)
                fabrica = Sesion.UsuarioActual.Fabrica.Value;

            List<SelectListItem> TiposMovimiento = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Ingreso Mercadería", Value="Ingreso Mercadería"},
                new SelectListItem() {Text="Factura Cuenta Corriente", Value="Factura Cuenta Corriente"},
                new SelectListItem() {Text="N/C Por Devolución", Value="N/C Por Devolución"},
                new SelectListItem() {Text="Factura Contado", Value="Factura Contado"},
                new SelectListItem() {Text="Mercadería S/Cargo", Value="Mercadería S/Cargo"},
                new SelectListItem() {Text="Devolución Al Proveedor", Value="Devolución Al Proveedor"},
                new SelectListItem() {Text="Baja Por Deterioro", Value="Baja Por Deterioro"},
                new SelectListItem() {Text="Salida Por Producción", Value="Salida Por Producción"},
                new SelectListItem() {Text="Salida Por Sucursal", Value="Salida Por Sucursal"},
                new SelectListItem() {Text="Ajuste +", Value="Ajuste +"},
                new SelectListItem() {Text="Ajuste -", Value="Ajuste -"},
                new SelectListItem() {Text="Salida Producto De Ventas", Value="Salida Producto De Ventas"},
            };
            ViewBag.TiposMovimiento = TiposMovimiento;
            ViewBag.Fabrica_Enabled = fabrica != -1 ? "disabled" : "enabled";
            ViewBag.Muestra_Costos = Sesion.UsuarioActual.Muestra_Costos != null || Sesion.UsuarioActual != null ? Sesion.UsuarioActual.Muestra_Costos.Value : false;
            if (id == 0)
            {
                Ajustes_Insumos ajustes = new Ajustes_Insumos();
                ajustes.Fecha = DateTime.Now;
                if (fabrica != -1)
                    ajustes.Fabrica = fabrica;
                return View(ajustes);
            }
            else
                return View(this.AjusteInsumosServicio.Obtener((ai) => ai.Codigo_Interno_Ajuste == id).SingleOrDefault());
        }

        [HttpPost]
        public ActionResult AddOrEdit(Ajustes_Insumos Ajustes_Insumos)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) 
            {
                try
                {
                    Ajustes_Insumos.Es_Traspaso = false;
                    Ajustes_Insumos.Fabrica_Origen = 0;
                    var mensaje = "Registro modificado satisfactoriamente.";
                    if (Ajustes_Insumos.Codigo_Interno_Ajuste == 0)
                    {
                        bool existe = this.AjusteInsumosServicio.Obtener((ai) => ai.Es_Traspaso == false
                                                                              && ai.Numero_Comprobante == Ajustes_Insumos.Numero_Comprobante
                                                                              && ai.Prefijo == Ajustes_Insumos.Prefijo).SingleOrDefault() != null ? true : false;
                        if (existe)
                            return Json(new { success = false, message = "Ya existe Ajuste con ese N.Comprob y Prefijo." }, JsonRequestBehavior.AllowGet);
                        Ajustes_Insumos.Historico = false;
                        foreach (var item in Ajustes_Insumos.Detalle_Ajuste_Insumos)
                        {
                            if(Ajustes_Insumos.Tipo_Movimiento == "Salida Producto De Ventas")
                            {
                                if(ProductosVentasServicio.Obtener((pv) => pv.Numero_de_Producto == item.Numero_Producto).Count == 0)
                                    return Json(new { success = false, message = "No se encontró uno o más de los productos de ventas cargados, revise el detalle del ajuste." }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                item.Numero_Producto = null;
                            }
                            var insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == item.Codigo_Insumo).SingleOrDefault();
                            if (double.IsNaN(item.Precio_Compra))
                            {
                                item.Precio_Compra = Convert.ToDouble(insumo.Precio_Ultima_Compra.HasValue ? insumo.Precio_Ultima_Compra.Value : 0);
                                if (item.Precio_Compra == 0)
                                {
                                    item.Precio_Compra = Convert.ToDouble(insumo.Precio_Lista.HasValue ? insumo.Precio_Lista.Value : 0);
                                }

                            }
                        }

                        this.AjusteInsumosServicio.Agregar(Ajustes_Insumos);
                        mensaje = "Registro guardado satisfactoriamente.";
                    }
                    else
                    {
                        var detallesViejos = this.DetalleAjusteInsumosServicio.Obtener((detViejo) => detViejo.Codigo_Interno_Ajuste == Ajustes_Insumos.Codigo_Interno_Ajuste);

                        foreach (var detalleViejo in detallesViejos)
                        {
                            this.DetalleAjusteInsumosServicio.Borrar(detalleViejo);
                        }
                        foreach (var detalleAgregar in Ajustes_Insumos.Detalle_Ajuste_Insumos)
                        {
                            this.DetalleAjusteInsumosServicio.Agregar(detalleAgregar);
                        }

                        this.AjusteInsumosServicio.Editar(Ajustes_Insumos);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (DbEntityValidationException e)
                {
                    return Json(new { success = false, message = e.EntityValidationErrors.FirstOrDefault().ValidationErrors.FirstOrDefault().ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Ajuste)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) { 
                try
                {
                    Ajustes_Insumos est = this.AjusteInsumosServicio.Obtener((x) => x.Codigo_Interno_Ajuste == Codigo_Ajuste).SingleOrDefault();
                    this.AjusteInsumosServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }            
            }
        }

        //static List<string> tiposAjustesPositivo = new List<string> { "Ingreso", "Ingreso Mercadería", "Factura Cuenta Corriente", "Factura Contado", "Ajuste +", "Traspaso" };
        //static List<string> tiposAjustesNegativo = new List<string> { "N/C Por Devolución", "Devolución al Proveedor", "Baja Por Deterioro", "Salida Por Producción", "Salida Por Sucursal", "Ajuste -" , "Mercadería S/Cargo", "Egreso", "Salida Producto De Ventas" };
        public async Task<ActionResult> Pasar_A_Historico(int Codigo_Interno_Ajuste = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    //int tipoAjuste = 2;
                    Ajustes_Insumos ajuste = AjusteInsumosServicio.Obtener((x) => x.Codigo_Interno_Ajuste == Codigo_Interno_Ajuste).FirstOrDefault();
                    if(ajuste.Tipo_Movimiento == "Salida Producto De Ventas")
                    {
                        dbAccesoDatos.Movimiento_Stock_Salida_Ventas(ajuste.Codigo_Interno_Ajuste);
                        //if (httpClient.EsSanSatur())
                        //{
                        //    HttpResponseMessage response = await httpClient.AjusteStockSalidaProdVentas(Codigo_Interno_Ajuste);
                        //    if (!response.IsSuccessStatusCode)
                        //    {
                        //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                        //    }
                        //}
                    }
                    ajuste.Fabrica_Origen = 0;
                    ajuste.Es_Traspaso = false;
                    ajuste.Historico = true;
                    AjusteInsumosServicio.Editar(ajuste);

                    //if (tiposAjustesPositivo.Contains(ajuste.Tipo_Movimiento))
                    //    tipoAjuste = 1;
                    //else if (tiposAjustesNegativo.Contains(ajuste.Tipo_Movimiento))
                    //    tipoAjuste = 2;

                    foreach(Detalle_Ajuste_Insumos det in ajuste.Detalle_Ajuste_Insumos)
                    {
                        Insumos insumo = InsumosServicio.Obtener((i) => i.Codigo_Insumo == det.Codigo_Insumo).FirstOrDefault();
                        insumo.Fecha_Ultima_Compra = ajuste.Fecha;
                        insumo.Precio_Ultima_Compra = (decimal?)det.Precio_Compra;
                        InsumosServicio.Editar(insumo);

                        //if (httpClient.EsSanSatur())
                        //{
                        //    detallesInsumos.Add(new IMovimientoInsumo
                        //    { 
                        //        Codigo_Insumo = det.Codigo_Insumo.Trim(),
                        //        Cantidad = (int)(det.Cantidad ?? 0),
                        //        Kilos_Litros = (float)(det.Kilos_Litros ?? 0)
                        //    });
                        //    if(ajuste.Tipo_Movimiento == "Salida Producto De Ventas")
                        //        detallesProductosVenta.Add(new IMovimientoProdVenta
                        //        {
                        //            Numero_Producto = (det.Numero_Producto ?? "").Trim(),
                        //            Cantidad = (int)(det.Cantidad ?? 0),
                        //            Kilos_Litros = (float)(det.Kilos_Litros ?? 0)
                        //        });
                        //}
                    }

                    if(ajuste.Tipo_Movimiento == "Salida Producto De Ventas")
                    {
                        dbAccesoDatos.Movimiento_Stock_Salida_Ventas(ajuste.Codigo_Interno_Ajuste);
                    }

                    //if (httpClient.EsSanSatur())
                    //{
                    //    if(ajuste.Tipo_Movimiento == "Salida Producto De Ventas")
                    //    {
                    //        HttpResponseMessage responseProdsVenta = await httpClient.AjusteStockProductoVentas(
                    //                    detallesProductosVenta,
                    //                    DateTime.Now,
                    //                    "Salida de insumos como productos de venta",
                    //                    1);
                    //        if (!responseProdsVenta.IsSuccessStatusCode)
                    //        {
                    //            return Json(new { success = false, message = responseProdsVenta.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                    //        }
                    //    }

                    //    HttpResponseMessage responseInsumos = await httpClient.AjusteStockInsumos(
                    //                    detallesInsumos, 
                    //                    DateTime.Now,
                    //                    tipoAjuste);
                    //    if (!responseInsumos.IsSuccessStatusCode)
                    //    {
                    //        return Json(new { success = false, message = responseInsumos.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                    //    }
                    //}
                    scope.Complete();
                    return Json(new { success = true, message = "Actualizado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
