﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Gastos;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class CondicionesPagoController : Controller
    {
        private IBaseServicio<Cond_Pago> CondicionPagoSercicio;

        public CondicionesPagoController(IBaseServicio<Cond_Pago> condicionPagoServicio)
        {
            this.CondicionPagoSercicio = condicionPagoServicio;
        }
        public ActionResult Index()
        {
            return View(this.CondicionPagoSercicio.Obtener());
        }

        public ActionResult GetData(int Codigo_Condicion = -1)
        {
            if (Codigo_Condicion == -1)
            {
                var listado = this.CondicionPagoSercicio.Obtener().Select(cp => new
                {
                    Codigo_Condicion = cp.CODIGO,
                    Descripcion = cp.DESCRIPCION,
                    Dias = cp.DIAS1
                });

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var condicion = this.CondicionPagoSercicio.Obtener((cp) => cp.CODIGO == Codigo_Condicion);
                if (condicion == null)
                    return Json(new { message = "Error, no existe registro con el Código ingresado.", success = false }, JsonRequestBehavior.AllowGet);

                return Json(new { data = condicion, success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetOne(int Codigo)
        {
           
                try
                {
                    var productoNuevo = this.CondicionPagoSercicio.Obtener(x => x.CODIGO == Codigo).ToList();

                    return Json(new { data = productoNuevo, success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
           
        }

        [HttpGet]
        public ActionResult Modal_Listado()
        {
            return View();
        }

        public ActionResult AddOrEdit(int codigo, string descipcion, int cant_dias)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var condicion = new Cond_Pago
                {
                    CODIGO = codigo,
                    DESCRIPCION = descipcion,
                    DIAS1 = cant_dias
                };
                try
                {
                    var mensaje = "Se agrego Correctamente";
                    if (condicion.CODIGO == -1)
                    {
                        var max = 1;
                        if (this.CondicionPagoSercicio.Obtener().Count > 0)
                            max = this.CondicionPagoSercicio.Obtener().Max(c => c.CODIGO + 1);
                        condicion.CODIGO = max;
                        this.CondicionPagoSercicio.Agregar(condicion);
                    }
                    else
                    {
                        this.CondicionPagoSercicio.Editar(condicion);
                        mensaje = "Se modifico Correctamente";
                    }
                    scope.Complete();
                    return Json(new { ok = true, mensaje = mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { ok = false, mensaje = ex.Message, error = ex }, JsonRequestBehavior.AllowGet);
                }
            }

        }
        public ActionResult eliminar(int Codigo_Condicion)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var eliminar = this.CondicionPagoSercicio.Obtener((cp) => (cp).CODIGO == Codigo_Condicion).SingleOrDefault();
                    if (eliminar != null)
                    {
                        this.CondicionPagoSercicio.Borrar(eliminar);
                        scope.Complete();
                        return Json(new { ok = true, mensaje = "Se elimino Correctamente." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { ok = false, mensaje = "No se encontro Condicion." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { ok = false, mensaje = ex.Message, error = ex }, JsonRequestBehavior.AllowGet);
                }

            }
        }
       
    }
}
