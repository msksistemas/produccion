﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Gastos;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class ProveedoresController : Controller
    {
        private IBaseServicio<Provee_Cta_Cte> ProveedoresServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Cond_Pago> CondicionPagoServicio;
        private IBaseServicio<Tipo_Proveedor> TipoProveedorServicio;
        dbProdSP dbAccesoDatos = new dbProdSP();

        private OpcionesTrabajo Configuraciones;

        public ProveedoresController(IBaseServicio<Provee_Cta_Cte> proveedoresServicio,
                                      IBaseServicio<Permisos> permisosServicio,
                                      IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                      IBaseServicio<Cond_Pago> condPagoServicio,
                                      IBaseServicio<Tipo_Proveedor> tipoProveedorServicio)
        {
            this.ProveedoresServicio = proveedoresServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.CondicionPagoServicio = condPagoServicio;
            this.TipoProveedorServicio = tipoProveedorServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            return View(this.ProveedoresServicio.Obtener());
        }

        public ActionResult GetData(int estado = 0)
        {
            var listado = dbAccesoDatos.ObtenerProveedores(estado).Select(p => new
            {
                Codigo_Proveedor = p.Codigo,
                Razon_Social = p.RazonSocial,
                Nro_Tipo_Proveedor = p.NroTipoProveedor,
                Tipo_Proveedor = p.TipoProveedor,
                Saldo = 0,
                PREFIJO = p.Prefijo,
                TIPO_FACTURA = p.TipoFactura
            }).ToList();
            

            return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOne(int Codigo_Proveedor)
        {
            if (Codigo_Proveedor > 0)
            {
                try
                {
                    var proveedor = this.ProveedoresServicio.Obtener((i) => i.CODIGO == Codigo_Proveedor).FirstOrDefault();
                    var dias_condicion = 0;
                    if (proveedor == null)
                    {
                        throw new Exception();
                    }
                    if (proveedor.Condicion_pagos != null)
                    {
                        var cond_pago = this.CondicionPagoServicio.Obtener((c) => c.CODIGO == proveedor.Condicion_pagos).SingleOrDefault();
                        int? dia = null;
                        if (cond_pago != null)
                            dia = cond_pago.DIAS1;
                        if (dia != null)
                            dias_condicion = dia.Value;
                    }

                    return Json(new
                    {
                        data = new
                        {
                            proveedor.Activo,
                            proveedor.Categoria_Ganancias,
                            proveedor.Categoria_IB,
                            proveedor.CELULAR,
                            proveedor.CODIGO,
                            proveedor.Codigo_Comprob,
                            proveedor.Codigo_Postal,
                            proveedor.Condicion_pagos,
                            proveedor.CONTACTO,
                            proveedor.CUIT,
                            proveedor.DIRECCION,
                            proveedor.EMAIL,
                            proveedor.Exceptuado,
                            proveedor.Extranjero,
                            proveedor.FAX,
                            proveedor.Identificacion_Extranjera,
                            proveedor.Inscripto_Ganancias,
                            proveedor.IVA,
                            proveedor.LINEA1,
                            proveedor.LOCALIDAD,
                            proveedor.MEMO,
                            proveedor.Nombre_Fantasia,
                            proveedor.Nro_Calle,
                            proveedor.Nro_Imputacion_OP,
                            proveedor.Nro_Imputación,
                            proveedor.Nro_IngresosBrutos,
                            proveedor.Particip_Total_Compras,
                            proveedor.Porcentaje_Retencion_IG,
                            proveedor.Porcentaje_Retencion_Iva,
                            proveedor.Porcent_Total_Compras,
                            proveedor.PREFIJO,
                            proveedor.RAZON_SOCIAL,
                            proveedor.RETENC_GANANCIA,
                            proveedor.RETENC_ING_BRUTOS,
                            proveedor.RETENC_IVA,
                            proveedor.TIPO_FACTURA,
                            proveedor.TIPO_PROVEEDOR
                        },
                        dias_condicion = dias_condicion,
                        success = true
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, mensaje = "Error, no existe registro con el Código ingresado." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, mensaje = "Error, no existe registro con Código negativo." }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Inicio(string mensaje = "", bool ok = true)
        {
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
            {
                new SelectListItem() {Text="Activo", Value="0"},
                new SelectListItem() {Text="Inactivo", Value="1"}
            };
            ViewBag.ListadoEstados = ListadoEstados;

            if (ok)
                ViewBag.Resultado = mensaje;
            else
                ViewBag.error = mensaje;

            return View();
        }

        [HttpGet]
        public ActionResult ObtenerProveedores(int estado = 0)
        {
            var listado = dbAccesoDatos.ObtenerProveedores(estado);

            return Json(new
            {
                success = true,
                mensaje = "",
                data = listado
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ObtenerTiposProveedores()
        {
            var tiposProv = dbAccesoDatos.ObtenerTiposProveedores();

            return Json(new
            {
                success = true,
                mensaje = "",
                data = tiposProv
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOneTipoProveedor(int codigoTipoProveedor)
        {
            if (codigoTipoProveedor > 0)
            {
                var tipoProveedor = this.TipoProveedorServicio.Obtener((i) => i.CODIGO == codigoTipoProveedor).FirstOrDefault();

                if (tipoProveedor != null)
                {
                    return Json(new
                    {
                        data = new
                        {
                            tipoProveedor.CODIGO,
                            tipoProveedor.DESCRIPCION
                        },
                        success = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, mensaje = "Error, el tipo de proveedor no puede ser nulo." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, mensaje = "Error, no existe tipo de proveedor con Código negativo." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int codigo = 0)
        {   try
            {
                List<SelectListItem> ListadoIva = new List<SelectListItem>()
                {
                    new SelectListItem() {Text="Inscripto", Value="Inscripto"},
                    new SelectListItem() {Text="No Inscripto", Value="No Inscripto"},
                    new SelectListItem() {Text="Exento", Value="Exento"},
                    new SelectListItem() {Text="Monotributo", Value="Monotributo"},
                    new SelectListItem() {Text="No Alcanzado", Value="No Alcanzado"},
                };

                ViewBag.ListadoIva = ListadoIva;

                List<SelectListItem> ListadoTipoFactura = new List<SelectListItem>()
                {
                    new SelectListItem() {Text="Tipo A", Value="Tipo A"},
                    new SelectListItem() {Text="Tipo B", Value="Tipo B"},
                    new SelectListItem() {Text="Tipo C", Value="Tipo C"},
                    new SelectListItem() {Text="Tipo M", Value="Tipo M"},
                    new SelectListItem() {Text="Tipo R", Value="Tipo R"},
                };

                ViewBag.ListadoTipoFactura = ListadoTipoFactura;
                var editar = new Provee_Cta_Cte();
                var agregar = true;
                if (codigo != 0)
                {
                    editar = this.ProveedoresServicio.Obtener((p) => p.CODIGO == codigo).FirstOrDefault();
                    agregar = false;
                }
                else
                {
                    if (this.ProveedoresServicio.Obtener().Count() > 0)
                    {
                        editar.CODIGO = this.ProveedoresServicio.Obtener().Max(p => p.CODIGO) + 1;
                    }
                    else
                    {
                        editar.CODIGO = 1;
                    }
                }
                ViewBag.agregar = agregar;
                return View(editar);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return RedirectToAction("Inicio", "Proveedores", new { mensaje = ex.Message, ok = false });
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(Provee_Cta_Cte Model, bool agregar)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var mensaje = "agregado correctamente.";
                    Model.Activo = 0;
                    if (Model.IVA == null)
                    {
                        Model.IVA = "Inscripto";
                    }
                    var existeCuit = this.ProveedoresServicio.Obtener((p) => p.CUIT == Model.CUIT && p.CODIGO != Model.CODIGO && Model.CUIT != null).FirstOrDefault() == null ? false : true;
                    if (existeCuit == true && agregar == true)
                    {
                        List<SelectListItem> ListadoIva = new List<SelectListItem>()
                        {
                            new SelectListItem() {Text="Inscripto", Value="Inscripto"},
                            new SelectListItem() {Text="No Inscripto", Value="No Inscripto"},
                            new SelectListItem() {Text="Exento", Value="Exento"},
                            new SelectListItem() {Text="Monotributo", Value="Monotributo"},
                            new SelectListItem() {Text="No Alcanzado", Value="No Alcanzado"},
                        };

                        ViewBag.ListadoIva = ListadoIva;

                        List<SelectListItem> ListadoTipoFactura = new List<SelectListItem>()
                        {
                            new SelectListItem() {Text="Tipo A", Value="Tipo A"},
                            new SelectListItem() {Text="Tipo B", Value="Tipo B"},
                            new SelectListItem() {Text="Tipo C", Value="Tipo C"},
                            new SelectListItem() {Text="Tipo M", Value="Tipo M"},
                            new SelectListItem() {Text="Tipo R", Value="Tipo R"},

                        };

                        ViewBag.ListadoTipoFactura = ListadoTipoFactura;
                        ViewBag.agregar = agregar;
                        ModelState.AddModelError("CUIT", "Ya existe Proveedor con ese CUIT.");
                        return View(Model);
                    }

                    if (agregar)
                    {
                        this.ProveedoresServicio.Agregar(Model);
                    }
                    else
                    {
                        mensaje = "modificado correctamente.";
                        this.ProveedoresServicio.Editar(Model);
                    }
                    scope.Complete();
                    return RedirectToAction("Inicio", "Proveedores", new { mensaje = $"Proveedor {mensaje}", ok = true });
                }
                catch (Exception ex)
                {
                    List<SelectListItem> ListadoIva = new List<SelectListItem>()
                    {
                        new SelectListItem() {Text="Inscripto", Value="Inscripto"},
                        new SelectListItem() {Text="No Inscripto", Value="No Inscripto"},
                        new SelectListItem() {Text="Exento", Value="Exento"},
                        new SelectListItem() {Text="Monotributo", Value="Monotributo"},
                        new SelectListItem() {Text="No Alcanzado", Value="No Alcanzado"},
                    };

                    ViewBag.ListadoIva = ListadoIva;
                    List<SelectListItem> ListadoTipoFactura = new List<SelectListItem>()
                    {
                        new SelectListItem() {Text="Tipo A", Value="Tipo A"},
                        new SelectListItem() {Text="Tipo B", Value="Tipo B"},
                        new SelectListItem() {Text="Tipo C", Value="Tipo C"},
                        new SelectListItem() {Text="Tipo M", Value="Tipo M"},
                        new SelectListItem() {Text="Tipo R", Value="Tipo R"},
                    };

                    ViewBag.ListadoTipoFactura = ListadoTipoFactura;
                    ViewBag.agregar = agregar;
                    ViewBag.error = ex.Message;
                    return View(Model);
                }

            }
        }

        public ActionResult CambiarEstado(int codigo_proveedor)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var proveedor = this.ProveedoresServicio.Obtener((p) => p.CODIGO == codigo_proveedor).SingleOrDefault();
                if (proveedor.Activo.Value == 1)
                    proveedor.Activo = 0;

                else
                    proveedor.Activo = 1;

                this.ProveedoresServicio.Editar(proveedor);
                scope.Complete();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbAccesoDatos.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
