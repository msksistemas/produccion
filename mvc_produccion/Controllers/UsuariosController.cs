﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static MVC_Produccion.ViewModels.LocalViewModels;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using System.Reflection;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class UsuariosController : Controller
    {
        private IBaseServicio<Usuarios> UsuariosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesDeTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<Fabricas> FabricasServicio;
        private IBaseServicio<AspNetUsers> AspNetUserServicio;
        private OpcionesTrabajo Configuraciones { get; set; }

        public UsuariosController(IBaseServicio<Permisos> permisosServicio,
                                  IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                  IBaseServicio<Usuarios> usuariosServicio,
                                  IBaseServicio<Fabricas> fabricasServicio,
                                  IBaseServicio<AspNetUsers> aspNetUserServicio)
        {
            this.AspNetUserServicio = aspNetUserServicio;
            this.UsuariosServicio = usuariosServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesDeTrabajoServicio = opcionesTrabajoServicio;
            this.FabricasServicio = fabricasServicio;
            this.Configuraciones = this.OpcionesDeTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";

            return View();
        }

        public ActionResult GetData(int idUsuario = 0)
        {
            var esMskSistemas = Sesion.UsuarioActual.Usuario.ToLower() == "msk_sistemas";
            var listado = this.UsuariosServicio.Obtener((u) => u.idUsuario == (idUsuario == 0 ? u.idUsuario : idUsuario)
                                                               && u.idUsuario != Sesion.UsuarioActual.idUsuario
                                                               && (u.Rol != "Administrador" || esMskSistemas))
                                               .Select(u => new
                                               {
                                                   u.idUsuario,
                                                   u.Usuario,
                                                   u.Contraseña,
                                                   u.Rol
                                               });
            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExisteUsuario(string Usuario)
        {
            Usuarios user = new Usuarios();

            user = this.UsuariosServicio.Obtener((u) => u.Usuario == Usuario).FirstOrDefault();

            if (user != null)
            {
                return Json("Ya existe un usuario con ese Nombre.", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Administrar_Fabricas(int idUsuario = 0)
        {
            var user = this.UsuariosServicio.Obtener((u) => u.idUsuario == idUsuario).FirstOrDefault();

            AdministrarFabricasModel objeto = new AdministrarFabricasModel();
            objeto.idUsuario = user.idUsuario;
            objeto.Fabrica = (int)user.Fabrica;
            objeto.Todas = user.Fabrica == -1;
            ViewBag.Fabricas = this.FabricasServicio.Obtener();
            return View(objeto);
        }

        [HttpGet]
        public ActionResult AdministrarRol(int idUsuario = 0)
        {
            List<SelectListItem> Listado_Roles = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="Administrador", Value="Administrador"},
                                    new SelectListItem() {Text="Usuario", Value="Usuario"}
                                };

            ViewBag.Listado_Roles = Listado_Roles;

            Usuarios user = new Usuarios();
            user = this.UsuariosServicio.Obtener((u) => u.idUsuario == idUsuario).FirstOrDefault();
            ViewBag.Muestra_Costos = user.Muestra_Costos != null || user != null ? user.Muestra_Costos.Value : false;
            return View(user);
        }

        [HttpPost]
        public ActionResult AdministrarRol(int idUsuario, string Rol, bool? Muestra_Costos)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var usuario = this.UsuariosServicio.Obtener((u) => u.idUsuario == idUsuario).SingleOrDefault();
                    usuario.Muestra_Costos = Muestra_Costos.HasValue ? Muestra_Costos.Value : false;
                    var permisosUsuario = this.PermisosServicio.Obtener((p) => p.idUsuario == idUsuario).FirstOrDefault();
                    usuario.Rol = Rol;
                    if (Rol.ToLower() == "administrador")
                    {
                        usuario.Fabrica = -1;
                        PropertyInfo[] properties = typeof(Permisos).GetProperties();
                        foreach (PropertyInfo p in properties)
                        {
                            var nombre = p.Name.ToLower();
                            if (nombre != "codigo_inteno_permiso" && nombre != "idusuario")
                                p.SetValue(permisosUsuario, true);
                        }
                        this.PermisosServicio.Editar(permisosUsuario);
                    }

                    this.UsuariosServicio.Editar(usuario);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpPost]
        public ActionResult Administrar_Fabricas(AdministrarFabricasModel modelo)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var user = this.UsuariosServicio.Obtener((u) => u.idUsuario == modelo.idUsuario).FirstOrDefault();

                    if (modelo.Todas == true)
                    {
                        user.Fabrica = -1;
                    }
                    else
                    {
                        user.Fabrica = modelo.Fabrica;
                    }

                    this.UsuariosServicio.Editar(user);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { succcess = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        public ActionResult Delete(int idUsuario)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var usuario = this.UsuariosServicio.Obtener((u) => u.idUsuario == idUsuario).SingleOrDefault();
                    var permiso = this.PermisosServicio.Obtener((p) => p.idUsuario == idUsuario).SingleOrDefault();
                    var aspnet = this.AspNetUserServicio.Obtener((asp) => asp.Id == usuario.Id_Login).SingleOrDefault();

                    this.PermisosServicio.Borrar(permiso);
                    this.UsuariosServicio.Borrar(usuario);
                    this.AspNetUserServicio.Borrar(aspnet);
                    scope.Complete();
                    return Json(new { message = "Se borro correctamente" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
