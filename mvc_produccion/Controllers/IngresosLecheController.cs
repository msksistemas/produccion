﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_Produccion.Sesiones;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Gastos;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class IngresosLecheController : Controller
    {
        private IBaseServicio<Ingreso_Leche> IngresoLecheServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Provee_Cta_Cte> ProveedorServicio;
        private IBaseServicio<Detalle_Ingreso> DetalleIngresoServicio;
        private OpcionesTrabajo Configuraciones;

        public IngresosLecheController(IBaseServicio<Ingreso_Leche> ingresoLecheServicio,
                                        IBaseServicio<Permisos> permisosServicio,
                                        IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                        IBaseServicio<Provee_Cta_Cte> proveedorServicio,
                                        IBaseServicio<Detalle_Ingreso> detalleIngreso)
        {
            this.IngresoLecheServicio = ingresoLecheServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.ProveedorServicio = proveedorServicio;
            this.DetalleIngresoServicio = detalleIngreso;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }
        public ActionResult Index()
        {
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="No Historica", Value="Pendiente"},
                                new SelectListItem() {Text="Historica", Value="Historica"}
                            };

            ViewBag.ListadoEstados = ListadoEstados;

            return View();
        }

        public ActionResult GetData(string Estado, int Codigo_Interno_Ingreso = -1)
        {
            if (Codigo_Interno_Ingreso == -1)
            {
                var listado = this.IngresoLecheServicio.Obtener((il) => il.Estado == Estado || Estado == "").Select(i => new
                {
                    i.Numero_Comprobante,
                    i.Fecha,
                    i.Codigo_Proveedor,
                    i.Codigo_Interno_Ingreso,
                    i.Prefijo,
                    i.Codigo_Comprobante,
                    i.Tipo_Factura,
                    Listros_Totales = i.Detalle_Ingreso.Sum(d=> d.Litros)
                }).ToList();

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (Codigo_Interno_Ingreso < 0)
                    return Json(new { success = false, message = "Error, no existe registro con Código negativo." }, JsonRequestBehavior.AllowGet);

                var Ingreso = this.IngresoLecheServicio.Obtener((l) => l.Codigo_Interno_Ingreso == Codigo_Interno_Ingreso).SingleOrDefault();

                if (Ingreso == null)
                    return Json(new { message = "Error, no existe registro con el Código ingresado.", success = false }, JsonRequestBehavior.AllowGet);

                return Json(new { data = Ingreso, success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetDetalles(int Codigo_Interno_Ingreso = 0)
        {
            if (Codigo_Interno_Ingreso == 0)
            {
                List<Detalle_Ingreso> list = new List<Detalle_Ingreso>();

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            var lista = this.IngresoLecheServicio.Obtener((il) => il.Codigo_Interno_Ingreso == Codigo_Interno_Ingreso).SingleOrDefault();

            var listado = lista.Detalle_Ingreso.Select(d => new
            {
                Codigo_Interno_Detalle = d.Codigo_Interno_Detalle,
                Codigo_Interno_Ingreso = d.Codigo_Interno_Ingreso,
                Codigo_Silo = d.Codigo_Silo,
                Litros = d.Litros
            });

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(int Codigo_Interno_Ingreso = 0)
        {
            List<SelectListItem> ListadoTipoFactura = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="Tipo A", Value="Tipo A"},
                                    new SelectListItem() {Text="Tipo B", Value="Tipo B"},
                                    new SelectListItem() {Text="Tipo C", Value="Tipo C"},
                                    new SelectListItem() {Text="Tipo M", Value="Tipo M"},
                                    new SelectListItem() {Text="Tipo R", Value="Tipo R"},

                };

            ViewBag.ListadoTipoFactura = ListadoTipoFactura;

            List<SelectListItem> ListadoMovimiento = new List<SelectListItem>()
            {
                                    new SelectListItem() {Text="Ingreso", Value="Ingreso"},
                                    new SelectListItem() {Text="Egreso", Value="Egreso"},
                                    new SelectListItem() {Text="Ajuste +", Value="Ajuste +"},
                                    new SelectListItem() {Text="Ajuste -", Value="Ajuste -"}
            };

            ViewBag.ListadoMovimiento = ListadoMovimiento;

            Ingreso_Leche ingreso = new Ingreso_Leche();

            ViewBag.proveedor = "";
            if (Codigo_Interno_Ingreso == 0)
                ingreso.Fecha = DateTime.Now;
            else
            {
                ingreso = this.IngresoLecheServicio.Obtener((il) => il.Codigo_Interno_Ingreso == Codigo_Interno_Ingreso).SingleOrDefault();
                var prov = this.ProveedorServicio.Obtener((p) => p.CODIGO == ingreso.Codigo_Proveedor).SingleOrDefault();
                ViewBag.proveedor = prov != null ? prov.RAZON_SOCIAL : "";
            }


            ViewBag.detalle = ingreso.Detalle_Ingreso.Select(di => new
            {
                di.Codigo_Fabrica,
                di.Codigo_Interno_Ingreso,
                di.Codigo_Interno_Detalle,
                di.Acidez,
                di.Densidad,
                di.Codigo_Silo,
                di.Litros,
                di.Lote,
                di.Proteina,
                di.Grasa,
                di.Ph,
                di.Codigo_Insumo
            });

            return View(ingreso);
        }

        [HttpPost]
        public ActionResult AddOrEdit(Ingreso_Leche ingreso)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                ingreso.Estado = "Historica";

                if (ingreso.Codigo_Interno_Ingreso == 0)
                {
                    this.IngresoLecheServicio.Agregar(ingreso);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro guardado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var detalle in this.DetalleIngresoServicio.Obtener((di) => di.Codigo_Interno_Ingreso == ingreso.Codigo_Interno_Ingreso))
                    {
                        this.DetalleIngresoServicio.Borrar(detalle);
                    }

                    foreach (var detalle in ingreso.Detalle_Ingreso.ToList())
                    {
                        this.DetalleIngresoServicio.Agregar(detalle);
                    }
                    this.IngresoLecheServicio.Editar(ingreso);

                    scope.Complete();
                    return Json(new { success = true, message = "Registro modificado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Interno_Ingreso)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Ingreso_Leche est = this.IngresoLecheServicio.Obtener((x) => x.Codigo_Interno_Ingreso == Codigo_Interno_Ingreso).SingleOrDefault();
                    this.IngresoLecheServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Actualizar(List<int> codigos)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (var cod in codigos)
                    {
                        var ingreso = this.IngresoLecheServicio.Obtener((il) => il.Codigo_Interno_Ingreso == cod).SingleOrDefault();
                        ingreso.Estado = "Historica";
                        this.IngresoLecheServicio.Editar(ingreso);
                    }
                    scope.Complete();
                    return Json(new { ok = true, mensaje = "Se actualizo Correctamente" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { ok = false, mensaje = "Ocurrio un problema", consola = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }

        }
    }
}
