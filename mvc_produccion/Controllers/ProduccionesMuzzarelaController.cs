﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using MVC_Produccion.Models;
using System.Net.Http;
using System.Threading.Tasks;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    [Authorize]
    public class ProduccionesMuzzarelaController : Controller
    {
        private IBaseServicio<Produccion_Muzzarela> ProduccionMuzzarellaServicio;
        private IBaseServicio<Detalle_Produccion> DetalleProduccionServicio;
        private IBaseServicio<Produccion> ProduccionServicio;
        private IBaseServicio<Envasado> EnvasadoServicio;
        private IBaseServicio<Detalle_Envasado> DetalleEnvasadoServicio;
        private IBaseServicio<Detalle_Insumo_Envasado> DetalleInsumoEnvasadoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Formulas> FormulasServicio;
        private IBaseServicio<Productos> ProductosServicio;
        private IBaseServicio<Familias> FamiliasServicio;
        private IBaseServicio<Costos_Indirectos_Produccion> CostosIndirectosProduccion;
        private IBaseServicio<Mov_Ins_Asoc> MovInsAsocServicio;
        private IBaseServicio<Queseros> QueserosServicio;
        private IBaseServicio<Ajustes_Insumos> AjusteInsumosServicio;
        private IProduccionServicio ProduccionServicioExtra;
        private OpcionesTrabajo Configuraciones;
        private dbProdSP db = new dbProdSP();
        private readonly HttpService httpClient;

        public ProduccionesMuzzarelaController(IBaseServicio<Produccion_Muzzarela> produccionMuzzarellaServicio,
                                            IBaseServicio<Ajustes_Insumos> ajusteInsumosServicio,
                                            IProduccionServicio produccionServicioExtra,
                                          IBaseServicio<Formulas> formulasServicio,
                                          IBaseServicio<Queseros> queserosServicio,
                                          IBaseServicio<Familias> familiasServicio,
                                          IBaseServicio<Mov_Ins_Asoc> movInsAsocServicio,
                                          IBaseServicio<Costos_Indirectos_Produccion> CostosIndirectosProduccion,
                                          IBaseServicio<Detalle_Produccion> detalleProduccionServicio,
                                          IBaseServicio<Produccion> produccionServicio,
                                          IBaseServicio<Envasado> envasadoServicio,
                                          IBaseServicio<Detalle_Envasado> detalleEnvasadoServicio,
                                          IBaseServicio<Detalle_Insumo_Envasado> detalleInsumoEnvasadoServicio,
                                          IBaseServicio<Permisos> permisosServicio,
                                          IBaseServicio<Productos> productosServicio,
                                          IBaseServicio<Costos_Indirectos_Produccion> costosIndirectosproduccion,
                                          IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                          HttpService httpClient)
        {
            this.CostosIndirectosProduccion = costosIndirectosproduccion;
            this.ProduccionServicioExtra = produccionServicioExtra;
            this.FamiliasServicio = familiasServicio;
            this.AjusteInsumosServicio = ajusteInsumosServicio;
            this.ProduccionMuzzarellaServicio = produccionMuzzarellaServicio;
            this.QueserosServicio = queserosServicio;
            this.MovInsAsocServicio = movInsAsocServicio;
            this.DetalleEnvasadoServicio = detalleEnvasadoServicio;
            this.DetalleInsumoEnvasadoServicio = detalleInsumoEnvasadoServicio;
            this.DetalleProduccionServicio = detalleProduccionServicio;
            this.EnvasadoServicio = envasadoServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.PermisosServicio = permisosServicio;
            this.ProduccionServicio = produccionServicio;
            this.FormulasServicio = formulasServicio;
            this.ProductosServicio = productosServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
            this.httpClient = httpClient;
        }


        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            else if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";

            /* USUARIO SIN FABRICA SETEADA */
            ViewBag.configuraciones = this.Configuraciones;

            if (Sesion.UsuarioActual.Fabrica == 0 || Sesion.UsuarioActual.Fabrica == null && Sesion.UsuarioActual.Rol.ToLower() != "administrador")
                ViewBag.Error = "No tiene Fábrica asignada, por favor, solicite Permisos al Administrador.";


            List<SelectListItem> ListadoHistoricos = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="No Históricos", Value="0"},
                                    new SelectListItem() {Text="Históricos", Value="1"},
                                    new SelectListItem() {Text="Todos", Value="-1"}
                                };

            ViewBag.ListadoHistoricos = ListadoHistoricos;

            return View();
        }

        public ActionResult GetData(string Lote = "")
        {
            if ((Sesion.UsuarioActual.Fabrica != 0 && Sesion.UsuarioActual.Fabrica != null) || Sesion.UsuarioActual.Rol.ToLower() == "administrador")
            {
                var listado = this.ProduccionMuzzarellaServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote)
                                                                                && p.Produccion.Historico == false
                                                                                && p.Produccion.Codigo_Fabrica == (Sesion.UsuarioActual.Fabrica == -1 ? p.Produccion.Codigo_Fabrica : Sesion.UsuarioActual.Fabrica))
                                .Select(p => new
                                {
                                    Fecha = p.Produccion.Fecha,
                                    p.Lote,
                                    p.Piezas_Obtenidas,
                                    p.Kilos_Teoricos,
                                    p.Kilos_Reales,
                                    p.Ph_Final,
                                    p.Fecha_Vencimiento,
                                    Kilos_Restantes = p.Kilos_Reales == null ? null : (p.Kilos_Reales - this.ProduccionServicioExtra.ObtenerCantidadesEnvasadas(p.Lote)["kilos"])
                                });
              
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { error = 1, Usuario = Sesion.UsuarioActual.Usuario, success = false }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetHistoricos(string Lote = "", int Historico = 0)
        {
            /* TODOS */
            if (Historico == -1)
            {
                var listado = this.ProduccionMuzzarellaServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote))
                                                                .Select(p => new
                                                                {
                                                                    Lote = p.Lote,
                                                                    Nro_Planilla = p.Produccion.Nro_Planilla,
                                                                    Fecha = p.Produccion.Fecha,
                                                                    Piezas_Obtenidas = p.Piezas_Obtenidas,
                                                                    Kilos_Teoricos = p.Kilos_Teoricos,
                                                                    Kilos_Reales = p.Kilos_Reales,
                                                                    Codigo_Producto = p.Produccion.Formulas != null ? p.Produccion.Formulas.Codigo_Producto : "",
                                                                    Producto = p.Produccion.Formulas.Productos != null ? p.Produccion.Formulas.Productos.Descripcion:"",
                                                                    Quesero = p.Produccion.Queseros != null ? p.Produccion.Queseros.Nombre:"",
                                                                    Kilos_Restantes = p.Kilos_Reales - this.ProduccionServicioExtra.ObtenerCantidadesEnvasadas(p.Lote)["piezas"]

                                                                });
                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var listado = this.ProduccionMuzzarellaServicio.Obtener((p) => p.Lote == (Lote == "" ? p.Lote : Lote) && p.Produccion.Historico == (Historico == 1 ? true : false))
                                                               .Select(p => new
                                                               {
                                                                   Lote = p.Lote,
                                                                   Nro_Planilla = p.Produccion.Nro_Planilla,
                                                                   Fecha = p.Produccion.Fecha,
                                                                   Piezas_Obtenidas = p.Piezas_Obtenidas,
                                                                   Kilos_Teoricos = p.Kilos_Teoricos,
                                                                   Kilos_Reales = p.Kilos_Reales,
                                                                   Codigo_Producto = p.Produccion.Formulas != null ? p.Produccion.Formulas.Codigo_Producto : "",
                                                                   Producto = p.Produccion.Formulas.Productos != null ? p.Produccion.Formulas.Productos.Descripcion : "",
                                                                   Quesero = p.Produccion.Queseros != null ? p.Produccion.Queseros.Nombre : "",
                                                                   Kilos_Restantes = p.Kilos_Reales - this.ProduccionServicioExtra.ObtenerCantidadesEnvasadas(p.Lote)["piezas"]

                                                               });

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(string Lote = "")
        {
            ViewBag.queseros = this.QueserosServicio.Obtener((q) => q.Activo == true).Select(q => new { q.Nombre, q.Codigo_Quesero });
            ViewBag.Codigo_Familia = this.FamiliasServicio.Obtener((f) => f.Nombre.ToLower() == "muzzarella").FirstOrDefault().Codigo_Familia;
            if (Lote == "")
            {
                Produccion_Muzzarela prod = new Produccion_Muzzarela();

                Produccion produccion = new Produccion();
                produccion.Fecha = DateTime.Now;

                prod.Produccion = produccion;

                if (!this.Configuraciones.NroPlanillaDigitable)
                {
                    produccion.Nro_Planilla = (this.ProduccionMuzzarellaServicio.Obtener().Count() + 1).ToString();
                    ViewBag.NroAuto = true;
                }
                else
                {
                    ViewBag.NroAuto = false;
                }

                return View(prod);
            }
            else
                return View(this.ProduccionMuzzarellaServicio.Obtener((x) => x.Produccion.Lote == Lote).FirstOrDefault());
        }

        [HttpPost]
        public async Task<ActionResult> AddOrEdit(Produccion_Muzzarela prod, string Codigo_Producto)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    prod.Lote = prod.Produccion.Lote;
                    prod.Produccion.idUsuario = Sesion.UsuarioActual.idUsuario;
                    prod.Fecha_Elaboracion = prod.Produccion.Fecha;
                    if (prod.Codigo_Interno_Produccion == 0)
                    {
                        //if (httpClient.EsSanSatur())
                        //{
                        //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                        //        prod.Produccion.Detalle_Produccion.ToList(),
                        //        HttpService.ProcesoProduccion.Creacion,
                        //        prod.Produccion.Fecha ?? DateTime.Now,
                        //        httpClient.ObtenerPrimerNumProdVentas(Codigo_Producto)
                        //    );
                        //    if (!response.IsSuccessStatusCode)
                        //    {
                        //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                        //    }
                        //}
                        if (this.ProduccionServicio.Obtener((p) => p.Lote == prod.Lote).SingleOrDefault() != null)
                            return Json(new { success = false, message = "Ya existe un registro con ese Lote." }, JsonRequestBehavior.AllowGet);

                        Formulas formula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                        Productos producto = formula.Productos;

                        prod.Produccion.Codigo_Formula = formula.Codigo_Formula;

                        /*GUARDA TODOS LOS DETALLES DE INSUMOS ASOCIADOS*/
                        foreach (var detalle in prod.Produccion.Detalle_Produccion)
                        {
                            detalle.Movimiento = "Producción";
                            if (detalle.Fecha == null)
                            {
                                detalle.Fecha = prod.Produccion.Fecha;
                            }
                            detalle.Lote = prod.Lote;
                        }

                        foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion)
                        {
                            detalle.Lote = prod.Produccion.Lote;
                        }

                        /*GUARDA TODOS LOS DETALLES DE PRODUCTOS NUEVOS ASOCIADOS*/

                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc)
                        {
                            movimiento.Lote = prod.Lote;
                        }
                        prod.Produccion.Historico = false;
                        this.ProduccionMuzzarellaServicio.Agregar(prod);
                        db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);
                        scope.Complete();
                        return Json(new { success = true, envasar = producto.Salida_Directa ?? false, resultado = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        /*TRAE EL PRODUCTO ASOCIADO A LA FÓRMULA PARA SABER SI ES UN INSUMO, PROD TERMINADO O AMBOS*/
                        var formula = this.FormulasServicio.Obtener((f) => f.Codigo_Producto == Codigo_Producto).FirstOrDefault();
                        prod.Produccion.Codigo_Formula = formula.Codigo_Formula;

                        List<Detalle_Produccion> detalleAnterior = this.DetalleProduccionServicio.Obtener((dp) => dp.Lote == prod.Lote).ToList();

                        //if (httpClient.EsSanSatur())
                        //{
                        //    HttpResponseMessage response = await httpClient.AjusteStockProduccion(
                        //        prod.Produccion.Detalle_Produccion.ToList(), 
                        //        HttpService.ProcesoProduccion.Edicion, 
                        //        Codigo_Producto, 
                        //        detalleAnterior);
                        //    if (!response.IsSuccessStatusCode)
                        //    {
                        //        return Json(new { success = false, message = response.ReasonPhrase }, JsonRequestBehavior.AllowGet);
                        //    }
                        //}

                        /*DETALLES DE LA PRODUCCIÓN A EDITAR*/
                        foreach (var detalle in detalleAnterior)
                        {
                            this.DetalleProduccionServicio.Borrar(detalle);
                        }
                        foreach (var detalle in prod.Produccion.Detalle_Produccion)
                        {
                            detalle.Movimiento = "Producción";
                            if (detalle.Fecha == null)
                            {
                                detalle.Fecha = prod.Produccion.Fecha;
                            }
                            detalle.Lote = prod.Lote;
                            this.DetalleProduccionServicio.Agregar(detalle);
                        }

                        //COSTOS
                        foreach (var detalle in this.CostosIndirectosProduccion.Obtener((ci) => ci.Lote == prod.Lote))
                        {
                            this.CostosIndirectosProduccion.Borrar(detalle);
                        }

                        foreach (var detalle in prod.Produccion.Costos_Indirectos_Produccion.ToList())
                        {
                            detalle.Lote = prod.Produccion.Lote;
                            this.CostosIndirectosProduccion.Agregar(detalle);
                        }

                        /*DETALLES DE PRODUCTOS ASOCIADOS*/
                        foreach (var movimiento in this.MovInsAsocServicio.Obtener((mia)=> mia.Lote == prod.Lote))
                        {
                            this.MovInsAsocServicio.Borrar(movimiento);
                        }
                        foreach (var movimiento in prod.Produccion.Mov_Ins_Asoc.ToList())
                        {
                            movimiento.Lote = prod.Produccion.Lote;

                            var detalleInsumo = prod.Produccion.Detalle_Produccion.Where(detalle => detalle.Codigo_Insumo == movimiento.Codigo_Insumo).FirstOrDefault();

                            if (detalleInsumo != null)
                            {
                                var ajuste = detalleInsumo.Ajuste.HasValue ? detalleInsumo.Ajuste.Value : 0;

                                if (ajuste != 0)
                                    this.MovInsAsocServicio.Agregar(movimiento);
                            }
                        }

                        this.ProduccionMuzzarellaServicio.Editar(prod);
                        this.ProduccionServicio.Editar(prod.Produccion);
                        db.Movimiento_Stock_Produccion(prod.Produccion.Codigo_Interno_Produccion, false);
                        scope.Complete();
                        return Json(new { success = true, envasar = false, resultado = 2 }, JsonRequestBehavior.AllowGet);
                    }

                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult ObtenerRestantes(string Lote = "")
        {
            decimal? kilosEnvasados = 0;
            decimal? piezasEnvasadas = 0;

            decimal? kilosRestantes = 0;
            decimal? piezasRestantes = 0;

            var prod = this.ProduccionMuzzarellaServicio.Obtener((p) => p.Lote == Lote).FirstOrDefault();

            foreach (var env in this.EnvasadoServicio.Obtener((e) => e.Lote == Lote).ToList())
            {
                if (env != null && env.Detalle_Envasado.Count() > 0)
                {
                    foreach (var detalle in env.Detalle_Envasado.ToList())
                    {
                        if (detalle != null)
                        {
                            kilosEnvasados += detalle.Kilos;
                            piezasEnvasadas += detalle.Cantidad;
                        }
                    }

                }
            }

            if (prod != null)
            {
                try
                {
                    kilosRestantes = prod.Kilos_Reales - kilosEnvasados;
                    piezasRestantes = prod.Piezas_Obtenidas - piezasEnvasadas;
                }
                catch { }
            }

            return Json(new
            {
                success = true,
                kilosRestantes = kilosRestantes,
                piezasRestantes = piezasRestantes
            }, JsonRequestBehavior.AllowGet);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
