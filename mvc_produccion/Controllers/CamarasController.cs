﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class CamarasController : Controller
    {
        private IBaseServicio<Camaras> CamarasServicio;


        private IBaseServicio<OpcionesTrabajo> OpcionesDeTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;

        private OpcionesTrabajo Configuraciones { get; set; }

        public CamarasController(IBaseServicio<Camaras> camarasServicio,
                                IBaseServicio<OpcionesTrabajo> opcionesDeTrabajoServicio,
                                IBaseServicio<Permisos> permisosServicio)
        {
            this.CamarasServicio = camarasServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesDeTrabajoServicio = opcionesDeTrabajoServicio;
            this.Configuraciones = this.OpcionesDeTrabajoServicio.Obtener().FirstOrDefault();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            var listado = this.CamarasServicio.Obtener().Select(c => new
            {
                c.Codigo_Camara,
                c.Codigo_Interno_Camara,
                c.Nombre
            });

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new Camaras());
            else
                return View(this.CamarasServicio.Obtener((cam) => cam.Codigo_Camara == id).SingleOrDefault());
        }

        [HttpPost]
        public ActionResult AddOrEdit(Camaras Camara)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var mensaje = "Se Guardo Correctamente.";
                    if (Camara.Codigo_Interno_Camara == 0)
                        this.CamarasServicio.Agregar(Camara);
                    else
                    {
                        this.CamarasServicio.Editar(Camara);
                        mensaje = "Se Edito Correctamente.";
                    }
                    scope.Complete();
                    return Json(new { message = mensaje, success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }

        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = this.CamarasServicio.Obtener().Max(cam => cam.Codigo_Camara);
                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { data = "", success = true }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult Delete(Camaras Camara)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    this.CamarasServicio.Borrar(Camara);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}

