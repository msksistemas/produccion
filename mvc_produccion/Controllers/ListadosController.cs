﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using MVC_Produccion.ContextoProduccion;
using AccesoDatos.Contexto.Produccion.SP;
using MVC_Produccion.Reports;
using MVC_Produccion.Sesiones;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Data.SqlClient;
using static MVC_Produccion.Listados.ListadosModels;
using System.Data;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class ListadosController : Controller
    {
        private IBaseServicio<AccesoDatos.Contexto.Produccion.Analisis_Microbiologico> AnalisisMicrobiologicoServicio;
        private IBaseServicio<AccesoDatos.Contexto.Produccion.Fabricas> FabricaServicio;
        Entities db = new Entities();
        dbProdSP dbAccesoDatos = new dbProdSP();

        public ListadosController(IBaseServicio<AccesoDatos.Contexto.Produccion.Analisis_Microbiologico> analisisMicrobiologicoServicio,
                                    IBaseServicio<AccesoDatos.Contexto.Produccion.Fabricas> fabricaServicio)
        {
            this.AnalisisMicrobiologicoServicio = analisisMicrobiologicoServicio;
            this.FabricaServicio = fabricaServicio;
        }

        private string Obtener_Path_Imagen()
        {
            var Datos = db.Datos_Empresa.FirstOrDefault();
            var ruta = Server.MapPath("~/DatosEmpresa/");
            var direccion = Path.Combine(ruta, Datos.Imagen);
            return direccion;
        }

        public JsonResult ExisteProducto(string Codigo_Producto)
        {
            Productos prod = db.Productos.Where(p => p.Codigo_Producto == Codigo_Producto).FirstOrDefault();

            if (prod == null)
                return Json("No existe Producto con el código ingresado.", JsonRequestBehavior.AllowGet);
            else
                return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExisteInsumo(string Codigo_Insumo)
        {
            Insumos insumo = db.Insumos.Where(p => p.Codigo_Insumo == Codigo_Insumo).FirstOrDefault();

            if (insumo == null)
                return Json("No existe Insumo con el código ingresado.", JsonRequestBehavior.AllowGet);
            else
                return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Impresion_Etiquetas_Liberados(String codigos)
        {
            var codigos_int = new List<int>();
            foreach (var item in codigos.Split(','))
            {
                var c = item.Replace("[", string.Empty).Replace("\"", string.Empty).Replace("]", string.Empty);
                codigos_int.Add(Convert.ToInt32(c));
            }

            Etiquetas_Liberados rpt = new Etiquetas_Liberados();

            rpt.Load();
            var datos = new DataTable();
            datos.Columns.Add("codigo", typeof(int));
            foreach (var item in codigos_int)
            {
                datos.Rows.Add(item);
            }
            var parametros = new SqlParameter("@Lista", SqlDbType.Structured);
            parametros.Value = datos;
            parametros.TypeName = "dbo.CodigosEtiquetas";
            dbAccesoDatos.Database.ExecuteSqlCommand("exec RPT_IMPRESION_ETIQUETA @Lista", parametros);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Movimientos_Volteo()
        {
            return View();
        }

        public ActionResult RPT_Movimientos_Volteo(bool Excel, int Codigo_Fabrica = -1)
        {
            dbAccesoDatos.RPT_Movimientos_Volteo(Codigo_Fabrica, false, "");

            RPT_Movimientos_Volteo rpt = new RPT_Movimientos_Volteo();

            rpt.Load();
            rpt.SetParameterValue("Excel", Excel);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Movimientos_Volteo") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Desvio_Consumo()
        {
            ComprasPorProveedorModel objeto = new ComprasPorProveedorModel();
            objeto.Fecha_Desde = DateTime.Now;
            objeto.Fecha_Hasta = DateTime.Now;
            var fab = Sesion.UsuarioActual.Fabrica;
            if (fab != null)
                objeto.Codigo_Fabrica = fab.Value == -1 ? 0 : fab.Value;
            return View(objeto);
        }

        public ActionResult RPT_Desvio_Consumo(string FechaDesde, string FechaHasta, bool Excel, string Codigo_Insumo = "", int Codigo_Fabrica = -1, int Codigo_Rubro = -1, int Codigo_Linea = -1, string Consumido = "")
        {
            DateTime Hasta;
            DateTime.TryParse(FechaHasta, out Hasta);
            DateTime Desde;
            DateTime.TryParse(FechaDesde, out Desde);

            dbAccesoDatos.RPT_Desvio_Consumo_Insumos(Codigo_Fabrica, Codigo_Insumo, Desde, Hasta, Codigo_Rubro, Codigo_Linea, Consumido);

            RPT_Desvio_Consumo rpt = new RPT_Desvio_Consumo();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            rpt.SetParameterValue("Excel", Excel);

            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Desvio_Consumo") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Compras_Por_Proveedor()
        {
            ComprasPorProveedorModel objeto = new ComprasPorProveedorModel();
            objeto.Fecha_Desde = DateTime.Now;
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.HasValue ? Sesion.UsuarioActual.Fabrica.Value : -1;
            List<SelectListItem> Listado_Tipos_Movimiento = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="Todos", Value=""},
                                    new SelectListItem() {Text="Ingreso Mercadería", Value="Ingreso Mercadería"},
                                    new SelectListItem() {Text="Nota de Crédito", Value="Nota de Crédito"},
                                    new SelectListItem() {Text="Remito", Value="Remito"},
                                    new SelectListItem() {Text="N/C Remito", Value="N/C Remito"},
                                };
            ViewBag.Listado_Tipos_Movimiento = Listado_Tipos_Movimiento;

            List<SelectListItem> detalles_imputaciones = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="Detalles", Value="detalles"},
                                    new SelectListItem() {Text="Imputaciones", Value="imputaciones"}
                                };
            ViewBag.detalles_imputaciones = detalles_imputaciones;

            return View(objeto);
        }

        [HttpGet]
        public ActionResult StockLeche()
        {
            StockLecheModel objeto = new StockLecheModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;

            return View(objeto);
        }

        [HttpGet]
        public ActionResult Detallado_Formulas()
        {
            DetalleFormulaModel detalladoFormula = new DetalleFormulaModel();

            return View(detalladoFormula);
        }

        [HttpGet]
        public ActionResult Detallado_Formula_Envasado()
        {
            DetalleFormulaEnvasadoModel detalladoFormula = new DetalleFormulaEnvasadoModel();

            return View(detalladoFormula);
        }

        [HttpGet]
        public ActionResult StockProductoEstado()
        {
            StockGeneralModel objeto = new StockGeneralModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;
            return View(objeto);
        }

        [HttpGet]
        public ActionResult Vista_Resumen_Produccion()
        {
            ResumenProdModel objeto = new ResumenProdModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }

        [HttpGet]
        public ActionResult Resumen_Produccion_Quesos(string Fecha_Desde, string Fecha_Hasta, int? Codigo_Fabrica, string Codigo_Producto, bool envasados, bool Actualizar_Kilos_Teoricos, bool Excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Resumen_Envasado_Produccion_Total(Desde, Hasta, Codigo_Fabrica, Codigo_Producto);

            Resumen_Produccion rpt = new Resumen_Produccion();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == null ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            rpt.SetParameterValue("Envasados", envasados);
            Usuarios usuario = db.Usuarios.SingleOrDefault(u => u.idUsuario == Sesion.UsuarioActual.idUsuario);
            bool puedeVerImportes = usuario.Muestra_Costos.HasValue ? usuario.Muestra_Costos.Value : false;

            rpt.SetParameterValue("OcultarImportes", true);
            rpt.SetParameterValue("OcultarImportes", !puedeVerImportes);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Resumen_Produccion") : RptToPDF(rpt);

        }
        [HttpGet]
        public ActionResult Vista_Resumen_Produccion_Muzzarella()
        {
            ResumenProdModel objeto = new ResumenProdModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }
        public ActionResult Resumen_Produccion_Muzzarella(string Fecha_Desde, string Fecha_Hasta, int? Codigo_Fabrica, string Codigo_Producto, bool Excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Resumen_Produccion_Muzzarella(Desde, Hasta, Codigo_Fabrica, Codigo_Producto);

            Resumen_Produccion_Muzzarella rpt = new Resumen_Produccion_Muzzarella();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            rpt.SetParameterValue("Excel", Excel);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Resumen_Produccion_Muzzarella") : RptToPDF(rpt);
        }
        [HttpGet]
        public ActionResult Vista_Producciones_Muzzarella_Detalladas()
        {
            ResumenProdModel objeto = new ResumenProdModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }
        public ActionResult Producciones_Muzzarella_Detalladas(string Fecha_Desde, string Fecha_Hasta, int? Codigo_Fabrica, string Codigo_Producto, bool Excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Produccion_Muzzarella_Detallado(Desde, Hasta, Codigo_Fabrica, Codigo_Producto);

            Producciones_Muzzarella_Detallado rpt = new Producciones_Muzzarella_Detallado();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            rpt.SetParameterValue("Excel", Excel);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Producciones_Muzzarella_Detalladas") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Producciones_Detallado()
        {
            ProdDetalladoModel objeto = new ProdDetalladoModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }

        [HttpGet]
        public ActionResult Producciones_Detallado(string Fecha_Desde, string Fecha_Hasta, int? Codigo_Fabrica, string Codigo_Producto)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Producciones_Detallado(Codigo_Producto, Codigo_Fabrica, Desde, Hasta);

            Producciones_Detallado rpt = new Producciones_Detallado();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Proyeccion_Produccion()
        {
            return View();
        }

        [HttpGet]
        public ActionResult RPT_Proyeccion_Produccion(bool Excel = false, int Codigo_Fabrica = 0, string Prod = "", decimal Kilos = 0)
        {
            dbAccesoDatos.RPT_Proyeccion_Produccion(Codigo_Fabrica, Prod, Kilos);
            RPT_Proyeccion_Produccion rpt = new RPT_Proyeccion_Produccion();

            rpt.Load();
            rpt.SetParameterValue("Excel", Excel);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Proyeccion_Produccion") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Punto_Pedido()
        {
            return View();
        }

        [HttpGet]
        public ActionResult RPT_Punto_Pedido(bool Excel, int Codigo_Fabrica = 1, int Prov = 0)
        {
            dbAccesoDatos.RPT_Punto_Pedido(Codigo_Fabrica, Prov);
            RPT_Punto_Pedido rpt = new RPT_Punto_Pedido();

            rpt.Load();
            rpt.SetParameterValue("Excel", Excel);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Punto_Pedido") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Insumos()
        {
            StockInsumosModel objeto = new StockInsumosModel();
            return View(objeto);
        }

        [HttpGet]
        public ActionResult RPT_Insumos(bool Excel, int Linea = 0, int Rubro = 0, int Prov = 0, int orden = 0)
        {
            dbAccesoDatos.RPT_Insumos(Linea, Rubro, Prov, orden);
            RPT_Insumos rpt = new RPT_Insumos();

            rpt.Load();
            rpt.SetParameterValue("Excel", Excel);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Insumos") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Stock_Insumos_Total(int? Codigo_Fabrica, int? Linea, int? Rubro, string Codigo_Insumo, string Fecha_Desde, string Fecha_Hasta, bool Excel, string Lote)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Stock_Insumos_Total(Codigo_Fabrica, Linea, Rubro, Codigo_Insumo, Desde, Hasta, "", Lote);
            ReportClass rpt = new ReportClass();
            if (Excel)
                rpt = new StockInsumos__excel();
            else
                rpt = new Reports.StockInsumos();

            rpt.Load();
            rpt.SetParameterValue("Excel", !Excel);
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == null ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);            

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Stock_Insumos_Detallado") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Stock_Insumos_Por_Producto(int? Codigo_Fabrica,
               int? Linea, 
               int? Rubro,
               string Codigo_Insumo,
               string Fecha_Desde,
               string Fecha_Hasta,
               string Codigo_Producto,
               bool Excel,
               bool Resumido = false,
               bool Solo_Totales = false)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);
            dbAccesoDatos.RPT_Insumos_Por_Productos(Codigo_Fabrica,
                Codigo_Insumo,
                Linea,
                Rubro,
                Desde,
                Hasta,
                Codigo_Producto,
                Resumido,
                Solo_Totales
             );

            ReportClass rpt;

            if (Resumido)
            {
                if (Solo_Totales)
                    rpt = new StockInsumos_Por_Productos_resumido_totales();
                else
                    rpt = new StockInsumos_Por_Productos__resumido();
            }
            else
                rpt = new StockInsumos_Por_Productos();

            rpt.Load();
            rpt.SetParameterValue("Excel", !Excel);
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Stock_Insumos_Por_Producto") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Stock_Insumos_Consumidos_Producto_Detallado(int? Codigo_Fabrica, int? Linea, int? Rubro, string Codigo_Insumo, string Fecha_Desde, string Fecha_Hasta, string Codigo_Producto, bool Excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);
            dbAccesoDatos.RPT_Insumos_Consumidos_Por_Productos_Detallado(Codigo_Fabrica, Linea, Rubro, Codigo_Insumo, Desde, Hasta, Codigo_Producto);

            ReportClass rpt;

            if (Excel)
                rpt = new StockInsumos_Por_Productos_consumidos_detallado_Excel();
            else
                rpt = new StockInsumos_Por_Productos_consumidos_detallado();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Insumos Consumidos por Producto Detallado") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Stock_Insumos_Consumidos_Producto(int? Codigo_Fabrica, int? Linea, int? Rubro, string Codigo_Insumo, string Fecha_Desde, string Fecha_Hasta, string Codigo_Producto, bool Excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Insumos_Consumidos_Por_Productos(Codigo_Fabrica, Linea, Rubro, Codigo_Insumo, Desde, Hasta, Codigo_Producto);

            ReportClass rpt;

            if (Excel)
                rpt = new StockInsumos_Por_Productos_consumidos_resumido_Excel();
            else
                rpt = new StockInsumos_Por_Productos_consumidos_resumido();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Insumos Consumidos por Producto Resumido") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Variacion_Precios()
        {
            StockInsumosModel objeto = new StockInsumosModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }

        [HttpGet]
        public ActionResult Variacion_Precios(string Codigo_Insumo, string Fecha_Desde, string Fecha_Hasta, bool Excel, int? Codigo_Proveedor)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Calculo_Variacion_Precios(Desde, Hasta, Codigo_Insumo, Codigo_Proveedor);

            RPT_Variacion_Precios rpt = new RPT_Variacion_Precios();

            rpt.Load();
            rpt.SetParameterValue("Excel", Excel);
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);

            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Stock_Insumos_Detallado") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Calidad()
        {
            CalidadModel objeto = new CalidadModel();
            objeto.Fecha_Desde = DateTime.Now;
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;

            List<SelectListItem> ListadoReportes = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="Envasados Pendientes", Value="1"},
                                new SelectListItem() {Text="Envasados Liberados", Value="2"},
                                new SelectListItem() {Text="Envasados Decomisados", Value="3"},
                                new SelectListItem() {Text="Envasados Reprocesados", Value="4"},
                                new SelectListItem() {Text="Envasados Retenidos", Value="5"},

                            };
            ViewBag.ListadoReportes = ListadoReportes;

            return View(objeto);
        }

        public ActionResult RPT_Calidad(string Numero_Producto, string Fecha_Hasta, string Fecha_Desde, bool Excel, int Reporte, int Fabrica = -1)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            ReportClass rpt;
            string titulo = "";
            if (Reporte <= 4)
            {
                switch (Reporte)
                {
                    case 1:
                        titulo = "Listado de Envasados Pendientes de Liberar";
                        break;
                    case 2:
                        titulo = "Listado de Envasados Liberados";
                        break;
                    case 3:
                        titulo = "Listado de Envasados Decomisados";
                        break;
                    case 4:
                        titulo = "Listado de Envasados Reprocesados";
                        break;
                }
                    
                if (Excel)
                    rpt = new RPT_Envasados_Excel();
                else
                    rpt = new RPT_Envasados();
            }
            else
            {
                if (Excel)
                    rpt = new RPT_Envasados_Retenidos_Excel();
                else
                    rpt = new RPT_Envasados_Retenidos();
            }

            dbAccesoDatos.RPT_Calidad(Reporte, Desde, Hasta, Fabrica, Numero_Producto);

            rpt.Load();
            string direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("Excel", Excel);
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            rpt.SetParameterValue("Pendientes", Reporte == 1);
            rpt.SetParameterValue("PathImagen", direccion);
            if (Reporte <= 4)
                rpt.SetParameterValue("Titulo", titulo);

            setConnectionInfo(rpt);

            if (Excel)
            {
                var nombre = "Listado_Envasados_Pendientes";
                if (Reporte == 2)
                    nombre = "Listado_Envasados_Liberados";
                if (Reporte == 3)
                    nombre = "Listado_Envasados_Decomisados";
                if (Reporte == 4)
                    nombre = "Listado_Envasados_Reprocesados";

                return RptToExcel(rpt, nombre);
            }
            else
                return RptToPDF(rpt);
        }
        [HttpGet]
        public ActionResult Vista_Listado_Factura_Remito()
        {
            List<SelectListItem> Listado_Tipos_Factura = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="A", Value="Tipo A"},
                                    new SelectListItem() {Text="B", Value="Tipo B"},
                                    new SelectListItem() {Text="C", Value="Tipo C"},
                                    new SelectListItem() {Text="Blanco", Value="Tipo X"},
                                };

            ViewBag.Listado_Tipos_Factura = Listado_Tipos_Factura;
            FacturaRemitoModel model = new FacturaRemitoModel();
            return View(model);
        }
        [HttpGet]
        public ActionResult Listado_Factura_Remito(int codigocompra)
        {
            dbAccesoDatos.RPT_Factura_Remito(codigocompra);

            RPT_Factura_Remito rpt = new RPT_Factura_Remito();

            rpt.Load();

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Stock_Insumos()
        {
            StockInsumosModel objeto = new StockInsumosModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;

            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }

        [HttpGet]
        public ActionResult Vista_Stock_Insumos_Consumidos_Por_Producto()
        {
            StockInsumosModel objeto = new StockInsumosModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;

            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }

        [HttpGet]
        public ActionResult Stock_Insumos_Resumido(int? Codigo_Fabrica, int? Proveedor, int? Linea, int? Rubro, string cod_insumo, int? Orden, string Fecha_Hasta, bool Excel, bool Corte_Lote, bool Incluir_Negativos)
        {
            DateTime Hasta;
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            var filtrarNegativos = Incluir_Negativos == true ? false : true;
            var ordenarPor = Orden.HasValue ? Orden : 0;
            dbAccesoDatos.RPT_Stock_Insumos_Resumido(Codigo_Fabrica,
                Proveedor,
                Linea,
                Rubro,
                Hasta,
                cod_insumo,
                Corte_Lote,
                filtrarNegativos,
                ordenarPor);

            ReportClass rpt = new ReportClass();
            if (Corte_Lote)
                rpt = new StockInsumos_Resumido_Corte_Por_Lote();
            else
                rpt = new StockInsumos_Resumido();

            rpt.Load();
            var mostrarCostos = Sesion.UsuarioActual.Muestra_Costos.HasValue ? Sesion.UsuarioActual.Muestra_Costos.Value : false;
            rpt.SetParameterValue("FechaHasta", Hasta);
            rpt.SetParameterValue("MostrarCostos", mostrarCostos);
            rpt.SetParameterValue("Excel", Excel);
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == null ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);
            //rpt.SetParameterValue("Orden", ordenarPor);

            setConnectionInfo(rpt);

            if (Corte_Lote)
            {
                rpt.SetParameterValue("Fabrica", Codigo_Fabrica == null ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            }

            return Excel ? RptToExcel(rpt, "Listado_Stock_Insumos_Resumido") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Merma_Producciones()
        {
            MermaProduccionesModel objeto = new MermaProduccionesModel();
            objeto.FechaDesde = DateTime.Now;
            objeto.FechaHasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;

            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;

            return View(objeto);
        }

        [HttpGet]
        public ActionResult Merma_Producciones(int? Codigo_Fabrica, string Codigo_Producto, string Motivo, string Fecha_Desde, string Fecha_Hasta, bool excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Restante_Envasado(Codigo_Fabrica, Codigo_Producto, Desde, Hasta, Motivo);

            RestanteProducciones rpt = new RestanteProducciones();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            rpt.SetParameterValue("Excel", excel);
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());

            setConnectionInfo(rpt);

            if (excel)
                return RptToExcel(rpt, "Listado_Restante_Producciones");
            else
                return RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult CostosProductos()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Vista_Stock_Producido_Expedido()
        {
            StockProductosModel objeto = new StockProductosModel();
            objeto.Fecha_Desde = DateTime.Now;
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }

        [HttpGet]
        public ActionResult Stock_Producido_Expedido(int? Codigo_Fabrica, string Fecha_Desde, string Fecha_Hasta)
        {
            // Fue removida la referencia al reporte RPT_Expedido_Producido
            // porque no se encontraba en el proyecto

            /// TODO: terminar de implementar la funcionalidad del listado con el reporte correcto
            throw new NotImplementedException("Éste listado aún no está terminado");

            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            db.RPT_Stock_Producido_Expedido(Desde, Hasta, Codigo_Fabrica);

            /// TODO: Crear instancia del reporte correcto

            /// TODO: cargar el reporte con los datos
            /// crear el stream con el resultado para devolver el archivo

            return null; /// TODO: reemplazar por el archivo a retornar
        }

        [HttpGet]
        public ActionResult Vista_Stock_Productos()
        {
            StockProductosModel objeto = new StockProductosModel();
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }

        [HttpGet]
        public ActionResult Stock_Productos(int? Codigo_Fabrica, string Fecha_Hasta, bool Solo_Totales, bool ExcluirVacios)
        {
            DateTime Hasta;
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Stock_Estado_Productos(Hasta, Hasta, Codigo_Fabrica);

            Stock_Producto_Estado rpt = new Stock_Producto_Estado();

            rpt.Load();
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == null ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            rpt.SetParameterValue("Solo_Totales", Solo_Totales);
            rpt.SetParameterValue("ExcluirVacios", ExcluirVacios);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Stock_Resumido()
        {
            StockProductosModel objeto = new StockProductosModel();
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }

        [HttpGet]
        public ActionResult Stock_Resumido(int? Codigo_Fabrica, string Fecha_Hasta, string agrupacion, bool excel = false)
        {
            DateTime Hasta;
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Stock_Resumido(Hasta, Codigo_Fabrica, agrupacion);

            ReportClass rpt;

            if (agrupacion == "Camara")
                rpt = new Stock_Resumido_a_por_camara();
            else if (agrupacion == "Estado")
                rpt = new Stock_Resumido_a_por_estado();
            else
                rpt = new Stock_Resumido();

            rpt.Load();
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            rpt.SetParameterValue("Excel", excel);
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == null ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            setConnectionInfo(rpt);

            return excel ? RptToExcel(rpt, "Stock_Resumido") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Stock_Productos_Detallado()
        {
            StockProductosDetalladoModel objeto = new StockProductosDetalladoModel();
            objeto.Fecha_Desde = DateTime.Now;
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;

            return View(objeto);
        }

        [HttpGet]
        public ActionResult Vista_Expedido()
        {
            StockProductosDetalladoModel objeto = new StockProductosDetalladoModel();
            objeto.Fecha_Desde = DateTime.Now;
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;

            return View(objeto);
        }
        
        [HttpGet]
        public ActionResult RPT_Expedido(string Fecha_Desde, string Fecha_Hasta, string Numero_Producto_Venta, bool Excel, int? Codigo_Fabrica = -1)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Expediciones(Codigo_Fabrica, Desde, Hasta, Numero_Producto_Venta);
            RPT_Expediciones rpt = new RPT_Expediciones();

            rpt.Load();
            rpt.SetParameterValue("Excel", !Excel);
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());
            rpt.SetParameterValue("Fecha_Hasta", Hasta);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Expediciones") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult RPT_Expedido_Excel(string Fecha_Desde, string Fecha_Hasta, string Numero_Producto_Venta, bool Excel, int? Codigo_Fabrica = -1)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Expediciones_Excel(Codigo_Fabrica, Desde, Hasta, Numero_Producto_Venta);
            RPT_Expediciones_Excel rpt = new RPT_Expediciones_Excel();

            rpt.Load();
            rpt.SetParameterValue("Excel", !Excel);
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());
            rpt.SetParameterValue("Fecha_Hasta", Hasta);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Expediciones") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Stock_Productos_Detallado(int? Codigo_Fabrica, string Fecha_Desde, string Fecha_Hasta, string Codigo_Producto, bool Excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Stock_Estado_Productos_Detallado(Desde, Hasta, Codigo_Fabrica, Codigo_Producto);
            Stock_Producto_Estado_Detallado rpt = new Stock_Producto_Estado_Detallado();

            rpt.Load();
            rpt.SetParameterValue("Excel", !Excel);
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("Fecha_Hasta", Hasta);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Stock_Productos_Detallado") : RptToPDF(rpt);
        }
        [HttpGet]
        public ActionResult Vista_Stock_Prod_Venta()
        {
            StockProductosDetalladoModel objeto = new StockProductosDetalladoModel();
            objeto.Fecha_Desde = DateTime.Now;
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;

            return View(objeto);
        }

        [HttpGet]
        public ActionResult Vista_Stock_Estados_Unificado()
        {
            StockProductosDetalladoModel objeto = new StockProductosDetalladoModel();
            objeto.Fecha_Desde = DateTime.Now.AddMonths(-1);
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;

            return View(objeto);
        }

        [HttpGet]
        public ActionResult Vista_Stock_Estados_Unificado_Detallado()
        {
            StockProductosDetalladoModel objeto = new StockProductosDetalladoModel();
            objeto.Fecha_Desde = DateTime.Now.AddMonths(-1);
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;

            return View(objeto);
        }

        [HttpGet]
        public ActionResult Vista_Stock_Detallado()
        {
            StockProductosDetalladoModel objeto = new StockProductosDetalladoModel();
            objeto.Fecha_Desde = DateTime.Now;
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            objeto.Cantidad_Dias_Frescura = 5;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            List<SelectListItem> Listado_Estados = new List<SelectListItem>();

            foreach (var item in db.Estados.ToList())
            {
                Listado_Estados.Add(new SelectListItem()
                {
                    Text = item.Descripcion,
                    Value = item.Descripcion
                });
            }

            Listado_Estados.Add(new SelectListItem()
            {
                Text = "Producido",
                Value = "Producido"
            });

            ViewBag.Listado_Estados = Listado_Estados;

            List<SelectListItem> Listado_agrupacion = new List<SelectListItem>();

            Listado_agrupacion.Add(new SelectListItem()
            {
                Text = "Producto",
                Value = "Producto"
            });
            Listado_agrupacion.Add(new SelectListItem()
            {
                Text = "Estado",
                Value = "Estado"
            });
            var opciones = this.db.OpcionesTrabajo.FirstOrDefault();
            if (opciones.Usa_Stock_Camara)
            {
                Listado_agrupacion.Add(new SelectListItem()
                {
                    Text = "Camara",
                    Value = "Camara"
                });
            }

            ViewBag.Listado_agrupacion = Listado_agrupacion;

            return View(objeto);
        }

        [HttpGet]
        public ActionResult Stock_Frescura(int Cant_Dias_Frescura, int? Codigo_Fabrica, string Fecha_Hasta, string Codigo_Producto, bool Excel, string estado = "")
        {
            DateTime Desde, Hasta;
            DateTime.TryParse("01-01-1800", out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Stock_Detallado(Desde, Hasta, Codigo_Producto, Codigo_Fabrica, estado, "existe", Cant_Dias_Frescura);
            Stock_Detallado rpt = new Stock_Detallado();

            rpt.Load();
            rpt.SetParameterValue("Excel", !Excel);
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == null ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Stock_Detallado") : RptToPDF(rpt);
        }

        public ActionResult Vista_Stock_Producto_Unificado()
        {
            return View();
        }

        public ActionResult Vista_Stock_Estados_Unificado_Resumido(int? Codigo_Fabrica, string Fecha_Desde, string Fecha_Hasta, bool Excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.GetDetallesEnvasadoEstado(Codigo_Fabrica, Desde, Hasta);
            ReportClass rpt = new ReportClass();
            if (Excel)
                rpt = new RPT_Estados_Unificado();
            else
                rpt = new RPT_Estados_Unificado();

            rpt.Load();
            rpt.SetParameterValue("@Fecha_Fin", Fecha_Hasta);
            rpt.SetParameterValue("@Fecha_Desde", Fecha_Desde);
            rpt.SetParameterValue("@Codigo_Fabrica", Codigo_Fabrica);
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Stock_Productos_Unificado") : RptToPDF(rpt);
        }

        public ActionResult Stock_Estados_Unificado_Detallado(int? Codigo_Fabrica, string Fecha_Desde, string Fecha_Hasta, string Numero_Producto, bool Excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.GetDetallesEnvasadoEstadoDetallado(Codigo_Fabrica, Desde, Hasta, Numero_Producto);
            ReportClass rpt = new ReportClass();
            if (Excel)
                rpt = new RPT_Estados_Unificado();
            else
                rpt = new RPT_Estados_Unificado_Detallado();

            rpt.Load();
            rpt.SetParameterValue("@Fecha_Fin", Fecha_Hasta);
            rpt.SetParameterValue("@Fecha_Desde", Fecha_Desde);
            rpt.SetParameterValue("@Codigo_Fabrica", Codigo_Fabrica);
            rpt.SetParameterValue("@Codigo_Producto", Numero_Producto);
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Stock_Productos_Unificado_Detallado") : RptToPDF(rpt);
        }

        public ActionResult Stock_Unificado_Productos(int? Codigo_Fabrica, string Fecha_Desde, string Fecha_Hasta, string Codigo_Producto, bool Excel, bool Resumido, bool Ficha, bool Anterior)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            if (Fecha_Desde == null || Fecha_Desde == "-undefined-undefined")
            {
                Fecha_Desde = "01/01/1990";
            }
            Desde = DateTime.Parse(Fecha_Desde);

            ReportClass rpt = new ReportClass();

            OpcionesTrabajo ot = db.OpcionesTrabajo.FirstOrDefault(); // ot.Fabrica_Deposito
            
            if (Resumido)
            {
                dbAccesoDatos.RPT_Stock_Productos_Unificado_Resumido(Desde, Hasta, Codigo_Producto, Codigo_Fabrica, ot.Fabrica_Deposito != "Fábrica", Anterior);
                if (Excel)
                {
                    rpt = new RPT_Stock_Unificado_Resumido_Excel();
                }
                else
                {
                    rpt = new RPT_Stock_Unificado_Resumido();
                }
            }
            else if (Ficha)
            {
                dbAccesoDatos.RPT_Stock_Productos_Unificado_Ficha(Desde, Hasta, Codigo_Producto, Codigo_Fabrica);//Verificado

                if (Excel)
                {
                    rpt = new RPT_Stock_Unificado_Ficha_Excel();
                }
                else
                {
                    rpt = new RPT_Stock_Unificado_Ficha();
                }
            }
            else
            {
                if (ot.Fabrica_Deposito != "Fábrica")
                    dbAccesoDatos.RPT_Stock_Productos_Unificado(Desde, Hasta, Codigo_Producto, Codigo_Fabrica); //Verificado
                else
                    dbAccesoDatos.RPT_Stock_Productos_Unificado_Fabrica(Hasta, Codigo_Producto, Codigo_Fabrica); //Verificado

                if (Excel)
                {
                    rpt = new RPT_Stock_Unificado_Excel();
                }
                else
                {
                    rpt = new RPT_Stock_Unificado();
                }
            }

            rpt.Load();
            rpt.SetParameterValue("Fecha_Hasta", Fecha_Hasta);

            if (!Excel)
            {
                rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());
            }

            if (Ficha)
            {
                rpt.SetParameterValue("Fecha_Desde", Fecha_Desde);
                rpt.SetParameterValue("Fabrica", Codigo_Fabrica == -1 ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            }

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, $"Stock_Productos_Unificado{(Resumido ? "_Resumido" : "")}") : RptToPDF(rpt);
        }

        public ActionResult Stock_Detallado(int? Codigo_Fabrica, string Fecha_Desde, string Fecha_Hasta, string Codigo_Producto, bool Excel, string agrupacion)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            var existe = "no_existe";
            dbAccesoDatos.RPT_Stock_Detallado(Desde, Hasta, Codigo_Producto, Codigo_Fabrica, "", existe, -1);
            ReportClass rpt = new ReportClass();
            if (agrupacion == "Estado")
                rpt = new Stock_Detallado_Ficha_a_por_estado();
            else if (agrupacion == "Camara")
                rpt = new Stock_Detallado_Ficha_a_por_camara();
            else
                rpt = new Stock_Detallado_Ficha();

            rpt.Load();
            rpt.SetParameterValue("Excel", !Excel);
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == null ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Ficha_Stock_Detallado") : RptToPDF(rpt);
        }

        public ActionResult Stock_Productos_Venta(int? Codigo_Fabrica, string Fecha_Hasta, string Fecha_Desde, string Numero_Producto_Venta, bool Excel, bool Resumido, bool CortePorLote)
        {
            DateTime Hasta;
            DateTime Desde;
            DateTime.TryParse(Fecha_Hasta, out Hasta);
            DateTime.TryParse(Fecha_Desde, out Desde);

            if (Fecha_Desde == null)
            {
                Fecha_Desde = "01-01-1900";
            }
            Desde = DateTime.Parse(Fecha_Desde);

            var titulo = string.Empty;
            var rpt = new ReportClass();
            if (Resumido)
            {
                if (CortePorLote)
                {
                    titulo = "Stock_Productos_Venta_Resumido";
                    if (Excel)
                        rpt = new Stock_Productos_Venta_Excel();
                    else
                        rpt = new Stock_Productos_Venta();
                }
                else
                {
                    titulo = "Stock_Productos_Venta_Resumido_Sin_Corte_Por_Lote";
                    if (Excel)
                        rpt = new Stock_Productos_Venta_Sin_Corte_Por_Lote_Excel();
                    else
                        rpt = new Stock_Productos_Venta_Sin_Corte_Por_Lote();
                }
            }
            else
            {
                titulo = "Stock_Productos_Venta_Detallado";
                if (Excel)
                    rpt = new Stock_Productos_Venta_Detallado_Excel();
                else
                    rpt = new Stock_Productos_Venta_Detallado();

            }

            dbAccesoDatos.RPT_Stock_Productos_Venta(Numero_Producto_Venta, Codigo_Fabrica, Desde, Hasta, Resumido, CortePorLote);

            rpt.Load();
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == null ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, $"{titulo}.xls") : RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Responsables()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Vista_Producciones_Detalladas()
        {
            StockProductosDetalladoModel objeto = new StockProductosDetalladoModel();
            objeto.Fecha_Desde = DateTime.Now;
            objeto.Fecha_Hasta = DateTime.Now;
            objeto.Codigo_Fabrica = 0;
            if (Sesion.UsuarioActual != null)
                objeto.Codigo_Fabrica = Sesion.UsuarioActual.Fabrica.Value != -1 ? Sesion.UsuarioActual.Fabrica.Value : 0;
            return View(objeto);
        }
        public ActionResult Producciones_Detalladas(int? Codigo_Fabrica, string Fecha_Desde, string Fecha_Hasta, string Codigo_Producto, string Tipo_Lote, bool Excel, string Tipo_Estado, string Horas_Acificacion, string PH_Final, string Acidez_Final, string Materia_Grasa, string Proteina, string Humedad, int? Responsable)
        {
            DateTime Desde, Hasta;
            TimeSpan Hora_Acificacion;

            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);
            TimeSpan.TryParse(Horas_Acificacion, out Hora_Acificacion);

            dbAccesoDatos.RPT_Producciones_Detalladas(Desde, Hasta, Codigo_Producto, Codigo_Fabrica, Tipo_Lote, Tipo_Estado, Hora_Acificacion, PH_Final, Acidez_Final, Materia_Grasa, Proteina, Humedad, Responsable);
            bool puedeVerImportes = Sesion.UsuarioActual.Muestra_Costos.HasValue ? Sesion.UsuarioActual.Muestra_Costos.Value : false;
            ReportClass rpt;

            if (Excel == true && puedeVerImportes == false)
            {
                rpt = new EXCEL_Producciones_Detalladas();
            }
            else if (Excel == true && puedeVerImportes)
            {
                rpt = new EXCEL_Producciones_Detalladas_Con_Precios();
            }
            else if (Excel == false && puedeVerImportes)
            {
                rpt = new REP_Producciones_Detalladas_Con_Precios();
            }
            else
            {
                rpt = new REP_Producciones_Detalladas();
            }

            rpt.Load();
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            rpt.SetParameterValue("OcultarImportes", !puedeVerImportes);
            rpt.SetParameterValue("Tipo_Estado", Tipo_Estado);
            rpt.SetParameterValue("Horas_Acificacion", Horas_Acificacion);
            rpt.SetParameterValue("PH_Final", PH_Final);
            rpt.SetParameterValue("Acidez_Final", Acidez_Final);
            rpt.SetParameterValue("Materia_Grasa", Materia_Grasa);
            rpt.SetParameterValue("Proteina", Proteina);
            rpt.SetParameterValue("Humedad", Humedad);
            rpt.SetParameterValue("Responsable", Responsable.ToString());

            if (Tipo_Estado == "0")
            {
                rpt.SetParameterValue("Tipo_Estado", "Todas");
            }
            else if (Tipo_Estado == "1")
            {
                rpt.SetParameterValue("Tipo_Estado", "Ya Envasadas");
            }
            else
            {
                rpt.SetParameterValue("Tipo_Estado", "No Envasadas");
            }

            if (!Excel)
                rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listados_Producciones_Detalladas") : RptToPDF(rpt);
        }
        public ActionResult Vista_Detalle_Remito()
        {
            List<SelectListItem> Tipo_Comprobante = new List<SelectListItem>()
            {
                new SelectListItem() {Text="A", Value="A"},
                new SelectListItem() {Text="B", Value="B"},
                new SelectListItem() {Text="C", Value="C"}
            };
            ViewBag.Tipo_Comprobante = Tipo_Comprobante;
            return View();
        }
        public ActionResult Detalle_Remito(int Prefijo, string Numero_Comprobante, bool Excel)
        {
            int respuesta = dbAccesoDatos.RPT_Detalle_Remito(Numero_Comprobante, Prefijo);

            RPT_Detalle_Remito rpt = new RPT_Detalle_Remito();

            rpt.Load();
            rpt.SetParameterValue("Excel", !Excel);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }
        public ActionResult Vista_Merma()
        {
            return View();
        }
        public ActionResult RPT_Merma_Produccion(string FechaDesde, string FechaHasta, string CodigoProducto, bool Resumido, bool Excel)
        {
            DateTime Hasta;
            DateTime.TryParse(FechaHasta, out Hasta);
            DateTime Desde;
            DateTime.TryParse(FechaDesde, out Desde);

            ReportClass rpt;

            if (Resumido)
            {
                dbAccesoDatos.RPT_Merma_Produccion_Resumida(CodigoProducto);
                if (Excel)
                    rpt = new RPT_Merma_Produccion_Resumida_excel();
                else
                    rpt = new RPT_Merma_Produccion_Resumida();
            }
            else
            {
                dbAccesoDatos.RPT_Merma_Produccion_Detallada(Desde, Hasta, CodigoProducto);
                if (Excel)
                    rpt = new RPT_Merma_Produccion_Detallada_excel();
                else
                    rpt = new RPT_Merma_Produccion_Detallada();
            }           
            rpt.Load();
            rpt.SetParameterValue("FechaDesde", FechaDesde);
            rpt.SetParameterValue("FechaHasta", FechaHasta);
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, Resumido ? "Listado_Merma_Resumido" : "Listado_Merma_Detallado") : RptToPDF(rpt);
        }
        public ActionResult Vista_Remitos_Por_Producto()
        {
            return View();
        }
        public ActionResult Remitos_Por_Producto(string Numero_Producto, string Lote, string Fecha_Desde, string Fecha_Hasta, bool Excel)
        {
            DateTime Desde, Hasta;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime.TryParse(Fecha_Hasta, out Hasta);
            int respuesta = dbAccesoDatos.RPT_Remitos_Por_Producto(Desde, Hasta, Numero_Producto, Lote);

            RPT_Remito_Por_Producto rpt = new RPT_Remito_Por_Producto();

            rpt.Load();
            rpt.SetParameterValue("Excel", !Excel);
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }
        public ActionResult Vista_Seguimiento_Lote()
        {
            if (TempData["Error"] != null)
                ViewBag.Error = TempData["Error"].ToString();
            return View();
        }
        public ActionResult Seguimiento_Lote(string Lote)
        {
            dbAccesoDatos.RPT_Seguimiento_Lote(Lote);

            RPT_Seguimiento_Lote rpt = new RPT_Seguimiento_Lote();

            rpt.Load();
            rpt.SetParameterValue("Lote", Lote);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        public ActionResult RPT_Orden_Compra(int Codigo_Orden, bool Mostrar_Costos = false)
        {
            ReportClass rpt;
            dbAccesoDatos.RPT_Orden_De_Compra(Codigo_Orden);
            if (Mostrar_Costos)
            {
                rpt = new IMP_Compra_Insumos_Muestra_Costos();
            } else
            {
                rpt = new IMP_Compra_Insumos();
            }

            rpt.Load();
            var datos = db.Datos_Empresa.FirstOrDefault();
            var cuit = datos == null ? "" : datos.CUIT;
            rpt.SetParameterValue("CUIT", cuit);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        public ActionResult RPT_Detallado_Formulas(string Codigo_Producto = "", Boolean Incluir_Costos_Indirectos = false)
        {
            dbAccesoDatos.RPT_Formulas_Detalle(Incluir_Costos_Indirectos, Codigo_Producto);

            RPT_Formula rpt = new RPT_Formula();

            rpt.Load();
            rpt.SetParameterValue("Incluir_Costos_Indirectos", Incluir_Costos_Indirectos);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        public ActionResult RPT_CostosProductos(string Codigo_Producto = null, bool Con_Costo_Envasado = false, bool Actualizar_Precios = false)
        {
            dbAccesoDatos.RPT_Precios_Por_Producto(Codigo_Producto, Con_Costo_Envasado, Actualizar_Precios);

            Reports.RPT_Precio_Por_Producto rpt = new Reports.RPT_Precio_Por_Producto();

            rpt.Load();
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);
            rpt.SetParameterValue("Incluir_Envasado", Con_Costo_Envasado);
            rpt.SetParameterValue("Actualizar_Precios", Actualizar_Precios);
            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }
        
        public ActionResult Compras_Por_Proveedor_RPT(int? Linea, int? Rubro, string Fecha_Desde, string Fecha_Hasta, bool Excel, int Codigo_Proveedor = 0, string Codigo_Insumo = "", int Codigo_Fabrica = 0, string Tipo_Movimiento = "", bool IncluirIVA = false, string Ver = "detalles", bool Resumido = false)
        {
            Lineas lineasObj = db.Lineas.Find(Linea);
            Rubros rubrosObj = db.Rubros.Find(Rubro);

            DateTime Desde;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime Hasta;
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Compras_X_Proveedor(Linea,
                Rubro,
                Codigo_Proveedor,
                Codigo_Insumo,
                Desde,
                Hasta,
                Codigo_Fabrica,
                Tipo_Movimiento,
                IncluirIVA,
                Ver,
                Resumido);

            ReportClass rpt;
            if (Excel && Resumido)
                rpt = new RPT_Compras_Por_Proveedor_excel_Resumido();
            else if (Excel & !Resumido)
                rpt = new RPT_Compras_Por_Proveedor_excel();
            else if (Resumido)
                rpt = new RPT_Compras_Por_Proveedor_Resumido();
            else
                rpt = new RPT_Compras_Por_Proveedor();

            rpt.Load();
            
            rpt.SetParameterValue("Linea", lineasObj == null ? "" : lineasObj.Nombre);
            rpt.SetParameterValue("Rubro", rubrosObj == null ? "" : rubrosObj.Nombre);
            rpt.SetParameterValue("FechaDesde", Desde);
            rpt.SetParameterValue("FechaHasta", Hasta);
            rpt.SetParameterValue("Imputaciones", Ver != "detalles");
            rpt.SetParameterValue("Excel", Excel);
            if (IncluirIVA)
                rpt.SetParameterValue("Aclaracion_Iva", "Los Precios Incluyen IVA");
            else
                rpt.SetParameterValue("Aclaracion_Iva", "Los Precios NO Incluyen IVA");

            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Compras_Por_Proveedor") : RptToPDF(rpt);
        }
        public ActionResult RPT_Detallado_Formula_Envasado(string Numero_Producto_Ventas = "", Boolean Incluir_Costos_Indirectos = false)
        {
            dbAccesoDatos.RPT_Formula_Envasado_Detalle(Incluir_Costos_Indirectos, Numero_Producto_Ventas);
            db.SaveChanges();

            Reports.RPT_Formula_Envasado rpt = new Reports.RPT_Formula_Envasado();

            rpt.Load();
            rpt.SetParameterValue("Incluir_Costos_Indirectos", Incluir_Costos_Indirectos);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("PathImagen", direccion);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }
        [HttpPost]
        public ActionResult Resumen_Produccion(ResumenProdModel produccion)
        {
            using (Entities dbContext = new Entities())
            {
                OpcionesTrabajo configuraciones = new OpcionesTrabajo();
                configuraciones = db.OpcionesTrabajo.FirstOrDefault();

                if (configuraciones.Utiliza_Login == true)
                {
                    if (Sesion.UsuarioActual != null)
                    {
                        Permisos permisosUsuario = new Permisos();
                        permisosUsuario = db.Permisos.Where(p => p.idUsuario == Sesion.UsuarioActual.idUsuario).FirstOrDefault();

                        if (permisosUsuario.Resumen_Produccion == true)
                        {
                            dbContext.RPT_Resumen_Envasado_Produccion_Total(produccion.FechaDesde, produccion.FechaHasta, produccion.Codigo_Fabrica, produccion.Codigo_Producto);

                            db.SaveChanges();

                            if (db.RPT_Resumen_Produccion.Count() > 0)
                            {
                                this.HttpContext.Session["FechaDesde"] = produccion.FechaDesde;
                                this.HttpContext.Session["FechaHasta"] = produccion.FechaHasta;

                                this.HttpContext.Session["ReportName"] = "Resumen_Produccion.rpt";
                                this.HttpContext.Session["rptSource"] = db.RPT_Resumen_Produccion.ToList();

                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false, message = "No existen registros." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account");

                    }
                }
                else
                {
                    dbContext.RPT_Resumen_Envasado_Produccion_Total(produccion.FechaDesde, produccion.FechaHasta, -1, produccion.Codigo_Producto/*, 1*/);

                    db.SaveChanges();

                    if (db.RPT_Resumen_Produccion.Count() > 0)
                    {
                        this.HttpContext.Session["FechaDesde"] = produccion.FechaDesde;
                        this.HttpContext.Session["FechaHasta"] = produccion.FechaHasta;

                        this.HttpContext.Session["ReportName"] = "Resumen_Produccion.rpt";
                        this.HttpContext.Session["rptSource"] = db.RPT_Resumen_Envasado.ToList();

                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, message = "No existen registros." }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
        }
        public ActionResult RPT_StockLeche(string FechaDesde, string FechaHasta, int Codigo_Silo = 0, bool Excel = false, bool Resumido = false)
        {
            DateTime Hasta;
            DateTime.TryParse(FechaHasta, out Hasta);
            DateTime Desde;
            DateTime.TryParse(FechaDesde, out Desde);

            dbAccesoDatos.RPT_Stock_Leche_Total(Desde, Hasta, Codigo_Silo, Resumido);
            ReportClass rpt;

            if (Resumido)
                rpt = new Stock_Leche_Resumido();
            else
                rpt = new Stock_Leche();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", FechaDesde);
            rpt.SetParameterValue("FechaHasta", FechaHasta);
            if (!Resumido)
                rpt.SetParameterValue("Excel", Excel);
            rpt.SetParameterValue("PathImagen", Obtener_Path_Imagen());

            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Stock_Leche") : RptToPDF(rpt);
        }

        public ActionResult Vista_Listado_Remitos_Traspaso_Fabricas()
        {
            return View();
        }

        public ActionResult Listado_Remitos_Traspaso_Fabricas(string FechaDesde, string FechaHasta, int FabricaOrigen, int FabricaDestino, int? Estado)
        {
            DateTime Desde, Hasta;
            Desde = DateTime.Parse(FechaDesde);
            Hasta = DateTime.Parse(FechaHasta);

            dbAccesoDatos.RPT_Listado_Remitos_Traspaso_Fabricas(Desde, Hasta, FabricaOrigen, FabricaDestino, Estado);

            RPT_Listado_Remitos_Traspaso_Fabricas rpt = new RPT_Listado_Remitos_Traspaso_Fabricas();

            rpt.Load();
            rpt.SetParameterValue("FechaDesde", FechaDesde);
            rpt.SetParameterValue("FechaHasta", FechaHasta);
            rpt.SetParameterValue("FabricaOrigen", FabricaOrigen);
            rpt.SetParameterValue("FabricaDestino", FabricaDestino);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        public ActionResult Ingresos_Egresos(
            string Fecha_Desde,
            string Fecha_Hasta,
            int Fabrica = -1,
            string Producto = "",
            int Linea = -1,
            int Rubro = -1)
        {
            DateTime Desde, Hasta;
            Desde = DateTime.Parse(Fecha_Desde);
            Hasta = DateTime.Parse(Fecha_Hasta);

            dbAccesoDatos.RPT_Ingresos_Egresos(Fabrica, Linea, Rubro, Producto, Hasta, Desde);

            RPT_Ingresos_Egresos rpt = new RPT_Ingresos_Egresos();

            rpt.Load();
            rpt.SetParameterValue("Fecha_Desde", Fecha_Desde);
            rpt.SetParameterValue("Fecha_Hasta", Fecha_Hasta);
            rpt.SetParameterValue("Fabrica", Fabrica == -1 ? "" : $"{Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Fabrica).FirstOrDefault().Descripcion}");
            rpt.SetParameterValue("Linea", Linea);
            rpt.SetParameterValue("Rubro", Rubro);

            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        public ActionResult Vista_Hormas_Consumidas()
        {
            return View();
        }

        public ActionResult RPT_Hormas_Consumidas(string Fecha_Desde, string Fecha_Hasta, string Numero_Producto = "", int Fabrica = -1)
        {
            DateTime Desde = DateTime.Parse(Fecha_Desde);
            DateTime Hasta = DateTime.Parse(Fecha_Hasta);

            dbAccesoDatos.RPT_Hormas_Consumidas(Numero_Producto, Fabrica, Desde, Hasta);

            RPT_Hormas_Consumidas rpt = new RPT_Hormas_Consumidas();

            rpt.Load();
            rpt.SetParameterValue("Path_Imagen", Obtener_Path_Imagen());
            rpt.SetParameterValue("Fabrica", Fabrica == -1 ? "" : $"{Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Fabrica).FirstOrDefault().Descripcion}");
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }
        
        public ActionResult Vista_Analisis_Microbiologicos()
        {
            List<AccesoDatos.Contexto.Produccion.Analisis_Microbiologico> listadoAnalisis = AnalisisMicrobiologicoServicio.Obtener().ToList();
            return View(listadoAnalisis);
        }

        public ActionResult RPT_Analisis_Microbiologicos(string Codigo_Producto, int Codigo_Fabrica, string Fecha_Desde, string Fecha_Hasta, List<int> Codigos_Analisis, bool Resumido)
        {
            DateTime Desde = DateTime.Parse(Fecha_Desde == null ? DateTime.Now.ToString() : Fecha_Desde);
            DateTime Hasta = DateTime.Parse(Fecha_Hasta == null ? DateTime.Now.ToString() : Fecha_Hasta);

            foreach (AccesoDatos.Contexto.Produccion.Analisis_Microbiologico analisis in AnalisisMicrobiologicoServicio.Obtener().ToList())
            {
                analisis.Visible_Listado = false;
                AnalisisMicrobiologicoServicio.Editar(analisis);
            }
            if (Codigos_Analisis.Count == 0)
                Codigos_Analisis = AnalisisMicrobiologicoServicio.Obtener().Select((am) => am.Codigo_Interno_Analisis).ToList();

            foreach(int Codigo in Codigos_Analisis)
            {
                AccesoDatos.Contexto.Produccion.Analisis_Microbiologico analisis = AnalisisMicrobiologicoServicio.Obtener((am) => am.Codigo_Interno_Analisis == Codigo).FirstOrDefault();
                analisis.Visible_Listado = true;
                AnalisisMicrobiologicoServicio.Editar(analisis);
            }

            ReportClass rpt = new ReportClass();
            if (Resumido)
            {
                dbAccesoDatos.RPT_Analisis_Microbiologicos_Resumido(Codigo_Producto, Codigo_Fabrica, Desde, Hasta);
                rpt = new RPT_Analisis_Microbiologicos_Resumido();
            }
            else
            {
                dbAccesoDatos.RPT_Analisis_Microbiologicos_Detallado(Codigo_Producto, Codigo_Fabrica, Desde, Hasta);
                rpt = new RPT_Analisis_Microbiologicos_Detallado();
            }


            rpt.Load();
            rpt.SetParameterValue("Path_Imagen", Obtener_Path_Imagen());
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == -1 ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            setConnectionInfo(rpt);

            return RptToPDF(rpt);
        }

        [HttpGet]
        public ActionResult Vista_Cambios_Estado()
        {
            return View();
        }

        [HttpGet]
        public ActionResult RPT_Cambios_Estado(int Codigo_Fabrica, string Fecha_Desde, string Fecha_Hasta, string Lote, bool Excel)
        {
            DateTime Desde;
            DateTime.TryParse(Fecha_Desde, out Desde);
            DateTime Hasta;
            DateTime.TryParse(Fecha_Hasta, out Hasta);

            dbAccesoDatos.RPT_Cambios_Estado(
                Codigo_Fabrica,
                Desde,
                Hasta,
                Lote);

            RPT_Cambios_Estado rpt = new RPT_Cambios_Estado();

            rpt.Load();
            rpt.SetParameterValue("Fabrica", Codigo_Fabrica == -1 ? "" : $"{Codigo_Fabrica} - {FabricaServicio.Obtener((f) => f.Codigo_Fabrica == Codigo_Fabrica).FirstOrDefault().Descripcion}");
            rpt.SetParameterValue("Fecha_Desde", Desde);
            rpt.SetParameterValue("Fecha_Hasta", Hasta);
            var direccion = Obtener_Path_Imagen();
            rpt.SetParameterValue("Path_Imagen", direccion);
            rpt.SetParameterValue("Excel", Excel);
            setConnectionInfo(rpt);

            return Excel ? RptToExcel(rpt, "Listado_Cambios_Estado") : RptToPDF(rpt);
        }

        private FileStreamResult RptToPDF(ReportClass rpt)
        {
            Stream s = rpt.ExportToStream(ExportFormatType.PortableDocFormat);
            return File(s, "application/pdf");
        }


        private FileStreamResult RptToExcel(ReportClass rpt, string filename)
        {
            Stream s = rpt.FormatEngine.ExportToStream(getMyExportRequestContext());
            return File(s, "vnd.ms-excel", filename + ".xls");
        }

        public void setConnectionInfo(ReportClass rpt)
        {
            Cadena_Conexion cadena = db.Cadena_Conexion.FirstOrDefault();

            ConnectionInfo crconnectioninfo = new ConnectionInfo();
            TableLogOnInfo crtablelogoninfo;

            crconnectioninfo.ServerName = cadena.Servidor;
            crconnectioninfo.DatabaseName = cadena.Tabla;
            crconnectioninfo.UserID = cadena.Usuario;
            crconnectioninfo.Password = cadena.Contraseña;

            foreach (Table t in rpt.Database.Tables)
            {
                crtablelogoninfo = t.LogOnInfo;
                crtablelogoninfo.ConnectionInfo = crconnectioninfo;
                t.ApplyLogOnInfo(crtablelogoninfo);
            }

            foreach (ReportDocument sr in rpt.Subreports)
            {
                foreach (Table t in sr.Database.Tables)
                {
                    crtablelogoninfo = t.LogOnInfo;
                    crtablelogoninfo.ConnectionInfo = crconnectioninfo;
                    t.ApplyLogOnInfo(crtablelogoninfo);
                }
            }
        }

        public ExportRequestContext getMyExportRequestContext()
        {
            ExcelDataOnlyFormatOptions excelOptsDataOnly = ExportOptions.CreateDataOnlyExcelFormatOptions();

            excelOptsDataOnly.ExcelUseConstantColumnWidth = true;

            ExportOptions exportOpts = new ExportOptions();
            exportOpts.ExportFormatOptions = excelOptsDataOnly;
            exportOpts.ExportFormatType = ExportFormatType.ExcelRecord;

            ExportRequestContext req = new ExportRequestContext();
            req.ExportInfo = exportOpts;
            return req;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                dbAccesoDatos.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}