﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;

using MVC_Produccion.ContextoGastos;
using MVC_Produccion.Sesiones;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Data.Entity.Validation;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class CompraInsumosController : Controller
    {
        private dbProdSP db = new dbProdSP();

        private IBaseServicio<CompraInsumos> CompraInsumosServicio;
        private IBaseServicio<Detalle_Compra> DetalleCompraServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoPermisos;
        private IBaseServicio<Monedas> MonedasServicio;
        private IBaseServicio<Insumos> InsumosServicio;
        private IBaseServicio<Detalle_Imputacion> DetalleImputacionServicio;
        private IBaseServicio<Orden_Compra> OrdenCompraServicio;

        private OpcionesTrabajo Configuraciones;

        public CompraInsumosController(IBaseServicio<CompraInsumos> compraInsumosServicio,
                                        IBaseServicio<Detalle_Compra> detalleCompraServicio,
                                        IBaseServicio<Permisos> permisosServicio,
                                        IBaseServicio<OpcionesTrabajo> opcionesTrabajoPermisos,
                                        IBaseServicio<Monedas> monedasServicio,
                                        IBaseServicio<Insumos> insumosServicio,
                                        IBaseServicio<Detalle_Imputacion> detalleImputacion,
                                        IBaseServicio<Orden_Compra> ordenCompraServicio)
        {
            this.CompraInsumosServicio = compraInsumosServicio;
            this.DetalleCompraServicio = detalleCompraServicio;
            this.PermisosServicio = permisosServicio;
            this.MonedasServicio = monedasServicio;
            this.OpcionesTrabajoPermisos = opcionesTrabajoPermisos;
            this.InsumosServicio = insumosServicio;
            this.DetalleImputacionServicio = detalleImputacion;
            this.OrdenCompraServicio = ordenCompraServicio;
            this.Configuraciones = this.OpcionesTrabajoPermisos.Obtener().FirstOrDefault();
        }



        private GastosEntities dbGastos = new GastosEntities();

        private void PasarListasViewBag()
        {
            ViewBag.Imputar_Total_O_Neto = this.Configuraciones.Imputar_Total_O_Neto;
            ViewBag.ImputarCompras = this.Configuraciones.ImputarCompras;
            ViewBag.CondicionPago = "";
            // SE RETORNA LA VISTA PARA CREAR UN NUEVO REGISTRO
            List<SelectListItem> Listado_Tipos_Factura = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="A", Value="Tipo A"},
                                    new SelectListItem() {Text="B", Value="Tipo B"},
                                    new SelectListItem() {Text="C", Value="Tipo C"},
                                    new SelectListItem() {Text="Blanco", Value="Tipo X"},
                                };

            ViewBag.Listado_Tipos_Factura = Listado_Tipos_Factura;
            List<SelectListItem> Listado_Tipos_Movimiento = new List<SelectListItem>()
                                {
                                    new SelectListItem() {Text="Ingreso Mercadería", Value="Ingreso Mercadería"},
                                    new SelectListItem() {Text="Nota de Crédito", Value="Nota de Crédito"},
                                    new SelectListItem() {Text="Remito", Value="Remito"},
                                    new SelectListItem() {Text="N/C Remito", Value="N/C Remito"},
                                };
            ViewBag.Listado_Tipos_Movimiento = Listado_Tipos_Movimiento;
        }
        private string ObtenerArchivo(int codigo)
        {
            try
            {
                var ruta = Server.MapPath("~/Facturas/");

                string nombre_ = codigo + ".*";
                DirectoryInfo directorio = new DirectoryInfo(ruta);
                FileInfo[] archivos = directorio.GetFiles(nombre_);
                if (archivos.Length > 0)
                {
                    var nombre = Path.GetFileName(archivos[0].Name);
                    var direccion = Path.Combine("Facturas/", nombre);
                    Random random = new Random();
                    int randomNumber = random.Next(0, 1000);

                    direccion += "?random = " + randomNumber;
                    return direccion;
                }
                return "";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException != null ? ex.InnerException.Message : "");
                Console.WriteLine(ex.Message);
                return "";
            }
        }

        public ActionResult Index(int resultado = 0)
        {
            if (resultado == 1)
                ViewBag.Resultado = "Registro guardado satisfactoriamente.";
            if (resultado == 2)
                ViewBag.Resultado = "Registro modificado satisfactoriamente.";

            var compras = this.CompraInsumosServicio.Obtener();
            List<SelectListItem> ListadoEstados = new List<SelectListItem>()
                            {
                                new SelectListItem() {Text="No Histórica", Value="false"},
                                new SelectListItem() {Text="Histórica", Value="true"}
                            };

            ViewBag.ListadoEstados = ListadoEstados;

            return View(compras);
        }

        // TRAE TODOS LOS REGISTROS
        public ActionResult ObtenerCotizacionActual()
        {
            try
            {
                var monedas = this.MonedasServicio.Obtener();
                var dolar = monedas.SingleOrDefault(m => m.nombre == "Dólares");
                var euros = monedas.SingleOrDefault(m => m.nombre == "Euros");

                return Json(new
                {
                    ok = true,
                    mensaje = "",
                    dolar = dolar.intercambio,
                    euros = euros.intercambio
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { ok = false, mensaje = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetData(bool historica = false)
        {
            var listado = db.JOIN_compraInsumos_proveedor(-1, historica).ToList();
            var jsonResult = Json(new { data = listado }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult GetComprasPorProveedor(string FechaDesde, string FechaHasta, int codProv = -1)
        {
            try
            {
                var f_desde = DateTime.Parse(FechaDesde);
                var f_hasta = DateTime.Parse(FechaHasta);

                var listado = db.Compras_Por_Proveedor(codProv, f_desde, f_hasta).ToList().Select(c => new
                {
                    c.Codigo_Comprobante,
                    c.Codigo_Interno_Compra,
                    c.Codigo_Proveedor,
                    c.Fecha,
                    c.Nro_Remito,
                    c.Numero_Comprobante,
                    c.Prefijo,
                    c.RAZON_SOCIAL,
                    c.Tipo_Factura,
                    c.Tipo_Movimiento,
                    c.Total,
                    link = ObtenerArchivo(c.Codigo_Interno_Compra)
                });

                return Json(new { data = listado }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, mensaje = "Ocurrió un error, por favor intente nuevamente", error = e }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetOne(int Codigo_Interno_Compra)
        {
            if (Codigo_Interno_Compra < 0)
                return Json(new { success = false, mensaje = "Error, no existe registro con Código negativo." }, JsonRequestBehavior.AllowGet);

            try
            {
                var compra = this.CompraInsumosServicio.Obtener((c) => c.Codigo_Interno_Compra == Codigo_Interno_Compra).SingleOrDefault();
                return Json(new { data = compra, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, mensaje = "Error, no existe registro con el Código ingresado." }, JsonRequestBehavior.AllowGet);
            }
        }

        //TRAE TODOS LOS DETALLES ASOCIADOS A LA COMPRA
        public ActionResult GetDetalles(int Codigo_Interno_Compra = 0)
        {
            try
            {
                if (Codigo_Interno_Compra == 0)
                {
                    List<Detalle_Compra> list = new List<Detalle_Compra>();

                    return Json(new { data = list }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var listado = db.GetDetallesProduccionCompra(Codigo_Interno_Compra).ToList().Select(item => new
                    {
                        item.Codigo_Detalle_Interno,
                        Lote = item.Lote,
                        item.Codigo_Insumo,
                        item.Cantidad,
                        item.Kilos_Litros,
                        item.Codigo_Fabrica,
                        item.Sin_Cargo_Kilos_Litros,
                        item.Precio_Costo,
                        item.Precio_Lista,
                        item.Codigo_Imputacion,
                        item.Importe,
                        item.Codigo_Interno_Compra,
                        item.Fecha,
                        item.Descuento,
                        item.Sin_Cargo_Cantidad,
                        item.Descripcion_Insumo,
                        item.Precio_Por,
                        item.Descripcion_Imputacion,
                        item.Total,
                        item.IVA,
                        item.Condicion_IVA,
                        item.Recibidos,
                        item.Faltantes,
                        item.codigo_detalle_OC,
                        item.Moneda,
                        precioCambio = item.Precio_Lista1,
                        Fecha_Vencimiento = item.Fecha_Vencimiento.HasValue ? item.Fecha_Vencimiento.Value.ToString("dd/MM/yyyy") : "",
                        item.Ph,
                        item.Humedad,
                        item.Masa
                    });

                    return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = e, success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetDetallesImputacion(int Codigo_Interno_Compra = 0)
        {
            try
            {
                var listado = db.GetDetallesImputaciones(Codigo_Interno_Compra).ToList();

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            ViewBag.error = "";
            try
            {
                PasarListasViewBag();

                if (id == 0)
                {
                    CompraInsumos compra = new CompraInsumos();
                    compra.Fecha = DateTime.Now;
                    compra.Periodo_Contable = DateTime.Now;
                    int fabrica = Sesion.UsuarioActual.Fabrica.HasValue ? Sesion.UsuarioActual.Fabrica.Value : -1;
                    if (fabrica != -1)
                        compra.Codigo_Fabrica = fabrica;
                    compra.Prefijo = 0;
                    compra.Suma_Stock = true;
                    compra.Precio_Dolar = Convert.ToDecimal(this.MonedasServicio.Obtener((m) => m.nombre == "Dólares").SingleOrDefault().intercambio);
                    compra.Precio_Euros = Convert.ToDecimal(this.MonedasServicio.Obtener((m) => m.nombre == "Euros").SingleOrDefault().intercambio);

                    return View(compra);
                }
                else
                {
                    var compra = this.CompraInsumosServicio.Obtener((c) => c.Codigo_Interno_Compra == id).SingleOrDefault();
                    try
                    {
                        Cond_Pago condicion = new Cond_Pago();
                        condicion = dbGastos.Cond_Pago.Where(cp => cp.CODIGO == compra.Condicion_Pago).FirstOrDefault();
                        ViewBag.CondicionPago = condicion != null ? condicion.DESCRIPCION : "";
                    }
                    catch (Exception ex)
                    {
                        ViewBag.error = ex.Message;
                        return Json(new { success = false, message_inner = (ex.InnerException != null ? ex.InnerException.Message : ""), ex.Message }, JsonRequestBehavior.AllowGet);
                    }

                    // SE RETORNA LA VISTA PARA EDITAR UN REGISTRO
                    try
                    {
                        var ruta = Server.MapPath("~/Facturas/");

                        string nombre_ = compra.Codigo_Interno_Compra + ".*";
                        DirectoryInfo directorio = new DirectoryInfo(ruta);
                        FileInfo[] archivos = directorio.GetFiles(nombre_);
                        if (archivos.Length > 0)
                        {
                            var nombre = Path.GetFileName(archivos[0].Name);
                            var direccion = Path.Combine("Facturas/", nombre);
                            ViewBag.ruta_archivo = direccion;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.InnerException != null ? ex.InnerException.Message : "");
                        Console.WriteLine(ex.Message);
                    }

                    if (compra.Precio_Dolar == null || compra.Precio_Dolar == 0)
                        compra.Precio_Dolar = Convert.ToDecimal(this.MonedasServicio.Obtener((m) => m.nombre == "Dólares").SingleOrDefault().intercambio);

                    if (compra.Precio_Euros == null || compra.Precio_Euros == 0)
                        compra.Precio_Euros = Convert.ToDecimal(this.MonedasServicio.Obtener((m) => m.nombre == "Euros").SingleOrDefault().intercambio);

                    return View(compra);
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return Json(new { success = false, message_inner = (ex.InnerException != null ? ex.InnerException.Message : ""), ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(CompraInsumos compra, HttpPostedFileBase file)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                if (compra.Codigo_Comprobante != "" && compra.Codigo_Comprobante != null)
                    compra.Codigo_Comprobante = compra.Codigo_Comprobante.Replace(" ", "");

                bool ActualizarCostoInsumos = (bool)Configuraciones.ActualizarCostoInsumos;

                if (compra.Codigo_Interno_Compra == 0)
                {
                    //DETALLES IMPUTACIÓN
                    foreach (var detalle in compra.Detalle_Imputacion.ToList())
                    {
                        if (detalle != null)
                            detalle.Codigo_Interno_Compra = compra.Codigo_Interno_Compra;
                    }
                    //DETALLES COMPRA
                    foreach (var detalle in compra.Detalle_Compra.ToList())
                    {
                        if (detalle != null)
                        {
                            detalle.Codigo_Interno_Compra = compra.Codigo_Interno_Compra;
                            detalle.Fecha = compra.Fecha;
                            if (detalle.Codigo_Fabrica == null)
                                detalle.Codigo_Fabrica = compra.Codigo_Fabrica;

                            Insumos insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == detalle.Codigo_Insumo).FirstOrDefault();
                            if (ActualizarCostoInsumos == true)
                            {
                                if (insumo.Moneda == "Dólares")
                                {
                                    insumo.Precio_Ultima_Compra = detalle.Precio_Costo / compra.Precio_Dolar;
                                    insumo.Precio_Lista = detalle.Precio_Lista / compra.Precio_Dolar;
                                }

                                if (insumo.Moneda == "Euros")
                                {
                                    insumo.Precio_Ultima_Compra = detalle.Precio_Costo.Value / compra.Precio_Euros;
                                    insumo.Precio_Lista = detalle.Precio_Lista.Value / compra.Precio_Euros;
                                }

                                if (insumo.Moneda == "Pesos")
                                {
                                    insumo.Precio_Ultima_Compra = detalle.Precio_Costo;
                                    insumo.Precio_Lista = detalle.Precio_Lista;
                                }
                            }

                            insumo.Fecha_Ultima_Compra = compra.Fecha.Value;
                            this.InsumosServicio.Editar(insumo);
                        }
                    }
                    compra.Historico = false;
                    //SE AGREGA UN REGISTRO
                    try
                    {
                        this.CompraInsumosServicio.Agregar(compra);
                        if (file != null && file.ContentLength > 0)
                        {
                            var ruta = Server.MapPath("~/Facturas/");

                            Directory.CreateDirectory(ruta);
                            var nombre_actual = file.FileName;
                            var poss = nombre_actual.IndexOf('.');
                            var extencion = nombre_actual.Substring(poss + 1);
                            string nuevo_nombre = compra.Codigo_Interno_Compra + "." + extencion;
                            var nombre = Path.GetFileName(nuevo_nombre);
                            var direccion = Path.Combine(ruta, nombre);
                            file.SaveAs(direccion);
                        }
                    }
                    catch (DbEntityValidationException ex)
                    {
                        string errorMessages = "";
                        foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += " -- " + (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        ViewBag.data = errorMessages;
                        PasarListasViewBag();
                        ViewBag.exInner = ex.InnerException != null ? ex.InnerException.Message : "";
                        ViewBag.error = ex.Message;
                        ViewBag.type = 1;

                        return View(compra);
                    }
                    catch (DbUpdateException ex)
                    {
                        PasarListasViewBag();

                        ViewBag.exInner = ex.InnerException != null ? ex.InnerException.Message : "";
                        ViewBag.error = ex.Message;
                        ViewBag.data = ex.Data.ToString();
                        ViewBag.type = 2;

                        return View(compra);
                    }
                    catch (Exception ex)
                    {
                        PasarListasViewBag();
                        ViewBag.exInner = ex.InnerException != null ? ex.InnerException.Message : "";
                        ViewBag.error = ex.Message;
                        var mensaje = "";
                        foreach (var item in ex.Data)
                        {
                            mensaje += " - " + item.ToString();
                        }
                        ViewBag.data = mensaje;
                        ViewBag.type = 3;
                        return View(compra);
                    }
                    scope.Complete();
                    return RedirectToAction("Index", new { resultado = 1 });

                    /*return Json(new { success = true, resultado = 1 }, JsonRequestBehavior.AllowGet);*/
                }
                else
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        var ruta = Server.MapPath("~/Facturas/");

                        Directory.CreateDirectory(ruta);
                        string nombre_ = compra.Codigo_Interno_Compra + ".*";
                        DirectoryInfo directorio = new DirectoryInfo(ruta);
                        FileInfo[] archivos = directorio.GetFiles(nombre_);
                        foreach (var archivo in archivos)
                        {
                            System.IO.File.Delete(ruta + archivo.Name);
                        }
                        var nombre_actual = file.FileName;
                        var poss = nombre_actual.IndexOf('.');
                        var extencion = nombre_actual.Substring(poss + 1);
                        string nuevo_nombre = compra.Codigo_Interno_Compra + "." + extencion;
                        var nombre = Path.GetFileName(nuevo_nombre);
                        var direccion = Path.Combine(ruta, nombre);

                        file.SaveAs(direccion);
                    }
                    //SE EDITA UN REGISTRO

                    if (compra.Historico.HasValue && compra.Historico.Value)
                        db.Anular_Compra_Insumos(compra.Codigo_Interno_Compra, Sesion.UsuarioActual.idUsuario);
                    compra.Historico = false;
                    //DETALLES IMPUTACIÓN
                    foreach (var detalle in this.DetalleImputacionServicio.Obtener((di) => di.Codigo_Interno_Compra == compra.Codigo_Interno_Compra))
                    {
                        this.DetalleImputacionServicio.Borrar(detalle);
                    }

                    foreach (var detalle in compra.Detalle_Imputacion.ToList())
                    {
                        detalle.Codigo_Interno_Compra = compra.Codigo_Interno_Compra;
                        this.DetalleImputacionServicio.Agregar(detalle);
                    }

                    //DETALLES COMPRA
                    foreach (var detalle in this.DetalleCompraServicio.Obtener((dc) => dc.Codigo_Interno_Compra == compra.Codigo_Interno_Compra))
                    {
                        detalle.Codigo_Interno_Compra = compra.Codigo_Interno_Compra;
                        this.DetalleCompraServicio.Borrar(detalle);
                    }
                    foreach (var detalle in compra.Detalle_Compra.ToList())
                    {
                        if (detalle != null)
                        {
                            detalle.Codigo_Interno_Compra = compra.Codigo_Interno_Compra;
                            detalle.Fecha = compra.Fecha;
                            Insumos insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == detalle.Codigo_Insumo).FirstOrDefault();
                            if (ActualizarCostoInsumos == true)
                            {
                                var mayorPL = compra.Detalle_Compra.Where(dc => dc.Codigo_Insumo == detalle.Codigo_Insumo).Max(dc => dc.Precio_Lista);
                                var mayorPC = compra.Detalle_Compra.Where(dc => dc.Codigo_Insumo == detalle.Codigo_Insumo).Max(dc => dc.Precio_Costo);
                                if (detalle.Codigo_Fabrica == null)
                                    detalle.Codigo_Fabrica = compra.Codigo_Fabrica;

                                if (insumo.Moneda == "Dólares")
                                {
                                    insumo.Precio_Ultima_Compra = mayorPC / compra.Precio_Dolar;
                                    insumo.Precio_Lista = mayorPL / compra.Precio_Dolar;
                                }

                                if (insumo.Moneda == "Euros")
                                {
                                    insumo.Precio_Ultima_Compra = mayorPC / compra.Precio_Euros;
                                    insumo.Precio_Lista = mayorPL / compra.Precio_Euros;
                                }

                                if (insumo.Moneda == "Pesos")
                                {
                                    insumo.Precio_Ultima_Compra = mayorPC;
                                    insumo.Precio_Lista = mayorPL;
                                }

                            }
                            insumo.Fecha_Ultima_Compra = compra.Fecha.Value;
                            this.InsumosServicio.Editar(insumo);
                            this.DetalleCompraServicio.Agregar(detalle);
                        }
                    }
                    this.CompraInsumosServicio.Editar(compra);
                    scope.Complete();
                    return RedirectToAction("Index", new { resultado = 2 });
                }
            }
        }

        [HttpPost]
        public ActionResult Actualizar(int Codigo_Interno_Compra = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    CompraInsumos compraActual = this.CompraInsumosServicio.Obtener((c) => c.Codigo_Interno_Compra == Codigo_Interno_Compra).SingleOrDefault();
                    db.Pasar_Compra_Insumos(Codigo_Interno_Compra);
                    compraActual.Historico = true;
                    this.CompraInsumosServicio.Editar(compraActual);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro actualizado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Interno_Compra)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    CompraInsumos ci = this.CompraInsumosServicio.Obtener((c) => c.Codigo_Interno_Compra == Codigo_Interno_Compra).SingleOrDefault();
                    if (ci.Historico.HasValue && ci.Historico.Value)
                        db.Anular_Compra_Insumos(ci.Codigo_Interno_Compra, Sesion.UsuarioActual.idUsuario);

                    if (ci.Codigo_Interno_Orden != null && ci.Codigo_Interno_Orden.Value != 0)
                        ActualizarRestantesOC(ci);

                    this.CompraInsumosServicio.Borrar(ci);

                    scope.Complete();
                    return Json(new
                    {
                        success = true,
                        message = "Registro eliminado satisfactoriamente."
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new
                    {
                        success = false,
                        message = "No se pudo eliminar, corrobore si el registro está en uso.",
                        errorDetail = e.ToString()
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        private void ActualizarRestantesOC(CompraInsumos oc)
        {            
            var orden = OrdenCompraServicio.Obtener(x => x.Codigo_Interno_Orden == oc.Codigo_Interno_Orden).FirstOrDefault();
            if (orden == null)
                return;

            var detallesOrden = orden.Detalle_Orden_Compra;
            foreach (var item in detallesOrden)
            {
                var detalleCompra = oc.Detalle_Compra.Where(x => x.Codigo_Insumo == item.Codigo_Insumo).FirstOrDefault();
                if (detalleCompra != null)
                {
                    decimal cantidadARestar = 0;
                    if (detalleCompra.Cantidad.HasValue)
                        cantidadARestar = (decimal)detalleCompra.Cantidad;
                    else
                        cantidadARestar = (decimal)detalleCompra.Kilos_Litros;

                    var total = item.Recibidos - cantidadARestar;
                    item.Recibidos = total < 0 ? 0 : total;
                    item.Faltantes += cantidadARestar;
                }
            }
            OrdenCompraServicio.Editar(orden);           
        }

        public ActionResult ExisteComprobante(int nrocomprob, int prefijo, int proveedor, string tipofactura, string tipoMovimiento)
        {
            var comprobantes = db.Existe_Comprobante(nrocomprob, prefijo, proveedor, tipofactura, tipoMovimiento).ToList();
            if (comprobantes.Count() == 0)
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, message = "Comprobante existente, verifique." }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetFechaCierre()
        {
            dbProdSP dbAccesoDatos = new dbProdSP();
            List<GetParametrosGlobales_Result> r = dbAccesoDatos.GetParametrosGlobales().ToList();
            DateTime fecha_cierre;
            if(r.Count == 0 || r[0].FECHA_CIERRE == null)
            {
                fecha_cierre = DateTime.Today;
            }
            else
            {
                fecha_cierre = r[0].FECHA_CIERRE.Value;
            }

            return Json(new { date = fecha_cierre.ToString("MM/dd/yyyy") }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
