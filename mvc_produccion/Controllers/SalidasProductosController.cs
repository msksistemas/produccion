﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_Produccion.Sesiones;
using AccesoDatos.Contexto.Ventas;
using AccesoDatos.Contexto.Produccion;
using AccesoDatos.Contexto.Produccion.SP;
using Servicios.Interfaces;
using System.Transactions;

namespace MVC_Produccion.Controllers
{
    public class SalidasProductosController : Controller
    {
        private IBaseServicio<Sucursales_Propias> SucursalesServicio;
        private IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> ProductosVentasServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Salida_Productos> SalidaProductosServicio;
        private IBaseServicio<Detalle_Salida_Productos> DetalleSalidaProductos;
        private IBaseServicio<Insumos> InsumosServicio;
        private OpcionesTrabajo Configuraciones;

        public SalidasProductosController(IBaseServicio<Insumos> insumosServicio,
                                          IBaseServicio<Sucursales_Propias> sucursalesPropiasServicio,
                                          IBaseServicio<AccesoDatos.Contexto.Ventas.Productos> productosVentasServicio,
                                          IBaseServicio<Permisos> permisosServicio,
                                          IBaseServicio<OpcionesTrabajo> opcionesTrabajoServicio,
                                          IBaseServicio<Salida_Productos> salidaProductos,
                                          IBaseServicio<Detalle_Salida_Productos> detalleSalidaProductos)
        {
            this.InsumosServicio = insumosServicio;
            this.SalidaProductosServicio = salidaProductos;
            this.DetalleSalidaProductos = detalleSalidaProductos;
            this.SucursalesServicio = sucursalesPropiasServicio;
            this.ProductosVentasServicio = productosVentasServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajoServicio;
            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().FirstOrDefault();
        }
        private dbProdSP db = new dbProdSP();


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData(int Codigo_Interno_Salida = 0)
        {
            if (Codigo_Interno_Salida == -1)
            {
                var listado = this.SalidaProductosServicio.Obtener((s) => s.Historico == false).Select(e => new
                {
                    Codigo_Interno_Salida = e.Codigo_Interno_Salida,
                    Fecha = e.Fecha,
                    e.Sucursal_Destino,
                    e.Sucursal_Origen,
                    e.Prefijo,
                    e.NroComprobante
                });

                return Json(new { data = listado/*, detalles = Detalles_Salida*/, success = true }, JsonRequestBehavior.AllowGet);
            }
            else if (Codigo_Interno_Salida >= 0)
            {
                var Salida = this.SalidaProductosServicio.Obtener((s) => s.Historico == false && s.Codigo_Interno_Salida == Codigo_Interno_Salida)
                                .Select(e => new
                                {
                                    Codigo_Interno_Salida = e.Codigo_Interno_Salida,
                                    Fecha = e.Fecha,
                                    e.Sucursal_Origen,
                                    e.Sucursal_Destino
                                });

                var Detalles_Salida = this.DetalleSalidaProductos.Obtener((det) => det.Codigo_Interno_Salida == Codigo_Interno_Salida)
                    .Select(e => new
                    {
                        Codigo_Interno_Detalle = e.Codigo_Interno_Detalle,
                        Codigo_Interno_Salida = e.Codigo_Interno_Salida,
                        Codigo_Producto = e.Codigo_Producto,
                        Cantidad = e.Cantidad,
                        Kilos_Litros = e.Kilos_Litros,
                        //Descripcion_Producto = (from p in dbVentas.Productos where p.Numero_de_Producto == e.Codigo_Producto select p.Descripcion),
                        e.Lote,
                        e.tipo,
                        e.Codigo_Insumo_Reproceso

                    });
                var lista = new List<object>();
                foreach (var item in Detalles_Salida)
                {
                    var prod = this.ProductosVentasServicio.Obtener((p) => p.Numero_de_Producto == item.Codigo_Producto).SingleOrDefault();
                    if (prod != null)
                        lista.Add(new
                        {
                            item.Codigo_Insumo_Reproceso,
                            item.Cantidad,
                            item.Kilos_Litros,
                            item.Codigo_Producto,
                            item.Codigo_Interno_Detalle,
                            item.Codigo_Interno_Salida,
                            item.Lote,
                            item.tipo,
                            Descripcion_Producto = prod.Descripcion
                        });

                }
                return Json(new { data = Salida, detalles = lista, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = "No existe registro con Código negativo." }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ObtenerLotes(string Cod_Producto, int Cod_fabrica)
        {
            var resultado = db.ObtenerLotesLiberados(Cod_Producto, Cod_fabrica).ToList();
            return Json(new { data = resultado }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
            {
                Salida_Productos salida = new Salida_Productos();
                salida.Fecha = DateTime.Now;

                return View(salida);
            }
            else
            {
                var salida = this.SalidaProductosServicio.Obtener((x) => x.Codigo_Interno_Salida == id).FirstOrDefault();
                var sucursalOrigen = this.SucursalesServicio.Obtener((s) => s.Numero == salida.Sucursal_Origen).SingleOrDefault();
                var sucursalDestino = this.SucursalesServicio.Obtener((s) => s.Numero == salida.Sucursal_Destino).SingleOrDefault();

                ViewBag.Origen = sucursalOrigen != null ? sucursalOrigen.Nombre : "";
                ViewBag.Sucursal = sucursalDestino != null ? sucursalDestino.Nombre : "";
                return View(salida);
            }



        }

        [HttpPost]
        public ActionResult AddOrEdit(Salida_Productos salida)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    if (salida.Prefijo.HasValue && salida.NroComprobante.HasValue)
                    {
                        if (SalidaProductosServicio.Obtener((s) => s.Prefijo.Value == salida.Prefijo.Value 
                                                                 && s.NroComprobante.Value == salida.NroComprobante.Value
                                                                 && (salida.Codigo_Interno_Salida != s.Codigo_Interno_Salida || salida.Codigo_Interno_Salida == 0)).Count != 0) 
                        {
                            return Json(new { success = false, message = "Ya existe un Registro con ese Prefijo y Nro Comprobante" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    string mensaje = "Registro guardado satisfactoriamente.";
                    salida.Historico = false;
                    if (salida.Codigo_Interno_Salida == 0)
                    {
                        foreach (var detalle in salida.Detalle_Salida_Productos)
                        {
                            var insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == detalle.Codigo_Insumo_Reproceso).SingleOrDefault();
                            if (insumo == null)
                                detalle.Codigo_Insumo_Reproceso = "";
                        }
                        this.SalidaProductosServicio.Agregar(salida);
                    }
                    else
                    {
                        /*BORRA TODOS LOS DETALLES ASOCIADOS*/
                        foreach (var detalle in this.DetalleSalidaProductos.Obtener((det) => det.Codigo_Interno_Salida == salida.Codigo_Interno_Salida))
                        {
                            this.DetalleSalidaProductos.Borrar(detalle);
                        }

                        /*AGREGA LOS DETALLES UNO A UNO PASADOS POR PARÁMETRO*/

                        foreach (var detalle in salida.Detalle_Salida_Productos)
                        {
                            var insumo = this.InsumosServicio.Obtener((i) => i.Codigo_Insumo == detalle.Codigo_Insumo_Reproceso).SingleOrDefault();
                            if (insumo == null)
                                detalle.Codigo_Insumo_Reproceso = "";
                            detalle.Codigo_Interno_Salida = salida.Codigo_Interno_Salida;
                            this.DetalleSalidaProductos.Agregar(detalle);
                        }
                        this.SalidaProductosServicio.Editar(salida);
                    }
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);

                }
            }
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = 1;
                if (this.SalidaProductosServicio.Obtener().Count != 0)
                    ultimoRegistro = this.SalidaProductosServicio.Obtener().Max(s => s.Codigo_Interno_Salida);

                return Json(new { data = ultimoRegistro, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Interno_Salida = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Salida_Productos est = this.SalidaProductosServicio.Obtener((x) => x.Codigo_Interno_Salida == Codigo_Interno_Salida).FirstOrDefault();
                    this.SalidaProductosServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { success = false, message = "Error al eliminar, corrobore si el registro está en uso." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Pasar_A_Historico(int Codigo_Interno_Salida = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    db.Actualizar_Salida_Productos(Codigo_Interno_Salida);
                    db.Movimiento_Stock_Venta_Salida_Productos(Codigo_Interno_Salida);
                    scope.Complete();
                    return Json(new { success = true, message = "Actualizado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = "Error al actualizar el registro con Código: " + Codigo_Interno_Salida + ".", error = ex }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
