﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class LineasController : Controller
    {
        private IBaseServicio<Lineas> LineaServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesDeTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;
        private OpcionesTrabajo Configuraciones { get; set; }

        public LineasController(IBaseServicio<Lineas> lineaServicio,
                                IBaseServicio<OpcionesTrabajo> opcionesDeTrabajoServicio,
                                IBaseServicio<Permisos> permisosServicio)
        {
            this.LineaServicio = lineaServicio;
            this.PermisosServicio = permisosServicio;
            this.OpcionesDeTrabajoServicio = opcionesDeTrabajoServicio;
            this.Configuraciones = this.OpcionesDeTrabajoServicio.Obtener().SingleOrDefault();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData(int Codigo_Linea = 0)
        {
            if (Codigo_Linea == -1)
            {
                var listado = this.LineaServicio.Obtener();

                return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var linea = this.LineaServicio.Obtener((l) => l.Codigo_Linea == Codigo_Linea).SingleOrDefault();

                if (linea == null)
                    return Json(new { message = "Error, no existe registro con el Código ingresado.", success = false }, JsonRequestBehavior.AllowGet);

                return Json(new { data = linea, success = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new Lineas());

            var linea = this.LineaServicio.Obtener((l) => l.Codigo_Linea == id).SingleOrDefault();

            return View(linea);
        }

        [HttpPost]
        public ActionResult AddOrEdit(Lineas linea)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    string mensaje = "Registro Guardado satisfactoriamente.";
                    if (linea.Codigo_Interno_Linea == 0)
                        this.LineaServicio.Agregar(linea);
                    else
                    {
                        this.LineaServicio.Editar(linea);
                        mensaje = "Registro Modificado satisfactoriamente.";
                    }
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpPost]
        public ActionResult Delete(int Codigo_Linea = 0)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var lineaBorrar = this.LineaServicio.Obtener((l) => l.Codigo_Linea == Codigo_Linea).SingleOrDefault();
                    this.LineaServicio.Borrar(lineaBorrar);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }         
        }

        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = this.LineaServicio.ObtenerConRawSql("SELECT TOP(1) [Codigo_Interno_Linea], [Codigo_Linea],[Nombre] " +
                                                                        "FROM dbo.Lineas " +
                                                                        "ORDER BY [Codigo_Interno_Linea] DESC").SingleOrDefault();

                return Json(new { data = ultimoRegistro.Codigo_Interno_Linea, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception  ex)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
          
        }
       
    }
}
