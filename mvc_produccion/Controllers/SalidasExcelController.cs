﻿using MVC_Produccion.ContextoProduccion;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static System.Net.Mime.MediaTypeNames;

namespace MVC_Produccion.Controllers
{
    //TODO revisar
    public class SalidasExcelController : Controller
    {
        public ActionResult Salida_Resumen_Produccion()
        {
            using (Entities dbContext = new Entities())
            {
                try
                {
                    OpcionesTrabajo configuraciones = new OpcionesTrabajo();
                    configuraciones = dbContext.OpcionesTrabajo.FirstOrDefault();

                    dbContext.Database.SqlQuery<string>("EXEC Salida_Excel_Resumen_Produccion @RutaArchivo", new Object[]{
                        new SqlParameter("@RutaArchivo", configuraciones.Ruta_EXCELS)}).ToList();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch(Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }
               
            }
        }

        public ActionResult Salida_Excel_Producciones_Por_Producto()
        {
            using (Entities dbContext = new Entities())
            {
                try
                {
                    OpcionesTrabajo configuraciones = new OpcionesTrabajo();
                    configuraciones = dbContext.OpcionesTrabajo.FirstOrDefault();

                    dbContext.Database.SqlQuery<string>("EXEC Salida_Excel_Producciones_Por_Producto @RutaArchivo", new Object[]{
                        new SqlParameter("@RutaArchivo", configuraciones.Ruta_EXCELS)}).ToList();

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.ToString() }, JsonRequestBehavior.AllowGet);
                }

            }
        }
    }

   
}
