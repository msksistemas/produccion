﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web.Mvc;
using AccesoDatos.Contexto.Produccion;
using MVC_Produccion.Sesiones;
using Servicios.Interfaces;

namespace MVC_Produccion.Controllers
{
    public class EstadosController : Controller
    {
        private IBaseServicio<Estados> EstadosServicio;
        private IBaseServicio<OpcionesTrabajo> OpcionesTrabajoServicio;
        private IBaseServicio<Permisos> PermisosServicio;

        private OpcionesTrabajo Configuraciones;

        public EstadosController(IBaseServicio<Estados> estadosServicio,
                                IBaseServicio<OpcionesTrabajo> opcionesTrabajaoServicio,
                                IBaseServicio<Permisos> permisosServicio)
        {
            this.EstadosServicio = estadosServicio;
            this.OpcionesTrabajoServicio = opcionesTrabajaoServicio;
            this.PermisosServicio = permisosServicio;

            this.Configuraciones = this.OpcionesTrabajoServicio.Obtener().SingleOrDefault();
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetData()
        {
            var listado = this.EstadosServicio.Obtener().Select(e => new
            {
                e.Codigo_Interno_Estados,
                e.Codigo_Estado,
                e.Descripcion,
                e.Se_Puede_Envasar
            });

            return Json(new { data = listado, success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOne(int Codigo_Estado)
        {
            var estado = this.EstadosServicio.Obtener((e) => e.Codigo_Estado == Codigo_Estado).Select(e => new
            {
                e.Codigo_Interno_Estados,
                e.Codigo_Estado,
                e.Descripcion,
                e.Se_Puede_Envasar
            }).SingleOrDefault();
            return Json(new { estado = estado, success = true }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new Estados());
            else
                return View(this.EstadosServicio.Obtener((x) => (x).Codigo_Estado == id).SingleOrDefault());
        }
        [HttpPost]
        public ActionResult AddOrEdit(Estados estado)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var mensaje = "Registro modificado satisfactoriamente.";
                    if (estado.Codigo_Interno_Estados == 0)
                    {
                        if (this.EstadosServicio.Obtener((e) => e.Codigo_Estado == estado.Codigo_Estado).SingleOrDefault() != null)
                            return Json(new { success = false, message = "Ya existe un registro con ese Código." }, JsonRequestBehavior.AllowGet);
                        this.EstadosServicio.Agregar(estado);

                        mensaje = "Registro guardado satisfactoriamente.";
                    }
                    else
                        this.EstadosServicio.Editar(estado);
                    scope.Complete();
                    return Json(new { success = true, message = mensaje }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = true, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }

        }
        [HttpGet]
        public ActionResult GetUltimo()
        {
            try
            {
                var ultimoRegistro = this.EstadosServicio.ObtenerConRawSql("SELECT top 1 * FROM Estados ORDER BY Codigo_Estado DESC").FirstOrDefault();
                return Json(new { data = ultimoRegistro.Codigo_Estado, success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Delete(Estados estado)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    Estados est = this.EstadosServicio.Obtener((x) => x.Codigo_Estado == estado.Codigo_Estado).SingleOrDefault();
                    if (est == null)
                        return Json(new { success = false, message = "Error al eliminar, no se encontro registro." }, JsonRequestBehavior.AllowGet);
                    this.EstadosServicio.Borrar(est);
                    scope.Complete();
                    return Json(new { success = true, message = "Registro eliminado satisfactoriamente." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
