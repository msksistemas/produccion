﻿/*ES LLAMADA CUANDO SE EDITA UNA FÓRMULA*/
function CargarDetallesEditar() {
    $.ajax({
        type: "GET",
        url: "/Formulas/GetDetalles/",
        data: { Codigo_Producto: $('#Codigo_Producto').val() },
        dataType: "json",
        success: function (data) {
            if (data.success) {
                for (var i in data.data) {
                    listadoDetalles.push(data.data[i]);
                }
                CargarDetalles();
            };
        }
    });
};

/*ES LLAMADA CUANDO SE EDITA UNA FÓRMULA*/
function CargarProductosAsociadosEditar() {
    $.ajax({
        type: "GET",
        url: "/Formulas/GetProductos/",
        data: { Codigo_Formula: CodigoFormula },
        dataType: "json",
        success: function (data) {
            if (data.success) {
                for (var i in data.data) {
                    /*LLENA EL ARRAY DE COSTOS INDIRECTOS CON EL LISTADO SOLICITADO*/
                    listadoProductos.push(data.data[i]);
                }

                CargarProductosAsociados();

            };
        }
    });
};

/*VUELVE A CARGAR LOS COSTOS*/

function refrescarCostos() {
    CargarCostosFormula();
};

/*ABRE EDICIÓN DE FILA EN LISTADO DE COSTOS*/

function editarCosto(Codigo)
{
    refrescarCostos();

    $('#CostosFormulaTabla').find('tr').find('td').each(
            function () {

                if ($(this).index() == 1) {

                    if ($(this).html() == Codigo) {

                        $(this).parent('tr').find('td').each(function () {

                            $(this).addClass("editable");

                            if ($(this).index() == 4) {

                                if ($(this).html() != "") {

                                    var Porcentaje = $(this).html().replace("% ", "");

                                    $(this).html("");

                                    $(this).prepend('<input id="porcentajeCostoEditable" class="col-md-8 col-md-offset-4 text-box single-line" style="text-align:right" readonly>');

                                    $('#porcentajeCostoEditable').val(Porcentaje);
                                    $('#porcentajeCostoEditable').focus();
                                }

                            }
                            if ($(this).index() == 5) {

                                if ($(this).html() != "") {
                                    var Precio = $(this).html().replace("$ ", "");

                                    $(this).html("");

                                    $(this).prepend('<input id="precioCostoEditable" class="col-md-8 col-md-offset-4 text-box single-line" style="text-align:right" readonly>');

                                    $('#precioCostoEditable').val(Precio);
                                    $('#precioCostoEditable').focus();
                                }

                            }

                            if ($(this).index() == 6) {

                                if ($(this).html() != "") {
                                    var PrecioFijo = $(this).html().replace("$ ", "");

                                    $(this).html("");

                                    $(this).prepend('<input id="precioFijoCostoEditable" class="col-md-8 col-md-offset-4 text-box single-line" style="text-align:right" readonly>');

                                    $('#precioFijoCostoEditable').val(PrecioFijo);
                                    $('#precioFijoCostoEditable').focus();
                                }

                            }

                            if ($(this).index() == 8) {

                                $(this).html("");

                                $(this).prepend(
                                    "<a class='btn btn-success btn-xs' onclick=confirmarEdicionCosto('" + Codigo + "')>" +
                                        "<span class='glyphicon glyphicon-ok'></span>" +
                                    "</a>" +
                                    "<a class='btn btn-danger btn-xs' onclick=refrescarCostos()>" +
                                        "<span class='glyphicon glyphicon-remove'></span>" +
                                    "</a>"
                                );
                            }
                        });
                    }
                }
            }
        )

    $("#porcentajeCostoEditable").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");

            confirmarEdicionCosto(Codigo);
        }
    });

    $("#precioCostoEditable").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");

            confirmarEdicionCosto(Codigo);
        }
    });

    $("#precioFijoCostoEditable").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");

            confirmarEdicionCosto(Codigo);
        }
    });
}

function confirmarEdicionDetalle(Codigo) {

    $.ajax({
        type: "GET",
        url: "/Insumos/GetOne/",
        data: { Codigo_Insumo: Codigo },
        dataType: "json",
        success: function (data) {
            if (data.success) {

                var Insumo = data.data[0];

                for (var i in listadoDetalles) {
                    if (listadoDetalles[i].Codigo_Insumo == Codigo) {

                        if (listadoDetalles[i].Precio_Unitario != null && listadoDetalles[i].Precio_Unitario != 0) {
                            listadoDetalles[i].Cantidad = parseFloat($('#cantidadEditable').val()).toString();
                            listadoDetalles[i].Precio_Unitario = parseFloat(Insumo.Precio_Ultima_Compra).toString();
                            listadoDetalles[i].Costo = (parseFloat($('#cantidadEditable').val()) * parseFloat(Insumo.Precio_Ultima_Compra));
                        }

                        notif({
                            msg: "Registro editado satisfactoriamente.",
                            type: "success",
                            position: "center"
                        });

                        CargarDetalles();

                        AgregarDetalle();
                    }
                }

            }
            else {

                notif({
                    msg: data.mensaje,
                    type: "error",
                    position: "center"
                });
            }

        }
    });

}

function confirmarEdicionCosto(Codigo) {

    for (var i in listadoCostos) {
        if (listadoCostos[i].Codigo_Costo_Indirecto == Codigo) {

            $('#CostosFormulaTabla').find('#porcentajeCostoEditable').each(function () {
                listadoCostos[i].Porcentaje = $('#porcentajeCostoEditable').val();
                //listadoCostos[i].Total = listadoCostos[i].Porcentaje * parseFloat(listadoCostos[i].Cantidad.toString().replace(",", "."));
            });

            $('#CostosFormulaTabla').find('#precioCostoEditable').each(function ()
            {
                listadoCostos[i].Precio = parseFloat($(this).val().toString().replace(",", "."));

                $('#CostosFormulaTabla').find('tr').find('td.editable').each(function ()
                {
                    if ($(this).index() == 3)
                    {
                        listadoCostos[i].Total = listadoCostos[i].Precio * parseFloat($(this).html().toString().replace(",", "."));
                    }

                });

            });

            $('#CostosFormulaTabla').find('#precioFijoCostoEditable').each(function () {
                listadoCostos[i].Fijo = parseFloat($('#precioFijoCostoEditable').val().toString().replace(",", "."));
            });

            notif({
                msg: "Registro editado satisfactoriamente.",
                type: "success",
                position: "center"
            });

            CargarCostosFormula();
        }
    }
};


/*BORRA FILA DEL LISTADO DE PRODUCTOS ASOCIADOS*/

function borrarProducto(Codigo) {
    for (var i in listadoProductos) {
        if (listadoProductos[i].Codigo_Producto == Codigo) {
            listadoProductos.splice(i, 1);
            CargarProductosAsociados();

            AgregarProducto();

            notif({
                msg: "Registro eliminado satisfactoriamente.",
                type: "success",
                position: "center"
            });
        }
    }
}

/*SE LLAMA AL SELECCIONAR UN PRODUCTO DEL MODAL*/
function ConfirmarProducto() {

    $('#ProductosTabla').find('tr.selected').find('td').each(
           function () {
               if ($(this).index() == 0) {
                   $("#Codigo_Producto").val($(this).html());
               }

               if ($(this).index() == 1) {
                   $('#producto-descripcion').html($(this).html());
               }
           }

       )

    $('#modalProductos').modal('hide');

    $("#ProductosTabla").DataTable().destroy();

    CargarProductos();

    $("#modalProductos").on('hidden.bs.modal', function () {
        $("#Comentario").focus();
    });
};

/*SE LLAMA AL CONFIRMAR LA SELECCIÓN DE UN PRODUCTO EN EL MODAL*/
function SeleccionTipoProducto() {

    if (tipoModalProductos == 0) {
        ConfirmarProducto();
    }
    if (tipoModalProductos == 1) {
        ConfirmarProductoAsociado();
    }
}