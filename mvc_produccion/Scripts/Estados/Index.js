﻿var dataTableEstados = [];
var listadoEstados = [];
/* METODO PARA AGREGAR UN REGISTRO*/
function SubmitForm(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        $.ajax({
            type: "POST",
            url: form.action,
            data: $(form).serialize(),
            success: function (data) {
                if (data.success) {
                    Popup.dialog('close');
                    dataTable.ajax.reload();

                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                }
                else {
                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });
                }
            }
        });
    }
    return false;
}
function AgregarEstado() {
    var filaAgregada = false;
    $('#estadosTabla').find('#insert').each(function () {
        filaAgregada = true;
    });
    if (filaAgregada == false) {
        /*AGREGA UNA FILA CON LOS SIGUIENTES CAMPOS EN EL DATATABLE*/
        var html = '<tr>';
        html += '<td><input id="data1" placeholder="Código" style="text-align:center" class="col-md-12 text-box single-line"></input></td>';
        html += '<td><input id="data2" placeholder="Descripción" style="text-transform:capitalize;" class="col-md-12 text-box single-line"></input></td>';
        html += '<td class="text-center"><button type="button" name="insert" onclick="InsertarEstado()" id="insert" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span></button>' +
            '<button type="button" name="cancel" id="cancel" onclick="CargarEstados()" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span></button></td>';
        html += '</tr>';
        $('#estadosTabla tbody').prepend(html);
        $.ajax({
            type: "GET",
            url: "/Estados/GetUltimo/",
            success: function (data) {
                if (data.success) {
                    $('#data1').val(data.data + 1);
                    $('#data1').focus();
                }
                else {
                    $('#data1').val(1);
                    $('#data1').focus();
                }
                $("#data1").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");
                        $("#data2").focus();
                    }
                });
                $("#data2").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");
                        InsertarEstado();
                    }
                });
            }
        });
    }
    else
        $('#data1').focus();
}
function refrescarEstados() {
    CargarEstados()
}
function EditarEstado(Codigo) {
    dataTableEstados.clear();
    dataTableEstados.rows.add(listadoEstados);
    dataTableEstados.draw();
    $('#estadosTabla').find('tr').find('td').each(
            function () {
                if ($(this).index() == 0) {
                    if ($(this).html() == Codigo) {
                        $(this).parent('tr').find('td').each(function () {
                            if ($(this).index() == 1) {
                                var Descripcion = $(this).html();
                                $(this).html("");
                                $(this).prepend('<input id="nuevaDescripcion" style="text-transform:capitalize; width:100%"/>');
                                $('#nuevaDescripcion').val(Descripcion);
                                $('#nuevaDescripcion').focus();
                            }
                            if ($(this).index() == 2) {
                                $(this).html("");
                                $(this).prepend(
                                    "<a class='btn btn-success btn-sm' onclick=confirmarEdicion('" + Codigo + "')>" +
                                        "<span class='glyphicon glyphicon-ok'></span>" +
                                    "</a>" +
                                    "<a class='btn btn-danger btn-sm' onclick=refrescarEstados()>" +
                                        "<span class='glyphicon glyphicon-remove'></span>" +
                                    "</a>");
                            }
                        });
                    }
                }
            }
        )
    $("#nuevaDescripcion").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");
            confirmarEdicion(Codigo);
        }
    });
}
function confirmarEdicion(Codigo) {
    $.ajax({
        type: "POST",
        url: "/Estados/AddOrEdit/",
        data: { Codigo_Estado: Codigo, Codigo_Interno_Estados: 1, Descripcion: $('#nuevaDescripcion').val() },
        success: function (data) {
            if (data.success) {
                CargarEstados();
                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });
            }
            else {
                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });
}
function modificarInput(Codigo) {
    $('input.codigo').text(Codigo);
}
function DeleteEstado() {
    $.ajax({
        type: "POST",
        url: "/Estados/Delete/",
        data: { Codigo_Estado: $('input.codigo').text() },
        success: function (data) {
            if (data.success) {
                $('#modalDelete').modal('hide');
                CargarEstados();
                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });
            }
            else {
                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });
};
function InsertarEstado() {
    var Codigo_Estado = $('#data1').val();
    var Descripcion = $('#data2').val();
    if (Codigo_Estado != '' && Descripcion != '') {
        $.ajax({
            url: "/Estados/AddOrEdit/",
            method: "POST",
            data: { Codigo_Estado: Codigo_Estado, Descripcion: Descripcion },
            success: function (data) {
                if (data.success) {
                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });
                    CargarEstados();
                }
                else {
                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });
                    $('#data1').focus();
                }
            }
        });
    }
    else
        alert("Ambos campos son requeridos");
}