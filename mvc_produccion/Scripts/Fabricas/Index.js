﻿var dataTableFabricas = [];
var listadoFabricas = [];

var dataTableLineas = [];
var listadoLineas = [];



/* METODO PARA AGREGAR UN REGISTRO*/

function SubmitForm(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        $.ajax({
            type: "POST",
            url: form.action,
            data: $(form).serialize(),
            success: function (data) {
                if (data.success) {
                    Popup.dialog('close');
                    dataTable.ajax.reload();

                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                }
                else {

                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });
                }
            }
        });
    }
    return false;
}

function AgregarFabrica() {

    var filaAgregada = false;

    $('#FabricasTabla').find('#insert').each(function () {
        filaAgregada = true;
    });

    if (filaAgregada == false) {
        /*AGREGA UNA FILA CON LOS SIGUIENTES CAMPOS EN EL DATATABLE*/

        var html = '<tr>';
        html += '<td><input id="data1" placeholder="Código" style="text-align:center" class="col-md-12 text-box single-line"></input></td>';
        html += '<td><input id="data2" placeholder="Descripción" style="text-transform:capitalize;" class="col-md-12 text-box single-line"></input></td>';
        html += '<td><input id="data3" placeholder="Número Planilla" style="text-transform:capitalize;" class="col-md-12 text-box single-line"></input></td>';
        html += '<td class="text-center"><button type="button" name="insert" onclick="InsertarFabrica()" id="insert" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span></button>' +
            '<button type="button" name="cancel" id="cancel" onclick="CargarFabricas()" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span></button></td>';
        html += '</tr>';

        $('#FabricasTabla tbody').prepend(html);

        $.ajax({
            type: "GET",
            url: "/Fabricas/GetUltimo/",
            success: function (data) {
                if (data.success) {
                    $('#data1').val(data.data + 1);
                    $('#data1').focus();
                }
                else {
                    $('#data1').val(1);
                    $('#data1').focus();
                }

                $("#data1").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        $("#data2").focus();
                    }
                });

                $("#data2").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        $("#data3").focus();
                    }
                });

                $("#data3").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        InsertarFabrica();
                    }
                });
            }
        });

    }
    else {
        $('#data1').focus();
    }

};

function refrescarFabricas() { CargarFabricas() };

function EditarFabrica(Codigo) {

    dataTableFabricas.clear();
    dataTableFabricas.rows.add(listadoFabricas);
    dataTableFabricas.draw();

    $('#FabricasTabla').find('tr').find('td').each(

            function () {
                if ($(this).index() == 0) {

                    if ($(this).html() == Codigo) {

                        $(this).parent('tr').find('td').each(function () {

                            if ($(this).index() == 1) {

                                var Descripcion = $(this).html();

                                $(this).html("");

                                $(this).prepend('<input id="nuevaDescripcion" style="text-transform:capitalize; width:100%"/>');

                                $('#nuevaDescripcion').val(Descripcion);

                                $('#nuevaDescripcion').focus();
                            }

                            if ($(this).index() == 2) {

                                var NroPlanilla = $(this).html();

                                $(this).html("");

                                $(this).prepend('<input id="nuevoNroPlanilla" style="text-transform:capitalize; width:100%"/>');

                                $('#nuevoNroPlanilla').val(NroPlanilla);

                                $('#nuevoNroPlanilla').focus();
                            }

                            if ($(this).index() == 3) {
                                $(this).html("");

                                $(this).prepend(
                                    "<a class='btn btn-success btn-sm' onclick=confirmarEdicion('" + Codigo + "')>" +
                                        "<span class='glyphicon glyphicon-ok'></span>" +
                                    "</a>" +
                                    "<a class='btn btn-danger btn-sm' onclick=refrescarFabricas()>" +
                                        "<span class='glyphicon glyphicon-remove'></span>" +
                                    "</a>"

                                    );
                            }

                        });
                    }
                }
            }
        )

    $("#nuevoNroPlanilla").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");

            confirmarEdicion(Codigo);
        }
    });
};

function confirmarEdicion(Codigo) {
    $.ajax({
        type: "POST",
        url: "/Fabricas/AddOrEdit/",
        data: {
            Codigo_Fabrica: Codigo,
            Codigo_Interno_Fabrica: 1,
            Descripcion: $('#nuevaDescripcion').val(),
            Nro_Planilla: $('#nuevoNroPlanilla').val()
        },
        success: function (data) {
            if (data.success) {
                CargarFabricas();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });

            }
            else {

                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });
};

function modificarInput(Codigo) {
    $('input.codigo').text(Codigo);
}

function DeleteFabrica() {
    $.ajax({
        type: "POST",
        url: "/Fabricas/Delete/",
        data: { Codigo_Fabrica: $('input.codigo').text() },
        success: function (data) {
            if (data.success) {
                $('#modalDelete').modal('hide');
                CargarFabricas();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });
            }
            else {

                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });

};

function InsertarFabrica() {
    var Codigo_Fabrica = $('#data1').val();
    var Descripcion = $('#data2').val();
    var Nro_Planilla = $('#data3').val();
    if (Codigo_Fabrica != '' && Descripcion != '') {
        $.ajax({
            url: "/Fabricas/AddOrEdit/",
            method: "POST",
            data: {
                Codigo_Fabrica: Codigo_Fabrica,
                Descripcion: Descripcion,
                Nro_Planilla: Nro_Planilla
            },
            success: function (data) {
                if (data.success) {

                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                    CargarFabricas();
                }
                else {

                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });

                    $('#data1').focus();
                }
            }
        });
    }
    else {
        alert("Verifique cambios vacíos.");
    }
};

