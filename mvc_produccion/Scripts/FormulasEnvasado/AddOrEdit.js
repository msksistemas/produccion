﻿function CargarProductos() {
    dataTable = $("#ProductosTabla").DataTable({
        "destroy": true,
        "autoWidth": true,
        "ajax": {
            "url": "/Productos/GetData",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            {
                "data": "Codigo_Producto",
                "title": "Código"
            },
            {
                "data": "Descripcion",
                "title": "Descripción",
                "sClass": "mayusculas-texto" 
            },
            {
                "data": "Codigo_Producto",
                "title": "Herramientas",
                "width": "10%",
                "render": function (data) {
                    return "<a class='btn btn-default btn-xs'><span class='glyphicon glyphicon-pencil'></span></a>" +
                           "<a class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></a>";
                },
            }
        ],
        "language": idiomaEspañol,
        "columnDefs": [
                { className: "text-center", "targets": [0, 2] },
        ],
        select: 'single',
    });
};

/*PASA A SIGUIENTE INPUT AL DAR ENTER*/
function SaltosEnFormulario() {
    $("#Codigo_Formula_Envasado").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#Codigo_Producto').focus();
        }
    });

    $("#Numero_Producto_Ventas").keyup(function (e) {
        if (e.keyCode == 13) {
            $.ajax({
                type: "GET",
                url: "/ProductosVentas/GetOne/",
                data: { Codigo_de_Producto: $('#Numero_Producto_Ventas').val() },
                success: function (data) {
                    if (data.success) {
                        var Producto = data.data;

                        $('#producto-ventas-descripcion').html(Producto.Descripcion);
                        $('#Cantidad_Envasada').focus();
                    }
                    else {
                        notif({
                            msg: data.message,
                            type: "error",
                            position: "center"
                        });

                        $("#Numero_Producto_Ventas").focus();
                    }
                }
            });
        }
    });

    $("#Cantidad_Envasada").keyup(function (e) {
        if (e.keyCode == 13) {
            $('.nuevoDetalle').focus();
        }
    });
}

/*BORRA FILA DEL LISTADO DE DETALLES*/
function borrarDetalle(Codigo) {
    for (var i in listadoDetalles) {
        if (listadoDetalles[i].Codigo_Insumo == Codigo) {
            listadoDetalles.splice(i, 1);
            CargarDetalles();

            AgregarDetalle();

            notif({
                msg: "Registro eliminado satisfactoriamente.",
                type: "success",
                position: "center"
            });

        }
    }
}

function CargarDetallesAsociados() {
    $.ajax({
        type: "GET",
        url: "/FormulasEnvasado/GetDetalles/",
        data: { Codigo_Interno_Formula: Codigo_Interno_Formula },
        dataType: "json",
        success: function (data) {
            if (data.success) {

                for (var i in data.data) {
                    /*LLENA EL ARRAY DE COSTOS INDIRECTOS CON EL LISTADO SOLICITADO*/
                    listadoDetalles.push(data.data[i]);
                }

                CargarDetalles();

            };
        }
    });
}

/*SE LLAMA AL SELECCIONAR UN PRODUCTO DEL MODAL*/
function ConfirmarProducto() {

    $('#ProductosTabla').find('tr.selected').find('td').each(
           function () {

               if ($(this).index() == 0) {

                   var Codigo = $(this).html();

                   $('#Codigo_Producto').val(Codigo);

               }

               if ($(this).index() == 1) {

                   var Descripcion = $(this).html();

                   $('#descripcion-prod-form-envasado').html(Descripcion);
               }
           }

       )

    $('#modalProductos').modal('hide');

    CargarProductos();

    $("#modalProductos").on('hidden.bs.modal', function () {
        $("#Numero_Producto_Ventas").focus();
    });
};



