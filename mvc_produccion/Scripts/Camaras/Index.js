﻿
var dataTableCamaras = [];

var listadoCamaras = [];

function CargarCamaras() {

    $.ajax({
        type: "GET",
        url: "/Camaras/GetData/",
        dataType: "json",
        success: function (data) {
            if (data.success) {

                listadoCamaras = data.data;

                dataTableCamaras = $("#CamarasTabla").DataTable({
                    "destroy": true,
                    "autoWidth": true,
                    data: listadoCamaras,
                    "columns": [
                        {
                            "data": "Codigo_Camara",
                            "title": "Código",
                            "width": "15%"
                        },
                        {
                            "data": "Nombre",
                            "title": "Descripción",
                            "sClass": "mayusculas-texto"
                        },
                        {
                            "data": "Codigo_Camara",
                            "width": "10%",
                            "title": "Herramientas",
                            "render": function (data) {
                                return " <a class='btn btn-default btn-sm' onclick=EditarCamara('" + data + "')><span class='glyphicon glyphicon-pencil'></span></a>" +
                                         "<a class='btn btn-danger btn-sm'onclick=modificarInput('" + data + "') data-toggle='modal' data-target='#modalDelete'><span class='glyphicon glyphicon-trash'></span></a>";
                            },
                        }

                    ],
                    "language": idiomaEspañol,
                    "columnDefs": [
                            { className: "text-center", "targets": [0, 2] },
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                       {
                           text: '<span class="glyphicon glyphicon-plus">',
                           className: 'nuevo',
                           action: function () {
                               AgregarCamara();
                           }
                       }
                    ],
                    "order": [[1, "asc"]],
                    initComplete: function (data) {
                        $('div.dataTables_filter input').focus();
                    }

                });
            }

        }
    });


};

/* METODO PARA AGREGAR UN REGISTRO*/

function SubmitForm(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        $.ajax({
            type: "POST",
            url: form.action,
            data: $(form).serialize(),
            success: function (data) {
                if (data.success) {
                    Popup.dialog('close');
                    dataTable.ajax.reload();

                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                }
                else {

                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });
                }
            }
        });
    }
    return false;
}

function AgregarCamara() {

    var filaAgregada = false;

    $('#CamarasTabla').find('#insert').each(function () {
        filaAgregada = true;
    });

    if (filaAgregada == false) {
        /*AGREGA UNA FILA CON LOS SIGUIENTES CAMPOS EN EL DATATABLE*/

        var html = '<tr>';
        html += '<td><input id="data1" placeholder="Código" style="text-align:center" class="col-md-12 text-box single-line"></input></td>';
        html += '<td><input id="data2" placeholder="Descripción" style="text-transform:capitalize;" class="col-md-12 text-box single-line"></input></td>';
        html += '<td class="text-center"><button type="button" name="insert" onclick="InsertarCamara()" id="insert" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span></button>' +
            '<button type="button" name="cancel" id="cancel" onclick="CargarCamaras()" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span></button></td>';
        html += '</tr>';

        $('#CamarasTabla tbody').prepend(html);

        $.ajax({
            type: "GET",
            url: "/Camaras/GetUltimo/",
            success: function (data) {
                if (data.success) {
                    $('#data1').val(data.data + 1);
                    $('#data1').focus();
                }
                else {
                    $('#data1').val(1);
                    $('#data1').focus();
                }

                $("#data1").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        $("#data2").focus();
                    }
                });

                $("#data2").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        InsertarCamara();
                    }
                });
            }
        });

    }
    else {
        $('#data1').focus();
    }

};

function refrescarCamaras() { CargarCamaras() };

function EditarCamara(Codigo) {

    dataTableCamaras.clear();
    dataTableCamaras.rows.add(listadoCamaras);
    dataTableCamaras.draw();

    $('#CamarasTabla').find('tr').find('td').each(

            function () {
                if ($(this).index() == 0) {

                    if ($(this).html() == Codigo) {

                        $(this).parent('tr').find('td').each(function () {

                            if ($(this).index() == 1) {

                                var Nombre = $(this).html();

                                $(this).html("");

                                $(this).prepend('<input id="nuevoNombre" style="text-transform:capitalize; width:100%"/>');

                                $('#nuevoNombre').val(Nombre);

                                $('#nuevoNombre').focus();
                            }

                            if ($(this).index() == 2) {
                                $(this).html("");

                                $(this).prepend(
                                    "<a class='btn btn-success btn-sm' onclick=confirmarEdicion('" + Codigo + "')>" +
                                        "<span class='glyphicon glyphicon-ok'></span>" +
                                    "</a>" +
                                    "<a class='btn btn-danger btn-sm' onclick=refrescarCamaras()>" +
                                        "<span class='glyphicon glyphicon-remove'></span>" +
                                    "</a>"

                                    );
                            }

                        });
                    }
                }
            }
        )

    $("#nuevoNombre").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");

            confirmarEdicion(Codigo);
        }
    });
};

function confirmarEdicion(Codigo) {
    $.ajax({
        type: "POST",
        url: "/Camaras/AddOrEdit/",
        data: { Codigo_Camara: Codigo, Codigo_Interno_Camara: 1, Nombre: $('#nuevoNombre').val() },
        success: function (data) {
            if (data.success) {
                CargarCamaras();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });

            }
            else {

                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });
};

function modificarInput(Codigo) {
    $('input.codigo').text(Codigo);
}

function DeleteCamara() {
    $.ajax({
        type: "POST",
        url: "/Camaras/Delete/",
        data: { Codigo_Camara: $('input.codigo').text() },
        success: function (data) {
            if (data.success) {
                $('#modalDelete').modal('hide');
                CargarCamaras();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });
            }
            else {

                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });

};

function InsertarCamara() {
    var Codigo_Camara = $('#data1').val();
    var Nombre = $('#data2').val();
    if (Codigo_Camara != '' && Nombre != '') {
        $.ajax({
            url: "/Camaras/AddOrEdit/",
            method: "POST",
            data: { Codigo_Camara: Codigo_Camara, Nombre: Nombre },
            success: function (data) {
                if (data.success) {

                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                    CargarCamaras();
                }
                else {

                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });

                    $('#data1').focus();
                }
            }
        });
    }
    else {
        alert("Ambos campos son requeridos");
    }
};

