﻿var dataTableQueseros = [];

var listadoQueseros = [];



/* METODO PARA AGREGAR UN REGISTRO*/

function SubmitForm(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        $.ajax({
            type: "POST",
            url: form.action,
            data: $(form).serialize(),
            success: function (data) {
                if (data.success) {
                    Popup.dialog('close');
                    dataTable.ajax.reload();

                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                }
                else {

                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });
                }
            }
        });
    }
    return false;
}

function InsertarRegistro() {
    var Codigo_Quesero = $('#data1').val();
    var Nombre = $('#data2').val();
    if (Codigo_Quesero != '' && Nombre != '') {
        $.ajax({
            url: "/Queseros/AddOrEdit/",
            method: "POST",
            data: { Codigo_Quesero: Codigo_Quesero, Nombre: Nombre },
            success: function (data) {
                if (data.success) {
                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                    CargarQueseros();
                }
                else {

                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });

                    $('#data1').focus();
                }
            }
        });
    }
    else {
        alert("Ambos campos son requeridos");
    }
};

function AgregarQuesero() {

    var filaAgregada = false;

    $('#QueserosTabla').find('#insert').each(function () {
        filaAgregada = true;
    });

    if (filaAgregada == false) {
        /*AGREGA UNA FILA CON LOS SIGUIENTES CAMPOS EN EL DATATABLE*/

        var html = '<tr>';
        html += '<td><input id="data1" placeholder="Código" class="col-md-12 text-box single-line" style="text-align:center"></input></td>';
        html += '<td><input id="data2" placeholder="Nombre" style="text-transform:capitalize;" class="col-md-12 text-box single-line"></input></td>';
        html += '<td class="text-center"><button type="button" name="insert" id="insert" onclick="InsertarRegistro()" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span></button>' +
            '<button type="button" name="cancel" id="cancel" onclick="CargarQueseros()" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span></button></td>';
        html += '</tr>';

        $('#QueserosTabla tbody').prepend(html);

        $.ajax({
            type: "GET",
            url: "/Queseros/GetUltimo/",
            success: function (data) {
                if (data.success) {
                    $('#data1').val(data.data + 1);
                    $('#data1').focus();
                }
                else {
                    $('#data1').val(1);
                    $('#data1').focus();
                }

                $("#data1").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        $("#data2").focus();
                    }
                });

                $("#data2").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        InsertarRegistro();
                    }
                });
            }
        });

    }
    else {
        $('#data1').focus();
    }
};

function EditarQuesero(Codigo) {

    dataTableQueseros.clear();
    dataTableQueseros.rows.add(listadoQueseros);
    dataTableQueseros.draw();

    $('#QueserosTabla').find('tr').find('td').each(

            function () {
                if ($(this).index() == 0) {

                    if ($(this).html() == Codigo) {

                        $(this).parent('tr').find('td').each(function () {

                            if ($(this).index() == 1) {

                                var Nombre = $(this).html();

                                $(this).html("");

                                $(this).prepend('<input id="nuevoNombre" style="text-transform:capitalize;" class="col-md-12"/>');

                                $('#nuevoNombre').val(Nombre);

                                $('#nuevoNombre').focus();
                            }

                            if ($(this).index() == 2) {
                                $(this).html("");

                                $(this).prepend(
                                    "<a class='btn btn-success btn-sm' onclick=ConfirmarEdicionQuesero('" + Codigo + "')>" +
                                        "<span class='glyphicon glyphicon-ok'></span>" +
                                    "</a>" +
                                    "<a class='btn btn-danger btn-sm' onclick=CargarQueseros()>" +
                                        "<span class='glyphicon glyphicon-remove'></span>" +
                                    "</a>"

                                    );
                            }

                        });
                    }
                }
            }
        )

    $("#nuevoNombre").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");

            ConfirmarEdicionQuesero(Codigo);
        }
    });
};

function ConfirmarEdicionQuesero(Codigo) {
    $.ajax({
        type: "POST",
        url: "/Queseros/AddOrEdit/",
        data: { Codigo_Quesero: Codigo, Codigo_Quesero_Interno: 1, Nombre: $('#nuevoNombre').val() },
        success: function (data) {
            if (data.success) {
                CargarQueseros();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });

            }
            else {

                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });
};

function modificarInput(Codigo) {
    $('input.codigo').text(Codigo);
}

function DeleteQuesero() {
    $.ajax({
        type: "POST",
        url: "/Queseros/Delete/",
        data: { Codigo_Quesero: $('input.codigo').text() },
        success: function (data) {
            if (data.success) {
                $('#modalDelete').modal('hide');
                CargarQueseros();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });
            }
            else {

                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });

};
