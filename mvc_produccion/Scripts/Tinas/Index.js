﻿
var dataTableTinas = [];

var listadoTinas = [];

function CargarTinas() {

    $.ajax({
        type: "GET",
        url: "/Tinas/GetData/",
        dataType: "json",
        success: function (data) {
            if (data.success) {

                listadoTinas = data.data;

                dataTableTinas = $("#TinaTabla").DataTable({
                    "destroy": true,
                    "autoWidth": true,
                    data : listadoTinas,
                    "columns": [
                        {
                            "data": "Codigo_Tina",
                            "title": "Código",
                            "width": "15%"
                        },
                        {
                            "data": "Descripcion",
                            "title": "Descripción",
                            "sClass": "mayusculas-texto"
                        },
                        {
                            "data": "Litros",
                            "title": "Litros"
                        },
                        {
                            "data": "Codigo_Tina",
                            "title": "Herramientas",
                            "width": "10%",
                            "render": function (data) {
                                return "<a class='btn btn-default btn-sm' onclick=EditarTina('" + data + "')><span class='glyphicon glyphicon-pencil'></span></a>" +
                                       "<a class='btn btn-danger btn-sm' onclick=modificarInput('" + data + "') data-toggle='modal' data-target='#modalDelete'><span class='glyphicon glyphicon-trash'></span></a>";
                            },
                        }

                    ],
                    "language": idiomaEspañol,
                    "columnDefs": [
                        { className: "text-center", "targets": [0, 3] },
                        { className: "text-right", "targets": [2] }
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                       {
                           text: '<span class="glyphicon glyphicon-plus">',
                           className: 'nuevo',
                           action: function () {
                               AgregarTina();
                           }
                       }
                    ],
                    "order": [[1, "asc"]]

                });
            }

        }
    });

};

function AgregarTina() {

    var filaAgregada = false;

    $('#TinaTabla').find('#insert').each(function () {
        filaAgregada = true;
    });

    if (filaAgregada == false) {
        /*AGREGA UNA FILA CON LOS SIGUIENTES CAMPOS EN EL DATATABLE*/

        var html = '<tr>';
        html += '<td><input id="data1" placeholder="Código" class="col-md-12 text-box single-line" style="text-align:center"></input></td>';
        html += '<td><input id="data2" placeholder="Descripción" style="text-transform:capitalize;" class="col-md-12 text-box single-line"></input></td>';
        html += '<td><input id="data3" placeholder="Litros" class="col-md-12 text-box single-line"></input></td>';
        html += '<td class="text-center"><button type="button" name="insert" onclick="InsertarRegistro()" id="insert" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span></button>' +
            '<button type="button" name="cancel" id="cancel" onclick="CargarTinas()" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span></button></td>';
        html += '</tr>';

        $('#TinaTabla tbody').prepend(html);

        $.ajax({
            type: "GET",
            url: "/Tinas/GetUltimo/",
            success: function (data) {
                if (data.success) {
                    $('#data1').val(data.data + 1);
                    $('#data1').focus();
                }
                else {
                    $('#data1').val(1);
                    $('#data1').focus();
                }

                $("#data1").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        $("#data2").focus();
                    }
                });

                $("#data2").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        $("#data3").focus();
                    }
                });

                $("#data3").keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterKey");

                        InsertarRegistro();
                    }
                });
            }
        });

    }
    else {
        $('#data1').focus();
    }

};

function modificarInput(Codigo) {
    $('input.codigo').text(Codigo);
}

/* METODO PARA AGREGAR UN REGISTRO*/

function SubmitForm(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        $.ajax({
            type: "POST",
            url: form.action,
            data: $(form).serialize(),
            success: function (data) {
                if (data.success) {
                    Popup.dialog('close');
                    dataTable.ajax.reload();

                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                }
                else {
                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });
                }
            }
        });
    }
    return false;
}

function RefrescarTinas() {
    CargarTinas();

};

function EditarTina(Codigo) {

    dataTableTinas.clear();
    dataTableTinas.rows.add(listadoTinas);
    dataTableTinas.draw();

    $('#TinaTabla').find('tr').find('td').each(

            function () {
                if ($(this).index() == 0) {

                    if ($(this).html() == Codigo) {

                        $(this).parent('tr').find('td').each(function () {

                            if ($(this).index() == 1) {

                                var Descripcion = $(this).html();

                                $(this).html("");

                                $(this).prepend('<input id="nuevaDescripcion" style="text-transform:capitalize; width:100%"/>');

                                $('#nuevaDescripcion').val(Descripcion);

                                $('#nuevaDescripcion').focus();
                            }

                            if ($(this).index() == 2) {

                                var Litros = $(this).html();

                                $(this).html("");

                                $(this).prepend('<input id="nuevosLitros" style="text-align:right; width:100%"/>');

                                $('#nuevosLitros').val(Litros);

                            }

                            if ($(this).index() == 3) {
                                $(this).html("");

                                $(this).prepend(
                                    "<a class='btn btn-success btn-sm' onclick=ConfirmarEdicionTina('" + Codigo + "')>" +
                                        "<span class='glyphicon glyphicon-ok'></span>" +
                                    "</a>" +
                                    "<a class='btn btn-danger btn-sm' onclick=RefrescarTinas()>" +
                                        "<span class='glyphicon glyphicon-remove'></span>" +
                                    "</a>"

                                    );
                            }
                        });
                    }
                }
            }
        )

    $("#nuevaDescripcion").keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");

            ConfirmarEdicionTina(Codigo);
        }
    });
};

function ConfirmarEdicionTina(Codigo) {
    $.ajax({
        type: "POST",
        url: "/Tinas/AddOrEdit/",
        data: {
            Codigo_Tina: Codigo,
            Codigo_Interno_Tina: 1,
            Descripcion: $('#nuevaDescripcion').val(),
            Litros: $('#nuevosLitros').val()
        },
        success: function (data) {
            if (data.success) {
                CargarTinas();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });
            }
            else {
                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });
};

function DeleteTina() {
    $.ajax({
        type: "POST",
        url: "/Tinas/Delete/",
        data: { Codigo_Tina: $('input.codigo').text() },
        success: function (data) {
            if (data.success) {
                $('#modalDelete').modal('hide');
                CargarTinas();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });
            }
            else {
                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });

};

function InsertarRegistro() {
    var Codigo_Tina = $('#data1').val();
    var Descripcion = $('#data2').val();
    var Litros = $('#data3').val();
    $.ajax({
        url: "/Tinas/AddOrEdit/",
        method: "POST",
        data: {
            Codigo_Tina: Codigo_Tina,
            Descripcion: Descripcion,
            Litros: Litros
        },
        success: function (data) {
            if (data.success) {
                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });

                CargarTinas();
            }
            else {
                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });

                $('#data1').focus();
            }
        }
    });
}