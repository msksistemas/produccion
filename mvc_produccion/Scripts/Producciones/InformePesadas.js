﻿
var Codigo_Produccion = 0;
var ProduccionActual = [];

function modificarInput(Codigo) {
    $('input.codigo').text(Codigo);
}

function Delete() {
    $.ajax({
        type: "POST",
        url: "/Producciones/Delete/",
        data: { Codigo_Produccion: $('input.codigo').text() },
        success: function (data) {
            if (data.success) {
                $('#modalDelete').modal('hide');
                CargarPesadas();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });

            }
            else {
                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });

};