﻿
var dataTableProducciones = [];

/*RESTANTES A ENVASAR*/

var kilosProduccionActual = 0;
var piezasObtenidasProduccionActual = 0;

var Codigo_Produccion = 0;
var ProduccionActual = [];

var listadoProducciones = [];

var ListadoDetalles = [];

var LoteActual;

function CalcularRestantes() {
    /*--------------CALCULA KILOS-----------------*/

    var KilosIngresados = 0;

    $('#FormulasEnvasadoTabla').find('.kilosEditable').each(function () {
        if ($(this).val() != "") {
            KilosIngresados += parseFloat($(this).val());
        }
    });

    $('#kilos-restantes').html("");
    $('#kilos-restantes').html(kilosProduccionActual - KilosIngresados);

    /*--------------CALCULA KILOS-----------------*/

    var PiezasIngresadas = 0;

    $('#FormulasEnvasadoTabla').find('.cantidadEditable').each(function () {
        if ($(this).val() != "") {
            PiezasIngresadas += parseFloat($(this).val());
        }
    });

    $('#piezas-restantes').html("");
    $('#piezas-restantes').html(piezasObtenidasProduccionActual - PiezasIngresadas);
}

/*VERIFICA REGISTRO SI ESTA EN LIBERADO POR CALIDAD*/

function VerificarCantidades(){
    /*AL SOLTAR FOCUS*/

    $(".kilosEditable").blur(function () {

        var KilosIngresados = 0;

        $('#FormulasEnvasadoTabla').find('.kilosEditable').each(
            function () {

                if ($(this).val() != "") {
                    KilosIngresados += parseFloat($(this).val());
                }
            }
        )

        if (kilosProduccionActual - KilosIngresados < 0) {
            notif({
                msg: "Error, los kilos envasados no pueden superar los " + parseFloat(kilosProduccionActual).toFixed(2) + " kilos.",
                type: "warning",
                width: 500,
                timeout: 4000,
                multiline: true,
                position: "center"
            });
        }
        else {
            $('#kilos-restantes').html("");
            $('#kilos-restantes').html(kilosProduccionActual - KilosIngresados);
        }
    })

    $(".cantidadEditable").blur(function () {
        var PiezasIngresadas = 0;

        $('#FormulasEnvasadoTabla').find('.cantidadEditable').each(
            function () {

                if ($(this).val() != "") {
                    var Kilos = parseFloat($(this).val());

                    $(this).parent().each(function () {

                        $(this).parent().find('td').each(function () {

                            if ($(this).index() == 1) {

                                if (parseFloat($(this).html()) != parseFloat(1)) {

                                    PiezasIngresadas += (Kilos / parseFloat($(this).html()));

                                }
                                else {
                                    PiezasIngresadas += Kilos
                                }
                            }
                        });
                    });
                }

            }
        )

        if (piezasObtenidasProduccionActual - PiezasIngresadas < 0) {
            notif({
                msg: "Error, las piezas envasadas no pueden superar las " + parseFloat(piezasObtenidasProduccionActual).toFixed(2) + " piezas.",
                type: "warning",
                width: 500,
                timeout: 4000,
                multiline: true,
                position: "center"
            });
        } else {
            $('#piezas-restantes').html("");
            $('#piezas-restantes').html(piezasObtenidasProduccionActual - PiezasIngresadas);
        }
    })
}

function modificarInput(Codigo) {
    $('input.codigo').text(Codigo);
}

/*SE LLAMA AL PRESIONAR EL BOTON CAMBIAR ESTADO*/

/* METODO PARA AGREGAR UN REGISTRO*/

function SubmitForm(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        $.ajax({
            type: "POST",
            url: form.action,
            data: $(form).serialize(),
            success: function (data) {
                if (data.success) {
                    Popup.dialog('close');
                    dataTable.ajax.reload();

                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                }
                else {
                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });
                }
            }
        });
    }
    return false;
}

function Delete() {
    $.ajax({
        type: "POST",
        url: "/Producciones/Delete/",
        data: { Lote: $('input.codigo').text() },
        success: function (data) {
            if (data.success) {
                $('#modalDelete').modal('hide');
                CargarProducciones();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });

            }
            else {
                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });

};


