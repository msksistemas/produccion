﻿
/*MAYUSCULAS AL INICIO DE CADENA DE TEXTO*/
function MaysPrimera(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/*PASA A SIGUIENTE INPUT AL DAR ENTER*/

function SaltosEnFormulario() {
    $("#Codigo_Insumo").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#Codigo_Insumo_Proveedor').focus();
        }
    });
    $("#Codigo_Insumo_Proveedor").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#Descripcion').focus();
        }
    });
    $("#Descripcion").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#Descripcion_Larga').val($('#Descripcion').val());
            $('#Descripcion_Larga').focus();

            $("#Descripcion").val(MaysPrimera($("#Descripcion").val()));
        }
    });

    $("#Descripcion").keyup(function (e) {
        if (e.keyCode == 9) {
            $('#Descripcion_Larga').val($('#Descripcion').val());
            $('#Descripcion_Larga').focus();
        }
    });

    $("#Descripcion_Larga").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#Codigo_Linea').focus();
        }
    });
    $("#Codigo_Linea").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#Codigo_Rubro').focus();
        }
    });
    $("#Codigo_Rubro").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#tb-prove-habitual').focus();
        }
    });
    $("#Unidad_Medida").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#Codigo_Imputacion').focus();
        }
    });

    $("#Comentario").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#btn-IVA').focus();
        }
    });
    $("#Precio_Ultima_Compra").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#Precio_Lista').focus();
        }
    });

    $("#Precio_Lista").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#unidades').focus();
        }
    });

    $("#tb-unidades").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#peso').focus();
        }
    });

    $("#tb-kilos").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#peso').focus();
        }
    });

    $("#tb-prove-habitual").keyup(function (e) {
        if (e.keyCode == 13) {
            $.ajax({
                type: "POST",
                url: "/Proveedores/GetOne/",
                data: { Codigo_Proveedor: $('#tb-prove-habitual').val() },
                success: function (data) {
                    if (data.success) {
                        $('#prove-habitual-descripcion').html(data.data.RAZON_SOCIAL);
                        $('#tb-prove-alternativo').focus();
                    }
                }
            });
        }
    });

    $("#tb-prove-alternativo").keyup(function (e) {
        if (e.keyCode == 13) {
            $.ajax({
                type: "POST",
                url: "/Proveedores/GetOne/",
                data: { Codigo_Proveedor: $('#tb-prove-alternativo').val() },
                success: function (data) {
                    if (data.success) {
                        $('#prove-alternativo-descripcion').html(data.data.RAZON_SOCIAL);
                        $('#tb-prove-ultimo').focus();
                    }
                }
            });
        }
    });

    $("#tb-prove-ultimo").keyup(function (e) {
        if (e.keyCode == 13) {
            $.ajax({
                type: "POST",
                url: "/Proveedores/GetOne/",
                data: { Codigo_Proveedor: $('#tb-prove-ultimo').val() },
                success: function (data) {
                    if (data.success) {
                        $('#prove-ultimo-descripcion').html(data.data.RAZON_SOCIAL);
                        $('#Unidad_Medida').focus();
                    }
                }
            });
        }
    });

    $("#Codigo_Imputacion").keyup(function (e) {
        if (e.keyCode == 13) {
            $.ajax({
                type: "POST",
                url: "/Imputaciones/GetOne/",
                data: { Codigo: $('#Codigo_Imputacion').val() },
                success: function (data) {
                    if (data.success) {
                        $('#imputacion-descripcion').html(data.data.DESCRIPCION);
                        $('#Comentario').focus();
                    }
                }
            });
        }
    });
}

/* LLAMA AL METODO POST DE ADDOREDIT*/

function SubmitForm(form) {

    //VALIDADOR DEL FORMULARIO

    $.validator.unobtrusive.parse(form);

    if ($(form).valid()) {
        $.ajax({
            type: "POST",
            url: "/Insumos/AddOrEdit/",
            data: $(form).serialize(),
            success: function (data) {
                if (data.success) {
                    window.location.href = "/Insumos/Index?resultado=" + data.resultado;
                }
                else {

                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });

                    $('#Codigo_Insumo').focus();
                }
            }
        });
    }
    return false;
}
