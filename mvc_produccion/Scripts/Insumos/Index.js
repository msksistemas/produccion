﻿var ListadoInsumos = [];

function modificarInput(Codigo) {
    $('input.codigo').text(Codigo);
}

/* METODO PARA AGREGAR UN REGISTRO*/

function SubmitForm(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        $.ajax({
            type: "POST",
            url: form.action,
            data: $(form).serialize(),
            success: function (data) {
                if (data.success) {
                    Popup.dialog('close');
                    dataTable.ajax.reload();

                    notif({
                        msg: data.message,
                        type: "success",
                        position: "center"
                    });

                }
                else {

                    notif({
                        msg: data.message,
                        type: "error",
                        position: "center"
                    });
                }
            }
        });
    }
    return false;
}

function Delete() {
    $.ajax({
        type: "POST",
        url: "/Insumos/Delete/",
        data: { Codigo_Insumo: $('input.codigo').text() },
        success: function (data) {
            if (data.success) {
                $('#modalDelete').modal('hide');
                CargarInsumos();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });

            }
            else {

                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });

};

function format(data) {
    var Fecha;

    if (data.Fecha_Ultima_Compra != null) {
        Fecha = moment(data.Fecha_Ultima_Compra).format('L');
    }
    else {
        Fecha = "";
    }

    if (data.LINEA == null) {
        data.LINEA = "";
    }

    if (data.RUBRO == null) {
        data.RUBRO = "";
    }

    if (data.RAZON_SOCIAL1 == null) {
        data.RAZON_SOCIAL1 = "";
    }

    if (data.RAZON_SOCIAL2 == null) {
        data.RAZON_SOCIAL2 = "";
    }

    // `d` is the original data object for the row
    return '<div class="col-md-12"><table  cellspacing="0" border="0" class="detallesTabla" style="margin-left:50px; width:auto; ">' +
        '<tr>' +
            '<td><b>Línea:</b></td>' +
            '<td style="text-transform:capitalize">' + data.LINEA + '</td>' +
            '<td><b>Rubro:</b></td>' +
            '<td style="text-transform:capitalize">' + data.RUBRO + '</td>' +
        '</tr>' +
        '<tr>' +
            '<td><b>Proveedor alternativo:</b></td>' +
            '<td style="text-transform:capitalize">' + data.RAZON_SOCIAL1 + '</td>' +
            '<td><b>Último proveedor:</b></td>' +
            '<td style="text-transform:capitalize">' + data.RAZON_SOCIAL2 + '</td>' +
        '</tr>' +
        '<tr>' +
            '<td><b>Fecha de última compra:</b></td>' +
            '<td>' + Fecha + '</td>' +
        '</tr>'
    '</table></div>';
    
}

