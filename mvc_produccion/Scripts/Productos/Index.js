﻿function format(data) {

    if (data.Numero_Producto_Ventas != null && data.Codigo_Insumo == null) {
        return '<div class="col-md-6"><table  cellspacing="0" border="0" style="margin-left:50px; width:auto; ">' +
        '<tr>' +
            '<td><b>Número Producto Ventas:</b></td>' +
            '<td>' + data.Numero_Producto_Ventas + '</td>' +
        '</tr>' +
        '<tr>' +
            '<td><b>Descripción Producto Ventas:</b></td>' +
            '<td>' + data.DescripcionProdVentas + '</td>' +
        '</tr>' +
    '</table></div>';
    }

    if (data.Numero_Producto_Ventas == null && data.Codigo_Insumo != null) {

        return '<div class="col-md-6"><table  cellspacing="0" border="0" style="margin-left:50px; width:auto; ">' +
        '<tr>' +
            '<td><b>Código Insumo:</b></td>' +
            '<td>' + data.Codigo_Insumo + '</td>' +
        '</tr>' +
        '<tr>' +
            '<td><b>Descripción Insumo:</b></td>' +
            '<td>' + data.DescripcionInsumo + '</td>' +
        '</tr>' +
    '</table></div>';

    }

    if (data.Numero_Producto_Ventas != null && data.Codigo_Insumo != null) {

        return '<div class="col-md-6"><table  cellspacing="0" border="0" style="margin-left:50px; width:auto; ">' +
        '<tr>' +
            '<td><b>Código Insumo:</b></td>' +
            '<td>' + data.Numero_Producto_Ventas + '</td>' +
        '</tr>' +
        '<tr>' +
            '<td><b>Descripción Insumo:</b></td>' +
            '<td>' + data.DescripcionProdVentas + '</td>' +
        '</tr>' +
        '<tr>' +
            '<td><b>Código Insumo:</b></td>' +
            '<td>' + data.Codigo_Insumo + '</td>' +
        '</tr>' +
        '<tr>' +
            '<td><b>Descripción Insumo:</b></td>' +
            '<td>' + data.DescripcionInsumo + '</td>' +
        '</tr>' +
    '</table></div>';

    }
}



function modificarInput(Codigo) {
    $('input.codigo').text(Codigo);
}

function refrescarProductos() {
    CargarProductos();

};

function Delete() {
    $.ajax({
        type: "POST",
        url: "/Productos/Delete/",
        data: { Codigo_Producto: $('input.codigo').text() },
        success: function (data) {
            if (data.success) {
                $('#modalDelete').modal('hide');
                CargarProductos();

                notif({
                    msg: data.message,
                    type: "success",
                    position: "center"
                });
            }
            else {

                notif({
                    msg: data.message,
                    type: "error",
                    position: "center"
                });
            }
        }
    });
};