﻿




function ConfirmarProducto() {
    $('#ProductosVentaTabla').find('tr.selected').find('td').each(function () {
        if ($(this).index() == 0) {

            $("#tb-producto").val($(this).html());
        }
        if ($(this).index() == 1) {

            $("#descripcion-prod-ventas").html($(this).html());
        }

    });

    $('#modalProductos').modal('hide');

    $("#modalProductos").on('hidden.bs.modal', function () {
        $("#Codigo_Insumo").focus();
    });

    CargarProductosVentas();
}

/*SE UTILIZA PARA MODIFICAR LOS CHECKBOX, SI ES INSUMO, PRODUCTO TERMINADO O AMBOS*/

function ModificarChecks() {

    if ($('#checkProdTerminado').is(":checked")) {
        $('#divProducto').attr('style', 'display:flex !important');
    }
    else {
        $('#divProducto').attr('style', 'display:none !important');
    }

    if ($('#checkInsumo').is(":checked")) {
        $('#divInsumo').attr('style', 'display:flex !important');
    }

    else {
        $('#divInsumo').attr('style', 'display:none !important');
    }


}