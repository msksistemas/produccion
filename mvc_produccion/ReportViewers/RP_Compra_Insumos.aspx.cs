﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using MVC_Produccion.ContextoProduccion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MVC_Produccion.ReportViewers
{
    public partial class RP_Compra_Insumos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Entities db = new Entities();

            try
            {
                Cadena_Conexion cadena = new Cadena_Conexion();
                cadena = db.Cadena_Conexion.FirstOrDefault();

                ReportDocument rd = new ReportDocument();
                rd.Load(Server.MapPath("~/Reports/") + "IMP_Compra_Insumos.rpt");

                Datos_Empresa datosEmpresa = new Datos_Empresa();
                datosEmpresa = db.Datos_Empresa.FirstOrDefault();

                ConnectionInfo crConn = new ConnectionInfo();
                crConn.ServerName = cadena.Servidor;
                crConn.DatabaseName = cadena.Tabla;
                crConn.UserID = cadena.Usuario;
                crConn.Password = cadena.Contraseña;

                TableLogOnInfo crTableLogOn = new TableLogOnInfo();

                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in rd.Database.Tables)
                {
                    crTableLogOn = CrTable.LogOnInfo;
                    crTableLogOn.ConnectionInfo = crConn;
                    CrTable.ApplyLogOnInfo(crTableLogOn);
                }

                foreach (ReportDocument subreport in rd.Subreports)
                {
                    foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in subreport.Database.Tables)
                    {
                        crTableLogOn = CrTable.LogOnInfo;
                        crTableLogOn.ConnectionInfo = crConn;
                        CrTable.ApplyLogOnInfo(crTableLogOn);
                    }
                }

                CrystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.DataBind();

                rd.SetParameterValue("CUIT", datosEmpresa.CUIT);
                rd.SetParameterValue("Razon_Social", datosEmpresa.Razon_Social);
            }
            catch { }

        }
    }
}