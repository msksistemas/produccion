﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ContextoProduccion
{
    public class Produccion_Muzzarela_MetaData
    {
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        public Nullable<System.DateTime> Fecha_Vencimiento { get; set; }
    }
}