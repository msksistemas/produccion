﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ContextoProduccion
{
    public class Detalle_Envasado_MetaData
    {
        [DefaultValue(false)]
        public bool Historico { get; set; }
    }
}