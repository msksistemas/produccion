﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Produccion.ContextoProduccion
{
    public class ProduccionMetaData
    {
        [StringLength(13)]
        [Required(ErrorMessage = "Campo requerido")]
        [Remote("ExisteProduccion", "Producciones", AdditionalFields = "Lote")]
        public string Lote { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Fecha { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public Nullable<int> Codigo_Quesero { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public Nullable<int> Codigo_Fabrica { get; set; }
    }
}