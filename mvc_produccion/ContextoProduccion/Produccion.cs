//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_Produccion.ContextoProduccion
{
    using System;
    using System.Collections.Generic;
    
    public partial class Produccion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Produccion()
        {
            this.Acidificacion = new HashSet<Acidificacion>();
            this.Cambio_Estado = new HashSet<Cambio_Estado>();
            this.Costos_Indirectos_Produccion = new HashSet<Costos_Indirectos_Produccion>();
            this.Detalle_Produccion = new HashSet<Detalle_Produccion>();
            this.Mov_Ins_Asoc = new HashSet<Mov_Ins_Asoc>();
            this.Produccion_Crema = new HashSet<Produccion_Crema>();
            this.Produccion_Dulce_de_Leche = new HashSet<Produccion_Dulce_de_Leche>();
            this.Produccion_Leche = new HashSet<Produccion_Leche>();
            this.Produccion_Muzzarela = new HashSet<Produccion_Muzzarela>();
            this.Produccion_Ricota = new HashSet<Produccion_Ricota>();
            this.Produccion_Yogur = new HashSet<Produccion_Yogur>();
            this.Produccion_Quesos = new HashSet<Produccion_Quesos>();
            this.Mov_Stock = new HashSet<Mov_Stock>();
            this.Movimientos_Volteo = new HashSet<Movimientos_Volteo>();
        }
    
        public int Codigo_Interno_Produccion { get; set; }
        public string Lote { get; set; }
        public Nullable<int> Codigo_Formula { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public Nullable<bool> Historico { get; set; }
        public Nullable<int> Codigo_Fabrica { get; set; }
        public string Nro_Planilla { get; set; }
        public Nullable<int> Codigo_Quesero { get; set; }
        public Nullable<bool> Expedida { get; set; }
        public Nullable<byte> Decomisado { get; set; }
        public string motivo_decomiso { get; set; }
        public string Lote_Padre { get; set; }
        public string Observacion { get; set; }
        public Nullable<int> idUsuario { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Acidificacion> Acidificacion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cambio_Estado> Cambio_Estado { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Costos_Indirectos_Produccion> Costos_Indirectos_Produccion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Produccion> Detalle_Produccion { get; set; }
        public virtual Fabricas Fabricas { get; set; }
        public virtual Formulas Formulas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Mov_Ins_Asoc> Mov_Ins_Asoc { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Crema> Produccion_Crema { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Dulce_de_Leche> Produccion_Dulce_de_Leche { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Leche> Produccion_Leche { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Muzzarela> Produccion_Muzzarela { get; set; }
        public virtual Queseros Queseros { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Ricota> Produccion_Ricota { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Yogur> Produccion_Yogur { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produccion_Quesos> Produccion_Quesos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Mov_Stock> Mov_Stock { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Movimientos_Volteo> Movimientos_Volteo { get; set; }
    }
}
