﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ContextoProduccion
{
    public class Salidas_Productos_MetaData
    {
        [Required]
        public int Sucursal_Origen { get; set; }
        [Required]
        public int Sucursal_Destino { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Fecha { get; set; }
    }
}