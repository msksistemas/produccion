﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ContextoProduccion
{
    public class Produccion_Quesos_MetaData
    {
        [StringLength(13)]
        [Required(ErrorMessage = "Campo requerido")]
        public string Lote { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public int Piezas_Obtenidas { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public Nullable<int> Codigo_Estado { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public Nullable<decimal> Kilos_Teoricos { get; set; }
    }
}