﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ContextoProduccion
{
    public class Ajustes_Insumos_MetaData
    {
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime? Fecha { get; set; }
        [Required]
        public string Numero_Comprobante { get; set; }
        [Required]
        public int? Fabrica { get; set; }
        [Required]
        public int? Fabrica_Origen { get; set; }
    }
}