﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ContextoProduccion
{
    public class Formulas_MetaData
    {
        [Required(ErrorMessage = "Campo requerido")]
        public Nullable<decimal> Kilos_Teoricos { get; set; }
    }
}