﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ContextoProduccion
{
    public class Costos_Indirectos_MetaData
    {

        [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> Porcentaje { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> Precio { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> Precio_Fijo { get; set; }
    }
}