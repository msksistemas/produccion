//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_Produccion.ContextoProduccion
{
    using System;
    using System.Collections.Generic;
    
    public partial class Productos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Productos()
        {
            this.Formulas = new HashSet<Formulas>();
            this.Formulas_Envasado = new HashSet<Formulas_Envasado>();
            this.Insumos1 = new HashSet<Insumos>();
        }
    
        public int Codigo_Interno_Producto { get; set; }
        public string Codigo_Producto { get; set; }
        public string Codigo_Insumo { get; set; }
        public string Descripcion { get; set; }
        public Nullable<bool> Salida_Directa { get; set; }
        public Nullable<decimal> Relacion_Teorica { get; set; }
        public string Que_Usa { get; set; }
        public Nullable<int> Dias_Frescura { get; set; }
        public Nullable<int> Dias_Volteo { get; set; }
        public Nullable<decimal> Tolerancia { get; set; }
        public Nullable<int> Codigo_Familia { get; set; }
        public Nullable<bool> Vencimiento_Envasado { get; set; }
        public Nullable<bool> Activo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Formulas> Formulas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Formulas_Envasado> Formulas_Envasado { get; set; }
        public virtual Insumos Insumos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Insumos> Insumos1 { get; set; }
        public virtual Familias Familias { get; set; }
    }
}
