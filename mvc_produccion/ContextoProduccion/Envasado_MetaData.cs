﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MVC_Produccion.ContextoProduccion
{
    public class Envasado_MetaData
    {
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Fecha { get; set; }
        [Display(Name ="Sucursal destino")]
        public Nullable<int> Sucursal { get; set; }
    }
}