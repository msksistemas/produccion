//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_Produccion.ContextoProduccion
{
    using System;
    using System.Collections.Generic;
    
    public partial class RPT_Detallado_Producciones
    {
        public int Codigo_Interno_Produccion { get; set; }
        public string Lote { get; set; }
        public Nullable<int> Codigo_Formula { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Nro_Planilla { get; set; }
        public Nullable<int> Litros_Tina { get; set; }
        public Nullable<decimal> Factor { get; set; }
        public Nullable<int> Sucursal { get; set; }
        public Nullable<int> Codigo_Estado { get; set; }
        public Nullable<int> Codigo_Silo_1 { get; set; }
        public Nullable<int> Codigo_Silo_2 { get; set; }
        public Nullable<int> Codigo_Quesero { get; set; }
        public int Piezas_Obtenidas { get; set; }
        public Nullable<decimal> Kilos_Teoricos { get; set; }
        public Nullable<decimal> Kilos_Reales { get; set; }
        public Nullable<int> Insumo_o_Producto { get; set; }
        public Nullable<decimal> Grasa { get; set; }
        public Nullable<decimal> Proteina { get; set; }
        public Nullable<decimal> Grasa_Suero { get; set; }
        public Nullable<int> Litros_Silo_1 { get; set; }
        public Nullable<int> Litros_Silo_2 { get; set; }
        public Nullable<System.TimeSpan> HoraAgregados { get; set; }
        public Nullable<int> TiempoCoagulacion { get; set; }
        public Nullable<int> TiempoLirado { get; set; }
        public Nullable<int> TiempoCoccion { get; set; }
        public Nullable<int> Temperatura_Coccion { get; set; }
        public Nullable<bool> Historico { get; set; }
        public Nullable<int> Codigo_Fabrica { get; set; }
        public Nullable<int> Codigo_Tina { get; set; }
        public Nullable<decimal> Grasa_Silo_1 { get; set; }
        public Nullable<decimal> Grasa_Silo_2 { get; set; }
        public Nullable<decimal> Proteina_Silo_1 { get; set; }
        public Nullable<decimal> Proteina_Silo_2 { get; set; }
        public Nullable<int> Codigo_Camara { get; set; }
        public string Codigo_Producto { get; set; }
        public string Descripcion_Producto { get; set; }
    }
}
