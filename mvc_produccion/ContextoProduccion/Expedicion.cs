//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_Produccion.ContextoProduccion
{
    using System;
    using System.Collections.Generic;
    
    public partial class Expedicion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Expedicion()
        {
            this.Detalle_Expedicion = new HashSet<Detalle_Expedicion>();
        }
    
        public int Codigo_Interno_Expedicion { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Egreso_Ingreso { get; set; }
        public string Comentario { get; set; }
        public string Numero_Comprobante { get; set; }
        public string Prefijo { get; set; }
        public Nullable<int> idUsuario { get; set; }
        public Nullable<int> Sucursal { get; set; }
        public Nullable<bool> Historico { get; set; }
        public Nullable<bool> Recepcion_Confirmada { get; set; }
        public string Motivo_Rechazo { get; set; }
        public Nullable<int> Origen { get; set; }
        public string Lote { get; set; }
        public Nullable<System.DateTime> FechaRecepcion { get; set; }
        public string Tipo_Comprobante { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Detalle_Expedicion> Detalle_Expedicion { get; set; }
    }
}
