//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_Produccion.ContextoProduccion
{
    using System;
    using System.Collections.Generic;
    
    public partial class RPT_Resumen_Envasado
    {
        public int idStock { get; set; }
        public string Numero_Producto_Ventas { get; set; }
        public string Descripcion_Producto_Ventas { get; set; }
        public string Codigo_Producto { get; set; }
        public string Descripcion_Producto { get; set; }
        public Nullable<int> Tinas { get; set; }
        public Nullable<decimal> Litros_Procesados { get; set; }
        public Nullable<decimal> Crema_Procesada { get; set; }
        public Nullable<decimal> Materia_Grasa { get; set; }
        public Nullable<decimal> Proteina { get; set; }
        public decimal Relacion_Real { get; set; }
        public Nullable<decimal> Diferencia_Relacion { get; set; }
        public Nullable<decimal> Piezas_Obtenidas { get; set; }
        public Nullable<decimal> Kilos_Teoricos { get; set; }
        public Nullable<decimal> Kilos_Obtenidos { get; set; }
        public Nullable<decimal> Kilos_Promedio { get; set; }
        public Nullable<decimal> Rendimiento_Real { get; set; }
        public Nullable<decimal> Rendimiento_Teorico { get; set; }
        public Nullable<decimal> Diferencia_Rendimiento { get; set; }
        public Nullable<decimal> Diferencia_Kilos { get; set; }
        public Nullable<decimal> Humedad { get; set; }
        public Nullable<decimal> Total_Litros_Ingresados { get; set; }
        public Nullable<decimal> Precio_Estimado { get; set; }
        public Nullable<decimal> Precio_Real { get; set; }
        public Nullable<int> Litros_Totales_Lote { get; set; }
        public Nullable<decimal> Litros_Suero { get; set; }
        public Nullable<int> Conos { get; set; }
    }
}
