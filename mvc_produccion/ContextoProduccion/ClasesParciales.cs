﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;

namespace MVC_Produccion.ContextoProduccion
{

    [MetadataType(typeof(CompraInsumosMetaData))]
    public partial class CompraInsumos {}

    [MetadataType(typeof(Ajustes_Insumos_MetaData))]
    public partial class Ajustes_Insumos {}

    [MetadataType(typeof(Cambio_Estado_MetaData))]
    public partial class Cambio_Estado { }

    [MetadataType(typeof(Envasado_MetaData))]
    public partial class Envasado { }

    [MetadataType(typeof(Expedicion_MetaData))]
    public partial class Expedicion { }

    [MetadataType(typeof(Ingreso_Leche_MetaData))]
    public partial class Ingreso_Leche { }

    [MetadataType(typeof(Insumos_MetaData))]
    public partial class Insumos { }

    [MetadataType(typeof(Orden_Compra_MetaData))]
    public partial class Orden_Compra { }

    [MetadataType(typeof(ProduccionMetaData))]
    public partial class Produccion { }

    [MetadataType(typeof(Produccion_Quesos_MetaData))]
    public partial class Produccion_Quesos { }

    [MetadataType(typeof(Salidas_Productos_MetaData))]
    public partial class Salida_Productos { }

    [MetadataType(typeof(Costos_Indirectos_MetaData))]
    public partial class Costos_Indirectos { }

    [MetadataType(typeof(Produccion_Muzzarela_MetaData))]
    public partial class Produccion_Muzzarela { }

    [MetadataType(typeof(Permisos_MetaData))]
    public partial class Permisos { }

    [MetadataType(typeof(Detalle_Envasado_MetaData))]
    public partial class Detalle_Envasado { }

    [MetadataType(typeof(Formulas_MetaData))]
    public partial class Formulas { }
} 