﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ContextoProduccion
{
    public class Permisos_MetaData
    {
        public Nullable<int> idUsuario { get; set; }

        [DefaultValue(false)]
        public bool Menu_Maestros { get; set; }

        [DefaultValue(false)]
        public bool Camaras { get; set; }

        [DefaultValue(false)]
        public bool Costos_Indirectos { get; set; }

        [DefaultValue(false)]
        public bool Estados { get; set; }

        [DefaultValue(false)]
        public bool Insumos { get; set; }

        [DefaultValue(false)]
        public bool Fabricas { get; set; }

        [DefaultValue(false)]
        public bool Lineas { get; set; }

        [DefaultValue(false)]
        public bool Productos { get; set; }

        [DefaultValue(false)]
        public bool Queseros { get; set; }

        [DefaultValue(false)]
        public bool Rubros { get; set; }

        [DefaultValue(false)]
        public bool Silos { get; set; }

        [DefaultValue(false)]
        public bool Menu_Costos { get; set; }

        [DefaultValue(false)]
        public bool Formulas { get; set; }

        [DefaultValue(false)]
        public bool Formulas_Envasado { get; set; }

        [DefaultValue(false)]
        public bool Menu_Novedades { get; set; }

        [DefaultValue(false)]
        public bool Elaboracion_Queso { get; set; }

        [DefaultValue(false)]
        public bool Informe_Pesadas { get; set; }

        [DefaultValue(false)]
        public bool Liberar_Por_Calidad { get; set; }

        [DefaultValue(false)]
        public bool Menu_Insumos { get; set; }

        [DefaultValue(false)]
        public bool Ajustes_Insumos { get; set; }

        [DefaultValue(false)]
        public bool Ingreso_Factura { get; set; }

        [DefaultValue(false)]
        public bool Ingreso_Leche { get; set; }

        [DefaultValue(false)]
        public bool Menu_Orden_Compra { get; set; }

        [DefaultValue(false)]
        public bool Ingreso_Orden_Compra { get; set; }

        [DefaultValue(false)]
        public bool Menu_Utilidades { get; set; }

        [DefaultValue(false)]
        public bool Opciones_Trabajo { get; set; }

        [DefaultValue(false)]
        public bool Datos_Empresa { get; set; }

        [DefaultValue(false)]
        public bool Duplicar_Formula { get; set; }

        [DefaultValue(false)]
        public bool Menu_Listados { get; set; }

        [DefaultValue(false)]
        public bool Compras_Proveedor { get; set; }

        [DefaultValue(false)]
        public bool Detallado_Formula { get; set; }

        [DefaultValue(false)]
        public bool Detallado_Formula_Envasado { get; set; }

        [DefaultValue(false)]
        public bool Detallado_Produccion { get; set; }

        [DefaultValue(false)]
        public bool Listados_Insumos { get; set; }

        [DefaultValue(false)]
        public bool Stock_Insumos { get; set; }

        [DefaultValue(false)]
        public bool Listados_Produccion { get; set; }

        [DefaultValue(false)]
        public bool Stock_General { get; set; }

        [DefaultValue(false)]
        public bool Stock_Producto_Estado { get; set; }

        [DefaultValue(false)]
        public bool Resumen_Produccion { get; set; }

        [DefaultValue(false)]
        public bool Stock_Leche { get; set; }

        [DefaultValue(false)]
        public bool Menu_Stock { get; set; }

        [DefaultValue(false)]
        public bool Expedicion { get; set; }

        [DefaultValue(false)]
        public bool Ajustes_Productos { get; set; }

        [DefaultValue(false)]
        public bool Elaboracion_Muzzarela { get; set; }

        [DefaultValue(false)]
        public bool Planillas_Elaboracion { get; set; }

        [DefaultValue(false)]
        public bool Elaboracion_Ricota { get; set; }

        [DefaultValue(false)]
        public bool Elaboracion_Crema { get; set; }

        [DefaultValue(false)]
        public bool Elaboracion_Leche { get; set; }

        [DefaultValue(false)]
        public bool Elaboracion_Yogurt { get; set; }

        [DefaultValue(false)]
        public bool Elaboracion_Dulce_De_Leche { get; set; }

        [DefaultValue(false)]
        public bool Listados_Productos { get; set; }

        [DefaultValue(false)]
        public bool Costos_Productos { get; set; }

        [DefaultValue(false)]
        public bool Producciones_Por_Producto { get; set; }
    }
}