﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MVC_Produccion.ContextoProduccion
{
    public class Expedicion_MetaData
    {
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Fecha { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FechaRecepcion { get; set; }
    }
}