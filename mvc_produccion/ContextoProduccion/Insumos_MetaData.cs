﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ContextoProduccion
{
    public class Insumos_MetaData
    {
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Fecha_Ultima_Compra { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public string Codigo_Insumo { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public string Descripcion { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.####}")]
        public Nullable<decimal> Precio_Ultima_Compra { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> Precio_Lista { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> Punto_Pedido { get; set; }
    }
}