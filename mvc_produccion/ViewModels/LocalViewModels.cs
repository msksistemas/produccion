﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Produccion.ViewModels
{
    public class LocalViewModels
    {
        public class CambioContraseñaViewModel
        {
            public int idUsuario { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Contraseña Nueva")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirmar contraseña")]
            [Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
            public string ConfirmPassword { get; set; }
        }

        public class LiberarPorCalidadModel
        {
            [Required]
            [Display(Name = "Codigo Interno")]
            public int Codigo_Interno_Envasado { get; set; }

            [Display(Name = "Usuario")]
            public int idUsuario { get; set; }

            [Display(Name = "Número Comprobante")]
            public string Numero_Comprobante { get; set; }

            [Display(Name = "Tipo Comprobante")]
            public string Tipo_Comprobante { get; set; }

            [Display(Name = "Prefijo")]
            public string Prefijo { get; set; }

            [Display(Name = "Sucursal")]
            public int Sucursal { get; set; }

            [Display(Name = "Comentario")]
            public string Comentario { get; set; }
        }

        public class AdministrarFabricasModel
        {
            [Required(ErrorMessage = "Campo requerido.")]
            [Display(Name = "Fabrica")]
            public int Fabrica { get; set; }

            [Display(Name = "idUsuario")]
            public int idUsuario { get; set; }

            [Display(Name = "Todas")]
            public bool Todas { get; set; }
        }
    }
}