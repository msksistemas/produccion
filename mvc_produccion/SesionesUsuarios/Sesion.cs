﻿using Microsoft.AspNet.Identity;
using AccesoDatos.Contexto.Produccion;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace MVC_Produccion.Sesiones
{
    public class DatosSesion
    {
        public string IdUser { get; set; }
        public string IdSession { get; set; }
        public DateTime UltimaPeticion { get; set; }
    }

    public static class Sesion
    {
        private static readonly List<DatosSesion> _Sesiones = new List<DatosSesion>();
        //private static readonly HashSet<string> _Sesiones = new HashSet<string>();
        private static readonly object padlock = new object();

        public static List<DatosSesion> SesionesActivas
        {
            get
            {
                return _Sesiones;
            }
        }
        private static void AgregarSesion(string iduser)
        {

            lock (padlock)
            {
                var sesion = _Sesiones.SingleOrDefault(s => s.IdUser == iduser);
                var idSession = HttpContext.Current.Session.SessionID;

                if (sesion == null)
                    _Sesiones.Add(new DatosSesion
                    {
                        IdUser = iduser,
                        UltimaPeticion = DateTime.Now,
                        IdSession = idSession
                    });
                else
                {
                    sesion.UltimaPeticion = DateTime.Now;
                    sesion.IdSession = idSession;

                }

            }
        }

        public static void QuitarSesion(string iduser)
        {
            lock (padlock)
            {
                var sesion = _Sesiones.SingleOrDefault(s => s.IdUser == iduser);
                _Sesiones.Remove(sesion);
            }
        }

        public static Usuarios UsuarioActual
        {
            get
            {
                var user = HttpContext.Current.User;
                //var ck = HttpContext.Current.Request.Cookies.AllKeys; 
                //var c = HttpContext.Current.Request.Cookies.Get(".AspNet.ApplicationCookie"); 
                //var s = HttpContext.Current.Request.Cookies.Get("ASP.NET_SessionId");
                //if (s.Value == HttpContext.Current.Session.SessionID)
                //{
                //
                //}



                //var IpUser = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                //var IdSession = HttpContext.Current.Session.SessionID;

                if (user == null)
                    return null;

                string id_user = user.Identity.GetUserId();
               // var SesionUsuario = SesionesActivas.SingleOrDefault(s => s.IdUser == id_user);

                if (string.IsNullOrEmpty(id_user))
                    return null;

                //if (SesionUsuario != null)
                //    if (((DateTime.Now - SesionUsuario.UltimaPeticion).Minutes < 1) && SesionUsuario.IdSession != IdSession)
                //        return null;

                Usuarios usuario = new Usuarios();
                using (dbProd db = new dbProd())
                {
                    usuario = db.Usuarios.SingleOrDefault(u => u.Id_Login == id_user);
                }
                //AgregarSesion(id_user);
                return usuario;
            }
        }
    }
}