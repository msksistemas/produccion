//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_Produccion.ContextoGastos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Items_Tmp
    {
        public int CODINTITMTMP { get; set; }
        public System.DateTime STRFECHA_ENTREGA { get; set; }
        public string NRO_DOCUMENTO { get; set; }
        public string NRO_DOCUMENTO_AUX { get; set; }
        public string NRO_PRODUCTO { get; set; }
        public Nullable<int> PROVEEDOR { get; set; }
        public Nullable<decimal> CANTIDAD { get; set; }
        public string SERIE { get; set; }
        public Nullable<decimal> KG_LTR { get; set; }
        public Nullable<decimal> DESCUENTO { get; set; }
        public Nullable<decimal> PRECIO { get; set; }
        public Nullable<decimal> PRECIO_DOLAR { get; set; }
        public Nullable<decimal> TOTAL { get; set; }
        public string COD_MOVIMIENTO { get; set; }
        public Nullable<int> CLIENTE { get; set; }
        public Nullable<int> NRO_COMPROB_STCK { get; set; }
        public Nullable<short> SUC_PROPIA { get; set; }
        public Nullable<int> NRO_PRODUCTOR { get; set; }
        public string DETALLE_PRODUCTO { get; set; }
        public Nullable<byte> MARCA_STOCK { get; set; }
        public Nullable<decimal> PORCENTAJE_IVA { get; set; }
        public Nullable<int> NRO_TERMINAL { get; set; }
        public string NRO_PRODUCTO_ASOCIADO { get; set; }
    }
}
